SHELL:=bash

# clean:
# 	conda remove --name dtop_stationkeep --all

# reset: clean
# 	conda env create --name dtop_stationkeep --file dependency.yml --force
# 	conda env list

# -------------------------------------------
# Run Pytest tests locally

x-rm-dist:
	rm -rf ./dist

x-create-dist:
	python setup.py sdist

x-uninstall:
	pip uninstall --yes dtop_stationkeep

x-install: x-uninstall x-rm-dist x-create-dist
	pip install dtop_stationkeep --no-cache-dir --find-links dist

pytest: x-install x-pytest-prepare x-pytest

x-pytest-prepare:
	pip install pytest pytest-cov
	pip install pact-python
	pip install dtop_stationkeep --no-cache-dir --find-links dist

x-pytest:
	pytest src/dtop_stationkeep --cov=dtop_stationkeep --junitxml=report.xml

# -------------------------------------------
# Run Dredd tests locally

dredd: x-install x-dredd-prepare x-dredd

x-dredd-prepare:
	pip install python-dotenv
	pip install dredd_hooks
	npm uninstall dredd --global
	npm install dredd@12.2.1 --global

x-dredd:
	bash -c "dredd --config dredd.yml"
