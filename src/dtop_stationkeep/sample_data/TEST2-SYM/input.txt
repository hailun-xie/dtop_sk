--- Calculation parameters ------------------------------------------------------------------------------------
0				! Indiq_solver 		! - 		! Solver (0) Direct Gauss (1) GMRES
20				! IRES 		! - 		! Restart parameter for GMRES
5.E-07			! TOL_GMRES 		! - 		! Stopping criterion for GMRES
100				! MAXIT 			! - 		! Maximum iterations for GMRES
