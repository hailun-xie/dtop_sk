# This is the Station Keeping module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Neil Luxcey, Nicolas Michelet, Rocio Isorna
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


import numpy as np
import pytest
import sys
import os
import copy
import pandas
from dtop_stationkeep.business.libraries.dtosk.run_analysis_cplx_module import run_analysis_cplx,parse_inputs_cplx,populate_outputs
from dtop_stationkeep.business.libraries.dtosk.catalogue.Lines import Lines
from dtop_stationkeep.business.libraries.dtosk.mooring.Designer import Designer
from dtop_stationkeep.business.libraries.dtosk.inputs.Cplx import Cplx
from dtop_stationkeep.business.libraries.dtosk.inputs.DeviceProperties import DeviceProperties
from dtop_stationkeep.business.libraries.dtosk.inputs.SteadyForceModel import SteadyForceModel
from dtop_stationkeep.business.libraries.dtosk.winching_module import winch_line_for_pretension
from dtop_stationkeep.business.libraries.dtosk.frequency_analysis_module import compute_eigenperiod,compute_max_dynamic_offset,run_frequency_analysis
from dtop_stationkeep.business.libraries.dtosk.inputs.CustomMooringInput import CustomMooringInput

def define_test_inputs_12():

    # Get path to nemoh file
    this_dir = os.path.dirname(os.path.realpath(__file__))
    data_dir = os.path.join(this_dir, "..", "sample_data")
    result_path = os.path.join(data_dir,'TEST-AXI')

    # Create input structure for cplx
    input_cplx = Cplx()

    # Project name
    input_cplx.name='project_test1'

    # Device properties
    input_cplx.device_properties=DeviceProperties()

    # User inputs for steady force model
    input_cplx.device_properties.steady_force_model=SteadyForceModel()
    input_cplx.device_properties.steady_force_model.method='main_dimensions'
    input_cplx.device_properties.steady_force_model.device_dry_profile='cylinder'
    input_cplx.device_properties.steady_force_model.device_wet_profile='cylinder'
    input_cplx.device_properties.steady_force_model.device_dry_width=10.0
    input_cplx.device_properties.steady_force_model.device_wet_width=10.0
    input_cplx.device_properties.steady_force_model.device_dry_height=4
    input_cplx.device_properties.steady_force_model.device_wet_height=4
    input_cplx.device_properties.mooring_design_criteria.fairlead_vertical_position = -4
    input_cplx.device_properties.mooring_design_criteria.fairlead_radius = 5.0

    
    # User inputs if nemoh run is used (standalone)
    input_cplx.device_properties.hdb_source='nemoh_run'
    input_cplx.device_properties.path_to_nemoh_result_folder=result_path
    input_cplx.device_properties.critical_damping_coefficient = np.array([0.1,0.1,0.1,0.1,0.1,0.1],dtype=float)
    
    # User inputs if mc_module is used
    #input_cplx.device_properties.hdb_source='mc_module'
    #input_cplx.device_properties.mass_matrix=np.zeros(shape=(6,6), dtype=float)
    #input_cplx.device_properties.hydrostatic_matrix=np.zeros(shape=(6,6), dtype=float)
    #input_cplx.device_properties.cog=np.zeros(shape=(3), dtype=float)
    input_cplx.device_properties.machine_type='wec'

    # Input for the tec devices
    #input_cplx.device_properties.thrust_coeff_curve_file=''
    #input_cplx.device_properties.rotor_diameter=0.0
    #input_cplx.device_properties.hub_position=np.zeros(shape=(3), dtype=float)
    
    # User input for mooring design
    input_cplx.device_properties.positioning_type  ='moored'
    input_cplx.device_properties.positioning_reference = 'seabed'
    input_cplx.device_properties.mooring_input_flag = 'design'
    input_cplx.device_properties.mooring_design_criteria.mooring_type = 'catenary'
    input_cplx.device_properties.mooring_design_criteria.nlines = 3
    input_cplx.device_properties.mooring_design_criteria.nlines_max = 10

    # Constrains for pretension
    input_cplx.device_properties.mooring_design_criteria.pretension_coeff_max=0.03 # 6% of MBL
    input_cplx.device_properties.mooring_design_criteria.pretension_coeff_min=0.02 # 3% of MBL
    input_cplx.device_properties.mooring_design_criteria.length_increment=1.0 # Winch 1.0m at a time (+or-)

    # Minimum horizontal egenperiod target [s]
    input_cplx.device_properties.mooring_design_criteria.Tmin = 30
    
    # Constrains for total offset
    input_cplx.device_properties.mooring_design_criteria.offset_max = 30

    # Proof load for anchor of catenary line installation
    input_cplx.device_properties.mooring_design_criteria.proof_load_coeff = 0.8
    input_cplx.device_properties.mooring_design_criteria.safety_factor = 1.7

    # Inputs for the masterstructure
    #input_cplx.master_structure_properties=MasterStructureProperties.MasterStructureProperties()

    # Inputs for motion analysis
    input_cplx.uls_analysis_parameters.weather_direction = np.array([30])

    # Parameters specific to foundation design
    input_cplx.device_properties.foundation_design_parameters.foundation_preference = 'evaluation'
    input_cplx.device_properties.foundation_design_parameters.dist_attachment_pa = 0
    input_cplx.device_properties.foundation_design_parameters.pile_tip = 'close_end'
    input_cplx.device_properties.foundation_design_parameters.deflection_criteria_for_pile = 0

    # Read from SC
    input_cplx.water_density=1025.0
    input_cplx.uls_analysis_parameters.hs_array=np.array([12, 13, 14])
    input_cplx.uls_analysis_parameters.tp_array=np.array([14, 15, 16])
    input_cplx.uls_analysis_parameters.current_vel=2.0
    input_cplx.uls_analysis_parameters.wind_vel=30.0
    input_cplx.water_depth=np.array([100,100,100,100])
    input_cplx.device_properties.foundation_design_parameters.soil_type = 'very_dense_sand'
    input_cplx.device_properties.foundation_design_parameters.soil_slope = 1

    # Read from EC
    input_cplx.east=np.array([0, 800, 0, 800])
    input_cplx.north=np.array([0, 0, 800, 800])
    input_cplx.yaw=np.array([0, 0, 0, 0])
    input_cplx.deviceId=np.array([0,1,2,3])

    return input_cplx

# def test_offset_max():
    
#     # Load inputs (cplx1 or 2)
#     input_cplx = define_test_inputs_12()
#     input_cplx.device_properties.mooring_design_criteria.offset_max = 65
#     # Read from SC
#     input_cplx.water_density=1025.0
#     input_cplx.uls_analysis_parameters.hs_array=np.array([14])
#     input_cplx.uls_analysis_parameters.tp_array=np.array([16])
#     input_cplx.uls_analysis_parameters.current_vel=2.0
#     input_cplx.uls_analysis_parameters.wind_vel=30.0
#     input_cplx.water_depth=np.array([200])
#     input_cplx.device_properties.foundation_design_parameters.soil_type = 'very_dense_sand'
#     input_cplx.device_properties.foundation_design_parameters.soil_slope = 1
#     input_cplx.device_properties.mooring_design_criteria.pretension_coeff_max=0.04 # 7% of MBL (not used)
#     input_cplx.device_properties.mooring_design_criteria.pretension_coeff_min=0.00# 2% of MBL (do not winch)
#     input_cplx.device_properties.mooring_design_criteria.length_increment=1.0 # Winch 1.0m at a time (+or-)


#     # Read from EC
#     input_cplx.east=np.array([0])
#     input_cplx.north=np.array([0])
#     input_cplx.yaw=np.array([0])
#     input_cplx.deviceId=np.array([0])

#     # Import catalogue
#     catalogue_chains = Lines()

#     # Modify the catalogues to force the algorithm to add a mooring line
#     catalogue_chains.catalogue_id = catalogue_chains.catalogue_id[0:19]
#     catalogue_chains.type=catalogue_chains.type[0:19]
#     catalogue_chains.material=catalogue_chains.material[0:19]
#     catalogue_chains.quality=catalogue_chains.quality[0:19]
#     catalogue_chains.diameter=catalogue_chains.diameter[0:19]
#     catalogue_chains.weight_in_air=catalogue_chains.weight_in_air[0:19]
#     catalogue_chains.weight_in_water=catalogue_chains.weight_in_water[0:19]
#     catalogue_chains.mbl=catalogue_chains.mbl[0:19]
#     catalogue_chains.ea=catalogue_chains.ea[0:19]
#     catalogue_chains.cost_per_meter=catalogue_chains.cost_per_meter[0:19]
#     catalogue_chains.ad=catalogue_chains.ad[0:19]
#     catalogue_chains.m=catalogue_chains.m[0:19]

#     # Run main
#     [farm, hierarchy, bom, env_impact, dum, dum1, dum2, dum3] = run_analysis_cplx(input_cplx,catalogue_chains)

#     assert (farm.body_array[0].results.uls_results.mooring_tension[0,0,0,1] == pytest.approx(84715.39954750211,0.0001))