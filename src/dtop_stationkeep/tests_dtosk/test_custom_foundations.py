# This is the Station Keeping module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Neil Luxcey, Nicolas Michelet, Rocio Isorna
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


import numpy as np
import pytest
import sys
import os
import copy
import pandas
from dtop_stationkeep.business.libraries.dtosk.run_analysis_cplx_module import run_analysis_cplx,parse_inputs_cplx,populate_outputs
from dtop_stationkeep.business.libraries.dtosk.catalogue.Lines import Lines
from dtop_stationkeep.business.libraries.dtosk.catalogue.AnchorCatalogue import AnchorCatalogue
from dtop_stationkeep.business.libraries.dtosk.mooring.Designer import Designer
from dtop_stationkeep.business.libraries.dtosk.inputs.Cplx import Cplx
from dtop_stationkeep.business.libraries.dtosk.inputs.DeviceProperties import DeviceProperties
from dtop_stationkeep.business.libraries.dtosk.inputs.SteadyForceModel import SteadyForceModel
from dtop_stationkeep.business.libraries.dtosk.winching_module import winch_line_for_pretension
from dtop_stationkeep.business.libraries.dtosk.frequency_analysis_module import compute_eigenperiod,compute_max_dynamic_offset,run_frequency_analysis
from dtop_stationkeep.business.libraries.dtosk.inputs.CustomMooringInput import CustomMooringInput



def define_test_inputs_3_fixed_custom_foundation():

    # Get path to nemoh file
    this_dir = os.path.dirname(os.path.realpath(__file__))
    data_dir = os.path.join(this_dir, "..", "sample_data")
    result_path = os.path.join(data_dir,'TEST-AXI')
    mooring_input_path = os.path.join(data_dir,'mooring_input_example.json')

    # Create input structure for cplx
    input_cplx = Cplx()

    # Project name
    input_cplx.name='project_test2'

    # Device properties
    input_cplx.device_properties=DeviceProperties()

    # User inputs for steady force model
    input_cplx.device_properties.steady_force_model=SteadyForceModel()
    input_cplx.device_properties.steady_force_model.method='main_dimensions'
    input_cplx.device_properties.steady_force_model.device_dry_profile='cylinder'
    input_cplx.device_properties.steady_force_model.device_wet_profile='cylinder'
    input_cplx.device_properties.steady_force_model.device_dry_width=10.0
    input_cplx.device_properties.steady_force_model.device_wet_width=10.0
    input_cplx.device_properties.steady_force_model.device_dry_height=4
    input_cplx.device_properties.steady_force_model.device_wet_height=4

    
    # User inputs if nemoh run is used (standalone)
    input_cplx.device_properties.hdb_source='nemoh_run'
    input_cplx.device_properties.path_to_nemoh_result_folder=result_path
    input_cplx.device_properties.critical_damping_coefficient = np.array([0.1,0.1,0.1,0.1,0.1,0.1],dtype=float)
    
    # User inputs if mc_module is used
    #input_cplx.device_properties.hdb_source='mc_module'
    #input_cplx.device_properties.mass_matrix=np.zeros(shape=(6,6), dtype=float)
    input_cplx.device_properties.mass = 100000
    #input_cplx.device_properties.hydrostatic_matrix=np.zeros(shape=(6,6), dtype=float)
    input_cplx.device_properties.cog=np.zeros(shape=(3), dtype=float)
    input_cplx.device_properties.machine_type='tec'

    # Input for the tec devices
    this_dir = os.path.dirname(os.path.realpath(__file__))
    data_dir = os.path.join(this_dir, "..", "sample_data")
    input_cplx.device_properties.thrust_coeff_curve_file=os.path.join(data_dir,'thrustcurv.txt')
    input_cplx.device_properties.rotor_diameter=6.0
    input_cplx.device_properties.hub_position=np.zeros(shape=(3,1))
    input_cplx.device_properties.hub_position[:,0] = [0.0,0.0,10.0]

    # User input for mooring design
    input_cplx.device_properties.positioning_type  ='fixed'
    input_cplx.device_properties.positioning_reference = 'seabed'
    input_cplx.device_properties.mooring_input_flag = 'custom'
    input_cplx.device_properties.mooring_design_criteria.mooring_type = 'catenary' # Important even if 'custom' is used because this drives the foundation choice as well (we will use the proofload to design the anchor)
    input_cplx.device_properties.mooring_design_criteria.nlines = 3
    input_cplx.device_properties.mooring_design_criteria.nlines_max = 10

    ########################################################################################
    ########################################################################################
    # Inputs required if mooring_input_flag = 'custom'
    ########################################################################################
    ########################################################################################
    input_cplx.device_properties.custom_mooring_input.append(CustomMooringInput())
    input_cplx.device_properties.custom_mooring_input[0].name='mooring_input_example'
    input_cplx.device_properties.custom_mooring_input[0].loadJSON(filePath = mooring_input_path)


    # Inputs required if mooring_input_flag = 'design'
    # # Constrains for pretension
    # input_cplx.device_properties.mooring_design_criteria.pretension_coeff_max=0.03 # 6% of MBL
    # input_cplx.device_properties.mooring_design_criteria.pretension_coeff_min=0.02 # 3% of MBL
    # input_cplx.device_properties.mooring_design_criteria.length_increment=1.0 # Winch 1.0m at a time (+or-)

    # # Minimum horizontal egenperiod target [s]
    # input_cplx.device_properties.mooring_design_criteria.Tmin = 30
    
    # # Constrains for total offset
    # input_cplx.device_properties.mooring_design_criteria.offset_max = 30

    # Proof load for anchor of catenary line installation
    input_cplx.device_properties.mooring_design_criteria.proof_load_coeff = 0.8
    input_cplx.device_properties.mooring_design_criteria.safety_factor = 1.7

    # Inputs for the masterstructure
    #input_cplx.master_structure_properties=MasterStructureProperties.MasterStructureProperties()

    # Inputs for motion analysis
    input_cplx.uls_analysis_parameters.weather_direction = np.array([30])

    # Parameters specific to foundation design
    input_cplx.device_properties.foundation_design_parameters.foundation_preference = 'evaluation'
    input_cplx.device_properties.foundation_design_parameters.dist_attachment_pa = 0
    input_cplx.device_properties.foundation_design_parameters.pile_tip = 'open_end'
    input_cplx.device_properties.foundation_design_parameters.deflection_criteria_for_pile = 10
    input_cplx.device_properties.foundation_design_parameters.foundation_design_flag = 'custom'
    input_cplx.device_properties.foundation_design_parameters.custom_foundation_input.base_radius  = 10.0
    input_cplx.device_properties.foundation_design_parameters.custom_foundation_input.thickness  = 0.3

    # Read from SC
    input_cplx.water_density=1025.0
    input_cplx.uls_analysis_parameters.hs_array=np.array([14])
    input_cplx.uls_analysis_parameters.tp_array=np.array([16])
    input_cplx.uls_analysis_parameters.current_vel=2.0
    input_cplx.uls_analysis_parameters.wind_vel=30.0
    input_cplx.water_depth=np.array([100])
    input_cplx.device_properties.foundation_design_parameters.soil_type = 'very_dense_sand'
    input_cplx.device_properties.foundation_design_parameters.soil_slope = 0.0

    # Read from EC
    input_cplx.east=np.array([0])
    input_cplx.north=np.array([0])
    input_cplx.yaw=np.array([0])
    input_cplx.deviceId=np.array([0])

    return input_cplx

def test_inputs_3_fixed_custom_foundation_gravity():

    # Load inputs (cplx3)
    input_cplx = define_test_inputs_3_fixed_custom_foundation()

    # Import catalogue
    catalogue_chains = Lines()
    catalogue_anchors = AnchorCatalogue()

    # Run main
    [farm, hierarchy, bom, env_impact, dum, dum1, dum2, dum3] = run_analysis_cplx(input_cplx,catalogue_chains,catalogue_anchors)

    assert farm.body_array[0].results.design_results.anchor_and_foundation.gravity_foundation_design.base_radius == pytest.approx(10.0,0.1) and \
            farm.body_array[0].results.design_results.anchor_and_foundation.gravity_foundation_design.thickness == pytest.approx(0.3,0.0001) and \
            farm.body_array[0].results.design_results.anchor_and_foundation.gravity_foundation_design.bearing_capacity == pytest.approx(174053502,0.0001)


def test_inputs_3_fixed_custom_foundation_pile():

    # Load inputs (cplx3)
    input_cplx = define_test_inputs_3_fixed_custom_foundation()

    # Modify the input to impose load_ap and foundation _type
    input_cplx.device_properties.foundation_design_parameters.foundation_preference = 'pile'
    input_cplx.device_properties.foundation_design_parameters.foundation_design_flag = 'custom'
    input_cplx.device_properties.foundation_design_parameters.design_load_flag='manual'
    input_cplx.device_properties.foundation_design_parameters.design_load=np.array([45.9e+06,45.9e+06 , 3E+06, 4E+04, 20e+06, 30e+06 ], dtype=float)
    input_cplx.device_properties.foundation_design_parameters.custom_foundation_input.w_t  = 0.12700000000000009
    input_cplx.device_properties.foundation_design_parameters.custom_foundation_input.d_ext  = 6.299999999999994
    input_cplx.device_properties.foundation_design_parameters.custom_foundation_input.length  = 37.79999999999996

    # Import catalogue
    catalogue_chains = Lines()
    catalogue_anchors = AnchorCatalogue()

    # Run main
    [farm, hierarchy, bom, env_impact, dum, dum1, dum2, dum3] = run_analysis_cplx(input_cplx,catalogue_chains,catalogue_anchors)

    assert farm.body_array[0].results.design_results.anchor_and_foundation.pile_foundation_design.lat_cap / farm.body_array[0].results.design_results.anchor_and_foundation.pile_foundation_design.SF == pytest.approx(179839802.0445434,0.0000001)

def test_inputs_3_fixed_custom_foundation_suction():

    # Load inputs (cplx3)
    input_cplx = define_test_inputs_3_fixed_custom_foundation()

    # Modify the input to impose load_ap and foundation _type
    input_cplx.device_properties.foundation_design_parameters.foundation_preference = 'suction_caisson'
    input_cplx.device_properties.foundation_design_parameters.foundation_design_flag = 'custom'
    input_cplx.device_properties.foundation_design_parameters.design_load_flag='manual'
    input_cplx.device_properties.foundation_design_parameters.design_load=np.array([0,45.9e+06 , 3E+06, 4E+04, 20e+06, 30e+06, 0]  , dtype=float)
    input_cplx.device_properties.foundation_design_parameters.custom_foundation_input.w_t  = 0.09
    input_cplx.device_properties.foundation_design_parameters.custom_foundation_input.d_ext  = 3.63
    input_cplx.device_properties.foundation_design_parameters.custom_foundation_input.length  = 14.5
    input_cplx.device_properties.foundation_design_parameters.SF = 1.8
    input_cplx.device_properties.positioning_type  ='moored'
    input_cplx.device_properties.hub_position[:,0] = [0.0,0.0,-10.0]


    # Import catalogue
    catalogue_chains = Lines()
    catalogue_anchors = AnchorCatalogue()

    # Run main
    [farm, hierarchy, bom, env_impact, dum, dum1, dum2, dum3] = run_analysis_cplx(input_cplx,catalogue_chains,catalogue_anchors)

    assert farm.body_array[0].results.design_results.anchor_and_foundation.suction_caisson_design.inc_verif == pytest.approx(0.4026862757852562,0.0000000001)

def test_inputs_3_fixed_custom_foundation_drag():

    # Load inputs (cplx3)
    input_cplx = define_test_inputs_3_fixed_custom_foundation()

    # Modify the input to impose load_ap and foundation _type
    input_cplx.device_properties.foundation_design_parameters.foundation_preference = 'drag_anchor'
    input_cplx.device_properties.foundation_design_parameters.foundation_design_flag = 'manual'
    input_cplx.device_properties.foundation_design_parameters.design_load_flag='manual'
    input_cplx.device_properties.foundation_design_parameters.design_load=np.array([-3E+06,0 , 3E+06, 0, 0, 0, 0]  , dtype=float)
    input_cplx.device_properties.positioning_type  ='moored'
    input_cplx.device_properties.hub_position[:,0] = [0.0,0.0,-10.0]
    input_cplx.device_properties.foundation_design_parameters.custom_foundation_input.mass = 9978.685123564599
    input_cplx.device_properties.foundation_design_parameters.catalogue_index = 2

    # Import catalogue
    catalogue_chains = Lines()
    catalogue_anchors = AnchorCatalogue()
    catalogue_anchors.catalogue_id=['anchor_1', 'anchor_2', 'anchor_3']
    catalogue_anchors.m_soft_soil = [569.26754221, 784.76661745, 784.76661745]
    catalogue_anchors.b_soft_soil = [0.93771259623, 0.90020490328, 0.90020490328]
    catalogue_anchors.m_mid_soil = [569.26754221, 1040.229976, 1040.229976]
    catalogue_anchors.b_mid_soil = [0.93771259623, 0.90820718302, 0.90820718302]
    catalogue_anchors.m_stiff_soil_sand = [779.048058, 1238.7656958, 1238.7656958]
    catalogue_anchors.b_stiff_soil_sand = [0.92864857452, 0.91006198174, 0.91006198174]
    catalogue_anchors.m_A = [0.22648820263, 0.25801019888,0.25801019888]
    catalogue_anchors.b_A = [0.33598055403, 0.33334431544, 0.33334431544]
    catalogue_anchors.m_B = [0.27453552794, 0.27806143152, 0.27806143152]
    catalogue_anchors.b_B = [0.33594454709, 0.33336556698, 0.33336556698]
    catalogue_anchors.m_EF = [0.16404368449, 0.15506619019, 0.15506619019]
    catalogue_anchors.b_EF = [0.33590684254, 0.33341638395, 0.33341638395]
    catalogue_anchors.Rel_soft_soil = [1, 3, 3]
    catalogue_anchors.Rel_mid_soil = [3, 1, 1]
    catalogue_anchors.Rel_stiff_soil = [3, 1, 1]

    # Run main
    [farm, hierarchy, bom, env_impact, dum, dum1, dum2, dum3] = run_analysis_cplx(input_cplx,catalogue_chains,catalogue_anchors)

    assert farm.body_array[0].results.design_results.anchor_and_foundation.drag_anchor_design.weight == pytest.approx(9978.685123564599,0.00001)
    