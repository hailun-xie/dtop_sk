# This is the Station Keeping module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Neil Luxcey, Nicolas Michelet, Rocio Isorna
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


import numpy as np
import pytest
import sys
import os
import copy
import json
from dtop_stationkeep.business.libraries.dtosk.inputs.Inputs import Inputs
from dtop_stationkeep.business.libraries.dtosk.frequency_analysis_module import from_reduced_dof_to_dof 

def load_json_from_file(filePath):
    if not(os.path.isfile(filePath)):
        raise Exception("file %s not found."%filePath)
    f = open(filePath,"r")
    data_read = f.read()
    f.close()
    data = json.loads(data_read)
    return data

def test_inputs_dto():

    # Get path to example files
    this_dir = os.path.dirname(os.path.realpath(__file__))
    data_dir = os.path.join(this_dir, "..", "sample_data")
    ec_farm_file = os.path.join(data_dir,'ec_farm.json')    
    ec_farm_input_file = os.path.join(data_dir,'ec_farm_input.json')
    
    mc_get_single_machine_hydrodynamic_file = os.path.join(data_dir,'hydrodynamic_model.json') 
    mc_get_wec_cplx1_file = os.path.join(data_dir,'modelling_wec_cpx1.json')
    mc_get_wec_cplx2_file = os.path.join(data_dir,'modelling_wec_cpx2.json')
    mc_get_wec_cplx3_file = os.path.join(data_dir,'modelling_wec_cpx3.json')
    mc_get_tec_cplx1_file = os.path.join(data_dir,'modelling_tec_cpx1.json')
    mc_get_tec_cplx2_file = os.path.join(data_dir,'modelling_tec_cpx2.json')
    mc_get_tec_cplx3_file = os.path.join(data_dir,'modelling_tec_cpx3.json')
    mc_get_general_file = os.path.join(data_dir,'general.json')
    mc_get_dimensions_file = os.path.join(data_dir,'dimensions.json')
    mc_get_machine_file = os.path.join(data_dir,'machine.json')
    sc_get_device_statistics_file = os.path.join(data_dir,'dtosc_devices_outputs_Statistics.json')
    sc_get_farm_info_file = os.path.join(data_dir,'dtosc_farm_outputs_FarmInfo.json')
    sc_get_farm_bathymetry_file = os.path.join(data_dir,'dtosc_farm_outputs_FarmBathymetry.json')
    sc_get_farm_seabed_type_file = os.path.join(data_dir,'dtosc_farm_outputs_FarmSeabedType.json')
    ed_get_output_results_file = os.path.join(data_dir,'EnergyDelivOutputResults.json') 

    inp = Inputs()
    inp.run_mode='dto'

    # Load data from file instead of calling API (for testing purpose)
    inp.url_ec_get_farm=load_json_from_file(ec_farm_file)
    inp.url_ec_get_farm_inputs=load_json_from_file(ec_farm_input_file)
    inp.url_mc_get_single_machine_hydrodynamic=load_json_from_file(mc_get_single_machine_hydrodynamic_file)
    inp.url_mc_get_wec_cplx1=load_json_from_file(mc_get_wec_cplx1_file)
    inp.url_mc_get_wec_cplx2=load_json_from_file(mc_get_wec_cplx2_file)
    inp.url_mc_get_wec_cplx3=load_json_from_file(mc_get_wec_cplx3_file)
    inp.url_mc_get_tec_cplx1=load_json_from_file(mc_get_tec_cplx1_file)
    inp.url_mc_get_tec_cplx2=load_json_from_file(mc_get_tec_cplx2_file)
    inp.url_mc_get_tec_cplx3=load_json_from_file(mc_get_tec_cplx3_file)
    inp.url_mc_get_general=load_json_from_file(mc_get_general_file)
    inp.url_mc_get_dimensions=load_json_from_file(mc_get_dimensions_file)
    inp.url_mc_get_machine=load_json_from_file(mc_get_machine_file)
    inp.url_sc_get_device_statistics=load_json_from_file(sc_get_device_statistics_file)
    inp.url_sc_get_farm_info=load_json_from_file(sc_get_farm_info_file)
    inp.url_sc_get_farm_bathymetry=load_json_from_file(sc_get_farm_bathymetry_file)
    inp.url_sc_get_farm_seabed_type=load_json_from_file(sc_get_farm_seabed_type_file)
    inp.url_ed_get_study_results=load_json_from_file(ed_get_output_results_file)

    # Populate input structure
    inp.populate()

    print(inp.sk_inputs.device_properties.hydro_data_MC_module.frequency)

    assert (inp.sk_inputs.east[1] == pytest.approx(595451.7045415622, 0.001) and \
        inp.sk_inputs.yaw[1] == pytest.approx(12.2, 0.001) and \
        inp.sk_inputs.uls_analysis_parameters.current_vel == pytest.approx(2.72, 0.001) and \
        inp.sk_inputs.uls_analysis_parameters.wind_vel == pytest.approx(35.79, 0.001) and \
        inp.sk_inputs.fls_analysis_parameters.associated_cur[1] == pytest.approx(1.5131075588188097, 0.0001) and \
        inp.sk_inputs.water_depth[0] ==  pytest.approx(4.039997875319225,0.000001) and \
        inp.sk_inputs.uls_analysis_parameters.hs_array[3] ==  pytest.approx(0.54,0.1) and \
        inp.sk_inputs.uls_analysis_parameters.tp_array[3] ==  pytest.approx(16.0,0.1) and \
        inp.sk_inputs.device_properties.steady_force_model.device_dry_profile == 'cylinder' and \
        inp.sk_inputs.device_properties.steady_force_model.device_wet_profile == 'cylinder'  and \
        inp.sk_inputs.device_properties.mooring_design_criteria.offset_max == pytest.approx(15.0, 0.0001) \
        )
    
def test_from_reduced_dof_to_dof():

    
    K_gendof = np.zeros(shape=(3,3),dtype = float)
    K_gendof[0,0] = 1.0
    K_gendof[1,1] = 2.0
    K_gendof[2,2] = 3.0
    K_gendof[1,2] = 5.0
    shared_dof = np.array([1,1,0,0,1,0])

    K = from_reduced_dof_to_dof(K_gendof,shared_dof)

    assert K[0,0] == pytest.approx(1.0, 0.0001) and K[1,1] == pytest.approx(2.0, 0.0001) and K[4,4] == pytest.approx(3.0, 0.0001) and K[1,4] == pytest.approx(5.0, 0.0001)


