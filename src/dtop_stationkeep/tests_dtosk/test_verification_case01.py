# This is the Station Keeping module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Neil Luxcey, Nicolas Michelet, Rocio Isorna
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


import numpy as np
import pytest
import sys
import os
import copy
import pandas
import matplotlib.pyplot as plt
from dtop_stationkeep.business.libraries.dtosk.rotation_module import Rab
from dtop_stationkeep.business.libraries.dtosk.run_analysis_cplx_module import run_analysis_cplx,parse_inputs_cplx,populate_outputs
from dtop_stationkeep.business.libraries.dtosk.catalogue.Lines import Lines
from dtop_stationkeep.business.libraries.dtosk.catalogue.AnchorCatalogue import AnchorCatalogue
from dtop_stationkeep.business.libraries.dtosk.mooring.Designer import Designer
from dtop_stationkeep.business.libraries.dtosk.inputs.Cplx import Cplx
from dtop_stationkeep.business.libraries.dtosk.inputs.DeviceProperties import DeviceProperties
from dtop_stationkeep.business.libraries.dtosk.inputs.SteadyForceModel import SteadyForceModel
from dtop_stationkeep.business.libraries.dtosk.winching_module import winch_line_for_pretension
from dtop_stationkeep.business.libraries.dtosk.frequency_analysis_module import compute_eigenperiod,compute_max_dynamic_offset,run_frequency_analysis
from dtop_stationkeep.business.libraries.dtosk.inputs.CustomMooringInput import CustomMooringInput


def define_input_test_case01_extreme_weather():

    # Get path to nemoh file
    this_dir = os.path.dirname(os.path.realpath(__file__))
    data_dir = os.path.join(this_dir, "..", "sample_data")
    result_path = os.path.join(data_dir,'TEST-AXI')
    mooring_input_path = os.path.join(data_dir,'mooring_input_example.json')

    # Create input structure for cplx
    input_cplx = Cplx()

    # Project name
    input_cplx.name='case01'

    # Device properties
    input_cplx.device_properties=DeviceProperties()

    # User inputs for steady force model
    input_cplx.device_properties.steady_force_model=SteadyForceModel()
    input_cplx.device_properties.steady_force_model.method='main_dimensions'
    input_cplx.device_properties.steady_force_model.device_dry_profile='cylinder'
    input_cplx.device_properties.steady_force_model.device_wet_profile='cylinder'
    input_cplx.device_properties.steady_force_model.device_dry_width=10.0
    input_cplx.device_properties.steady_force_model.device_wet_width=10.0
    input_cplx.device_properties.steady_force_model.device_dry_height=4
    input_cplx.device_properties.steady_force_model.device_wet_height=4

    
    # User inputs if nemoh run is used (standalone)
    input_cplx.device_properties.hdb_source='nemoh_run'
    input_cplx.device_properties.path_to_nemoh_result_folder=result_path
    input_cplx.device_properties.critical_damping_coefficient = np.array([0.1,0.1,0.1,0.1,0.1,0.1],dtype=float)
    
    # User inputs if mc_module is used
    #input_cplx.device_properties.hdb_source='mc_module'
    #input_cplx.device_properties.mass_matrix=np.zeros(shape=(6,6), dtype=float)
    #input_cplx.device_properties.hydrostatic_matrix=np.zeros(shape=(6,6), dtype=float)
    #input_cplx.device_properties.cog=np.zeros(shape=(3), dtype=float)
    input_cplx.device_properties.machine_type='wec'

    # Input for the tec devices
    #input_cplx.device_properties.thrust_coeff_curve_file=''
    #input_cplx.device_properties.rotor_diameter=0.0
    #input_cplx.device_properties.hub_position=np.zeros(shape=(3), dtype=float)
    
    # User input for mooring design
    input_cplx.device_properties.positioning_type  ='moored'
    input_cplx.device_properties.positioning_reference = 'seabed'
    input_cplx.device_properties.mooring_input_flag = 'custom'
    input_cplx.device_properties.mooring_design_criteria.mooring_type = 'catenary' # Important even if 'custom' is used because this drives the foundation choice as well (we will use the proofload to design the anchor)
    input_cplx.device_properties.mooring_design_criteria.nlines = 3
    input_cplx.device_properties.mooring_design_criteria.nlines_max = 10

    ########################################################################################
    ########################################################################################
    # Inputs required if mooring_input_flag = 'custom'
    ########################################################################################
    ########################################################################################
    input_cplx.device_properties.custom_mooring_input.append(CustomMooringInput())
    input_cplx.device_properties.custom_mooring_input[0].name='mooring_input_example'
    input_cplx.device_properties.custom_mooring_input[0].loadJSON(filePath = mooring_input_path)
    # Inputs required if mooring_input_flag = 'design'
    # # Constrains for pretension
    # input_cplx.device_properties.mooring_design_criteria.pretension_coeff_max=0.03 # 6% of MBL
    # input_cplx.device_properties.mooring_design_criteria.pretension_coeff_min=0.02 # 3% of MBL
    # input_cplx.device_properties.mooring_design_criteria.length_increment=1.0 # Winch 1.0m at a time (+or-)

    # # Minimum horizontal egenperiod target [s]
    # input_cplx.device_properties.mooring_design_criteria.Tmin = 30
    
    # # Constrains for total offset
    # input_cplx.device_properties.mooring_design_criteria.offset_max = 30

    # Proof load for anchor of catenary line installation
    input_cplx.device_properties.mooring_design_criteria.proof_load_coeff = 0.8
    input_cplx.device_properties.mooring_design_criteria.safety_factor = 1.7

    # Inputs for the masterstructure
    #input_cplx.master_structure_properties=MasterStructureProperties.MasterStructureProperties()

    # Inputs for motion analysis
    input_cplx.uls_analysis_parameters.weather_direction = np.array([0])

    # Parameters specific to foundation design
    input_cplx.device_properties.foundation_design_parameters.foundation_preference = 'none'
    input_cplx.device_properties.foundation_design_parameters.dist_attachment_pa = 0
    input_cplx.device_properties.foundation_design_parameters.pile_tip = 'close_end'
    input_cplx.device_properties.foundation_design_parameters.deflection_criteria_for_pile = 0

    # Read from SC
    input_cplx.water_density=1025.0
    input_cplx.uls_analysis_parameters.hs_array=np.array([12,13,14])
    input_cplx.uls_analysis_parameters.tp_array=np.array([14,15,16])
    input_cplx.uls_analysis_parameters.current_vel=2.0
    input_cplx.uls_analysis_parameters.wind_vel=30.0
    input_cplx.water_depth=np.array([100,100,100,100])
    input_cplx.device_properties.foundation_design_parameters.soil_type = 'very_dense_sand'
    input_cplx.device_properties.foundation_design_parameters.soil_slope = 1

    # Read from EC
    input_cplx.east=np.array([0, 800, 0, 800])
    input_cplx.north=np.array([0, 0, 800, 800])
    input_cplx.yaw=np.array([0, 0, 0, 0])
    input_cplx.deviceId=np.array([0,1,2,3])

    # Save inputs
    #input_cplx.saveJSON('./storage/case01_inputs.json')

    return input_cplx

def define_input_test_case01_mild_weather():

    # Get path to nemoh file
    this_dir = os.path.dirname(os.path.realpath(__file__))
    data_dir = os.path.join(this_dir, "..", "sample_data")
    result_path = os.path.join(data_dir,'TEST-AXI')
    mooring_input_path = os.path.join(data_dir,'mooring_input_example.json')

    # Create input structure for cplx
    input_cplx = Cplx()

    # Project name
    input_cplx.name='project_test2'

    # Device properties
    input_cplx.device_properties=DeviceProperties()

    # User inputs for steady force model
    input_cplx.device_properties.steady_force_model=SteadyForceModel()
    input_cplx.device_properties.steady_force_model.method='main_dimensions'
    input_cplx.device_properties.steady_force_model.device_dry_profile='cylinder'
    input_cplx.device_properties.steady_force_model.device_wet_profile='cylinder'
    input_cplx.device_properties.steady_force_model.device_dry_width=10.0
    input_cplx.device_properties.steady_force_model.device_wet_width=10.0
    input_cplx.device_properties.steady_force_model.device_dry_height=4
    input_cplx.device_properties.steady_force_model.device_wet_height=4

    
    # User inputs if nemoh run is used (standalone)
    input_cplx.device_properties.hdb_source='nemoh_run'
    input_cplx.device_properties.path_to_nemoh_result_folder=result_path
    input_cplx.device_properties.critical_damping_coefficient = np.array([0.1,0.1,0.1,0.1,0.1,0.1],dtype=float)
    
    # User inputs if mc_module is used
    #input_cplx.device_properties.hdb_source='mc_module'
    #input_cplx.device_properties.mass_matrix=np.zeros(shape=(6,6), dtype=float)
    #input_cplx.device_properties.hydrostatic_matrix=np.zeros(shape=(6,6), dtype=float)
    #input_cplx.device_properties.cog=np.zeros(shape=(3), dtype=float)
    input_cplx.device_properties.machine_type='wec'

    # Input for the tec devices
    #input_cplx.device_properties.thrust_coeff_curve_file=''
    #input_cplx.device_properties.rotor_diameter=0.0
    #input_cplx.device_properties.hub_position=np.zeros(shape=(3), dtype=float)
    
    # User input for mooring design
    input_cplx.device_properties.positioning_type  ='moored'
    input_cplx.device_properties.positioning_reference = 'seabed'
    input_cplx.device_properties.mooring_input_flag = 'custom'
    input_cplx.device_properties.mooring_design_criteria.mooring_type = 'catenary' # Important even if 'custom' is used because this drives the foundation choice as well (we will use the proofload to design the anchor)
    input_cplx.device_properties.mooring_design_criteria.nlines = 3
    input_cplx.device_properties.mooring_design_criteria.nlines_max = 10

    ########################################################################################
    ########################################################################################
    # Inputs required if mooring_input_flag = 'custom'
    ########################################################################################
    ########################################################################################
    input_cplx.device_properties.custom_mooring_input.append(CustomMooringInput())
    input_cplx.device_properties.custom_mooring_input[0].name='mooring_input_example'
    input_cplx.device_properties.custom_mooring_input[0].loadJSON(filePath = mooring_input_path)

    # Inputs required if mooring_input_flag = 'design'
    # # Constrains for pretension
    # input_cplx.device_properties.mooring_design_criteria.pretension_coeff_max=0.03 # 6% of MBL
    # input_cplx.device_properties.mooring_design_criteria.pretension_coeff_min=0.02 # 3% of MBL
    # input_cplx.device_properties.mooring_design_criteria.length_increment=1.0 # Winch 1.0m at a time (+or-)

    # # Minimum horizontal egenperiod target [s]
    # input_cplx.device_properties.mooring_design_criteria.Tmin = 30
    
    # # Constrains for total offset
    # input_cplx.device_properties.mooring_design_criteria.offset_max = 30

    # Proof load for anchor of catenary line installation
    input_cplx.device_properties.mooring_design_criteria.proof_load_coeff = 0.8
    input_cplx.device_properties.mooring_design_criteria.safety_factor = 1.7

    # Inputs for the masterstructure
    #input_cplx.master_structure_properties=MasterStructureProperties.MasterStructureProperties()

    # Inputs for motion analysis
    input_cplx.uls_analysis_parameters.weather_direction = np.array([0])

    # Parameters specific to foundation design
    input_cplx.device_properties.foundation_design_parameters.foundation_preference = 'none'
    input_cplx.device_properties.foundation_design_parameters.dist_attachment_pa = 0
    input_cplx.device_properties.foundation_design_parameters.pile_tip = 'close_end'
    input_cplx.device_properties.foundation_design_parameters.deflection_criteria_for_pile = 0

    # Read from SC
    input_cplx.water_density=1025.0
    input_cplx.uls_analysis_parameters.hs_array=np.array([4])
    input_cplx.uls_analysis_parameters.tp_array=np.array([8])
    input_cplx.uls_analysis_parameters.current_vel=0.2
    input_cplx.uls_analysis_parameters.wind_vel=12.0
    input_cplx.water_depth=np.array([100,100,100,100])
    input_cplx.device_properties.foundation_design_parameters.soil_type = 'very_dense_sand'
    input_cplx.device_properties.foundation_design_parameters.soil_slope = 1

    # Read from EC
    input_cplx.east=np.array([0, 800, 0, 800])
    input_cplx.north=np.array([0, 0, 800, 800])
    input_cplx.yaw=np.array([0, 0, 0, 0])
    input_cplx.deviceId=np.array([0,1,2,3])

    return input_cplx


def test_case01_extreme_weather():

    # Load inputs (cplx3)
    input_cplx = define_input_test_case01_extreme_weather()

    # Import catalogue
    catalogue_chains = Lines()
    catalogue_anchors = AnchorCatalogue()

    # Run main
    [farm, hierarchy, bom, env_impact, dum, dum1, dum2, dum3] = run_analysis_cplx(input_cplx,catalogue_chains,catalogue_anchors)

    print('****************')
    print('**** inputs ****')
    print('****************')
    print('Hs')
    print(input_cplx.uls_analysis_parameters.hs_array)
    print('Tp')
    print(input_cplx.uls_analysis_parameters.tp_array)
    print('Wave direction (going-to convention)')
    print(input_cplx.uls_analysis_parameters.weather_direction)
    print('water_depth')
    print(input_cplx.water_depth[0])

    print('****************')
    print('*** outputs ****')
    print('****************')
    print('initial mooring pretension')
    print(farm.body_array[0].results.uls_results.mooring_pretension)

    print('steady forces in {b}')
    print(farm.body_array[0].results.uls_results.steady_force)
    print('steady forces in {n}')
    x = copy.deepcopy(farm.body_array[0].results.uls_results.steady_position)
    f_b = farm.body_array[0].results.uls_results.steady_force
    f_n = copy.deepcopy(f_b)*0.0
    #steady_position[idir,idx,0:6] 
    #steady_force[idir,idx,0:6]
    [ndir,nenv,ndof] = np.shape(x)
    for idir in range(0,ndir):
        for ienv in range(0,nenv):
            Rbn = Rab(x[idir,ienv,3:6])
            f_n[idir,ienv,0:3]=(Rbn).dot(f_b[idir,ienv,0:3].T).T
            f_n[idir,ienv,3:6]=(Rbn).dot(f_b[idir,ienv,3:6].T).T

    print(f_n)
    print('steady equilibrium position (rotation in [deg])')
    x[:,:,3:6]=x[:,:,3:6]*180/np.pi
    print(x)
    print('steady equilibrium position (rotation in [rad])')
    print(farm.body_array[0].results.uls_results.steady_position)
    print('steady mooring tension')
    print(farm.body_array[0].results.uls_results.steady_mooring_tension)

    print('damping matrix')
    print(farm.body_array[0].results.uls_results.damping_matrix)
    print('total mooring tension')
    print(farm.body_array[0].results.uls_results.mooring_tension)
    print('total mooring tension versus mbl')
    print(farm.body_array[0].results.uls_results.tension_versus_mbl)
    print('total position')
    print(farm.body_array[0].results.uls_results.total_position)

    print('Offset')
    print(farm.body_array[0].results.uls_results.steady_offset)
    print(farm.body_array[0].results.uls_results.dynamic_offset)
    print(farm.body_array[0].results.uls_results.total_offset)

    farm.body_array[0].results.saveJSON('results_case01_extreme_weather.json')

    # Compute mooring stiffness as a function of offset
    X = np.linspace(0, 40, 9)

    # plt.plot(X,'o')
    # plt.show()

    print(farm.body_array[0].initial_position)
    K = np.zeros(shape=(6,6,len(X)))
    F = np.zeros(shape=(6,len(X)))
    

    for idx in range(0,len(X)):
        farm.body_array[0].position = copy.deepcopy(farm.body_array[0].initial_position)
        farm.body_array[0].position[0] = X[idx]
        K[:,:,idx] = farm.body_array[0].mooring_system_force.compute_linear_stiffness(farm.body_array[0].position)
        F[:,idx] = farm.body_array[0].mooring_system_force.compute_force(farm.body_array[0].position,np.array([0,0,0]))
        print('stiffness for X =' +str(X[idx]))
        print(K[:,:,idx])
    print('K11')
    for idx in range(0,len(X)):
        print(K[0,0,idx]/1000)
    print('F11')
    for idx in range(0,len(X)):
        print(F[0,idx]/1000)


    assert True

def test_case01_mild_weather():

    # Load inputs (cplx3)
    input_cplx = define_input_test_case01_mild_weather()

    # Import catalogue
    catalogue_chains = Lines()
    catalogue_anchors = AnchorCatalogue()

    # Run main
    [farm, hierarchy, bom, env_impact, dum, dum1, dum2, dum3] = run_analysis_cplx(input_cplx,catalogue_chains,catalogue_anchors)

    print('****************')
    print('**** inputs ****')
    print('****************')
    print('Hs')
    print(input_cplx.uls_analysis_parameters.hs_array)
    print('Tp')
    print(input_cplx.uls_analysis_parameters.tp_array)
    print('Wave direction (going-to convention)')
    print(input_cplx.uls_analysis_parameters.weather_direction)
    print('water_depth')
    print(input_cplx.water_depth[0])

    print('****************')
    print('*** outputs ****')
    print('****************')
    print('initial mooring pretension')
    print(farm.body_array[0].results.uls_results.mooring_pretension)

    print('steady forces in {b}')
    print(farm.body_array[0].results.uls_results.steady_force)
    print('steady forces in {n}')
    x = copy.deepcopy(farm.body_array[0].results.uls_results.steady_position)
    f_b = farm.body_array[0].results.uls_results.steady_force
    f_n = copy.deepcopy(f_b)*0.0
    #steady_position[idir,idx,0:6] 
    #steady_force[idir,idx,0:6]
    [ndir,nenv,ndof] = np.shape(x)
    for idir in range(0,ndir):
        for ienv in range(0,nenv):
            Rbn = Rab(x[idir,ienv,3:6])
            f_n[idir,ienv,0:3]=(Rbn).dot(f_b[idir,ienv,0:3].T).T
            f_n[idir,ienv,3:6]=(Rbn).dot(f_b[idir,ienv,3:6].T).T

    print(f_n)
    print('steady equilibrium position (rotation in [deg])')
    x[:,:,3:6]=x[:,:,3:6]*180/np.pi
    print(x)
    print('steady equilibrium position (rotation in [rad])')
    print(farm.body_array[0].results.uls_results.steady_position)
    print('steady mooring tension')
    print(farm.body_array[0].results.uls_results.steady_mooring_tension)

    print('damping matrix')
    print(farm.body_array[0].results.uls_results.damping_matrix)
    print('total mooring tension')
    print(farm.body_array[0].results.uls_results.mooring_tension)
    print('total mooring tension versus mbl')
    print(farm.body_array[0].results.uls_results.tension_versus_mbl)
    print('total position')
    print(farm.body_array[0].results.uls_results.total_position)

    print('Offset')
    print(farm.body_array[0].results.uls_results.steady_offset)
    print(farm.body_array[0].results.uls_results.dynamic_offset)
    print(farm.body_array[0].results.uls_results.total_offset)

    farm.body_array[0].results.saveJSON('results_case01_extreme_weather.json')

    # Compute mooring stiffness as a function of offset
    X = np.linspace(-40, 40, 17)

    # plt.plot(X,'o')
    # plt.show()

    print(farm.body_array[0].initial_position)
    K = np.zeros(shape=(6,6,len(X)))
    F = np.zeros(shape=(6,len(X)))

    for idx in range(0,len(X)):
        farm.body_array[0].position = copy.deepcopy(farm.body_array[0].initial_position)
        farm.body_array[0].position[0] = X[idx]
        K[:,:,idx] = farm.body_array[0].mooring_system_force.compute_linear_stiffness(farm.body_array[0].position)
        F[:,idx] = farm.body_array[0].mooring_system_force.compute_force(farm.body_array[0].position,np.array([0,0,0]))
        print('stiffness for X =' +str(X[idx]))
        print(K[:,:,idx])
    print('K11')
    for idx in range(0,len(X)):
        print(K[0,0,idx]/1000)
    print('F11')
    for idx in range(0,len(X)):
        print(F[0,idx]/1000)

    farm.body_array[0].results.saveJSON('results_case01_mild_weather.json')

    assert True
