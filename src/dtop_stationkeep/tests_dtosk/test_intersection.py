# This is the Station Keeping module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Neil Luxcey, Nicolas Michelet, Rocio Isorna
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


import numpy as np
import pytest
import sys
from dtop_stationkeep.business.libraries.dtosk.geometry_module import plot_rectangle_in_n
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

def test_intersection_z0():

    from dtop_stationkeep.business.libraries.dtosk.intersection_module import intersection_segment_plane_z0

    p1=np.array([5,2,5])
    p2=np.array([16,2,-3])

    [point,intersection_exists]=intersection_segment_plane_z0(p1,p2)

    assert ((point[0] == pytest.approx(11.875, 0.0001)) and (point[1] == pytest.approx(2.0,0.0001)))
