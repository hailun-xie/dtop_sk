# This is the Station Keeping module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Neil Luxcey, Nicolas Michelet, Rocio Isorna
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import collections 
import numpy as np 
import os
import sys
import pandas as pd
import pytest
try: 
    import json 
except: 
    print('WARNING: json is not installed.') 

from dtop_stationkeep.business.libraries.dtosk.foundations.foundationtype.FoundationConditions import FoundationConditions
from dtop_stationkeep.business.libraries.dtosk.foundations.inputs.FoundationInputs import FoundationInputs
from dtop_stationkeep.business.libraries.dtosk.foundations.foundationdesign.DragAnchors import DragAnchors
from dtop_stationkeep.business.libraries.dtosk.foundations.foundationdesign.GravityFoundation import GravityFoundation
from dtop_stationkeep.business.libraries.dtosk.foundations.foundationdesign.PileFoundation import PileFoundation
from dtop_stationkeep.business.libraries.dtosk.foundations.foundationdesign.SuctionCaisson import SuctionCaisson
#------------------------------------------------------------------------------------------------
## Inputs : This main is devopped for CL1-2 and 3

def test_user_sand():

    inputs = FoundationInputs()
    found_def = FoundationConditions()

    soil_type_from_sc = 'custom' # if  soil_type_from_sc ='user' then soil data needed
    user_foundation_pref = 'suction_caisson' # gravity or pile or drag_anchor or suction_caisson or evaluation
    soil_properties_sf= 'user' # user must choose between: user or default (1.3 - DNV-OSJ103)
    user_soil_properties_sf = 3 # if soil_properties_sf = 'user' --> value for sf applied to soil properties
    foundation_material = 'steel' # 'steel' or 'concrete' or 'user'(concrete is only available for gravity solutions)
    foundation_material_density = 8050 #for gravity kg/m3
    
    # variable needed only if soil_type_from_sc ='user'
    soil_type_def = 'cohesive' # then must choose between : cohesive or cohesionless
    undrained_shear_strength = 0 #soil cohesion requiered for soil_type = cohesive; in Pa
    internal_friction_angle = 40 #data required for soil_type = cohesionless, in deg
    soil_bouyant_weight = 8.1e+03 # soil bouyant weight in N/m3
    relative_density = 0.45 #data requiered for soil_type = cohesionless, no unit
    

    soil_slope_from_sc = 11 # data from user CPL1 to 3
    seabed_connection_type = 'moored'
    device_geometry = {'geometry':'cylinder', 'lr':2}
    load_ap = [0,45.9e+06 , 3E+06, 4E+04, 20e+06, 30e+06, 0]  

    dist_attachment_pa = 0
    pile_tip = "open_end"
    deflection_criteria_for_pile = 0


    ''' inputs'''

    inputs.soil_type = soil_type_from_sc
    inputs.user_foundation_pref = user_foundation_pref
    inputs.material = foundation_material
    inputs.density = foundation_material_density
    #user soil data or Non
    inputs.int_friction_angle = internal_friction_angle
    inputs.sand_dr = relative_density
    inputs.undrained_shear_strength = undrained_shear_strength 
    inputs.g_b = soil_bouyant_weight

    #user soil properties safety factor
    inputs.soil_properties_sf = soil_properties_sf
    inputs.soil_properties_sf_value = user_soil_properties_sf
        
    inputs.soil_slope = soil_slope_from_sc
    inputs.seabed_connection_type = seabed_connection_type
    inputs.loads_ap = load_ap

    inputs.device_geometry = device_geometry
    inputs.loads_from_mooring = load_ap
    inputs.dist_attachment_pa = dist_attachment_pa
    inputs.deflection_criteria_for_pile = deflection_criteria_for_pile
    inputs.pile_tip = pile_tip

    inputs.outputs_for_design()

    print(inputs.outputs_for_design())

    ''' Foundation selection'''
    if inputs.user_foundation_pref != 'evaluation':

        found_def.foundation_type = inputs.user_foundation_pref
    else :

        found_def.soil_type = inputs.soil_type
        found_def.slope_class = inputs.slope_class
        found_def.main_load_direction = inputs.main_load_direction
        found_def.seabed_connection_type = inputs.seabed_connection_type
        found_def.foundation_preference = inputs.foundation_preference

        found_def.foundation_type_evaluation()
        found_def.outputs()

    if inputs.soil_type == 'user' and inputs.user_foundation_pref == 'evaluation':
        
        error_message = "not possible to evaluate foundation suitaiblity for user soil type, please choose a foundation type "
        print(error_message)

    '''Foundation_design : shallow, gravity_anchor, pile, drag_anchor, suction_caisson'''

    if found_def.foundation_type == 'shallow' or found_def.foundation_type =='gravity_anchor' or found_def.foundation_type == 'gravity': 
        found_design = GravityFoundation()
        found_design.device_geometry = inputs.device_geometry
        found_design.slope = inputs.soil_slope
        found_design.seabed_connection_type = inputs.seabed_connection_type
        found_design.material =  inputs.material
        found_design.material_density = inputs.density

    elif found_def.foundation_type == 'pile':
        found_design = PileFoundation()
        found_design.dist_attachment = inputs.dist_attachment_pa
        found_design.tip_type = inputs.pile_tip
        found_design.deflection_crit = inputs.deflection_criteria_for_pile
        found_design.sand_dr = inputs.sand_dr
        found_design.seabed_connection_type = inputs.seabed_connection_type

    elif found_def.foundation_type =='drag_anchor':
        found_design = DragAnchors()

    elif found_def.foundation_type == 'suction_caisson':
        found_design = SuctionCaisson()
        found_design.d_chain = 0.1 #to be included by Neil

    found_design.loads_from_mooring = inputs.loads_from_mooring
    found_design.int_friction_angle_d = inputs.int_friction_angle_d
    found_design.sand_dr = inputs.sand_dr
    found_design.u_shear_strenght_d = inputs.u_shear_strenght_d
    found_design.g_b = inputs.g_b

    found_design.design()
    found_design.outputs()
    # found_design.outputs_convergence()

    e, f, g, h = found_design.outputs()
    print(e)
    print(f)
    print(g)
    print(h)

    print(found_design.d_ext)
    print(found_design.length)
    print(found_design.w_t)

    assert  found_design.d_ext == pytest.approx(3.63, 0.01) and found_design.length== pytest.approx(13.18, 0.01) and found_design.w_t == pytest.approx(0.07, 0.01)


def test_medium_dense_sand():

    inputs = FoundationInputs()
    found_def = FoundationConditions()

    soil_type_from_sc = 'medium_dense_sand' # if  soil_type_from_sc ='user' then soil data needed
    user_foundation_pref = 'suction_caisson' # gravity or pile or drag_anchor or suction_caisson or evaluation
    soil_properties_sf= 'default' # user must choose between: user or default (1.3 - DNV-OSJ103)
    user_soil_properties_sf = 0 # if soil_properties_sf = 'user' --> value for sf applied to soil properties
    foundation_material = 'concrete' # 'steel' or 'concrete' or 'user'(concrete is only available for gravity solutions)
    foundation_material_density = 2400 #for gravity kg/m3
    
    # variable needed only if soil_type_from_sc ='user'
    soil_type_def = 'none' # then must choose between : cohesive or cohesionless
    undrained_shear_strength = 0 #soil cohesion requiered for soil_type = cohesive; in Pa
    internal_friction_angle = 0 #data required for soil_type = cohesionless, in deg
    soil_bouyant_weight = 0 # soil bouyant weight in N/m3
    relative_density = 0 #data requiered for soil_type = cohesionless, no unit
    

    soil_slope_from_sc = 10
    seabed_connection_type = 'moored'
    device_geometry = {}
    load_ap = [0,45.9e+06 , 3E+06, 4E+04, 20e+06, 30e+06, 0]  

    dist_attachment_pa = 0
    pile_tip = "open_end"
    deflection_criteria_for_pile = 0


    ''' inputs'''

    inputs.soil_type = soil_type_from_sc
    inputs.user_foundation_pref = user_foundation_pref
    inputs.material = foundation_material
    inputs.density = foundation_material_density
    #user soil data or Non
    inputs.int_friction_angle = internal_friction_angle
    inputs.sand_dr = relative_density
    inputs.undrained_shear_strength = undrained_shear_strength 
    inputs.g_b = soil_bouyant_weight

    #user soil properties safety factor
    inputs.soil_properties_sf = soil_properties_sf
    inputs.soil_properties_sf_value = user_soil_properties_sf
        
    inputs.soil_slope = soil_slope_from_sc
    inputs.seabed_connection_type = seabed_connection_type
    inputs.loads_ap = load_ap

    inputs.device_geometry = device_geometry
    inputs.loads_from_mooring = load_ap
    inputs.dist_attachment_pa = dist_attachment_pa
    inputs.deflection_criteria_for_pile = deflection_criteria_for_pile
    inputs.pile_tip = pile_tip

    inputs.outputs_for_design()

    print(inputs.outputs_for_design())

    ''' Foundation selection'''
    if inputs.user_foundation_pref != 'evaluation':

        found_def.foundation_type = inputs.user_foundation_pref
    else :

        found_def.soil_type = inputs.soil_type
        found_def.slope_class = inputs.slope_class
        found_def.main_load_direction = inputs.main_load_direction
        found_def.seabed_connection_type = inputs.seabed_connection_type
        found_def.foundation_preference = inputs.foundation_preference

        found_def.foundation_type_evaluation()
        found_def.outputs()

    if inputs.soil_type == 'user' and inputs.user_foundation_pref == 'evaluation':
        
        error_message = "not possible to evaluate foundation suitaiblity for user soil type, please choose a foundation type "
        print(error_message)

    '''Foundation_design : shallow, gravity_anchor, pile, drag_anchor, suction_caisson'''

    if found_def.foundation_type == 'shallow' or found_def.foundation_type =='gravity_anchor' or found_def.foundation_type == 'gravity': 
        found_design = GravityFoundation()
        found_design.device_geometry = inputs.device_geometry
        found_design.slope = inputs.soil_slope
        found_design.seabed_connection_type = inputs.seabed_connection_type
        found_design.material =  inputs.material
        found_design.material_density = inputs.density

    elif found_def.foundation_type == 'pile':
        found_design = PileFoundation()
        found_design.dist_attachment = inputs.dist_attachment_pa
        found_design.tip_type = inputs.pile_tip
        found_design.deflection_crit = inputs.deflection_criteria_for_pile
        found_design.sand_dr = inputs.sand_dr
        found_design.seabed_connection_type = inputs.seabed_connection_type

    elif found_def.foundation_type =='drag_anchor':
        found_design = DragAnchors()

    elif found_def.foundation_type == 'suction_caisson':
        found_design = SuctionCaisson()
        found_design.d_chain = 0.1 #to be included by Neil

    found_design.loads_from_mooring = inputs.loads_from_mooring
    found_design.int_friction_angle_d = inputs.int_friction_angle_d
    found_design.sand_dr = inputs.sand_dr
    found_design.u_shear_strenght_d = inputs.u_shear_strenght_d
    found_design.g_b = inputs.g_b

    #design 
    found_design.design()
    found_design.outputs()
    # found_design.outputs_convergence()

    e, f, g, h = found_design.outputs()
    print(e)
    print(f)
    print(g)
    print(h)

    print(found_design.d_ext)
    print(found_design.length)
    print(found_design.w_t)

    assert  found_design.d_ext == pytest.approx(3.63, 0.01) and found_design.length== pytest.approx(14.49, 0.01) and found_design.w_t == pytest.approx(0.09, 0.01)



def test_stiff_clay_evaluation():

    inputs = FoundationInputs()
    found_def = FoundationConditions()

    soil_type_from_sc = 'stiff_clay' # if  soil_type_from_sc ='user' then soil data needed
    user_foundation_pref = 'suction_caisson' # gravity or pile or drag_anchor or suction_caisson or evaluation
    soil_properties_sf= 'default' # user must choose between: user or default (1.3 - DNV-OSJ103)
    user_soil_properties_sf = 0 # if soil_properties_sf = 'user' --> value for sf applied to soil properties
    foundation_material = 'steel' # 'steel' or 'concrete' or 'user'(concrete is only available for gravity solutions)
    foundation_material_density = 7850#for gravity kg/m3
    
    # variable needed only if soil_type_from_sc ='user'
    soil_type_def = 'none' # then must choose between : cohesive or cohesionless
    undrained_shear_strength = 0 #soil cohesion requiered for soil_type = cohesive; in Pa
    internal_friction_angle = 0 #data required for soil_type = cohesionless, in deg
    soil_bouyant_weight = 0 # soil bouyant weight in N/m3
    relative_density = 0 #data requiered for soil_type = cohesionless, no unit
    

    soil_slope_from_sc = 2
    seabed_connection_type = 'moored'
    device_geometry = {}
    load_ap = [-5e+06, 0 , 1E+06, 2e+06, 0, 0, 0]  

    dist_attachment_pa = 0
    pile_tip = "open_end"
    deflection_criteria_for_pile = 5


    ''' inputs'''

    inputs.soil_type = soil_type_from_sc
    inputs.user_foundation_pref = user_foundation_pref
    inputs.material = foundation_material
    inputs.density = foundation_material_density
    #user soil data or Non
    inputs.int_friction_angle = internal_friction_angle
    inputs.sand_dr = relative_density
    inputs.undrained_shear_strength = undrained_shear_strength 
    inputs.g_b = soil_bouyant_weight

    #user soil properties safety factor
    inputs.soil_properties_sf = soil_properties_sf
    inputs.soil_properties_sf_value = user_soil_properties_sf
        
    inputs.soil_slope = soil_slope_from_sc
    inputs.seabed_connection_type = seabed_connection_type
    inputs.loads_ap = load_ap

    inputs.device_geometry = device_geometry
    inputs.loads_from_mooring = load_ap
    inputs.dist_attachment_pa = dist_attachment_pa
    inputs.deflection_criteria_for_pile = deflection_criteria_for_pile
    inputs.pile_tip = pile_tip

    inputs.outputs_for_design()

    print(inputs.outputs_for_design())

    ''' Foundation selection'''
    if inputs.user_foundation_pref != 'evaluation':

        found_def.foundation_type = inputs.user_foundation_pref
    else :

        found_def.soil_type = inputs.soil_type
        found_def.slope_class = inputs.slope_class
        found_def.main_load_direction = inputs.main_load_direction
        found_def.seabed_connection_type = inputs.seabed_connection_type
        found_def.foundation_preference = inputs.user_foundation_pref

        found_def.foundation_type_evaluation()
        found_def.outputs()

    if inputs.soil_type == 'user' and inputs.user_foundation_pref == 'evaluation':
        
        error_message = "not possible to evaluate foundation suitaiblity for user soil type, please choose a foundation type "
        print(error_message)

    '''Foundation_design : shallow, gravity_anchor, pile, drag_anchor, suction_caisson'''

    if found_def.foundation_type == 'shallow' or found_def.foundation_type =='gravity_anchor' or found_def.foundation_type == 'gravity': 
        found_design = GravityFoundation()
        found_design.device_geometry = inputs.device_geometry
        found_design.slope = inputs.soil_slope
        found_design.seabed_connection_type = inputs.seabed_connection_type
        found_design.material =  inputs.material
        found_design.material_density = inputs.density

    elif found_def.foundation_type == 'pile':
        found_design = PileFoundation()
        found_design.dist_attachment = inputs.dist_attachment_pa
        found_design.tip_type = inputs.pile_tip
        found_design.deflection_crit = inputs.deflection_criteria_for_pile
        found_design.sand_dr = inputs.sand_dr
        found_design.seabed_connection_type = inputs.seabed_connection_type

    elif found_def.foundation_type =='drag_anchor':
        found_design = DragAnchors()

    elif found_def.foundation_type == 'suction_caisson':
        found_design = SuctionCaisson()
        found_design.d_chain = 0.1 #to be included by Neil

    found_design.loads_from_mooring = inputs.loads_from_mooring
    found_design.int_friction_angle_d = inputs.int_friction_angle_d
    found_design.sand_dr = inputs.sand_dr
    found_design.u_shear_strenght_d = inputs.u_shear_strenght_d
    found_design.g_b = inputs.g_b

    #design 
    found_design.design()
    found_design.outputs()  
    #found_design.outputs_convergence()

    e, f, g, h = found_design.outputs()
    print(e)
    print(f)
    print(g)
    print(h)

    print(found_design.d_ext)
    print(found_design.length)
    print(found_design.w_t)

    assert  found_design.criteria_horizontal_capacity and found_design.d_ext == pytest.approx(4.39, 0.01) and found_design.length== pytest.approx(17.53, 0.01) and found_design.w_t == pytest.approx(0.09, 0.01)


