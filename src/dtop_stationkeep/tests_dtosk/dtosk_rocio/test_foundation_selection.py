# This is the Station Keeping module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Neil Luxcey, Nicolas Michelet, Rocio Isorna
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import collections 
import numpy as np 
import os
import sys
import pandas as pd
import pytest
try: 
    import json 
except: 
    print('WARNING: json is not installed.') 
from dtop_stationkeep.business.libraries.dtosk.foundations.foundationtype.FoundationConditions import FoundationConditions
from dtop_stationkeep.business.libraries.dtosk.foundations.inputs.FoundationInputs import FoundationInputs
#------------------------------------------------------------------------------------------------
## Inputs : This main is devopped for CL1 and 2
''' OTHER MODULES:
    soil_type_from_sc (string) : soil type from sc - CL 1 and 2 from liste
    soil_slope_from_sc (string): 'steep' if slope >= 10°, 'moderate' of slope < 10°
    
    FROM MOORING DESIGN:
    loads_ap (array): loadsin absolute value and application point [V_min, V_max, H_x, H_y, M_x, M_y, z_ap] : - V_min: Vertical minimal value (upwards) [N]
                                                                                                              - V_max: Vertical maximal value (downwards) [N]
                                                                                                              - H_x: horizonal maximal load on x axis [N]
                                                                                                              - H_y: horizonal maximal load on y axis [N]
                                                                                                              - M_x: Maximal moment on x axis [Nm]
                                                                                                              - M_y: Maximal moment on x axis [Nm]
                                                                                                              - z_ap: distance between soil and the application point of loads [m]

    machine_geometry (string):'rectangular' or 'ciruclar' (in case of gravity foundation this will define the geometry of the foundation)
   
    USER:
    found_preference (string) : 'none' or 'type_of_foundation available', second option only available for CL3 

    '''
print('start')

def test_foundation_selection_1():
    
    inputs = FoundationInputs()
    found_def = FoundationConditions()
    
    soil_type_from_sc = 'soft_clay'
    soil_slope_from_sc = 3
    seabed_connection_type = 'moored'
    device_geometry = {'rectangular', 2, 1}
    load_ap = [0, 0, 3, 4, 0, 0, 0 ]  
    found_preference_usr = 'none'
    
    ''' inputs'''
   
    
    inputs.soil_type = soil_type_from_sc
    inputs.soil_slope = soil_slope_from_sc
    inputs.seabed_connection_type = seabed_connection_type
    inputs.loads_ap = load_ap
    inputs.foundation_preference = found_preference_usr
    inputs.device_geometry = device_geometry
    inputs.outputs_for_design()

    inputs.saveJSON("inputs_test.json")
    inputs2 = FoundationInputs()
    inputs2.loadJSON("inputs_test")

    print(inputs.outputs_for_design())
    
    ''' Foundation selection'''


    found_def.soil_type = inputs.soil_type
    found_def.slope_class = inputs.slope_class
    found_def.main_load_direction = inputs.main_load_direction
    found_def.seabed_connection_type = inputs.seabed_connection_type
    found_def.foundation_preference = inputs.foundation_preference

    found_def.foundation_type_evaluation()

    foundation_type = found_def.outputs()

    found_def.saveJSON("foundation_definition_test.json")
    found_def2 = FoundationConditions()
    found_def2.loadJSON("foundation_definition_test")

    assert((found_def.foundation_type == 'drag_anchor') and (inputs.int_friction_angle == 0))
    
   

def test_foundation_selection_2():
    
    inputs_2= FoundationInputs()
    found_def_2 = FoundationConditions()
    
    soil_type_from_sc_2 = 'medium_dense_sand'
    soil_slope_from_sc_2 = 11
    seabed_connection_type_2 = 'fixed'
    device_geometry_2 = {'geometry':'cylinder', 'Lr':2}
    load_ap_2 = [2, 0, 3, 4, 0, 0, 0 ]  
    found_preference_usr_2 = 'none'
    dist_attachment_pa = 0.20
    
    ''' inputs'''
   
    
    inputs_2.soil_type = soil_type_from_sc_2
    inputs_2.soil_slope = soil_slope_from_sc_2
    inputs_2.seabed_connection_type = seabed_connection_type_2
    inputs_2.loads_ap = load_ap_2
    inputs_2.foundation_preference = found_preference_usr_2
    inputs_2.device_geometry = device_geometry_2
    inputs_2.dist_attachment_pa = dist_attachment_pa
    inputs_2.outputs_for_design() 

    inputs_2.saveJSON("inputs_test2.json")
    inputs3 = FoundationInputs()
    inputs3.loadJSON("inputs_test2")

    print(inputs_2.outputs_for_design())
    
    ''' Foundation selection'''


    found_def_2.soil_type = inputs_2.soil_type
    found_def_2.slope_class = inputs_2.slope_class
    found_def_2.main_load_direction = inputs_2.main_load_direction
    found_def_2.seabed_connection_type = inputs_2.seabed_connection_type
    found_def_2.foundation_preference = inputs_2.foundation_preference

    found_def_2.foundation_type_evaluation()

    foundation_type = found_def_2.outputs()

    found_def_2.saveJSON("foundation_definition_test2.json")
    found_def3 = FoundationConditions()
    found_def3.loadJSON("foundation_definition_test2")

    assert((found_def_2.foundation_type == 'pile') and (inputs_2.int_friction_angle == 32))
    
def test_foundation_selection_3():
    
    inputs_3= FoundationInputs()
    found_def_3 = FoundationConditions()
    
    soil_type_from_sc_3 = 'dense_sand'
    soil_slope_from_sc_3 = 2
    seabed_connection_type_3 = 'fixed'
    device_geometry_3 = {'geometry':'cylinder', 'Lr':2}
    load_ap_3 = [2, 5, 3, 4, 1, 1, 0.1 ]  
    found_preference_usr_3 = 'none'
    dist_attachment_pa = 0.20
    
    ''' inputs'''
   
    
    inputs_3.soil_type = soil_type_from_sc_3
    inputs_3.soil_slope = soil_slope_from_sc_3
    inputs_3.seabed_connection_type = seabed_connection_type_3
    inputs_3.loads_ap = load_ap_3
    inputs_3.foundation_preference = found_preference_usr_3
    inputs_3.device_geometry = device_geometry_3
    inputs_3.dist_attachment_pa = dist_attachment_pa
    inputs_3.outputs_for_design() 

    inputs_3.saveJSON("inputs_test3.json")
    inputs4 = FoundationInputs()
    inputs4.loadJSON("inputs_test3")

    print(inputs_3.outputs_for_design())
    
    ''' Foundation selection'''


    found_def_3.soil_type = inputs_3.soil_type
    found_def_3.slope_class = inputs_3.slope_class
    found_def_3.main_load_direction = inputs_3.main_load_direction
    found_def_3.seabed_connection_type = inputs_3.seabed_connection_type
    found_def_3.foundation_preference = inputs_3.foundation_preference

    found_def_3.foundation_type_evaluation()

    foundation_type = found_def_3.outputs()

    found_def_3.saveJSON("foundation_definition_test3.json")
    found_def4 = FoundationConditions()
    found_def4.loadJSON("foundation_definition_test3")

    assert((found_def_3.foundation_type == 'shallow') and (inputs_3.int_friction_angle == 35))

print('end')

