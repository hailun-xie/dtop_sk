# This is the Station Keeping module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Neil Luxcey, Nicolas Michelet, Rocio Isorna
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


import numpy as np
import pytest
import sys
import os
import copy
import pandas
from dtop_stationkeep.business.libraries.dtosk.mooring import (Mooring, LineTypeData, NodeData, LineData)

def create_mooring_example():

    # Init two line type data
    line_type=LineTypeData.LineTypeData()
    line_type.line_type_name='line_steel1' 
    line_type.diameter=0.0766
    line_type.mass_density_in_air=113.35  
    line_type.ea=7.536e8 
    line_type.cb=1.0
    line_type.c_int_damp=1.0E8  
    line_type.ca=0.6 
    line_type.cdn=-1.0 
    line_type.cdt=0.05 
    line_type.name='line_type1' 
    line_type.description='line type example'
    line_type2=LineTypeData.LineTypeData()
    line_type2.line_type_name='line_steel2' 
    line_type2.diameter=0.09
    line_type2.mass_density_in_air=115 
    line_type2.ea=7.536e8 
    line_type2.cb=1.0
    line_type2.c_int_damp=1.0E8  
    line_type2.ca=0.6 
    line_type2.cdn=-1.0 
    line_type2.cdt=0.05 
    line_type2.name='line_type2' 
    line_type2.description='line type example'

    # Init node properties
    node1=NodeData.NodeData()
    node1.node_number=1.0 
    node1.type='Fix' 
    node1.position=np.array([-800.00,0.00,-200.00])
    node1.point_mass=0.1 
    node1.point_volume=0.2 
    node1.force=np.zeros(shape=(3), dtype=float) 
    node1.name='node1' 
    node1.description='node number 1'
    node2=NodeData.NodeData()
    node2.node_number=2.0 
    node2.type='Vessel' 
    node2.position=np.array([-40.00,0.00,14.00])
    node2.point_mass=0.1 
    node2.point_volume=0.2 
    node2.force=np.zeros(shape=(3), dtype=float) 
    node2.name='node2' 
    node2.description='node number 2'

    # Init line properties 
    line1=LineData.LineData()
    line1.line_number=1.0 
    line1.line_type_name='line_steel1' 
    line1.unstretched_length=835.35 
    line1.anchor_node_number=1.0 
    line1.fairlead_node_number=2.0 
    line1.flags=['GX_POS','GY_POS','GZ_POS'] 
    line1.name='line1' 
    line1.description='line number 1'
    line2=LineData.LineData()
    line2.line_number=2.0 
    line2.line_type_name='line_steel1' 
    line2.unstretched_length=835.35 
    line2.anchor_node_number=1.0 
    line2.fairlead_node_number=2.0 
    line2.flags=['GX_POS','GY_POS','GZ_POS'] 
    line2.name='line2' 
    line2.description='line number 2' 

    # Init mooring
    mooring=Mooring.Mooring()
    mooring.line_dictionnary.append(line_type)
    mooring.line_dictionnary.append(line_type2)
    mooring.node_properties.append(node1)
    mooring.node_properties.append(node2)
    mooring.line_properties.append(line1)
    mooring.line_properties.append(line2)

    return mooring


def test_pymap_input_as_string():

    # Create a mooring system
    mooring = create_mooring_example()

    # Write as a string list (stored in self.finput)
    mooring.write_to_string_list()

    # Write mooring system to map++ input file
    # mooring.write_to_map_input_file('my_solution.map')

    assert (len(mooring.finput)==24)


def test_append_list():

    input_list = []

    input_list.append('toto')

    input_list2=[]
    input_list2.append('toto1')
    input_list2.append('toto2')
    input_list.extend(input_list2)

    assert (len(input_list) == 3)

    


    