# This is the Station Keeping module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Neil Luxcey, Nicolas Michelet, Rocio Isorna
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


import numpy as np
import pytest
import sys
import os
import copy
import pandas
from dtop_stationkeep.business.libraries.dtosk.run_analysis_cplx_module import run_analysis_cplx,parse_inputs_cplx,populate_outputs
from dtop_stationkeep.business.libraries.dtosk.catalogue.Lines import Lines
from dtop_stationkeep.business.libraries.dtosk.catalogue.AnchorCatalogue import AnchorCatalogue
from dtop_stationkeep.business.libraries.dtosk.mooring.Designer import Designer
from dtop_stationkeep.business.libraries.dtosk.inputs.Cplx import Cplx
from dtop_stationkeep.business.libraries.dtosk.inputs.DeviceProperties import DeviceProperties
from dtop_stationkeep.business.libraries.dtosk.inputs.SteadyForceModel import SteadyForceModel
from dtop_stationkeep.business.libraries.dtosk.winching_module import winch_line_for_pretension
from dtop_stationkeep.business.libraries.dtosk.frequency_analysis_module import compute_eigenperiod,compute_max_dynamic_offset,run_frequency_analysis
from dtop_stationkeep.business.libraries.dtosk.inputs.CustomMooringInput import CustomMooringInput
from dtop_stationkeep.business.libraries.dtosk.inputs.MasterStructureProperties import MasterStructureProperties
from dtop_stationkeep.business.libraries.dtosk.inputs.FoundationDesignParameters import FoundationDesignParameters
from dtop_stationkeep.business.libraries.dtosk.outputs.Project import Project
from dtop_stationkeep.business.libraries.dtosk.run_main_module import populate_design_assessment_results
from dtop_stationkeep.business.libraries.dtosk.inputs.SubstationProperties import SubstationProperties



def define_test_inputs_with_substation():

    # Get path to nemoh file
    this_dir = os.path.dirname(os.path.realpath(__file__))
    data_dir = os.path.join(this_dir, "..", "sample_data")
    result_path = os.path.join(data_dir,'TEST-AXI')
    mooring_input_path = os.path.join(data_dir,'mooring_input_example.json')
    mooring_input_slave_path = os.path.join(data_dir,'mooring_input_masterstructure_slave.json')
    mooring_input_master_path = os.path.join(data_dir,'mooring_input_masterstructure.json')
    # Create input structure for cplx
    input_cplx = Cplx()

    # Project name
    input_cplx.name='project_test2'

    # Device properties
    input_cplx.device_properties=DeviceProperties()

    # User inputs for steady force model
    input_cplx.device_properties.steady_force_model=SteadyForceModel()
    input_cplx.device_properties.steady_force_model.method='main_dimensions'
    input_cplx.device_properties.steady_force_model.device_dry_profile='cylinder'
    input_cplx.device_properties.steady_force_model.device_wet_profile='cylinder'
    input_cplx.device_properties.steady_force_model.device_dry_width=3.0
    input_cplx.device_properties.steady_force_model.device_wet_width=3.0
    input_cplx.device_properties.steady_force_model.device_dry_height=4
    input_cplx.device_properties.steady_force_model.device_wet_height=4

    
    # User inputs if nemoh run is used (standalone)
    input_cplx.device_properties.hdb_source='nemoh_run'
    input_cplx.device_properties.path_to_nemoh_result_folder=result_path
    input_cplx.device_properties.critical_damping_coefficient = np.array([0.1,0.1,0.1,0.1,0.1,0.1],dtype=float)
    
    # User inputs if mc_module is used
    #input_cplx.device_properties.hdb_source='mc_module'
    #input_cplx.device_properties.mass_matrix=np.zeros(shape=(6,6), dtype=float)
    #input_cplx.device_properties.hydrostatic_matrix=np.zeros(shape=(6,6), dtype=float)
    #input_cplx.device_properties.cog=np.zeros(shape=(3), dtype=float)
    input_cplx.device_properties.machine_type='wec'

    # Input for the tec devices
    #input_cplx.device_properties.thrust_coeff_curve_file=''
    #input_cplx.device_properties.rotor_diameter=0.0
    #input_cplx.device_properties.hub_position=np.zeros(shape=(3), dtype=float)
    
    # User input for mooring design
    input_cplx.device_properties.positioning_type  ='moored'
    input_cplx.device_properties.positioning_reference = 'masterstructure'
    input_cplx.device_properties.mooring_input_flag = 'custom'
    input_cplx.device_properties.mooring_design_criteria.mooring_type = 'catenary' # Important even if 'custom' is used because this drives the foundation choice as well (we will use the proofload to design the anchor)
    input_cplx.device_properties.mooring_design_criteria.nlines = 3
    input_cplx.device_properties.mooring_design_criteria.nlines_max = 10

    ########################################################################################
    ########################################################################################
    # Inputs required if mooring_input_flag = 'custom'
    ########################################################################################
    ########################################################################################
    input_cplx.device_properties.custom_mooring_input.append(CustomMooringInput())
    input_cplx.device_properties.custom_mooring_input[0].name='mooring_input_masterstructure_slave'
    input_cplx.device_properties.custom_mooring_input[0].loadJSON(filePath = mooring_input_slave_path)

    # Inputs required if mooring_input_flag = 'design'
    # # Constrains for pretension
    # input_cplx.device_properties.mooring_design_criteria.pretension_coeff_max=0.03 # 6% of MBL
    # input_cplx.device_properties.mooring_design_criteria.pretension_coeff_min=0.02 # 3% of MBL
    # input_cplx.device_properties.mooring_design_criteria.length_increment=1.0 # Winch 1.0m at a time (+or-)

    # # Minimum horizontal egenperiod target [s]
    # input_cplx.device_properties.mooring_design_criteria.Tmin = 30
    
    # # Constrains for total offset
    # input_cplx.device_properties.mooring_design_criteria.offset_max = 30

    # Proof load for anchor of catenary line installation
    input_cplx.device_properties.mooring_design_criteria.proof_load_coeff = 0.8
    input_cplx.device_properties.mooring_design_criteria.safety_factor = 1.7

    # Inputs for motion analysis
    input_cplx.uls_analysis_parameters.weather_direction = np.array([0])

    # Inputs for the masterstructure, required here since input_cplx.device_properties.positioning_reference == 'masterstructure'
    input_cplx.master_structure_properties=MasterStructureProperties()
    input_cplx.master_structure_properties.positioning_type='moored'
    input_cplx.master_structure_properties.mooring_type='catenary'
    input_cplx.master_structure_properties.mooring_input_flag='custom'
    input_cplx.master_structure_properties.custom_mooring_input=CustomMooringInput()
    input_cplx.master_structure_properties.custom_mooring_input.name='mooring_input_masterstructure'
    input_cplx.master_structure_properties.custom_mooring_input.loadJSON(filePath = mooring_input_master_path)

    # Foundation parameters for the masterstructure
    input_cplx.master_structure_properties.foundation_design_parameters=FoundationDesignParameters()
    input_cplx.master_structure_properties.foundation_design_parameters.foundation_preference = 'none'
    input_cplx.master_structure_properties.foundation_design_parameters.dist_attachment_pa = 0
    input_cplx.master_structure_properties.foundation_design_parameters.pile_tip = 'close_end'
    input_cplx.master_structure_properties.foundation_design_parameters.deflection_criteria_for_pile = 0   
    input_cplx.master_structure_properties.foundation_design_parameters.soil_type = 'very_dense_sand'
    input_cplx.master_structure_properties.foundation_design_parameters.soil_slope = 1

    # Other parameters
    input_cplx.master_structure_properties.water_depth = 100.0  
    input_cplx.master_structure_properties.mooring_design_criteria.proof_load_coeff = 0.8
    input_cplx.master_structure_properties.mooring_design_criteria.safety_factor = 1.7

    # Read from SC
    input_cplx.water_density=1025.0
    input_cplx.uls_analysis_parameters.hs_array=np.array([12,13,14])
    input_cplx.uls_analysis_parameters.tp_array=np.array([14,15,16])
    input_cplx.uls_analysis_parameters.current_vel=2.0
    input_cplx.uls_analysis_parameters.wind_vel=30.0

    input_cplx.device_properties.foundation_design_parameters.soil_type = 'very_dense_sand'
    input_cplx.device_properties.foundation_design_parameters.soil_slope = 1

    # Read from EC
    input_cplx.water_depth=np.array([100,100,100,100])
    input_cplx.east=np.array([-25, 25, 25, -25])
    input_cplx.north=np.array([25, -25, 25, -25])
    input_cplx.yaw=np.array([0, 0, 0, 0])
    input_cplx.deviceId=np.array([0,1,2,3])

    # Substation properties
    input_cplx.substation_is_present = True
    input_cplx.substation_properties.append(SubstationProperties())
    input_cplx.substation_properties[0].water_depth = 100
    input_cplx.substation_properties[0].steady_force_model=SteadyForceModel()
    input_cplx.substation_properties[0].steady_force_model.method='main_dimensions'
    input_cplx.substation_properties[0].steady_force_model.device_dry_profile='rectangle'
    input_cplx.substation_properties[0].steady_force_model.device_wet_profile='cylinder'
    input_cplx.substation_properties[0].steady_force_model.device_dry_width=25
    input_cplx.substation_properties[0].steady_force_model.device_wet_width=10 # monopile!
    input_cplx.substation_properties[0].steady_force_model.device_dry_height=20
    input_cplx.substation_properties[0].steady_force_model.device_wet_height=100 # monopile!
    input_cplx.substation_properties[0].east = -250
    input_cplx.substation_properties[0].north = 100
    input_cplx.substation_properties[0].mass = 18000000 # including monopile!
    input_cplx.substation_properties[0].cog[2] = -50 # including monopile
    input_cplx.substation_properties[0].cost_of_support_structure = 2250000 # price of monopile only
    input_cplx.substation_properties[0].foundation_design_parameters=FoundationDesignParameters()
    input_cplx.substation_properties[0].foundation_design_parameters.foundation_preference = 'none' # Pile if monopile, else use shallow
    input_cplx.substation_properties[0].foundation_design_parameters.dist_attachment_pa = 0
    input_cplx.substation_properties[0].foundation_design_parameters.pile_tip = 'close_end'
    input_cplx.substation_properties[0].foundation_design_parameters.deflection_criteria_for_pile = 0
    input_cplx.substation_properties[0].foundation_design_parameters.soil_type = 'very_dense_sand'
    input_cplx.substation_properties[0].foundation_design_parameters.soil_slope = 1

    return input_cplx


def test_cplx_substation_cplx3():

    # Load inputs (cplx3)
    input_cplx = define_test_inputs_with_substation()

    # Import catalogue
    catalogue_chains = Lines()
    catalogue_anchors = AnchorCatalogue()
    project = Project()

    # Run main
    # [farm, hierarchy, bom, env_impact, dum, dum1, dum2, dum3] = run_analysis_cplx(input_cplx,catalogue_chains)

    # Run analysis
    [farm, project.hierarchy, project.bom, project.environmental_impact, project.design_assessment.uls_environments, uls_weather_directions, project.design_assessment.fls_environments, project.representation] = run_analysis_cplx(input_cplx,catalogue_chains,catalogue_anchors)

	# Store weather directions used for the uls analysis in [deg]
    project.design_assessment.uls_weather_directions = copy.deepcopy(uls_weather_directions)*180.0/np.pi

	# Populate results from ULS, FLS and Design analyses and save mooring system as map++ file
    [project.design_assessment.devices,project.design_assessment.master_structure,project.design_assessment.master_structure_is_present,project.design_assessment.substation,project.design_assessment.substation_is_present]=populate_design_assessment_results(farm) 
	

    # pandas.set_option('display.max_columns', 500)

    # print('hierarchy')
    # d = pandas.read_json(hierarchy.hierarchy_as_pandas_table)
    # print(d)

    # print('bom')
    # d = pandas.read_json(bom.bom_as_pandas_table)
    # print(d)

    # print('envimpact')
    # print(env_impact.farm.footprint)

    # farm.plot()
    # project.saveJSON(fileName='nico_complete_result.json')
    # with open('my_hierarchy.txt', 'w') as f:
    #     for item in hierarchy.hierarchy_as_pandas_table:
    #         f.write("%s\n" % item)
    # table = pandas.read_json(hierarchy.hierarchy_as_pandas_table)
    # table.to_csv('toto_csv.csv')
    # table = pandas.read_json(bom.bom_as_pandas_table)
    # table.to_csv('bom_csv.csv')

    assert (project.design_assessment.substation_is_present and project.environmental_impact.farm.footprint == pytest.approx(99381.62405467, 0.1)) 
    