# This is the Station Keeping module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Neil Luxcey, Nicolas Michelet, Rocio Isorna
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


import numpy as np
import pytest
import sys
import os
import copy
import pandas
from dtop_stationkeep.business.libraries.dtosk.run_analysis_cplx_module import run_analysis_cplx,parse_inputs_cplx,populate_outputs
from dtop_stationkeep.business.libraries.dtosk.catalogue.Lines import Lines
from dtop_stationkeep.business.libraries.dtosk.catalogue.AnchorCatalogue import AnchorCatalogue
from dtop_stationkeep.business.libraries.dtosk.mooring.Designer import Designer
from dtop_stationkeep.business.libraries.dtosk.inputs.Cplx import Cplx
from dtop_stationkeep.business.libraries.dtosk.inputs.DeviceProperties import DeviceProperties
from dtop_stationkeep.business.libraries.dtosk.inputs.SteadyForceModel import SteadyForceModel
from dtop_stationkeep.business.libraries.dtosk.winching_module import winch_line_for_pretension
from dtop_stationkeep.business.libraries.dtosk.frequency_analysis_module import compute_eigenperiod,compute_max_dynamic_offset,run_frequency_analysis
from dtop_stationkeep.business.libraries.dtosk.inputs.CustomMooringInput import CustomMooringInput


def define_test_inputs_RM1_single():

    # Get path to nemoh file
    this_dir = os.path.dirname(os.path.realpath(__file__))
    data_dir = os.path.join(this_dir, "..", "sample_data")
    # result_path = os.path.join(data_dir,'TEST-AXI')
    # mooring_input_path = os.path.join(data_dir,'mooring_input_example.json')

    # Create input structure for cplx
    input_cplx = Cplx()

    # Project name
    input_cplx.name='standard_reference_RM1_single'

    # Device properties
    input_cplx.device_properties=DeviceProperties()

    # User inputs for steady force model
    input_cplx.device_properties.steady_force_model=SteadyForceModel()
    input_cplx.device_properties.steady_force_model.method='main_dimensions'
    input_cplx.device_properties.steady_force_model.device_dry_profile='cylinder'
    input_cplx.device_properties.steady_force_model.device_wet_profile='cylinder'
    input_cplx.device_properties.steady_force_model.device_dry_width=0.1
    input_cplx.device_properties.steady_force_model.device_wet_width=3.5
    input_cplx.device_properties.steady_force_model.device_dry_height=0.1
    input_cplx.device_properties.steady_force_model.device_wet_height=30.0

    
    # User inputs if nemoh run is used (standalone)
    # input_cplx.device_properties.hdb_source='nemoh_run'
    # input_cplx.device_properties.path_to_nemoh_result_folder=result_path
    # input_cplx.device_properties.critical_damping_coefficient = np.array([0.1,0.1,0.1,0.1,0.1,0.1],dtype=float)
    
    # User inputs if mc_module is used
    #input_cplx.device_properties.hdb_source='mc_module'
    #input_cplx.device_properties.mass_matrix=np.zeros(shape=(6,6), dtype=float)
    input_cplx.device_properties.mass = 219370
    #input_cplx.device_properties.hydrostatic_matrix=np.zeros(shape=(6,6), dtype=float)
    input_cplx.device_properties.cog=np.zeros(shape=(3), dtype=float)
    input_cplx.device_properties.machine_type='tec'

    # Input for the tec devices
    this_dir = os.path.dirname(os.path.realpath(__file__))
    data_dir = os.path.join(this_dir, "..", "sample_data")
    input_cplx.device_properties.thrust_coeff_curve_file=os.path.join(data_dir,'thrustcurv_RM1.txt')
    input_cplx.device_properties.rotor_diameter=20.0
    input_cplx.device_properties.hub_position=np.zeros(shape=(3,2))
    input_cplx.device_properties.hub_position[:,0] = [0.0,-14.0,30.0]
    input_cplx.device_properties.hub_position[:,1] = [0.0,14.0,30.0]

    # User input for mooring design
    input_cplx.device_properties.positioning_type  ='fixed'
    input_cplx.device_properties.positioning_reference = 'seabed'
    # input_cplx.device_properties.mooring_input_flag = 'custom'
    # input_cplx.device_properties.mooring_design_criteria.mooring_type = 'catenary' # Important even if 'custom' is used because this drives the foundation choice as well (we will use the proofload to design the anchor)
    # input_cplx.device_properties.mooring_design_criteria.nlines = 3
    # input_cplx.device_properties.mooring_design_criteria.nlines_max = 10

    ########################################################################################
    ########################################################################################
    # Inputs required if mooring_input_flag = 'custom'
    ########################################################################################
    ########################################################################################
    # input_cplx.device_properties.custom_mooring_input.name='mooring_input_example'
    # input_cplx.device_properties.custom_mooring_input.loadJSON(filePath = mooring_input_path)

    # Inputs required if mooring_input_flag = 'design'
    # # Constrains for pretension
    # input_cplx.device_properties.mooring_design_criteria.pretension_coeff_max=0.03 # 6% of MBL
    # input_cplx.device_properties.mooring_design_criteria.pretension_coeff_min=0.02 # 3% of MBL
    # input_cplx.device_properties.mooring_design_criteria.length_increment=1.0 # Winch 1.0m at a time (+or-)

    # # Minimum horizontal egenperiod target [s]
    # input_cplx.device_properties.mooring_design_criteria.Tmin = 30
    
    # # Constrains for total offset
    # input_cplx.device_properties.mooring_design_criteria.offset_max = 30

    # Proof load for anchor of catenary line installation
    # input_cplx.device_properties.mooring_design_criteria.proof_load_coeff = 0.8
    # input_cplx.device_properties.mooring_design_criteria.safety_factor = 1.7

    # Inputs for the masterstructure
    #input_cplx.master_structure_properties=MasterStructureProperties.MasterStructureProperties()

    # Inputs for motion analysis
    input_cplx.uls_analysis_parameters.weather_direction = np.array([0])

    # Parameters specific to foundation design
    input_cplx.device_properties.foundation_design_parameters.foundation_preference = 'pile'
    input_cplx.device_properties.foundation_design_parameters.dist_attachment_pa = 0.0
    input_cplx.device_properties.foundation_design_parameters.pile_tip = 'open_end'
    input_cplx.device_properties.foundation_design_parameters.deflection_criteria_for_pile = 5 # In % !!!!!

    # Safety factor
    input_cplx.device_properties.foundation_design_parameters.soil_properties_sf= 'user' # user must choose between: user or default (1.3 - DNV-OSJ103)
    input_cplx.device_properties.foundation_design_parameters.user_soil_properties_sf = 1.3 # if soil_properties_sf = 'user' --> value for sf applied to soil properties

    # Read from SC
    input_cplx.water_density=1025.0
    input_cplx.uls_analysis_parameters.hs_array=np.array([8])
    input_cplx.uls_analysis_parameters.tp_array=np.array([10])
    input_cplx.uls_analysis_parameters.current_vel=1.92 # Try with 3m/s and 1.92 m/s since this corresponds to maximum rotor force
    input_cplx.uls_analysis_parameters.wind_vel=0.0
    input_cplx.water_depth=np.array([50])
    input_cplx.device_properties.foundation_design_parameters.soil_type = 'gravels_pebbles'
    input_cplx.device_properties.foundation_design_parameters.soil_slope = 0.0

    # Read from EC
    input_cplx.east=np.array([0])
    input_cplx.north=np.array([0])
    input_cplx.yaw=np.array([0])
    input_cplx.deviceId=np.array([0])

    return input_cplx

def test_standard_reference_cases_RM1_single():

    # Load inputs (cplx3)
    input_cplx = define_test_inputs_RM1_single()

    # Import catalogue
    catalogue_chains = Lines()
    catalogue_anchors = AnchorCatalogue()

    # Run main
    [farm, hierarchy, bom, env_impact, dum, dum1, dum2, dum3] = run_analysis_cplx(input_cplx,catalogue_chains,catalogue_anchors)

    assert (farm.body_array[0].results.design_results.anchor_and_foundation.pile_foundation_design.plength == pytest.approx(12.6,0.0001) and farm.body_array[0].results.design_results.anchor_and_foundation.pile_foundation_design.criteria_axial_compression_capacity)





    