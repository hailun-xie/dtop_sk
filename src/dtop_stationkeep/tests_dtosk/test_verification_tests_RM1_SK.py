# This is the Station Keeping module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Neil Luxcey, Nicolas Michelet, Rocio Isorna
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


import numpy as np
import pytest
import sys
import os
import copy
import pandas
import matplotlib.pyplot as plt
from dtop_stationkeep.business.libraries.dtosk.rotation_module import Rab
from dtop_stationkeep.business.libraries.dtosk.run_analysis_cplx_module import run_analysis_cplx,parse_inputs_cplx,populate_outputs
from dtop_stationkeep.business.libraries.dtosk.catalogue.Lines import Lines
from dtop_stationkeep.business.libraries.dtosk.catalogue.AnchorCatalogue import AnchorCatalogue
from dtop_stationkeep.business.libraries.dtosk.mooring.Designer import Designer
from dtop_stationkeep.business.libraries.dtosk.inputs.Cplx import Cplx
from dtop_stationkeep.business.libraries.dtosk.inputs.DeviceProperties import DeviceProperties
from dtop_stationkeep.business.libraries.dtosk.inputs.SteadyForceModel import SteadyForceModel
from dtop_stationkeep.business.libraries.dtosk.winching_module import winch_line_for_pretension
from dtop_stationkeep.business.libraries.dtosk.frequency_analysis_module import compute_eigenperiod,compute_max_dynamic_offset,run_frequency_analysis
from dtop_stationkeep.business.libraries.dtosk.inputs.CustomMooringInput import CustomMooringInput
from dtop_stationkeep.business.libraries.dtosk.run_main_module import create_logger
# create_logger(log_to_file = False, log_to_stream = False)


def define_test_inputs_RM1_SK1():

    # Get path to nemoh file
    this_dir = os.path.dirname(os.path.realpath(__file__))
    data_dir = os.path.join(this_dir, "..", "sample_data")

    # Create input structure for cplx
    input_cplx = Cplx()

    # Project name
    input_cplx.name='standard_reference_RM1_single'

    # Device properties
    input_cplx.device_properties=DeviceProperties()

    # User inputs for steady force model
    input_cplx.device_properties.steady_force_model=SteadyForceModel()
    input_cplx.device_properties.steady_force_model.method='main_dimensions'
    input_cplx.device_properties.steady_force_model.device_dry_profile='cylinder'
    input_cplx.device_properties.steady_force_model.device_wet_profile='cylinder'
    input_cplx.device_properties.steady_force_model.device_dry_width=0.0
    input_cplx.device_properties.steady_force_model.device_wet_width=3.5
    input_cplx.device_properties.steady_force_model.device_dry_height=0.0
    input_cplx.device_properties.steady_force_model.device_wet_height=30.0
    
    # User inputs if mc_module is used
    input_cplx.device_properties.mass = 219370
    input_cplx.device_properties.cog=np.zeros(shape=(3), dtype=float)
    input_cplx.device_properties.machine_type='tec'

    # Input for the tec devices
    this_dir = os.path.dirname(os.path.realpath(__file__))
    data_dir = os.path.join(this_dir, "..", "sample_data")
    input_cplx.device_properties.thrust_coeff_curve_file=os.path.join(data_dir,'thrustcurv_RM1.txt')
    input_cplx.device_properties.rotor_diameter=20.0
    input_cplx.device_properties.hub_position=np.zeros(shape=(3,2))
    input_cplx.device_properties.hub_position[:,0] = [0.0,-14.0,30.0]
    input_cplx.device_properties.hub_position[:,1] = [0.0,14.0,30.0]

    # User input for mooring design
    input_cplx.device_properties.positioning_type  ='fixed'
    input_cplx.device_properties.positioning_reference = 'seabed'

    # Inputs for ULS analysis
    input_cplx.uls_analysis_parameters.weather_direction = np.array([0])

    # Parameters specific to foundation design
    input_cplx.device_properties.foundation_design_parameters.foundation_preference = 'evaluation'
    input_cplx.device_properties.foundation_design_parameters.dist_attachment_pa = 0.0
    input_cplx.device_properties.foundation_design_parameters.pile_tip = 'open_end'
    input_cplx.device_properties.foundation_design_parameters.deflection_criteria_for_pile = 5 # In % !!!!!

    # Safety factor
    input_cplx.device_properties.foundation_design_parameters.soil_properties_sf= 'user' # user must choose between: user or default (1.3 - DNV-OSJ103)
    input_cplx.device_properties.foundation_design_parameters.user_soil_properties_sf = 1.3 # if soil_properties_sf = 'user' --> value for sf applied to soil properties

    # Read from SC
    input_cplx.water_density=1025.0
    input_cplx.uls_analysis_parameters.hs_array=np.array([8])
    input_cplx.uls_analysis_parameters.tp_array=np.array([10])
    input_cplx.uls_analysis_parameters.current_vel=2.85 # Try with 3m/s and 1.92 m/s since this corresponds to maximum rotor force
    input_cplx.uls_analysis_parameters.wind_vel=0.0
    input_cplx.water_depth=np.array([50])
    input_cplx.device_properties.foundation_design_parameters.soil_type = 'dense_sand'
    input_cplx.device_properties.foundation_design_parameters.soil_slope = 0.0

    # Read from EC
    input_cplx.east=np.array([0])
    input_cplx.north=np.array([0])
    input_cplx.yaw=np.array([0])
    input_cplx.deviceId=np.array([0])

    return input_cplx

def test_case_RM1_SK1():

    # Load inputs (cplx3)
    input_cplx = define_test_inputs_RM1_SK1()
   

    # Import catalogue
    catalogue_chains = Lines()
    catalogue_anchors = AnchorCatalogue()

    # Run main
    [farm, hierarchy, bom, env_impact, dum, dum1, dum2, dum3] = run_analysis_cplx(input_cplx,catalogue_chains,catalogue_anchors)

    assert (farm.body_array[0].results.design_results.anchor_and_foundation.gravity_foundation_design.base_radius*2 == pytest.approx(15.0,0.0001)) 


def test_case_RM1_SK2_version1():

    # Load inputs (cplx3)
    input_cplx = define_test_inputs_RM1_SK1()
    input_cplx.device_properties.foundation_design_parameters.foundation_preference = 'pile'

    # Import catalogue
    catalogue_chains = Lines()
    catalogue_anchors = AnchorCatalogue()

    # Run main
    [farm, hierarchy, bom, env_impact, dum, dum1, dum2, dum3] = run_analysis_cplx(input_cplx,catalogue_chains,catalogue_anchors)

    assert (farm.body_array[0].results.design_results.anchor_and_foundation.pile_foundation_design.plength == pytest.approx(13.8,0.0001))

def test_case_RM1_SK2_version2():

    # Load inputs (cplx3)
    input_cplx = define_test_inputs_RM1_SK1()
    input_cplx.device_properties.foundation_design_parameters.foundation_preference = 'pile'
    input_cplx.device_properties.steady_force_model.device_wet_height=0.00
    input_cplx.device_properties.foundation_design_parameters.pile_length_above_seabed = 30.0
    input_cplx.device_properties.mass = 119700

    # Import catalogue
    catalogue_chains = Lines()
    catalogue_anchors = AnchorCatalogue()
    
    # Run main
    [farm, hierarchy, bom, env_impact, dum, dum1, dum2, dum3] = run_analysis_cplx(input_cplx,catalogue_chains,catalogue_anchors)

    assert (farm.body_array[0].results.design_results.anchor_and_foundation.pile_foundation_design.plength == pytest.approx(12.6,0.0001))


def test_case_RM1_SK3_version1():

    # Load inputs (cplx3)
    input_cplx = define_test_inputs_RM1_SK1()
    input_cplx.device_properties.foundation_design_parameters.foundation_preference = 'pile'
    input_cplx.device_properties.foundation_design_parameters.foundation_design_flag = 'custom'
    input_cplx.device_properties.foundation_design_parameters.custom_foundation_input.w_t  = 0.039
    input_cplx.device_properties.foundation_design_parameters.custom_foundation_input.d_ext  = 3.5
    input_cplx.device_properties.foundation_design_parameters.custom_foundation_input.length  = 15.0

    # Import catalogue
    catalogue_chains = Lines()
    catalogue_anchors = AnchorCatalogue()

    # Run main
    [farm, hierarchy, bom, env_impact, dum, dum1, dum2, dum3] = run_analysis_cplx(input_cplx,catalogue_chains,catalogue_anchors)

    assert (farm.body_array[0].results.design_results.anchor_and_foundation.pile_foundation_design.lat_cap == pytest.approx(14388544.880761,0.0001))


def test_case_RM1_SK3_version2():

    # Load inputs (cplx3)
    input_cplx = define_test_inputs_RM1_SK1()
    input_cplx.device_properties.foundation_design_parameters.foundation_preference = 'pile'
    input_cplx.device_properties.foundation_design_parameters.foundation_design_flag = 'custom'
    input_cplx.device_properties.steady_force_model.device_wet_height=0.00
    input_cplx.device_properties.foundation_design_parameters.pile_length_above_seabed = 30.0
    input_cplx.device_properties.foundation_design_parameters.custom_foundation_input.w_t  = 0.039
    input_cplx.device_properties.foundation_design_parameters.custom_foundation_input.d_ext  = 3.5
    input_cplx.device_properties.foundation_design_parameters.custom_foundation_input.length  = 15.0
    input_cplx.device_properties.mass = 119700

    # Import catalogue
    catalogue_chains = Lines()
    catalogue_anchors = AnchorCatalogue()

    # Run main
    [farm, hierarchy, bom, env_impact, dum, dum1, dum2, dum3] = run_analysis_cplx(input_cplx,catalogue_chains,catalogue_anchors)
    assert (farm.body_array[0].results.design_results.anchor_and_foundation.pile_foundation_design.lat_cap == pytest.approx(14388544.880761918,0.0001))
