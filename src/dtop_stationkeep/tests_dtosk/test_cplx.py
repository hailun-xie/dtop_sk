# This is the Station Keeping module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Neil Luxcey, Nicolas Michelet, Rocio Isorna
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


import numpy as np
import pytest
import sys
import os
import copy
import pandas
from dtop_stationkeep.business.libraries.dtosk.foundations.foundationdesign.DragAnchors import DragAnchors
from dtop_stationkeep.business.libraries.dtosk.run_analysis_cplx_module import run_analysis_cplx,parse_inputs_cplx,populate_outputs
from dtop_stationkeep.business.libraries.dtosk.catalogue.Lines import Lines
from dtop_stationkeep.business.libraries.dtosk.catalogue.AnchorCatalogue import AnchorCatalogue
from dtop_stationkeep.business.libraries.dtosk.mooring.Designer import Designer
from dtop_stationkeep.business.libraries.dtosk.inputs.Cplx import Cplx
from dtop_stationkeep.business.libraries.dtosk.inputs.DeviceProperties import DeviceProperties
from dtop_stationkeep.business.libraries.dtosk.inputs.SteadyForceModel import SteadyForceModel
from dtop_stationkeep.business.libraries.dtosk.winching_module import winch_line_for_pretension
from dtop_stationkeep.business.libraries.dtosk.frequency_analysis_module import compute_eigenperiod,compute_max_dynamic_offset,run_frequency_analysis
from dtop_stationkeep.business.libraries.dtosk.inputs.CustomMooringInput import CustomMooringInput

def define_test_inputs_12():

    # Get path to nemoh file
    this_dir = os.path.dirname(os.path.realpath(__file__))
    data_dir = os.path.join(this_dir, "..", "sample_data")
    result_path = os.path.join(data_dir,'TEST-AXI')

    # Create input structure for cplx
    input_cplx = Cplx()

    # Project name
    input_cplx.name='project_test1'

    # Device properties
    input_cplx.device_properties=DeviceProperties()

    # User inputs for steady force model
    input_cplx.device_properties.steady_force_model=SteadyForceModel()
    input_cplx.device_properties.steady_force_model.method='main_dimensions'
    input_cplx.device_properties.steady_force_model.device_dry_profile='cylinder'
    input_cplx.device_properties.steady_force_model.device_wet_profile='cylinder'
    input_cplx.device_properties.steady_force_model.device_dry_width=10.0
    input_cplx.device_properties.steady_force_model.device_wet_width=10.0
    input_cplx.device_properties.steady_force_model.device_dry_height=4
    input_cplx.device_properties.steady_force_model.device_wet_height=4

    
    # User inputs if nemoh run is used (standalone)
    input_cplx.device_properties.hdb_source='nemoh_run'
    input_cplx.device_properties.path_to_nemoh_result_folder=result_path
    input_cplx.device_properties.critical_damping_coefficient = np.array([0.1,0.1,0.1,0.1,0.1,0.1],dtype=float)
    
    # User inputs if mc_module is used
    #input_cplx.device_properties.hdb_source='mc_module'
    #input_cplx.device_properties.mass_matrix=np.zeros(shape=(6,6), dtype=float)
    #input_cplx.device_properties.hydrostatic_matrix=np.zeros(shape=(6,6), dtype=float)
    #input_cplx.device_properties.cog=np.zeros(shape=(3), dtype=float)
    input_cplx.device_properties.machine_type='wec'

    # Input for the tec devices
    #input_cplx.device_properties.thrust_coeff_curve_file=''
    #input_cplx.device_properties.rotor_diameter=0.0
    #input_cplx.device_properties.hub_position=np.zeros(shape=(3), dtype=float)
    
    # User input for mooring design
    input_cplx.device_properties.positioning_type  ='moored'
    input_cplx.device_properties.positioning_reference = 'seabed'
    input_cplx.device_properties.mooring_input_flag = 'design'
    input_cplx.device_properties.mooring_design_criteria.mooring_type = 'catenary'
    input_cplx.device_properties.mooring_design_criteria.nlines = 3
    input_cplx.device_properties.mooring_design_criteria.nlines_max = 10
    input_cplx.device_properties.mooring_design_criteria.fairlead_vertical_position = -4
    input_cplx.device_properties.mooring_design_criteria.fairlead_radius = 5.0

    # Constrains for pretension
    input_cplx.device_properties.mooring_design_criteria.pretension_coeff_min=0.00 # 3% of MBL
    input_cplx.device_properties.mooring_design_criteria.length_increment=1.0 # Winch 1.0m at a time (+or-)

    # Minimum horizontal egenperiod target [s]
    input_cplx.device_properties.mooring_design_criteria.Tmin = 30
    
    # Constrains for total offset
    input_cplx.device_properties.mooring_design_criteria.offset_max = 30

    # Proof load for anchor of catenary line installation
    input_cplx.device_properties.mooring_design_criteria.proof_load_coeff = 0.8
    input_cplx.device_properties.mooring_design_criteria.safety_factor = 1.7

    # Inputs for the masterstructure
    #input_cplx.master_structure_properties=MasterStructureProperties.MasterStructureProperties()

    # Inputs for motion analysis
    input_cplx.uls_analysis_parameters.weather_direction = np.array([30])

    # Parameters specific to foundation design
    input_cplx.device_properties.foundation_design_parameters.foundation_preference = 'evaluation'
    input_cplx.device_properties.foundation_design_parameters.dist_attachment_pa = 0
    input_cplx.device_properties.foundation_design_parameters.pile_tip = 'close_end'
    input_cplx.device_properties.foundation_design_parameters.deflection_criteria_for_pile = 0
    input_cplx.device_properties.foundation_design_parameters.SF = 1.8

    # Read from SC
    input_cplx.water_density=1025.0
    input_cplx.uls_analysis_parameters.hs_array=np.array([12, 13, 14])
    input_cplx.uls_analysis_parameters.tp_array=np.array([14, 15, 16])
    input_cplx.uls_analysis_parameters.current_vel=2.0
    input_cplx.uls_analysis_parameters.wind_vel=30.0
    input_cplx.water_depth=np.array([100,100,100,100])
    input_cplx.device_properties.foundation_design_parameters.soil_type = 'very_dense_sand'
    input_cplx.device_properties.foundation_design_parameters.soil_slope = 1

    # Read from EC
    input_cplx.east=np.array([0, 800, 0, 800])
    input_cplx.north=np.array([0, 0, 800, 800])
    input_cplx.yaw=np.array([0, 0, 0, 0])
    input_cplx.deviceId=np.array([0,1,2,3])

    return input_cplx

def define_test_inputs_3_fixed():

    # Get path to nemoh file
    this_dir = os.path.dirname(os.path.realpath(__file__))
    data_dir = os.path.join(this_dir, "..", "sample_data")
    result_path = os.path.join(data_dir,'TEST-AXI')
    mooring_input_path = os.path.join(data_dir,'mooring_input_example.json')

    # Create input structure for cplx
    input_cplx = Cplx()

    # Project name
    input_cplx.name='project_test2'

    # Device properties
    input_cplx.device_properties=DeviceProperties()

    # User inputs for steady force model
    input_cplx.device_properties.steady_force_model=SteadyForceModel()
    input_cplx.device_properties.steady_force_model.method='main_dimensions'
    input_cplx.device_properties.steady_force_model.device_dry_profile='cylinder'
    input_cplx.device_properties.steady_force_model.device_wet_profile='cylinder'
    input_cplx.device_properties.steady_force_model.device_dry_width=10.0
    input_cplx.device_properties.steady_force_model.device_wet_width=10.0
    input_cplx.device_properties.steady_force_model.device_dry_height=4
    input_cplx.device_properties.steady_force_model.device_wet_height=4

    
    # User inputs if nemoh run is used (standalone)
    input_cplx.device_properties.hdb_source='nemoh_run'
    input_cplx.device_properties.path_to_nemoh_result_folder=result_path
    input_cplx.device_properties.critical_damping_coefficient = np.array([0.1,0.1,0.1,0.1,0.1,0.1],dtype=float)
    
    # User inputs if mc_module is used
    #input_cplx.device_properties.hdb_source='mc_module'
    #input_cplx.device_properties.mass_matrix=np.zeros(shape=(6,6), dtype=float)
    input_cplx.device_properties.mass = 100000
    #input_cplx.device_properties.hydrostatic_matrix=np.zeros(shape=(6,6), dtype=float)
    input_cplx.device_properties.cog=np.zeros(shape=(3), dtype=float)
    input_cplx.device_properties.machine_type='tec'

    # Input for the tec devices
    this_dir = os.path.dirname(os.path.realpath(__file__))
    data_dir = os.path.join(this_dir, "..", "sample_data")
    input_cplx.device_properties.thrust_coeff_curve_file=os.path.join(data_dir,'thrustcurv.txt')
    input_cplx.device_properties.rotor_diameter=6.0
    input_cplx.device_properties.hub_position=np.zeros(shape=(3,1))
    input_cplx.device_properties.hub_position[:,0] = [0.0,0.0,10.0]

    # User input for mooring design
    input_cplx.device_properties.positioning_type  ='fixed'
    input_cplx.device_properties.positioning_reference = 'seabed'
    # input_cplx.device_properties.mooring_input_flag = 'custom'
    # input_cplx.device_properties.mooring_design_criteria.mooring_type = 'catenary' # Important even if 'custom' is used because this drives the foundation choice as well (we will use the proofload to design the anchor)
    # input_cplx.device_properties.mooring_design_criteria.nlines = 3
    # input_cplx.device_properties.mooring_design_criteria.nlines_max = 10

    ########################################################################################
    ########################################################################################
    # Inputs required if mooring_input_flag = 'custom'
    ########################################################################################
    ########################################################################################
    # input_cplx.device_properties.custom_mooring_input.name='mooring_input_example'
    # input_cplx.device_properties.custom_mooring_input.loadJSON(filePath = mooring_input_path)

    # Inputs required if mooring_input_flag = 'design'
    # # Constrains for pretension
    # input_cplx.device_properties.mooring_design_criteria.pretension_coeff_max=0.03 # 6% of MBL
    # input_cplx.device_properties.mooring_design_criteria.pretension_coeff_min=0.02 # 3% of MBL
    # input_cplx.device_properties.mooring_design_criteria.length_increment=1.0 # Winch 1.0m at a time (+or-)

    # # Minimum horizontal egenperiod target [s]
    # input_cplx.device_properties.mooring_design_criteria.Tmin = 30
    
    # # Constrains for total offset
    # input_cplx.device_properties.mooring_design_criteria.offset_max = 30

    # Proof load for anchor of catenary line installation
    # input_cplx.device_properties.mooring_design_criteria.proof_load_coeff = 0.8
    # input_cplx.device_properties.mooring_design_criteria.safety_factor = 1.7

    # Inputs for the masterstructure
    #input_cplx.master_structure_properties=MasterStructureProperties.MasterStructureProperties()

    # Inputs for motion analysis
    input_cplx.uls_analysis_parameters.weather_direction = np.array([30])

    # Parameters specific to foundation design
    input_cplx.device_properties.foundation_design_parameters.foundation_preference = 'evaluation'
    input_cplx.device_properties.foundation_design_parameters.dist_attachment_pa = 0
    input_cplx.device_properties.foundation_design_parameters.pile_tip = 'close_end'
    input_cplx.device_properties.foundation_design_parameters.deflection_criteria_for_pile = 0

    # Read from SC
    input_cplx.water_density=1025.0
    input_cplx.uls_analysis_parameters.hs_array=np.array([12, 13, 14])
    input_cplx.uls_analysis_parameters.tp_array=np.array([14, 15, 16])
    input_cplx.uls_analysis_parameters.current_vel=2.0
    input_cplx.uls_analysis_parameters.wind_vel=30.0
    input_cplx.water_depth=np.array([100,100,100,100])
    input_cplx.device_properties.foundation_design_parameters.soil_type = 'very_dense_sand'
    input_cplx.device_properties.foundation_design_parameters.soil_slope = 0.0
    input_cplx.device_properties.foundation_design_parameters.SF = 1.8

    # Read from EC
    input_cplx.east=np.array([0, 800, 0, 800])
    input_cplx.north=np.array([0, 0, 800, 800])
    input_cplx.yaw=np.array([0, 0, 0, 0])
    input_cplx.deviceId=np.array([0,1,2,3])

    return input_cplx

def define_test_inputs_3():

    # Get path to nemoh file
    this_dir = os.path.dirname(os.path.realpath(__file__))
    data_dir = os.path.join(this_dir, "..", "sample_data")
    result_path = os.path.join(data_dir,'TEST-AXI')
    mooring_input_path = os.path.join(data_dir,'mooring_input_example.json')

    # Create input structure for cplx
    input_cplx = Cplx()

    # Project name
    input_cplx.name='project_test2'

    # Device properties
    input_cplx.device_properties=DeviceProperties()

    # User inputs for steady force model
    input_cplx.device_properties.steady_force_model=SteadyForceModel()
    input_cplx.device_properties.steady_force_model.method='main_dimensions'
    input_cplx.device_properties.steady_force_model.device_dry_profile='cylinder'
    input_cplx.device_properties.steady_force_model.device_wet_profile='cylinder'
    input_cplx.device_properties.steady_force_model.device_dry_width=10.0
    input_cplx.device_properties.steady_force_model.device_wet_width=10.0
    input_cplx.device_properties.steady_force_model.device_dry_height=4
    input_cplx.device_properties.steady_force_model.device_wet_height=4

    
    # User inputs if nemoh run is used (standalone)
    input_cplx.device_properties.hdb_source='nemoh_run'
    input_cplx.device_properties.path_to_nemoh_result_folder=result_path
    input_cplx.device_properties.critical_damping_coefficient = np.array([0.1,0.1,0.1,0.1,0.1,0.1],dtype=float)
    
    # User inputs if mc_module is used
    #input_cplx.device_properties.hdb_source='mc_module'
    #input_cplx.device_properties.mass_matrix=np.zeros(shape=(6,6), dtype=float)
    #input_cplx.device_properties.hydrostatic_matrix=np.zeros(shape=(6,6), dtype=float)
    #input_cplx.device_properties.cog=np.zeros(shape=(3), dtype=float)
    input_cplx.device_properties.machine_type='wec'

    # Input for the tec devices
    #input_cplx.device_properties.thrust_coeff_curve_file=''
    #input_cplx.device_properties.rotor_diameter=0.0
    #input_cplx.device_properties.hub_position=np.zeros(shape=(3), dtype=float)
    
    # User input for mooring design
    input_cplx.device_properties.positioning_type  ='moored'
    input_cplx.device_properties.positioning_reference = 'seabed'
    input_cplx.device_properties.mooring_input_flag = 'custom'
    input_cplx.device_properties.file_mooring_input = mooring_input_path
    input_cplx.device_properties.mooring_design_criteria.mooring_type = 'catenary' # Important even if 'custom' is used because this drives the foundation choice as well (we will use the proofload to design the anchor)
    input_cplx.device_properties.mooring_design_criteria.nlines = 3
    input_cplx.device_properties.mooring_design_criteria.nlines_max = 10

    ########################################################################################
    ########################################################################################
    # Inputs required if mooring_input_flag = 'custom'
    ########################################################################################
    ########################################################################################
    input_cplx.device_properties.custom_mooring_input.append(CustomMooringInput())
    input_cplx.device_properties.custom_mooring_input[0].name='mooring_input_example'
    input_cplx.device_properties.custom_mooring_input[0].loadJSON(filePath = mooring_input_path)
    input_cplx.device_properties.custom_mooring_input[0].reference_system = 'device'



    # Inputs required if mooring_input_flag = 'design'
    # # Constrains for pretension
    # input_cplx.device_properties.mooring_design_criteria.pretension_coeff_max=0.03 # 6% of MBL
    # input_cplx.device_properties.mooring_design_criteria.pretension_coeff_min=0.02 # 3% of MBL
    # input_cplx.device_properties.mooring_design_criteria.length_increment=1.0 # Winch 1.0m at a time (+or-)

    # # Minimum horizontal egenperiod target [s]
    # input_cplx.device_properties.mooring_design_criteria.Tmin = 30
    
    # # Constrains for total offset
    # input_cplx.device_properties.mooring_design_criteria.offset_max = 30

    # Proof load for anchor of catenary line installation
    input_cplx.device_properties.mooring_design_criteria.proof_load_coeff = 0.8
    input_cplx.device_properties.mooring_design_criteria.safety_factor = 1.7

    # Inputs for the masterstructure
    #input_cplx.master_structure_properties=MasterStructureProperties.MasterStructureProperties()

    # Inputs for motion analysis
    input_cplx.uls_analysis_parameters.weather_direction = np.array([30])

    # Parameters specific to foundation design
    input_cplx.device_properties.foundation_design_parameters.foundation_preference = 'evaluation'
    input_cplx.device_properties.foundation_design_parameters.dist_attachment_pa = 0
    input_cplx.device_properties.foundation_design_parameters.pile_tip = 'close_end'
    input_cplx.device_properties.foundation_design_parameters.deflection_criteria_for_pile = 0
    input_cplx.device_properties.foundation_design_parameters.SF = 1.8

    # Read from SC
    input_cplx.water_density=1025.0
    input_cplx.uls_analysis_parameters.hs_array=np.array([12,13,14])
    input_cplx.uls_analysis_parameters.tp_array=np.array([14,15,16])
    input_cplx.uls_analysis_parameters.current_vel=2.0
    input_cplx.uls_analysis_parameters.wind_vel=30.0
    input_cplx.water_depth=np.array([100,100,100,100])
    input_cplx.device_properties.foundation_design_parameters.soil_type = 'very_dense_sand'
    input_cplx.device_properties.foundation_design_parameters.soil_slope = 1

    # Read from EC
    input_cplx.east=np.array([0,800,0, 800])
    input_cplx.north=np.array([0,0, 800, 800])
    input_cplx.yaw=np.array([0,0, 0, 0])
    input_cplx.deviceId=np.array([0,1,2,3])

    return input_cplx


# def test_cplx_main_cplx12():

#     # Load inputs (cplx1 or 2)
#     input_cplx = define_test_inputs_12()

#     # Import catalogue
#     catalogue_chains = Lines()

#     # Run main
#     [farm, hierarchy, bom, env_impact, dum, dum1, dum2, dum3] = run_analysis_cplx(input_cplx,catalogue_chains)

#     # print(farm.body_array[0].results.uls_results.mooring_stiffness[0,0])
#     # print(farm.body_array[0].results.uls_results.eigenperiod[0])
#     # print(hierarchy.hierarchy_data.anchor_list[2].mass)
#     # print(env_impact.farm.material_quantity_list[0].material_quantity)
#     # print(farm.body_array[2].results.design_results.anchor_and_foundation.foundation_type)
#     # print(farm.body_array[0].mooring_system_force.line_dictionnary[0].diameter)

#     assert (farm.body_array[0].results.uls_results.mooring_stiffness[0,0] == pytest.approx(6495.911497742054,0.0001) and farm.body_array[0].results.uls_results.eigenperiod[0] == pytest.approx(41.6566200865981,0.0001) and hierarchy.hierarchy_data.anchor_list[2].mass == pytest.approx(44693.501433534,0.0001) and env_impact.farm.material_quantity_list[0].material_quantity == pytest.approx(1304995.3496590073, 0.1) and farm.body_array[2].results.design_results.anchor_and_foundation.foundation_type == 'drag_anchor' and farm.body_array[0].mooring_system_force.line_dictionnary[0].diameter == pytest.approx(0.142, 0.0001))

def test_cplx_main_cplx3_single():

    # Load inputs (cplx3)
    input_cplx = define_test_inputs_3()

    # Import catalogue
    catalogue_chains = Lines()
    catalogue_anchors = AnchorCatalogue()

    # Run main
    [farm, hierarchy, bom, env_impact, dum, dum1, dum2, dum3] = run_analysis_cplx(input_cplx,catalogue_chains,catalogue_anchors)

    assert (hierarchy.hierarchy_data.anchor_list[2].mass == pytest.approx(28192.731260809403,0.0001) and env_impact.farm.material_quantity_list[0].material_quantity == pytest.approx(1284257.496377672, 0.1) and farm.body_array[2].results.design_results.anchor_and_foundation.foundation_type == 'drag_anchor' and farm.body_array[0].mooring_system_force.line_dictionnary[0].diameter == pytest.approx(0.111, 0.0001))

def test_cplx_main_cplx3_multi():
    # Get path to nemoh file
    this_dir = os.path.dirname(os.path.realpath(__file__))
    data_dir = os.path.join(this_dir, "..", "sample_data")
    result_path = os.path.join(data_dir,'TEST-AXI')
    mooring_input_path = os.path.join(data_dir,'mooring_input_example.json')

    # Load inputs (cplx3)
    input_cplx = define_test_inputs_3()
    input_cplx.device_properties.custom_mooring_input.append(CustomMooringInput())
    input_cplx.device_properties.custom_mooring_input[1].name='mooring_input_example'
    input_cplx.device_properties.custom_mooring_input[1].loadJSON(filePath = mooring_input_path)
    input_cplx.device_properties.custom_mooring_input[1].reference_system = 'device'
    input_cplx.device_properties.custom_mooring_input.append(CustomMooringInput())
    input_cplx.device_properties.custom_mooring_input[2].name='mooring_input_example'
    input_cplx.device_properties.custom_mooring_input[2].loadJSON(filePath = mooring_input_path)
    input_cplx.device_properties.custom_mooring_input[2].reference_system = 'device'
    input_cplx.device_properties.custom_mooring_input.append(CustomMooringInput())
    input_cplx.device_properties.custom_mooring_input[3].name='mooring_input_example'
    input_cplx.device_properties.custom_mooring_input[3].loadJSON(filePath = mooring_input_path)
    input_cplx.device_properties.custom_mooring_input[3].reference_system = 'device'
    input_cplx.uls_analysis_parameters.hs_array=np.array([8])
    input_cplx.uls_analysis_parameters.tp_array=np.array([10])
    input_cplx.uls_analysis_parameters.current_vel=0.35
    input_cplx.uls_analysis_parameters.wind_vel=10

    # Import catalogue
    catalogue_chains = Lines()
    catalogue_anchors = AnchorCatalogue()

    # Run main
    [farm, hierarchy, bom, env_impact, dum, dum1, dum2, dum3] = run_analysis_cplx(input_cplx,catalogue_chains,catalogue_anchors)

    assert (hierarchy.hierarchy_data.anchor_list[2].mass == pytest.approx(28192.731260809403,0.0001) and env_impact.farm.material_quantity_list[0].material_quantity == pytest.approx(1399963.74994, 0.1) and farm.body_array[2].results.design_results.anchor_and_foundation.foundation_type == 'drag_anchor' and farm.body_array[0].mooring_system_force.line_dictionnary[0].diameter == pytest.approx(0.111, 0.0001))




def test_cplx_main_fixed():

    # Load inputs (cplx3)
    input_cplx = define_test_inputs_3_fixed()

    # Import catalogue
    catalogue_chains = Lines()
    catalogue_anchors = AnchorCatalogue()

    # Run main
    [farm, hierarchy, bom, env_impact, dum, dum1, dum2, dum3] = run_analysis_cplx(input_cplx,catalogue_chains,catalogue_anchors)

    assert farm.body_array[0].results.design_results.anchor_and_foundation.gravity_foundation_design.base_radius == pytest.approx(10.0,0.1) and \
            farm.body_array[0].results.design_results.anchor_and_foundation.gravity_foundation_design.thickness == pytest.approx(0.3,0.0001) and \
            bom.unit_cost[0] == pytest.approx(22619.467,0.0001) and \
            env_impact.farm.material_quantity_list[0].material_quantity == pytest.approx(904778.68423,0.0001) and \
            farm.body_array[2].results.design_results.anchor_and_foundation.foundation_type == 'shallow' and \
            hierarchy.hierarchy_data.foundation_list[2].mass == pytest.approx(226194.6710584651,0.0001)

def test_stdv():
    import matplotlib.pyplot as plt

    t = np.linspace(0,100, num=1000)
    T = 100
    om = 2*np.pi/T
    amp = 5.5
    S1 = amp*np.sin(om*t)

    t = np.linspace(0,100, num=1000)
    T = 100
    om = 2*np.pi/T
    amp = 2.2
    S2 = amp*np.sin(om*t+np.pi/2)
    
    #print(amp/(np.sqrt(2)))
    S1_std = np.std(S1)
    S2_std = np.std(S2)

    c1 = 2.6
    c2 = 3.3
    S = c1*S1 + c2*S2
    S_std = np.std(S)

    print(S_std)

    SS_std = c1*S1_std+ c2*S2_std
    print(SS_std)


    #plt.plot(t,S)
    #plt.show()

    assert True

def test_fitting_closest():
    
    from dtop_stationkeep.business.libraries.dtosk.frequency_analysis_module import find_closest_neighbours_fitting_matrix

    mat_in = np.zeros(shape=(3,4,5,2,2))
    tp_array = np.linspace(1,3,3)
    hs_array = np.linspace(5,8,4)
    dir_array = np.linspace(40,80,5)*np.pi/180
    ndof = 2
    tp0= 2
    hs0= 7   
    dir0 = 70*np.pi/180

    mat_in[1,2,3,:,:] = 2.1

    mat_out = find_closest_neighbours_fitting_matrix(mat_in,tp_array,hs_array,dir_array,tp0,hs0,dir0,ndof)

    assert mat_out[0,0] == pytest.approx(2.100,0.0001)


def test_reddof_to_dof():

    from dtop_stationkeep.business.libraries.dtosk.frequency_analysis_module import from_reduced_dof_to_dof

    K_gendof = np.zeros(shape=(3,3),dtype=float)

    K_gendof[0,:]=np.array([1,2,3])
    K_gendof[1,:]=np.array([4,5,6])
    K_gendof[2,:]=np.array([7,8,9])

    K_66 = np.zeros(shape=(6,6),dtype=float)
    shared_dof = np.array([1,1,0,0,0,1])
    K_66 = from_reduced_dof_to_dof(K_gendof,shared_dof)

    assert K_66[0,5] == pytest.approx(3.0,0.0001)

def test_dof_to_reddof():

    from dtop_stationkeep.business.libraries.dtosk.frequency_analysis_module import from_dof_to_reduced_dof

    K_66 = np.zeros(shape=(6,6),dtype=float)

    K_66[0,:]=np.array([1,2,0,0,0,3])
    K_66[1,:]=np.array([4,5,0,0,0,6])
    K_66[5,:]=np.array([7,8,0,0,0,9])

    K_gendof = np.zeros(shape=(3,3),dtype=float)
    shared_dof = np.array([1,1,0,0,0,1])

    K_gendof = from_dof_to_reduced_dof(K_66,shared_dof)

    l = K_gendof.shape

    assert ((K_gendof[1,1] == pytest.approx(5.0,0.0001)) and (l[0] == 3))


    