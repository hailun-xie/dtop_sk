# This is the Station Keeping module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Neil Luxcey, Nicolas Michelet, Rocio Isorna
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


import numpy as np
import pytest
import sys

def test_current():

    from dtop_stationkeep.business.libraries.dtosk.environment.Current import Current

    # Create current object    
    current=Current()
    water_depth=30.0
    current.velocity=2.5
    current.direction=0.0
    current.profile_type='uniform'

    # Compute velocity
    velocity1=current.get_current_velocity(-12.0,water_depth)

    # Change profile type to 1/7 power law
    current.profile_type='1/7 power law'
    velocity2=current.get_current_velocity(-12.0,water_depth)

    assert ((velocity1 == pytest.approx(2.5, 0.001)) and (velocity2 == pytest.approx(2.32405, 0.001)))

def test_wave():

    from dtop_stationkeep.business.libraries.dtosk.environment.Wave import Wave

    # Create wave
    wave=Wave()
    wave.hs=10.0
    wave.tp=11.0
    water_depth = 100.0

    # Max wave height
    hmax = wave.hmax(water_depth)

    assert ((hmax == pytest.approx(17.69256, 0.001)))

