# This is the Station Keeping module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Neil Luxcey, Nicolas Michelet, Rocio Isorna
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

# This package uses business logic functions
# It provides business logic via HTTP API.
# It create a blueprint and define routes.

from flask import Blueprint, render_template, url_for, jsonify, request

bp = Blueprint('api_test_rocio', __name__)

#import os
#from pathlib import Path, PurePosixPath

#working= os.getcwd()
#data_path = Path('src/dtop_site/Databases/')
#PATH_DATABASES = os.path.join(working, data_path)
#PATH_GEOMETRIES = os.path.join(PATH_DATABASES, Path('Shapefiles/EXAMPLES/'))

dico_foundations = {'0':'To be defined',
            '1':'Shallow foundation',
            '2':'Pile foundation/anchor',
            '3':'Deadweight anchor',
            '4':'Drag anchor',
            '5':'Direct embedded anchor',
            '6':'Suction caisson'
            }

dico_soils = {'sc':'Soft clay',
            'stc':'Stiff clay',
            'ls':'Loose sand',
            'ms':'Medium dense sand',
            'hs':'High dense sand',
            'sr':'Soft rock',
            'hr':'Hard rock'
            }

class MyEarly:
    """ Return information for early stage """

    def __init__(self):
        """Initializes the MyEarly instance."""

#        self.project_name = request.args['site_name']
#        self.site_description = request.args['site_description']

    @bp.route('/test_1', methods=['GET'])
    def test_1():
        print("################# I am in test_1 ###############")
        return "Test 1 OK"

    @bp.route('/early', methods=['GET'])
    def return_all():
        """
          @oas [get] /api/early
          description: test API SK
        """
        project_name = request.args['site_name']
        my_text = "Welcome to the project " + project_name

        return my_text
    
    @bp.route('/found1', methods=['POST'], strict_slashes=False)
    def found1():
        """
          @oas [get] /api/foundation1
          description: test API SK with list of inputs
        """
        # project info
        print("heheheheheheheeh")
        project_name = request.args['name']
        project_description = request.args['description']
        # foundation
        foundation_type_value = request.args['found']
        foundation_type = dico_foundations[foundation_type_value]
        # foundation
        soil_type_value = request.args['soil']
        soil_type = dico_soils[soil_type_value]
        
        result = {}
        result['project_name'] = project_name
        result['project_description'] = project_description
        result['foundation_type'] = foundation_type
        result['soil_type'] = soil_type
        
        return jsonify(result)