# This is the Station Keeping module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Neil Luxcey, Nicolas Michelet, Rocio Isorna
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

from flask import Blueprint, render_template, url_for, jsonify, request, current_app, send_file, abort
from os import listdir
import os
import os.path
import sys
import collections
from dtop_stationkeep.business.libraries.dtosk.inputs.Inputs import Inputs
#------------------------------------
# @ USER DEFINED IMPORTS START
from dtop_stationkeep.business.libraries.dtosk.run_main_module import run_main
from dtop_stationkeep.business.libraries.dtosk.inputs.InputStatus import InputStatus
from dtop_stationkeep.business.libraries.dtosk.outputs.Project import Project
from dtop_stationkeep.business.libraries.dtosk.inputs.CustomMooringInput import CustomMooringInput
from dtop_stationkeep.business.libraries.dtosk.inputs.SubstationProperties import SubstationProperties
import time
import base64
import shutil
import numpy as np
import json
# @ USER DEFINED IMPORTS END
#------------------------------------


bp = Blueprint('postInputs', __name__)

#------------------------------------
# @ USER DEFINED FUNCTIONS START

def CopyMooringCheckFiles(ProjectId):

    from shutil import copyfile
    
    # Save the project input
    storagePath = './storage'

    if os.path.exists('./storage/Tmp/Mooring_system/mooring_check_results.json'):
        copyfile('./storage/Tmp/Mooring_system/mooring_check_results.json', './storage/'+ProjectId+'/Mooring_system/mooring_check_results.json')

    if os.path.exists('./storage/Tmp/Mooring_system/mooring_check_plot.html'):
        copyfile('./storage/Tmp/Mooring_system/mooring_check_plot.html', './storage/'+ProjectId+'/figures/mooring_check_plot.html')

def CreateProjectFolderHierarchy(ProjectId):

    # Save the project input
    storagePath = './storage'

    if not os.path.exists('./storage/'+ ProjectId):
        os.mkdir('./storage/'+ ProjectId)
    if not os.path.exists('./storage/'+ ProjectId +'/figures'):
        os.mkdir('./storage/'+ ProjectId +'/figures')
    if not os.path.exists('./storage/'+ ProjectId +'/Nemoh'):
        os.mkdir('./storage/'+ ProjectId +'/Nemoh')
    if not os.path.exists('./storage/'+ ProjectId +'/Nemoh/results'):
        os.mkdir('./storage/'+ ProjectId +'/Nemoh/results')
    if not os.path.exists('./storage/'+ ProjectId +'/Nemoh/mesh'):
        os.mkdir('./storage/'+ ProjectId +'/Nemoh/mesh')
    if not os.path.exists('./storage/'+ ProjectId +'/Mooring_system'):
        os.mkdir('./storage/'+ ProjectId +'/Mooring_system')

def Write_My_File(my_path,fileContent):

        file = open(my_path, "w")
        file.write(fileContent)
        file.close()

        file2 = open(my_path,"r")
        my_filecontent = file2.read()
        my_filecontent = my_filecontent.replace('\n\n','\n')
        file2.close()
        
        file3 = open(my_path,"w")
        file3.write(my_filecontent)
        file3.close()

def read_files_in_folder(my_folder):

    variable1 = []
    variable2 = []
    src_files = os.listdir(my_folder)

    for file_name in src_files:
        full_file_name = os.path.join(my_folder,file_name)
        if os.path.isfile(full_file_name):
            variable1.append(file_name)
            temp = open(full_file_name)
            variable2.append(temp.read())
    
    if (variable1 == []):
        variable1 = False
        variable2 = False

    return (variable1, variable2)

# API that get entity name
@bp.route('/<eid>/GetEntityName', methods=['GET'], strict_slashes=False)
def get_entity_name(eid):
    
    testlist = []
    Name = 'Error'

    # Get the ID
    with open('./storage/Entity_table.json') as json_file:
        List = json.load(json_file)
        for entity in List['List_of_entity']:
            if(entity['EntityId'] == int(eid)):
                Name = entity['EntityName']

    if (Name == 'Error'):
        return "Error", 500
    else:
        return jsonify(Name),200

# API that save the status structure
@bp.route('/<ProjectId>/saveStatus', methods=['POST'], strict_slashes=False)
def save_inputstatus(ProjectId):

    # Create status structure from request
    my_inputstatus = InputStatus()

    # Load status from request
    my_inputstatus_rep = request.get_json()
    my_inputstatus.loadFromJSONDict(my_inputstatus_rep)
    my_inputstatus.name = ProjectId

    if (my_inputstatus.entity_id == ''):
        cpt = 1
        testlist = []

        # Get the ID
        with open('./storage/Entity_table.json') as json_file:
            List = json.load(json_file)
            for entity in List['List_of_entity']:
                testlist.append(entity['EntityId'])

        while cpt in testlist:
            cpt = cpt+1

        List['List_of_entity'].append({"EntityName": ProjectId, "EntityId": cpt})
        my_inputstatus.entity_id = cpt

        ListtoWrite = json.dumps(List)
        f=open('./storage/Entity_table.json', "w")
        f.write(ListtoWrite)
        f.close()

        foldername = ProjectId + '_' + str(cpt)
        my_inputstatus.entity_id = cpt
    else:

        # Get the ID
        with open('./storage/Entity_table.json') as json_file:
            List = json.load(json_file)
            for entity in List['List_of_entity']:
                if (int(my_inputstatus.entity_id) == entity['EntityId'] and my_inputstatus.name == entity['EntityName']):
                    foldername = entity['EntityName'] + '_' + str(entity['EntityId'])
        my_inputstatus.entity_id = entity['EntityId']


    # Create the hierarchy in the storage directory
    CreateProjectFolderHierarchy(foldername)
    
    # Save the input status structure
    storagePath = './storage'
    fileName=os.path.join(storagePath,foldername + '/status.json')
    my_inputstatus.saveJSON(fileName)

    # Copy file from mooring check results (if any) into the project folder
    CopyMooringCheckFiles(foldername)

    return jsonify(my_inputstatus.prop_rep())

# API that save the status structure
@bp.route('/PurgeTmpFolder', methods=['POST'], strict_slashes=False)
def Purge_folder():

    if os.path.exists('./storage/Tmp/Nemoh/'):
        src_files = os.listdir('./storage/Tmp/Nemoh/')
        for file_name in src_files:
            full_file_name = os.path.join('./storage/Tmp/Nemoh/', file_name)
            if os.path.isfile(full_file_name):
                os.remove(full_file_name)

        if os.path.exists('./storage/Tmp/Nemoh/results'):
            src_files = os.listdir('./storage/Tmp/Nemoh/results')
            for file_name in src_files:
                full_file_name = os.path.join('./storage/Tmp/Nemoh/results', file_name)
                if os.path.isfile(full_file_name):
                    os.remove(full_file_name)
        else:
            os.mkdir('./storage/Tmp/Nemoh/results')

                
        if os.path.exists('./storage/Tmp/Nemoh/mesh'):
            src_files = os.listdir('./storage/Tmp/Nemoh/mesh')
            for file_name in src_files:
                full_file_name = os.path.join('./storage/Tmp/Nemoh/mesh', file_name)
                if os.path.isfile(full_file_name):
                    os.remove(full_file_name)
        else:
            os.mkdir('./storage/Tmp/Nemoh/mesh')
    else:
        os.mkdir('./storage/Tmp')
        os.mkdir('./storage/Tmp/Nemoh')
        os.mkdir('./storage/Tmp/Nemoh/results')
        os.mkdir('./storage/Tmp/Nemoh/mesh')

    if os.path.exists('./storage/Tmp/Mooring_system/'):
        src_files = os.listdir('./storage/Tmp/Mooring_system/')
        for file_name in src_files:
            full_file_name = os.path.join('./storage/Tmp/Mooring_system/', file_name)
            if os.path.isfile(full_file_name):
                os.remove(full_file_name)
    else:
        os.mkdir('./storage/Tmp/Mooring_system/')
    
    return "ok"

# API that save the status structure
@bp.route('/PurgeNemohTmpFolder', methods=['POST'], strict_slashes=False)
def Purge_Nemoh_folder():

    if os.path.exists('./storage/Tmp/Nemoh/'):
        src_files = os.listdir('./storage/Tmp/Nemoh/')
        for file_name in src_files:
            full_file_name = os.path.join('./storage/Tmp/Nemoh/', file_name)
            if os.path.isfile(full_file_name):
                os.remove(full_file_name)

        if os.path.exists('./storage/Tmp/Nemoh/results'):
            src_files = os.listdir('./storage/Tmp/Nemoh/results')
            for file_name in src_files:
                full_file_name = os.path.join('./storage/Tmp/Nemoh/results', file_name)
                if os.path.isfile(full_file_name):
                    os.remove(full_file_name)
        else:
            os.mkdir('./storage/Tmp/Nemoh/results')

                
        if os.path.exists('./storage/Tmp/Nemoh/mesh'):
            src_files = os.listdir('./storage/Tmp/Nemoh/mesh')
            for file_name in src_files:
                full_file_name = os.path.join('./storage/Tmp/Nemoh/mesh', file_name)
                if os.path.isfile(full_file_name):
                    os.remove(full_file_name)
        else:
            os.mkdir('./storage/Tmp/Nemoh/mesh')
    else:
        os.mkdir('./storage/Tmp')
        os.mkdir('./storage/Tmp/Nemoh')
        os.mkdir('./storage/Tmp/Nemoh/results')
        os.mkdir('./storage/Tmp/Nemoh/mesh')
    
    return "ok"

# API that save the status structure
@bp.route('/GetNemohImage', methods=['GET'], strict_slashes=False)
def load_2d_image():

    FolderName = request.args['folder']

    if ((FolderName == 'false')or(FolderName == 'Import')):
        return 'false'
    else:
        if FolderName == 'Buoy_D10':
            path_to_image = os.path.join('./src/dtop_stationkeep/sample_data/TEST-AXI/mesh.png')
        elif FolderName == 'RM3_6dofs':
            path_to_image = os.path.join('./src/dtop_stationkeep/sample_data/RM3_six_dofs/mesh.png')
        elif FolderName == 'Cylinder_horizontal':
            path_to_image = os.path.join('./src/dtop_stationkeep/sample_data/Cylinder_horizontal/mesh.png')
        elif FolderName == 'Cylinder_vertical':
            path_to_image = os.path.join('./src/dtop_stationkeep/sample_data/Cylinder_vertical/mesh.png')
        data_uri = base64.b64encode(open(path_to_image, 'rb').read()).decode('utf-8')
        img_tag = data_uri
        return img_tag

# API that save the status structure
@bp.route('/SaveNemohFiles', methods=['POST'], strict_slashes=False)
def Save_nemoh_files():

    my_inputs_rep = request.get_json()
    fileContent = my_inputs_rep["file"]

    displacement = 0
    water_plane_area = 0

    if (my_inputs_rep["name"] == 'Nemoh.cal'):
        my_path = "./storage/Tmp/Nemoh/nemoh.cal"
        Write_My_File(my_path, fileContent)
    elif (my_inputs_rep["name"] == 'ExcitationForce.tec'):
        my_path = "./storage/Tmp/Nemoh/results/ExcitationForce.tec"
        Write_My_File(my_path, fileContent)
    elif (my_inputs_rep["name"] == 'RadiationCoefficients.tec'):
        my_path = "./storage/Tmp/Nemoh/results/RadiationCoefficients.tec"
        Write_My_File(my_path, fileContent)
    elif (my_inputs_rep["name"] == 'KH.dat'):
        my_path = "./storage/Tmp/Nemoh/mesh/KH.dat"
        Write_My_File(my_path, fileContent)
    elif (my_inputs_rep["name"] == 'Hydrostatics.dat'):
        my_path = "./storage/Tmp/Nemoh/mesh/Hydrostatics.dat"
        splitted_file = fileContent.split()
        for i in range(len(splitted_file)):
            if ((splitted_file[i] == 'Displacement')or(splitted_file[i] == 'displacement')):
                displacement = float(splitted_file[i+2])
            if ((splitted_file[i] == 'Waterplane')or(splitted_file[i] == 'waterplane')):
                water_plane_area = float(splitted_file[i+3])
        Write_My_File(my_path, fileContent)
    elif (my_inputs_rep["name"] == 'Inertia_hull.dat'):
        my_path = "./storage/Tmp/Nemoh/mesh/Inertia_hull.dat"
        Write_My_File(my_path, fileContent)
    elif (my_inputs_rep["name"] == 'mesh.tec'):
        my_path = "./storage/Tmp/Nemoh/mesh/mesh.tec"
        Write_My_File(my_path, fileContent)
    elif (my_inputs_rep["name"] == 'mesh_info.dat'):
        my_path = "./storage/Tmp/Nemoh/mesh/mesh_info.dat"
        Write_My_File(my_path, fileContent)
    elif (my_inputs_rep["name"] == 'mesh.png'):
        file = open("./storage/Tmp/Nemoh/mesh.png", "w")
        file.write(fileContent)
        file.close()
    elif (my_inputs_rep["name"] == 'mesh.dat'):
        my_path = "./storage/Tmp/Nemoh/mesh/mesh.dat"
        Write_My_File(my_path, fileContent)
    elif (my_inputs_rep["name"] == 'Description_Full.tec'):
        my_path = "./storage/Tmp/Nemoh/mesh/Description_Full.tec"
        Write_My_File(my_path, fileContent)
    elif (my_inputs_rep["name"] == 'input.txt'):
        my_path = "./storage/Tmp/Nemoh/input.txt"
        Write_My_File(my_path, fileContent)

    return jsonify(displacement, water_plane_area)

# API that save the status structure
@bp.route('/loadimage', methods=['POST'], strict_slashes=False)
def Load_nemoh_image():

    name = request.args['name']

    if (name == 'none'):
        if os.path.exists('./storage/Tmp/Nemoh/mesh.png'):
            file = open("./storage/Tmp/Nemoh/mesh.png", "r")
            test = file.read()
            file.close()
            return jsonify(test)
        else:
            return jsonify(False)
    else:
        if os.path.exists('./storage/'+name+'/Nemoh/mesh.png'):
            file = open('./storage/'+name+'/Nemoh/mesh.png', "r")
            test = file.read()
            file.close()
            return jsonify(test)
        else:
            return jsonify(False)

# API that save the status structure
@bp.route('/CopyNemohFolfer', methods=['POST'], strict_slashes=False)
def Copy_nemoh_files():

    name = request.args['name']

    if os.path.exists('./storage/Tmp/Nemoh/'):
        src_files = os.listdir('./storage/Tmp/Nemoh/')
        for file_name in src_files:
            full_file_name = os.path.join('./storage/Tmp/Nemoh/', file_name)
            if os.path.isfile(full_file_name):
                shutil.copy(full_file_name, './storage/'+name+'/Nemoh')

        src_files = os.listdir('./storage/Tmp/Nemoh/results/')
        for file_name in src_files:
            full_file_name = os.path.join('./storage/Tmp/Nemoh/results/', file_name)
            if os.path.isfile(full_file_name):
                shutil.copy(full_file_name, './storage/'+name+'/Nemoh/results')

        src_files = os.listdir('./storage/Tmp/Nemoh/mesh/')
        for file_name in src_files:
            full_file_name = os.path.join('./storage/Tmp/Nemoh/mesh/', file_name)
            if os.path.isfile(full_file_name):
                shutil.copy(full_file_name, './storage/'+name+'/Nemoh/mesh')
    else:
        os.mkdir('./storage/Tmp')
        os.mkdir('./storage/Tmp/Nemoh')
        os.mkdir('./storage/Tmp/Nemoh/results')
        os.mkdir('./storage/Tmp/Nemoh/mesh')
        os.mkdir('./storage/Tmp/Mooring_system')
        
    return "ok"

# API that save the status structure
@bp.route('/CopyNemohDatabase', methods=['POST'], strict_slashes=False)
def Copy_Nemoh_Database():

    my_path = request.args['path']

    if(len(my_path)!=0):

        if not(os.path.exists(my_path)):
            os.mkdir(my_path)

        name = request.args['name']

        src_files = os.listdir('./storage/'+name+'/Nemoh/')
        for file_name in src_files:
            full_file_name = os.path.join('./storage/'+name+'/Nemoh/', file_name)
            if os.path.isfile(full_file_name):
                os.remove(full_file_name)

        src_files = os.listdir('./storage/'+name+'/Nemoh/results')
        for file_name in src_files:
            full_file_name = os.path.join('./storage/'+name+'/Nemoh/results', file_name)
            if os.path.isfile(full_file_name):
                os.remove(full_file_name)

        src_files = os.listdir('./storage/'+name+'/Nemoh/mesh')
        for file_name in src_files:
            full_file_name = os.path.join('./storage/'+name+'/Nemoh/mesh', file_name)
            if os.path.isfile(full_file_name):
                os.remove(full_file_name)

        src_files = os.listdir(my_path)
        for file_name in src_files:
            if (file_name != 'mesh.png'):
                full_file_name = os.path.join(my_path, file_name)
                if os.path.isfile(full_file_name):
                    shutil.copy(full_file_name, './storage/'+name+'/Nemoh')

        src_files = os.listdir(my_path + '/results/')
        for file_name in src_files:
            full_file_name = os.path.join(my_path + '/results', file_name)
            if os.path.isfile(full_file_name):
                shutil.copy(full_file_name, './storage/'+name+'/Nemoh/results')

        src_files = os.listdir(my_path + '/mesh/')
        for file_name in src_files:
            full_file_name = os.path.join(my_path + '/mesh', file_name)
            if os.path.isfile(full_file_name):
                shutil.copy(full_file_name, './storage/'+name+'/Nemoh/mesh')
        
    return "ok"

# API that save the status structure
@bp.route('/CopyNemohtoTMP', methods=['POST'], strict_slashes=False)
def Copy_nemoh_files2():

    name = request.args['name']

    src_files = os.listdir('./storage/'+name+'/Nemoh')
    for file_name in src_files:
        full_file_name = os.path.join('./storage/',name+'/Nemoh/'+file_name)
        if os.path.isfile(full_file_name):
            shutil.copy(full_file_name, './storage/Tmp/Nemoh/')

    src_files = os.listdir('./storage/'+name+'/Nemoh/results/')
    for file_name in src_files:
        full_file_name = os.path.join('./storage/'+name+'/Nemoh/results/', file_name)
        if os.path.isfile(full_file_name):
            shutil.copy(full_file_name, './storage/Tmp/Nemoh/results')

    src_files = os.listdir('./storage/'+name+'/Nemoh/mesh/')
    for file_name in src_files:
        full_file_name = os.path.join('./storage/'+name+'/Nemoh/mesh/', file_name)
        if os.path.isfile(full_file_name):
            shutil.copy(full_file_name, './storage/Tmp/Nemoh/mesh')
        
    return "ok"

# API that save the status structure
@bp.route('/CheckNemohFiles', methods=['POST'], strict_slashes=False)
def Check_nemoh_files():

    my_input = request.get_json()
    checklist = my_input['checkList']

    if (os.path.exists('./storage/Tmp/Nemoh/Nemoh.cal')):
        checklist[0] = True
    if (os.path.exists('./storage/Tmp/Nemoh/results/ExcitationForce.tec')):
        checklist[1] = True
    if (os.path.exists('./storage/Tmp/Nemoh/results/RadiationCoefficients.tec')):
        checklist[2] = True
    if (os.path.exists('./storage/Tmp/Nemoh/mesh/KH.dat')):
        checklist[3] = True
    if (os.path.exists('./storage/Tmp/Nemoh/mesh/Hydrostatics.dat')):
        checklist[4] = True
    if (os.path.exists('./storage/Tmp/Nemoh/mesh/Inertia_hull.dat')):
        checklist[5] = True
    if (os.path.exists('./storage/Tmp/Nemoh/mesh/mesh.tec')):
        checklist[6] = True
    if (os.path.exists('./storage/Tmp/Nemoh/mesh/mesh_info.dat')):
        checklist[7] = True
    if (os.path.exists('./storage/Tmp/Nemoh/mesh/mesh.dat')):
        checklist[8] = True
    if (os.path.exists('./storage/Tmp/Nemoh/mesh/Description_Full.tec')):
        checklist[9] = True
    if (os.path.exists('./storage/Tmp/Nemoh/input.txt')):
        checklist[10] = True

    return jsonify(checklist)

# API that save the status structure
@bp.route('/<eid>/<ProjectId>/GetFLSImage', methods=['GET'], strict_slashes=False)
def load_current_image(eid,ProjectId):

    foldername = ProjectId + '_' + eid
    index = request.args['index']
    path_to_image = os.path.join('./storage/',foldername + '/figures/fls_cdf_' + index+ '.png')
    if os.path.exists(path_to_image):
        data_uri = base64.b64encode(open(path_to_image, 'rb').read()).decode('utf-8')
        img_tag1 = data_uri
        return jsonify(img_tag1)
    else:
        return "False"

# API that create and saves the structure (the project name is defined by the GUI)
@bp.route('/initialise_Project_ProjectStatus', methods=['GET'], strict_slashes=False)
def initiate():

    if not(os.path.exists('./storage/Entity_table.json')):
        f = open("./storage/Entity_table.json",'w')
        f.write('{"List_of_entity": []}')
        f.close()

    my_inputs = Inputs()
    my_inputstatus = InputStatus()

    return jsonify(my_inputs.prop_rep(), my_inputstatus.prop_rep())

# API that create and saves the structure (the project name is defined by the GUI)
@bp.route('/<ProjectId>/DeleteProject', methods=['DELETE'], strict_slashes=False)
def delete(ProjectId):

    testlist = []

    # Get the Name
    with open('./storage/Entity_table.json') as json_file:
        List = json.load(json_file)
        for entity in List['List_of_entity']:
            if not(ProjectId == entity['EntityName'] + '_' + str(entity['EntityId'])):
                testlist.append(entity)
    List['List_of_entity'] = testlist
    ListtoWrite = json.dumps(List)
    f=open('./storage/Entity_table.json', "w")
    f.write(ListtoWrite)
    f.close()

    src_files = os.listdir('./storage/'+ProjectId)
    for file_name in src_files:
        full_file_name = os.path.join('./storage/', ProjectId+'/'+file_name)
        if os.path.isfile(full_file_name):
            os.remove(full_file_name)
    
    src_files = os.listdir('./storage/'+ProjectId+'/figures')
    for file_name in src_files:
        full_file_name = os.path.join('./storage/', ProjectId+'/figures/'+file_name)
        if os.path.isfile(full_file_name):
            os.remove(full_file_name)
    os.rmdir(os.path.join('./storage/', ProjectId+'/figures'))
    
    src_files = os.listdir('./storage/'+ProjectId+'/Mooring_system')
    for file_name in src_files:
        full_file_name = os.path.join('./storage/', ProjectId+'/Mooring_system/'+file_name)
        if os.path.isfile(full_file_name):
            os.remove(full_file_name)
    os.rmdir(os.path.join('./storage/', ProjectId+'/Mooring_system'))
    
    src_files = os.listdir('./storage/'+ProjectId+'/Nemoh')
    for file_name in src_files:
        full_file_name = os.path.join('./storage/', ProjectId+'/Nemoh/'+file_name)
        if os.path.isfile(full_file_name):
            os.remove(full_file_name)
    
    src_files = os.listdir('./storage/'+ProjectId+'/Nemoh/results')
    for file_name in src_files:
        full_file_name = os.path.join('./storage/', ProjectId+'/Nemoh/results/'+file_name)
        if os.path.isfile(full_file_name):
            os.remove(full_file_name)
    os.rmdir(os.path.join('./storage/', ProjectId+'/Nemoh/results'))
    
    src_files = os.listdir('./storage/'+ProjectId+'/Nemoh/mesh')
    for file_name in src_files:
        full_file_name = os.path.join('./storage/', ProjectId+'/Nemoh/mesh/'+file_name)
        if os.path.isfile(full_file_name):
            os.remove(full_file_name)
    os.rmdir(os.path.join('./storage/', ProjectId+'/Nemoh/mesh'))

    os.rmdir(os.path.join('./storage/', ProjectId+'/Nemoh'))

    os.rmdir(os.path.join('./storage/', ProjectId))

    return "ok"

# API that load the input structure from a saved json file (the project name is defined by the GUI)
@bp.route('/<eid>/<ProjectId>/loadinputs', methods=['GET'], strict_slashes=False)
def load_inputs_structure(eid,ProjectId):

    # Load inputs from stored file
    if eid == 'none':
        foldername = ProjectId
    else:
        foldername = ProjectId + '_' + eid
    filename=os.path.join('./storage', foldername +'/inputs.json')
    my_inputs=Inputs()
    my_inputs.loadJSON(filePath = filename)

    # Load inputstatus from stored file
    filename=os.path.join('./storage',foldername+'/status.json')
    my_inputstatus=InputStatus()
    if (os.path.exists(os.path.join('./storage',foldername+'/status.json'))):
        my_inputstatus.loadJSON(filePath = filename)

    return jsonify(my_inputs.prop_rep(), my_inputstatus.prop_rep())

@bp.route('/GetProjectsList', methods=['GET'], strict_slashes=False)
def MyProjects():
    MyTable = list_files('./storage','json')
    return jsonify(MyTable)

def list_files(directory, extension):
    import time
    fl=listdir(directory)
    MyTable = []
    for i, fli in enumerate(fl):
        if (os.path.exists(os.path.join('./storage/',fli + '/inputs.json'))):
            path_file = os.path.join(directory, fl[i])
            path_file = os.path.abspath(path_file)
            dict = {}
            dict['path_file'] = path_file
            tim = os.path.getctime(path_file)
            dict['time'] = time.ctime(tim)
            dict['name'] = fli
            MyTable.append(dict)
    return MyTable

# API that load the output structure from a saved json file (the project name is defined by the GUI)
@bp.route('/<eid>/<ProjectId>/loadoutputs', methods=['POST'], strict_slashes=False)
def load_outputs_structure(eid,ProjectId):

    # Load inputs from stored file
    if eid == 'none':
        foldername = ProjectId
    else:
        foldername = ProjectId + '_' + eid

    if os.path.exists(os.path.join('./storage',foldername+'/outputs.json')):

        # Load inputs from stored file
        filename=os.path.join('./storage',foldername+'/outputs.json')
        my_outputs=Project()
        my_outputs.loadJSON(filePath = filename)

        return jsonify(my_outputs.prop_rep())

    else:

        return jsonify(False)

# API that create and saves the structure (the project name is defined by the GUI)
@bp.route('/AddMooringSystem', methods=['POST'], strict_slashes=False)
def add_mooringsystem():

    # Create input main structure
    my_inputs = Inputs()

    # Populate input data
    my_inputs_rep = request.get_json()
    my_inputs.loadFromJSONDict(my_inputs_rep)
    my_inputs.sk_inputs.device_properties.custom_mooring_input = []

    if (my_inputs_rep['UseSameForAll'] == False):
        for i in my_inputs.sk_inputs.deviceId:
            my_inputs.sk_inputs.device_properties.custom_mooring_input.append(CustomMooringInput())
            my_inputs.sk_inputs.device_properties.custom_mooring_input[i-1].name = ''
    else:
        my_inputs.sk_inputs.device_properties.custom_mooring_input.append(CustomMooringInput())
        my_inputs.sk_inputs.device_properties.custom_mooring_input[0].name = ''

    return(my_inputs.prop_rep())

# API that create and saves the structure (the project name is defined by the GUI)
@bp.route('/<eid>/<ProjectId>/ReadLog', methods=['GET'], strict_slashes=False)
def read_log(eid,ProjectId):

    foldername = ProjectId + '_' + eid
    logfilefullpath = os.path.join('./storage', foldername + '/sk.log')

    if (os.path.exists(logfilefullpath)):
        # Extract the Log File
        logfile = open(logfilefullpath,"r")

        # Return the file and the date
        return(logfile.read())
    else:
        return "ok"

# API that create and saves the structure (the project name is defined by the GUI)
@bp.route('/<eid>/<ProjectId>/DeleteLog', methods=['POST'], strict_slashes=False)
def delete_log(eid,ProjectId):

    foldername = ProjectId + '_' + eid
    logfilefullpath = os.path.join('./storage', foldername + '/sk.log')

    # Delete the Log File
    if os.path.exists(logfilefullpath):
        os.remove(logfilefullpath)

    return "ok"

# API that create and saves the structure (the project name is defined by the GUI)
@bp.route('/<cplx>/CheckComplexityLevel', methods=['POST'], strict_slashes=False)
def Check_Complexity_Level(cplx):

    data = request.get_json()
    fileContent = data['files']

    my_inputstatus = json.loads(fileContent)
    if int(my_inputstatus["complexity_level"]) == int(cplx):
        return "true"
    else:
        return "false"

# API that create and saves the structure (the project name is defined by the GUI)
@bp.route('/ImportProject', methods=['POST'], strict_slashes=False)
def ImportProject():

    data = request.get_json()
    name = data['name']
    fileContent = data['files']
    entity_name = data['entity_name']
    entity_id = data['entity_id']

    my_path = './storage/' + entity_name + '_' + str(entity_id) + '/' + name

    if name == 'status.json':
        my_inputstatus = json.loads(fileContent)
        my_inputstatus["name"] = entity_name
        my_inputstatus["entity_id"] = entity_id
        my_inputstatus_treated = InputStatus()
        my_inputstatus_treated.loadFromJSONDict(my_inputstatus)

        my_inputstatus_treated.saveJSON(my_path)

    elif name == 'inputs.json':
        my_input = json.loads(fileContent)
        my_input["name"] = entity_name

        my_inputs_treated = Inputs()
        my_inputs_treated.loadFromJSONDict(my_input)

        my_inputs_treated.saveJSON(my_path)

    elif name == 'outputs.json':
        pass

    else:
        file = open(my_path, "w")
        file.write(fileContent)
        file.close()

    return "ok"

# API that create and saves the structure (the project name is defined by the GUI)
@bp.route('/<ProjectId>/List2Zip', methods=['GET'], strict_slashes=False)
def list_files_in_directory(ProjectId):

    temp = open('./storage/'+ProjectId+'/status.json')
    statusfile = temp.read()

    temp = open('./storage/'+ProjectId+'/inputs.json')
    inputfile = temp.read()

    if (os.path.exists('./storage/'+ProjectId+'/outputs.json')):
        temp = open('./storage/'+ProjectId+'/outputs.json')
        outputfile = temp.read()
    else:
        outputfile = False

    if (os.path.exists('./storage/'+ProjectId+'/sk.log')):
        temp = open('./storage/'+ProjectId+'/sk.log')
        logfile = temp.read()
    else:
        logfile = False

    figures_names = []
    figures = []

    src_files = os.listdir('./storage/'+ProjectId+'/figures')
    for file_name in src_files:
        full_file_name = os.path.join('./storage/', ProjectId+'/figures/'+file_name)
        if os.path.isfile(full_file_name):
            figures_names.append(file_name)
            temp = open(full_file_name,'rb')
            figures.append(str(base64.b64encode(temp.read())))
    
    if (figures_names == []):
        figures_names = False
        figures = False

    src_files = './storage/'+ProjectId+'/Mooring_system'
    [mooring_system_names, mooring_system] = read_files_in_folder(src_files)

    src_files = './storage/'+ProjectId+'/Nemoh'
    [nemoh_names, nemoh] = read_files_in_folder(src_files)

    src_files = './storage/'+ProjectId+'/Nemoh/results'
    [nemoh_results_names, nemoh_results] = read_files_in_folder(src_files)

    src_files = './storage/'+ProjectId+'/Nemoh/mesh'
    [nemoh_mesh_names, nemoh_mesh] = read_files_in_folder(src_files)

    # Return the file and the date
    return jsonify(statusfile, inputfile, outputfile, figures_names, figures, mooring_system_names, mooring_system, nemoh_names, nemoh, nemoh_results_names, nemoh_results, nemoh_mesh_names, nemoh_mesh, logfile)

# API that saves all inputs from request structure (request is to be created by the GUI frontend)
@bp.route('/<idx>/add_substation', methods=['POST'])
def add_substation(idx):

    # Create input main structure
    my_inputs = Inputs()

    # Load inputs from request
    my_inputs = load_inputs_from_request()

    # Add substation
    my_inputs.sk_inputs.substation_properties.insert(int(idx),SubstationProperties())

    return jsonify(my_inputs.prop_rep())

# API that create and saves the structure (the project name is defined by the GUI)
@bp.route('/GetMooringSystem', methods=['POST'], strict_slashes=False)
def get_mooringsystem():

    # Create input main structure
    my_mooring_system = CustomMooringInput()

    # Populate input data
    my_mooring_system_rep = request.get_json()
    my_mooring_system.loadFromJSONDict(my_mooring_system_rep)

    return(my_mooring_system.prop_rep())


# @ USER DEFINED FUNCTIONS END
#------------------------------------


def load_inputs_from_request():

    # Create input main structure
    my_inputs = Inputs()

    # Populate input data
    my_inputs_rep = request.get_json()
    my_inputs.loadFromJSONDict(my_inputs_rep)

#------------------------------------
# @ USER DEFINED PREPROCESS START
    my_inputs.populate() # If my_inputs.run_mode='dto', this command calls other modules' API and populate the input structure
# @ USER DEFINED PREPROCESS END
#------------------------------------

    return my_inputs

def save_inputs_to_storage(my_inputs,ProjectId):

    # Save the project input
    storagePath = './storage'
    fileName=os.path.join(storagePath,ProjectId + '/inputs.json')
    my_inputs.saveJSON(fileName)

# API that saves all inputs from request structure (request is to be created by the GUI frontend)
@bp.route('/<ProjectId>/inputs', methods=['POST'])
def save_inputs(ProjectId):

    # Load inputs from request
    my_inputs = load_inputs_from_request()

    # Save the project input
    save_inputs_to_storage(my_inputs,ProjectId)

    log = collections.OrderedDict()
    log["status"] = "created"
    return jsonify(log), 201

# API that reads all inputs from request, run SK analysis and save results in <projectId>.json file
@bp.route('/<ProjectId>/run', methods=['POST'])
def run(ProjectId):

    # Load inputs from request
    my_inputs = load_inputs_from_request()

    # Set project name
    my_inputs.name = str(ProjectId)

    # Run analysis
    log = collections.OrderedDict()

    # Save the project input
    save_inputs_to_storage(my_inputs,ProjectId)

    try:

#------------------------------------
# @ USER DEFINED RUN START

        # Run analysis
        project = run_main(my_inputs, ProjectId)

        # Save the project output
        storagePath = './storage'
        fileName=os.path.join(storagePath,ProjectId + '/outputs.json')
        project.saveJSON(fileName=fileName)

# @ USER DEFINED RUN END
#------------------------------------


        # Report success
        log["analysis_status"] = "success"

    except Exception as e:

        # Report fail and error message
        log["analysis_status"] = "failed"
        if hasattr(e, 'message'):
            log["error_message"] =str(e.message)
        else:
            log["error_message"] =str(e)

    return jsonify(log),201
