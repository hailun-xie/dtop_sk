# This is the Station Keeping module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Neil Luxcey
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import os
from flask import Flask, request, make_response, jsonify
from flask_babel import Babel
from flask_cors import CORS

#------------------------------------
# @ USER DEFINED IMPORTS START
# @ USER DEFINED IMPORTS END
#------------------------------------


babel = Babel()

def create_app(test_config=None):
    # create and configure the app
    app = Flask(__name__)
    CORS(app, resources={r"/*": {"origins": "*"}})
    babel.init_app(app)
    if test_config is None:
        # load the instance config, if it exists, when not testing
        app.config.from_pyfile('config.py', silent=True)
    else:
        # load the test config if passed in
        app.config.from_mapping(test_config)
    # Registering Blueprints
    from .api import getResults as res
    app.register_blueprint(res.bp, url_prefix='/sk')

    from .api import postInputs as inp
    app.register_blueprint(inp.bp, url_prefix='/sk')

    from .api import mooringCheck as moc
    app.register_blueprint(moc.bp, url_prefix='/sk')

    from .api import mm_integration as mmi
    app.register_blueprint(mmi.bp, url_prefix='/sk')

#------------------------------------
# @ USER DEFINED BLUEPRINT REGISTRATION START
    from .api import digitalRepresentation as dr
    app.register_blueprint(dr.bp, url_prefix='')
    from .gui import main
    app.register_blueprint(main.bp)

    if os.environ.get("FLASK_ENV") == "development":
        from .api.integration import provider_states
        app.register_blueprint(provider_states.bp)
# @ USER DEFINED BLUEPRINT REGISTRATION END
#------------------------------------



#------------------------------------
# @ USER DEFINED FUNCTIONS START
# @ USER DEFINED FUNCTIONS END
#------------------------------------

    return app

@babel.localeselector
def get_locale():
    return request.accept_languages.best_match(['en', 'fr'])   
