from flask import Blueprint, render_template, url_for, request

bp = Blueprint('main', __name__)


@bp.route('/')
def index():
  return render_template('index2.html')
