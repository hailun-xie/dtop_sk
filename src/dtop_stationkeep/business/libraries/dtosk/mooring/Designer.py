# This is the Station Keeping module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Neil Luxcey, Nicolas Michelet, Rocio Isorna
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import collections
import numpy as np
import os
import json
import h5py
#------------------------------------
# @ USER DEFINED IMPORTS START
from .Mooring import Mooring
from ..global_parameters import steel_density
from ..global_parameters import steel_young_modulus
from ..global_parameters import acceleration_of_gravity
from ..line_characteristics_module import compute_anchor_point_position
from ..solver.Static import Static
import copy
import matplotlib.pyplot as plt
from ..winching_module import winch_line_for_pretension
from ..mooring.LineTypeData import LineTypeData
import logging
import sys
logger = logging.getLogger()
# @ USER DEFINED IMPORTS END
#------------------------------------

class Designer():

    """data model representing a mooring system designer
    """

    #------------
    # Constructor
    #------------
    def __init__(self,name=None):
#------------------------------------
# @ USER DEFINED DESCRIPTION START
# @ USER DEFINED DESCRIPTION END
#------------------------------------

        self._name='none'
        self._description=''
        if not(name == None): # pragma: no cover
            self._name = name

#------------------------------------
# @ USER DEFINED PROPERTIES START
# Curve MooringRadius/WaterDepth vs. WaterDepth [m]. Kai-Tung Ma, Mooring System Engineering for Offshore Structures, Figure 4.2.
        self.wd_mr = np.array( [[34.578153699095,       13.696849966391769] ,\
                                [47.270004132003265,    11.610755775759802] ,\
                                [62.42384603813255,     9.504119626253642] ,\
                                [84.3380077651195,      7.776721398254657] ,\
                                [110.6358143759438,     6.3889852599829755] ,\
                                [146.09905459004628,    5.400023611446981] ,\
                                [191.18274993383082,    4.489208235825037] ,\
                                [260.43133848342154,    3.6936014882828196] ,\
                                [380.8414099080486,     2.847359611858277] ,\
                                [510.4169149359541,     2.399892225169422] ,\
                                [710.5938850160614,     1.9178074105668834] ,\
                                [922.7111074720708,     1.5935232277609686] ,\
                                [1244.2528816438137,    1.306735546081356] ,\
                                [1719.0231548320635,    1.0483958497169485] ,\
                                [2084.141365541378,     0.9128737606370514] ,\
                                [2437.063374266189,     0.7999044117227019]])

# @ USER DEFINED PROPERTIES END
#------------------------------------

#------------------------------------
# @ USER DEFINED METHODS START

    def initiate_mooring_system(self,body,environment_array,water_depth,catalogue_chains):

        # Instantiate object
        mooring = Mooring()

        # Initiate other variables
        sea_water_density = environment_array[0].current.water_density
        g = acceleration_of_gravity()
        rho_steel = steel_density()

        # Compute maximum horizontal force on body to be moored
        Th = 0.0
        for idx in range(0,len(environment_array)):
            force = body.compute_static_force(current = environment_array[idx].current, wind = environment_array[idx].wind, wave = environment_array[idx].wave, water_depth = water_depth)
            Th = max(np.linalg.norm(force[0:2]),Th)

        # Select line type based on maximum horizontal load and water depth (catalogue)
        #Th = max(Th,body.mass*g)
        [candidate_found,line_type] = self.select_line_from_mbl(Th,rho_steel,sea_water_density,water_depth,catalogue_chains)

        # If candidate not found, this probably mean that we are in deepwater, and semi-taut is necessary
        if not candidate_found:
            raise Exception("Error from initiate_mooring_system: chain candidate not found (it does not tolerate its own weight). If ultradeep water, try semi-taut system (custom).")

        # Initiate mooring system
        # Assume circular distribution of the fairlead
        n_bundle = body.mooring_design_criteria.nlines
        n_line_per_batch = 1
        mooring_ref_angle = 0.0
        batch_angle = 10*np.pi/180.0

        # Define fairlead position
        fairlead_depth = body.mooring_design_criteria.fairlead_vertical_position
        fairlead_radius = body.mooring_design_criteria.fairlead_radius

        # First estimate of minimum line length to avoid vertical force on anchor point (Faltinsen, 1990, Equation 8.19)
        mooring_line_length = ((water_depth+fairlead_depth)*(2*line_type.mbl/(line_type.weight_in_water*g*(water_depth+fairlead_depth)) -1)**0.5)/3

        # Compute mooring radius for a pretension of 10%MBL
        mooring_radius = compute_anchor_point_position(0.1*line_type.mbl,line_type.weight_in_water,mooring_line_length,water_depth+fairlead_depth)+ fairlead_radius

        # Limit the length of mooring line so that it is still catenary, even at max offset
        mooring_line_length = (0.99*(water_depth + fairlead_depth + mooring_radius - fairlead_radius - body.mooring_design_criteria.offset_max))

        # Add Lmin and Lmax limitations (increased robustness)
        w = water_depth + fairlead_depth # fairlead_depth < 0.0 means the fairlead is below waterline
        x = body.mooring_design_criteria.offset_max

        # Check that maximum offset is smaller than 50% of water depth (mathematical condition for a line length solution to exist)
        if x>=0.5*w:
            x = 0.4999*w
            body.mooring_design_criteria.offset_max = 0.4999*w
            raise Exception('In catenary design mode, with mutiple lines, maximum offset cannot exceed '+ str(0.5*w) + 'm (50% of (water_depth + fairlead_vertical_position))')

        # Compute minimum r so that a line length solution exists
        rmin = w*x/(w-2*x)
        mooring_radius = max(rmin + fairlead_radius,mooring_radius) # Select the largest mooring radius
        r = mooring_radius - fairlead_radius

        # Compute minimum and maximum mooring line length
        Lmin = np.sqrt(w**2 + (r+x)**2) # Mimimum length to cope with  + maximum offset
        Lmax = w+r-x # Maximum length to cope with - maximum offset
        if Lmin>Lmax:
            raise Exception('Lmin > Lmax : this error should never happen')

        # Set mooring line length equal to maximum allowable length
        mooring_line_length = Lmax

        # Create map++ model
        mooring.create_from_simple_input_line_type(line_type,body.position,n_bundle,n_line_per_batch,mooring_radius,mooring_ref_angle,mooring_line_length,water_depth,batch_angle,fairlead_radius,fairlead_depth,sea_water_density,vessel_position = None,tag_name = body.name + '_init_')

        return mooring

    def select_line_from_mbl(self,Th,rho_steel,sea_water_density,water_depth,catalogue_chains):

        # Initiate
        candidate_found = False
        g = acceleration_of_gravity()

        # Go through the elements and select the line with the smallest diameter
        # Init
        idx = 0
        Tmax = Th + catalogue_chains.weight_in_water[0]*g*water_depth

        # Iterate
        while (Tmax >= catalogue_chains.mbl[idx] and idx<len(catalogue_chains.diameter)):
            idx = idx + 1
            Tmax = Th + catalogue_chains.weight_in_water[idx]*g*water_depth

        # Check if one line has been selected
        if Tmax < catalogue_chains.mbl[idx]:
            candidate_found = True

            line_type = LineTypeData()
            line_type.define_from_catalogue_index(catalogue_chains,idx)

        return candidate_found, line_type

    def refine_mooring_system_for_steady_offset(self,steady_offset_max,body,environment_array,catalogue_chains,water_depth,pretension_coeff_min,length_increment):
        # Refine mooring system design so that it can cope with steady forces.
        # One constrain needs to be satisfied here:
        #   - mooring system is stiff enough so that, at maximum steady offset, steady forces are compensated by the mooring system forces

        logger.info('-- Increase line diameter to cope with maximum steady offset')

        # Init
        body.position = copy.deepcopy(body.initial_position)
        beta =  np.linspace(0.0, 2*np.pi, num=5)
        f_env = 0.0
        water_density = environment_array[0].current.water_density

        # Compute maximum steady force from environmental conditions
        for idx in range(0,len(environment_array)):
            for idir in range(0,len(beta)):
                # Set colinear direction for current/wind/wave
                environment_array[idx].set_colinear_direction(beta[idir])
                # Compute environmental force
                force = body.compute_static_force(current = environment_array[idx].current, wind = environment_array[idx].wind, wave = environment_array[idx].wave, water_depth = water_depth ,include_mooring = False)
                # Gather max horizontal force
                f_env = max(f_env,np.linalg.norm(force[0:2]))

        # Design loop
        counter = 0
        need_more_stiffness = True
        # Iteration loop
        while (need_more_stiffness and (counter < len(catalogue_chains.diameter))):

            # Init
            need_more_stiffness = False

            # Increase iteration number
            counter = counter +1

            # If we are in the second iteration, increase diameter:
            if counter>1:

                # Increase line type diameter
                self.increase_segment_diameter(body.mooring_system_force,catalogue_chains)
                # logger.info('Increasing line diameter to ' + str(body.mooring_system_force.line_dictionnary[0].diameter))

                # Update line type name for each line properties (we know here that all lines are of the same line type)
                for idl in range(0,len(body.mooring_system_force.line_properties)):
                    body.mooring_system_force.line_properties[idl].line_type_name = body.mooring_system_force.line_dictionnary[0].line_type_name

                # Initiate calculation
                body.mooring_system_force.initiate_calculation(water_depth,water_density,body.initial_position)

                # Winch the lines to get acceptable pretension
                winch_line_for_pretension(body,pretension_coeff_min,length_increment,water_depth,water_density)

            # Compute steady force from mooring system for an offset in all directions
            f_moor = np.zeros(shape=(len(beta)),dtype=float)
            for idir in range(0,len(beta)):
                body.position[0] = body.initial_position[0] + steady_offset_max*np.cos(beta[idir])
                body.position[1] = body.initial_position[1] + steady_offset_max*np.sin(beta[idir])
                force = body.compute_static_force()
                f_moor[idir] = np.linalg.norm(force[0:2])
                if f_moor[idir] < f_env:
                    need_more_stiffness = True

            # logger.info('fmoor ' +str(min(f_moor)) + ' vs fenv ' + str(f_env))
        print("test8")

        # Restore position of body to its initial position
        body.position[0] = body.initial_position[0]
        body.position[1] = body.initial_position[1]

        # Check that algorithm found a solution
        additional_line_needed = False
        if need_more_stiffness:
            if steady_offset_max > 0.4*water_depth:
                raise Exception("Error in refine_mooring_system_for_steady_offset: no solution found. Try to reduce the maximum offset.")
            else:
                additional_line_needed = True
        else:
            logger.info('Selected diameter: ' + str(body.mooring_system_force.line_dictionnary[0].diameter) + ' m ')

        return additional_line_needed

    def increase_segment_diameter(self,mooring,catalogue_chains):

        additional_line_needed_all = [False] * (len(mooring.line_dictionnary))

        for idl in range(0,len(mooring.line_dictionnary)):

            [candidate_found, idx] = catalogue_chains.select_one_diameter_larger(mooring.line_dictionnary[idl].diameter)

            if candidate_found:

                mooring.line_dictionnary[idl].define_from_catalogue_index(catalogue_chains,idx)

            else:
                additional_line_needed_all[idl] = True

        if all(additional_line_needed_all):
            additional_line_needed = True
        else:
            additional_line_needed = False

        return additional_line_needed

    def duplicate_mooring_design(self,body_ref,body,water_density):

        # Create instance of mooring system
        body.mooring_system_force = Mooring()

        # Copy the mooring data. Note: we do not deepcopy the object here because of ctypes object that cannot be pickled
        body.mooring_system_force.anchor_position_in_n  = copy.deepcopy(body_ref.mooring_system_force.anchor_position_in_n)
        body.mooring_system_force.fairlead_position_in_b = copy.deepcopy(body_ref.mooring_system_force.fairlead_position_in_b)
        body.mooring_system_force.line_dictionnary= copy.deepcopy(body_ref.mooring_system_force.line_dictionnary)
        body.mooring_system_force.node_properties= copy.deepcopy(body_ref.mooring_system_force.node_properties)
        body.mooring_system_force.line_properties= copy.deepcopy(body_ref.mooring_system_force.line_properties)

        # Set name
        body.mooring_system_force.name = 'mooring_' + body.name

        # Set vessel position
        body.mooring_system_force.vessel_position= copy.deepcopy(body.initial_position)

        # Translate anchor position
        vect = body.initial_position[0:3] - body_ref.initial_position[0:3]
        body.mooring_system_force.translate(vect)

        # Initiate pymap for the mooring design
        body.mooring_system_force.initiate_calculation(-body.mooring_system_force.anchor_position_in_n[0,2],water_density,body.initial_position)

# @ USER DEFINED METHODS END
#------------------------------------

    #------------
    # Get functions
    #------------
    @ property
    def name(self): # pragma: no cover
        """str: name of the instance object
        """
        return self._name
    #------------
    @ property
    def description(self): # pragma: no cover
        """str: description of the instance object
        """
        return self._description
    #------------
    #------------
    # Set functions
    #------------
    @ name.setter
    def name(self,val): # pragma: no cover
        self._name=str(val)
    #------------
    @ description.setter
    def description(self,val): # pragma: no cover
        self._description=str(val)
    #------------
    #-------------------------
    # Representation functions
    #-------------------------
    def type_rep(self): # pragma: no cover
        """Generate a representation of the object type

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains the representation of the object type
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtosk:mooring:Designer"
        rep["name"] = self.name
        rep["description"] = self.description
        return rep
    def prop_rep(self, short = False, deep = True):

        """Generate a representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains a representation of the object properties
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtosk:mooring:Designer"
        rep["name"] = self.name
        rep["description"] = self.description
        if self.is_set("name"):
            rep["name"] = self.name
        if self.is_set("description"):
            rep["description"] = self.description
        return rep
    #-------------------------
    # Save functions
    #-------------------------
    def json_rep(self, short=False, deep=True):

        """Generate a JSON representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`str`: string that contains a JSON representation of the object properties
        """

        return ( json.dumps(self.prop_rep(short=short, deep=deep),indent=4, separators=(",",": ")))
    def saveJSON(self, fileName=None):
        """Save the instance of the object to JSON format file

        Args:
            fileName (:obj:`str`, optional): Name of the JSON file, included extension. Defaults is None. If None, the name of the JSON file will be self.name.json. It can also contain an absolute or relative path.

        """

        if fileName==None:
            fileName=self.name + ".json"
        f=open(fileName, "w")
        f.write(self.json_rep())
        f.write("\n")
        f.close()

    def saveHDF5(self,filePath=None):
        """Save the instance of the object to HDF5 format file
        Args:
            filePath (:obj:`str`, optional): Name of the HDF5 file, included extension. Defaults is None. If None, the name of the HDF5 file will be "self.name".h5. It can also contain an absolute or relative path.
        """
        # Define filepath
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".h5"
            else:
                raise Exception("name is required for saving")
        # Open hdf5 file
        h = h5py.File(filePath,"w")
        group = h.create_group(self.name)
        # Save the object to the group
        self.saveToHDF5Handle(group)
        # Close file
        h.close()
        pass
    def saveToHDF5Handle(self, handle):
        """Save the properties of the object to the hdf5 handle.
        Args:
            handle (:obj:`h5py.Group`): Handle used to store the object properties
        """
        ar = []
        ar.append(self.name.encode("ascii"))
        handle["name"] = np.asarray(ar)
        ar = []
        ar.append(self.description.encode("ascii"))
        handle["description"] = np.asarray(ar)
    #-------------------------
    # Load functions
    #-------------------------
    def loadJSON(self,name = None, filePath = None):
        """Load an instance of the object from JSON format file

        Args:
            name (:obj:`str`, optional): Name of the object to load.
            filePath (:obj:`str`, optional): Path of the JSON file to load. If None, the function looks for a file with name "name".json.

        The JSON file must contain a datastructure representing an instance of this object's class, as generated by e.g. the function :func:`~saveJSON`.

        """

        if not(name == None):
            self.name = name
        if (name == None) and not(filePath == None):
            self.name = ".".join(filePath.split(os.path.sep)[-1].split(".")[0:-1])
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".json"
            else:
                raise Exception("object needs name for loading.")
        if not(os.path.isfile(filePath)):
            raise Exception("file %s not found."%filePath)
        self._loadedItems = []
        f = open(filePath,"r")
        data = f.read()
        f.close()
        dd = json.loads(data)
        self.loadFromJSONDict(dd)
    def loadFromJSONDict(self, data):
        """Load an instance of the object from a dictionnary representing a JSON structure)

        Args:
            name (:obj:`collections.OrderedDict`): Dictionnary containing a JSON structure, as produced by e.g. :func:`json.loads`

        """

        varName = "name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "description"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
    def loadHDF5(self,name=None, filePath=None):
        """Load an instance of the object from HDF5 format file
        Args:
            name (:obj:`str`, optional): Name of the object to load. Default is None.
            filePath (:obj:`str`, optional): Path of the HDF5 file to load. If None, the function looks for a file with name "self.name".h5.
        The HDF5 file must contain a datastructure representing an instance of this objects class, as generated by e.g. the function :func:`~saveHDF5`.
        """
        # Define filepath and name
        if not(name == None):
            self.name = name
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".h5"
            else:
                raise Exception("name is required for loading")
        # Open hdf5 file
        h = h5py.File(filePath,"r")
        group = h[self.name]
        # Save the object to the group
        self.loadFromHDF5Handle(group)
        # Close file
        h.close()
        pass
    def loadFromHDF5Handle(self, gr):
        """Load the properties of the object from a hdf5 handle.
        Args:
            gr (:obj:`h5py.Group`): Handle used to read the object properties from
        """
        if ("name" in list(gr.keys())):
            self.name = gr["name"][0].decode("ascii")
        if ("description" in list(gr.keys())):
            self.description = gr["description"][0].decode("ascii")
    #------------------------
    # is_set function
    #------------------------
    def is_set(self, varName): # pragma: no cover

        """Check if a given property of the object is set

        Args:
            varName (:obj:`str`): name of the property to check

        Returns:
            :obj:`bool`: True if the property is set, else False
        """

        if (isinstance(getattr(self,varName),list) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (isinstance(getattr(self,varName),np.ndarray) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (getattr(self,varName) != None):
            return True
        return False
    #------------------------
