# This is the Station Keeping module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Neil Luxcey, Nicolas Michelet, Rocio Isorna
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import collections
import numpy as np
import os
import json
import h5py
from . import Ancillaries
#------------------------------------
# @ USER DEFINED IMPORTS START
# @ USER DEFINED IMPORTS END
#------------------------------------

class MooringDesignCriteria():

    """data model representing criteria for mooring system design
    """

    #------------
    # Constructor
    #------------
    def __init__(self,name=None):
#------------------------------------
# @ USER DEFINED DESCRIPTION START
# @ USER DEFINED DESCRIPTION END
#------------------------------------

        self._proof_load_coeff=0.8
        self._pretension_coeff_min=0.03
        self._fairlead_vertical_position=0.0
        self._fairlead_radius=0.0
        self._length_increment=0.5
        self._offset_max=30.0
        self._Tmin=30.0
        self._mooring_type='catenary'
        self._line_quality='R3'
        self._safety_factor=1.7
        self._gamma_f=8.0
        self._gamma_f_fiber_rope=60.0
        self._nlines=3
        self._nlines_max=10
        self._n_years_lifetime=25
        self._mooring_output_folder_path=''
        self._mooring_output_reference_system='farm'
        self._ancillaries=Ancillaries.Ancillaries()
        self._ancillaries.description = 'List of ancillaries'
        self._name='none'
        self._description=''
        if not(name == None): # pragma: no cover
            self._name = name

#------------------------------------
# @ USER DEFINED PROPERTIES START
# @ USER DEFINED PROPERTIES END
#------------------------------------

#------------------------------------
# @ USER DEFINED METHODS START
# @ USER DEFINED METHODS END
#------------------------------------

    #------------
    # Get functions
    #------------
    @ property
    def proof_load_coeff(self): # pragma: no cover
        """float: Maximum tension on anchor under installation in terms of MBL (Tension = proof_load_coeff*MBL) []
        """
        return self._proof_load_coeff
    #------------
    @ property
    def pretension_coeff_min(self): # pragma: no cover
        """float: Minimum pretension coefficient in terms of MBL (pretension_min = pretension_coeff_min*MBL) []
        """
        return self._pretension_coeff_min
    #------------
    @ property
    def fairlead_vertical_position(self): # pragma: no cover
        """float: Fairlead vertical position [m]. Negative if below water. []
        """
        return self._fairlead_vertical_position
    #------------
    @ property
    def fairlead_radius(self): # pragma: no cover
        """float: Fairlead radius [m]. []
        """
        return self._fairlead_radius
    #------------
    @ property
    def length_increment(self): # pragma: no cover
        """float: Line length increment when winching the line in order to match the pretension []
        """
        return self._length_increment
    #------------
    @ property
    def offset_max(self): # pragma: no cover
        """float: Total maximum offset (steady + dynamic) []
        """
        return self._offset_max
    #------------
    @ property
    def Tmin(self): # pragma: no cover
        """float: Minimum eigenperiod [s] []
        """
        return self._Tmin
    #------------
    @ property
    def mooring_type(self): # pragma: no cover
        """str: device profile : catenary, taut, semitaut
        """
        return self._mooring_type
    #------------
    @ property
    def line_quality(self): # pragma: no cover
        """str: quality of the lines (used to select correct catalogue data)
        """
        return self._line_quality
    #------------
    @ property
    def safety_factor(self): # pragma: no cover
        """float: Safety factor used to check tension against mbl : Tension * SF < MBL is ok. []
        """
        return self._safety_factor
    #------------
    @ property
    def gamma_f(self): # pragma: no cover
        """float: safety factor for fatigue analysis. Applied to steel material []
        """
        return self._gamma_f
    #------------
    @ property
    def gamma_f_fiber_rope(self): # pragma: no cover
        """float: safety factor for fatigue analysis. Applied to material other than steel (fiber rope in practice) []
        """
        return self._gamma_f_fiber_rope
    #------------
    @ property
    def nlines(self): # pragma: no cover
        """int: Number of lines to start the design loop with []
        """
        return self._nlines
    #------------
    @ property
    def nlines_max(self): # pragma: no cover
        """int: Maximum number of lines to limit the number of iterations in the design loop []
        """
        return self._nlines_max
    #------------
    @ property
    def n_years_lifetime(self): # pragma: no cover
        """float: Lifetime of the mooring system in years []
        """
        return self._n_years_lifetime
    #------------
    @ property
    def mooring_output_folder_path(self): # pragma: no cover
        """str: Path to folder where map++ files will be saved
        """
        return self._mooring_output_folder_path
    #------------
    @ property
    def mooring_output_reference_system(self): # pragma: no cover
        """str: Reference system in which the mooring will be exported: device or farm
        """
        return self._mooring_output_reference_system
    #------------
    @ property
    def ancillaries(self): # pragma: no cover
        """:obj:`~.Ancillaries.Ancillaries`: List of ancillaries
        """
        return self._ancillaries
    #------------
    @ property
    def name(self): # pragma: no cover
        """str: name of the instance object
        """
        return self._name
    #------------
    @ property
    def description(self): # pragma: no cover
        """str: description of the instance object
        """
        return self._description
    #------------
    #------------
    # Set functions
    #------------
    @ proof_load_coeff.setter
    def proof_load_coeff(self,val): # pragma: no cover
        self._proof_load_coeff=float(val)
    #------------
    @ pretension_coeff_min.setter
    def pretension_coeff_min(self,val): # pragma: no cover
        self._pretension_coeff_min=float(val)
    #------------
    @ fairlead_vertical_position.setter
    def fairlead_vertical_position(self,val): # pragma: no cover
        self._fairlead_vertical_position=float(val)
    #------------
    @ fairlead_radius.setter
    def fairlead_radius(self,val): # pragma: no cover
        self._fairlead_radius=float(val)
    #------------
    @ length_increment.setter
    def length_increment(self,val): # pragma: no cover
        self._length_increment=float(val)
    #------------
    @ offset_max.setter
    def offset_max(self,val): # pragma: no cover
        self._offset_max=float(val)
    #------------
    @ Tmin.setter
    def Tmin(self,val): # pragma: no cover
        self._Tmin=float(val)
    #------------
    @ mooring_type.setter
    def mooring_type(self,val): # pragma: no cover
        self._mooring_type=str(val)
    #------------
    @ line_quality.setter
    def line_quality(self,val): # pragma: no cover
        self._line_quality=str(val)
    #------------
    @ safety_factor.setter
    def safety_factor(self,val): # pragma: no cover
        self._safety_factor=float(val)
    #------------
    @ gamma_f.setter
    def gamma_f(self,val): # pragma: no cover
        self._gamma_f=float(val)
    #------------
    @ gamma_f_fiber_rope.setter
    def gamma_f_fiber_rope(self,val): # pragma: no cover
        self._gamma_f_fiber_rope=float(val)
    #------------
    @ nlines.setter
    def nlines(self,val): # pragma: no cover
        self._nlines=int(val)
    #------------
    @ nlines_max.setter
    def nlines_max(self,val): # pragma: no cover
        self._nlines_max=int(val)
    #------------
    @ n_years_lifetime.setter
    def n_years_lifetime(self,val): # pragma: no cover
        self._n_years_lifetime=float(val)
    #------------
    @ mooring_output_folder_path.setter
    def mooring_output_folder_path(self,val): # pragma: no cover
        self._mooring_output_folder_path=str(val)
    #------------
    @ mooring_output_reference_system.setter
    def mooring_output_reference_system(self,val): # pragma: no cover
        self._mooring_output_reference_system=str(val)
    #------------
    @ ancillaries.setter
    def ancillaries(self,val): # pragma: no cover
        self._ancillaries=val
    #------------
    @ name.setter
    def name(self,val): # pragma: no cover
        self._name=str(val)
    #------------
    @ description.setter
    def description(self,val): # pragma: no cover
        self._description=str(val)
    #------------
    #-------------------------
    # Representation functions
    #-------------------------
    def type_rep(self): # pragma: no cover
        """Generate a representation of the object type

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains the representation of the object type
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtosk:mooring:MooringDesignCriteria"
        rep["name"] = self.name
        rep["description"] = self.description
        return rep
    def prop_rep(self, short = False, deep = True):

        """Generate a representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains a representation of the object properties
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtosk:mooring:MooringDesignCriteria"
        rep["name"] = self.name
        rep["description"] = self.description
        if self.is_set("proof_load_coeff"):
            rep["proof_load_coeff"] = self.proof_load_coeff
        if self.is_set("pretension_coeff_min"):
            rep["pretension_coeff_min"] = self.pretension_coeff_min
        if self.is_set("fairlead_vertical_position"):
            rep["fairlead_vertical_position"] = self.fairlead_vertical_position
        if self.is_set("fairlead_radius"):
            rep["fairlead_radius"] = self.fairlead_radius
        if self.is_set("length_increment"):
            rep["length_increment"] = self.length_increment
        if self.is_set("offset_max"):
            rep["offset_max"] = self.offset_max
        if self.is_set("Tmin"):
            rep["Tmin"] = self.Tmin
        if self.is_set("mooring_type"):
            rep["mooring_type"] = self.mooring_type
        if self.is_set("line_quality"):
            rep["line_quality"] = self.line_quality
        if self.is_set("safety_factor"):
            rep["safety_factor"] = self.safety_factor
        if self.is_set("gamma_f"):
            rep["gamma_f"] = self.gamma_f
        if self.is_set("gamma_f_fiber_rope"):
            rep["gamma_f_fiber_rope"] = self.gamma_f_fiber_rope
        if self.is_set("nlines"):
            rep["nlines"] = self.nlines
        if self.is_set("nlines_max"):
            rep["nlines_max"] = self.nlines_max
        if self.is_set("n_years_lifetime"):
            rep["n_years_lifetime"] = self.n_years_lifetime
        if self.is_set("mooring_output_folder_path"):
            rep["mooring_output_folder_path"] = self.mooring_output_folder_path
        if self.is_set("mooring_output_reference_system"):
            rep["mooring_output_reference_system"] = self.mooring_output_reference_system
        if self.is_set("ancillaries"):
            if (short and not(deep)):
                rep["ancillaries"] = self.ancillaries.type_rep()
            else:
                rep["ancillaries"] = self.ancillaries.prop_rep(short, deep)
        if self.is_set("name"):
            rep["name"] = self.name
        if self.is_set("description"):
            rep["description"] = self.description
        return rep
    #-------------------------
    # Save functions
    #-------------------------
    def json_rep(self, short=False, deep=True):

        """Generate a JSON representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`str`: string that contains a JSON representation of the object properties
        """

        return ( json.dumps(self.prop_rep(short=short, deep=deep),indent=4, separators=(",",": ")))
    def saveJSON(self, fileName=None):
        """Save the instance of the object to JSON format file

        Args:
            fileName (:obj:`str`, optional): Name of the JSON file, included extension. Defaults is None. If None, the name of the JSON file will be self.name.json. It can also contain an absolute or relative path.

        """

        if fileName==None:
            fileName=self.name + ".json"
        f=open(fileName, "w")
        f.write(self.json_rep())
        f.write("\n")
        f.close()

    def saveHDF5(self,filePath=None):
        """Save the instance of the object to HDF5 format file
        Args:
            filePath (:obj:`str`, optional): Name of the HDF5 file, included extension. Defaults is None. If None, the name of the HDF5 file will be "self.name".h5. It can also contain an absolute or relative path.
        """
        # Define filepath
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".h5"
            else:
                raise Exception("name is required for saving")
        # Open hdf5 file
        h = h5py.File(filePath,"w")
        group = h.create_group(self.name)
        # Save the object to the group
        self.saveToHDF5Handle(group)
        # Close file
        h.close()
        pass
    def saveToHDF5Handle(self, handle):
        """Save the properties of the object to the hdf5 handle.
        Args:
            handle (:obj:`h5py.Group`): Handle used to store the object properties
        """
        handle["proof_load_coeff"] = np.array([self.proof_load_coeff],dtype=float)
        handle["pretension_coeff_min"] = np.array([self.pretension_coeff_min],dtype=float)
        handle["fairlead_vertical_position"] = np.array([self.fairlead_vertical_position],dtype=float)
        handle["fairlead_radius"] = np.array([self.fairlead_radius],dtype=float)
        handle["length_increment"] = np.array([self.length_increment],dtype=float)
        handle["offset_max"] = np.array([self.offset_max],dtype=float)
        handle["Tmin"] = np.array([self.Tmin],dtype=float)
        ar = []
        ar.append(self.mooring_type.encode("ascii"))
        handle["mooring_type"] = np.asarray(ar)
        ar = []
        ar.append(self.line_quality.encode("ascii"))
        handle["line_quality"] = np.asarray(ar)
        handle["safety_factor"] = np.array([self.safety_factor],dtype=float)
        handle["gamma_f"] = np.array([self.gamma_f],dtype=float)
        handle["gamma_f_fiber_rope"] = np.array([self.gamma_f_fiber_rope],dtype=float)
        handle["nlines"] = np.array([self.nlines],dtype=int)
        handle["nlines_max"] = np.array([self.nlines_max],dtype=int)
        handle["n_years_lifetime"] = np.array([self.n_years_lifetime],dtype=float)
        ar = []
        ar.append(self.mooring_output_folder_path.encode("ascii"))
        handle["mooring_output_folder_path"] = np.asarray(ar)
        ar = []
        ar.append(self.mooring_output_reference_system.encode("ascii"))
        handle["mooring_output_reference_system"] = np.asarray(ar)
        subgroup = handle.create_group("ancillaries")
        self.ancillaries.saveToHDF5Handle(subgroup)
        ar = []
        ar.append(self.name.encode("ascii"))
        handle["name"] = np.asarray(ar)
        ar = []
        ar.append(self.description.encode("ascii"))
        handle["description"] = np.asarray(ar)
    #-------------------------
    # Load functions
    #-------------------------
    def loadJSON(self,name = None, filePath = None):
        """Load an instance of the object from JSON format file

        Args:
            name (:obj:`str`, optional): Name of the object to load.
            filePath (:obj:`str`, optional): Path of the JSON file to load. If None, the function looks for a file with name "name".json.

        The JSON file must contain a datastructure representing an instance of this object's class, as generated by e.g. the function :func:`~saveJSON`.

        """

        if not(name == None):
            self.name = name
        if (name == None) and not(filePath == None):
            self.name = ".".join(filePath.split(os.path.sep)[-1].split(".")[0:-1])
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".json"
            else:
                raise Exception("object needs name for loading.")
        if not(os.path.isfile(filePath)):
            raise Exception("file %s not found."%filePath)
        self._loadedItems = []
        f = open(filePath,"r")
        data = f.read()
        f.close()
        dd = json.loads(data)
        self.loadFromJSONDict(dd)
    def loadFromJSONDict(self, data):
        """Load an instance of the object from a dictionnary representing a JSON structure)

        Args:
            name (:obj:`collections.OrderedDict`): Dictionnary containing a JSON structure, as produced by e.g. :func:`json.loads`

        """

        varName = "proof_load_coeff"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "pretension_coeff_min"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "fairlead_vertical_position"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "fairlead_radius"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "length_increment"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "offset_max"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "Tmin"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "mooring_type"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "line_quality"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "safety_factor"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "gamma_f"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "gamma_f_fiber_rope"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "nlines"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "nlines_max"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "n_years_lifetime"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "mooring_output_folder_path"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "mooring_output_reference_system"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "ancillaries"
        try :
            if data[varName] != None:
                self.ancillaries=Ancillaries.Ancillaries()
                self.ancillaries.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "description"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
    def loadHDF5(self,name=None, filePath=None):
        """Load an instance of the object from HDF5 format file
        Args:
            name (:obj:`str`, optional): Name of the object to load. Default is None.
            filePath (:obj:`str`, optional): Path of the HDF5 file to load. If None, the function looks for a file with name "self.name".h5.
        The HDF5 file must contain a datastructure representing an instance of this objects class, as generated by e.g. the function :func:`~saveHDF5`.
        """
        # Define filepath and name
        if not(name == None):
            self.name = name
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".h5"
            else:
                raise Exception("name is required for loading")
        # Open hdf5 file
        h = h5py.File(filePath,"r")
        group = h[self.name]
        # Save the object to the group
        self.loadFromHDF5Handle(group)
        # Close file
        h.close()
        pass
    def loadFromHDF5Handle(self, gr):
        """Load the properties of the object from a hdf5 handle.
        Args:
            gr (:obj:`h5py.Group`): Handle used to read the object properties from
        """
        if ("proof_load_coeff" in list(gr.keys())):
            self.proof_load_coeff = gr["proof_load_coeff"][0]
        if ("pretension_coeff_min" in list(gr.keys())):
            self.pretension_coeff_min = gr["pretension_coeff_min"][0]
        if ("fairlead_vertical_position" in list(gr.keys())):
            self.fairlead_vertical_position = gr["fairlead_vertical_position"][0]
        if ("fairlead_radius" in list(gr.keys())):
            self.fairlead_radius = gr["fairlead_radius"][0]
        if ("length_increment" in list(gr.keys())):
            self.length_increment = gr["length_increment"][0]
        if ("offset_max" in list(gr.keys())):
            self.offset_max = gr["offset_max"][0]
        if ("Tmin" in list(gr.keys())):
            self.Tmin = gr["Tmin"][0]
        if ("mooring_type" in list(gr.keys())):
            self.mooring_type = gr["mooring_type"][0].decode("ascii")
        if ("line_quality" in list(gr.keys())):
            self.line_quality = gr["line_quality"][0].decode("ascii")
        if ("safety_factor" in list(gr.keys())):
            self.safety_factor = gr["safety_factor"][0]
        if ("gamma_f" in list(gr.keys())):
            self.gamma_f = gr["gamma_f"][0]
        if ("gamma_f_fiber_rope" in list(gr.keys())):
            self.gamma_f_fiber_rope = gr["gamma_f_fiber_rope"][0]
        if ("nlines" in list(gr.keys())):
            self.nlines = gr["nlines"][0]
        if ("nlines_max" in list(gr.keys())):
            self.nlines_max = gr["nlines_max"][0]
        if ("n_years_lifetime" in list(gr.keys())):
            self.n_years_lifetime = gr["n_years_lifetime"][0]
        if ("mooring_output_folder_path" in list(gr.keys())):
            self.mooring_output_folder_path = gr["mooring_output_folder_path"][0].decode("ascii")
        if ("mooring_output_reference_system" in list(gr.keys())):
            self.mooring_output_reference_system = gr["mooring_output_reference_system"][0].decode("ascii")
        if ("ancillaries" in list(gr.keys())):
            subgroup = gr["ancillaries"]
            self.ancillaries.loadFromHDF5Handle(subgroup)
        if ("name" in list(gr.keys())):
            self.name = gr["name"][0].decode("ascii")
        if ("description" in list(gr.keys())):
            self.description = gr["description"][0].decode("ascii")
    #------------------------
    # is_set function
    #------------------------
    def is_set(self, varName): # pragma: no cover

        """Check if a given property of the object is set

        Args:
            varName (:obj:`str`): name of the property to check

        Returns:
            :obj:`bool`: True if the property is set, else False
        """

        if (isinstance(getattr(self,varName),list) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (isinstance(getattr(self,varName),np.ndarray) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (getattr(self,varName) != None):
            return True
        return False
    #------------------------
