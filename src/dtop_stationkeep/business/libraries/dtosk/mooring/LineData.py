# This is the Station Keeping module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Neil Luxcey, Nicolas Michelet, Rocio Isorna
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import collections
import numpy as np
import os
import json
import h5py
#------------------------------------
# @ USER DEFINED IMPORTS START
# @ USER DEFINED IMPORTS END
#------------------------------------

class LineData():

    """data model representing a line in Map++
    """

    #------------
    # Constructor
    #------------
    def __init__(self,name=None):
#------------------------------------
# @ USER DEFINED DESCRIPTION START
# @ USER DEFINED DESCRIPTION END
#------------------------------------

        self._line_number=0
        self._line_type_name=''
        self._line_type_index=0
        self._unstretched_length=0.0
        self._anchor_node_number=0
        self._fairlead_node_number=0
        self._flags=[]
        self._name='none'
        self._description=''
        if not(name == None): # pragma: no cover
            self._name = name

#------------------------------------
# @ USER DEFINED PROPERTIES START
# @ USER DEFINED PROPERTIES END
#------------------------------------

#------------------------------------
# @ USER DEFINED METHODS START
    def write_to_string_list(self):

        # Init list
        finput = []

        # Populate
        line='{}  {}  {: >6.2f}  {: >5.0f}  {: >5.0f} '.format(int(self.line_number),self.line_type_name,self.unstretched_length,self.anchor_node_number,self.fairlead_node_number)
        for idx in range(0,len(self.flags)):
            line = line + ' ' + self.flags[idx]
        finput.append(line)

        return finput

# @ USER DEFINED METHODS END
#------------------------------------

    #------------
    # Get functions
    #------------
    @ property
    def line_number(self): # pragma: no cover
        """int: Line ID number []
        """
        return self._line_number
    #------------
    @ property
    def line_type_name(self): # pragma: no cover
        """str: Line type name. Must be one type defined in the line_dictionnary
        """
        return self._line_type_name
    #------------
    @ property
    def line_type_index(self): # pragma: no cover
        """int: Line type index in the line_dictionnary []
        """
        return self._line_type_index
    #------------
    @ property
    def unstretched_length(self): # pragma: no cover
        """float: Unstretched line length. [m] []
        """
        return self._unstretched_length
    #------------
    @ property
    def anchor_node_number(self): # pragma: no cover
        """int: Anchor node number []
        """
        return self._anchor_node_number
    #------------
    @ property
    def fairlead_node_number(self): # pragma: no cover
        """int: Fairlead node number []
        """
        return self._fairlead_node_number
    #------------
    @ property
    def flags(self): # pragma: no cover
        """:obj:`list` of :obj:`str`:Line flag. Can be: GX_POS,GY_POS,GZ_POS,GX_A_POS,GY_A_POS,GZ_A_POS,GX_FORCE,GY_FORCE,GZ_FORCE,H_FAIR,H_ANCH,V_FAIR,V_ANCH,TENSION_FAIR,TENSION_ANCH,X_EXCURSION,Z_EXCURSION,AZIMUTH,ALTITUDE,ALTITUDE_ANCH,LINE_TENSION,OMIT_CONTACT,LINEAR_SPRING,LAY_LENGTH,DIAGNOSTIC,DAMAGE_TIME
        """
        return self._flags
    #------------
    @ property
    def name(self): # pragma: no cover
        """str: name of the instance object
        """
        return self._name
    #------------
    @ property
    def description(self): # pragma: no cover
        """str: description of the instance object
        """
        return self._description
    #------------
    #------------
    # Set functions
    #------------
    @ line_number.setter
    def line_number(self,val): # pragma: no cover
        self._line_number=int(val)
    #------------
    @ line_type_name.setter
    def line_type_name(self,val): # pragma: no cover
        self._line_type_name=str(val)
    #------------
    @ line_type_index.setter
    def line_type_index(self,val): # pragma: no cover
        self._line_type_index=int(val)
    #------------
    @ unstretched_length.setter
    def unstretched_length(self,val): # pragma: no cover
        self._unstretched_length=float(val)
    #------------
    @ anchor_node_number.setter
    def anchor_node_number(self,val): # pragma: no cover
        self._anchor_node_number=int(val)
    #------------
    @ fairlead_node_number.setter
    def fairlead_node_number(self,val): # pragma: no cover
        self._fairlead_node_number=int(val)
    #------------
    @ flags.setter
    def flags(self,val): # pragma: no cover
        self._flags=val
    #------------
    @ name.setter
    def name(self,val): # pragma: no cover
        self._name=str(val)
    #------------
    @ description.setter
    def description(self,val): # pragma: no cover
        self._description=str(val)
    #------------
    #-------------------------
    # Representation functions
    #-------------------------
    def type_rep(self): # pragma: no cover
        """Generate a representation of the object type

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains the representation of the object type
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtosk:mooring:LineData"
        rep["name"] = self.name
        rep["description"] = self.description
        return rep
    def prop_rep(self, short = False, deep = True):

        """Generate a representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains a representation of the object properties
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtosk:mooring:LineData"
        rep["name"] = self.name
        rep["description"] = self.description
        if self.is_set("line_number"):
            rep["line_number"] = self.line_number
        if self.is_set("line_type_name"):
            rep["line_type_name"] = self.line_type_name
        if self.is_set("line_type_index"):
            rep["line_type_index"] = self.line_type_index
        if self.is_set("unstretched_length"):
            rep["unstretched_length"] = self.unstretched_length
        if self.is_set("anchor_node_number"):
            rep["anchor_node_number"] = self.anchor_node_number
        if self.is_set("fairlead_node_number"):
            rep["fairlead_node_number"] = self.fairlead_node_number
        if self.is_set("flags"):
            if short:
                rep["flags"] = str(len(self.flags))
            else:
                rep["flags"] = self.flags
        else:
            rep["flags"] = []
        if self.is_set("name"):
            rep["name"] = self.name
        if self.is_set("description"):
            rep["description"] = self.description
        return rep
    #-------------------------
    # Save functions
    #-------------------------
    def json_rep(self, short=False, deep=True):

        """Generate a JSON representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`str`: string that contains a JSON representation of the object properties
        """

        return ( json.dumps(self.prop_rep(short=short, deep=deep),indent=4, separators=(",",": ")))
    def saveJSON(self, fileName=None):
        """Save the instance of the object to JSON format file

        Args:
            fileName (:obj:`str`, optional): Name of the JSON file, included extension. Defaults is None. If None, the name of the JSON file will be self.name.json. It can also contain an absolute or relative path.

        """

        if fileName==None:
            fileName=self.name + ".json"
        f=open(fileName, "w")
        f.write(self.json_rep())
        f.write("\n")
        f.close()

    def saveHDF5(self,filePath=None):
        """Save the instance of the object to HDF5 format file
        Args:
            filePath (:obj:`str`, optional): Name of the HDF5 file, included extension. Defaults is None. If None, the name of the HDF5 file will be "self.name".h5. It can also contain an absolute or relative path.
        """
        # Define filepath
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".h5"
            else:
                raise Exception("name is required for saving")
        # Open hdf5 file
        h = h5py.File(filePath,"w")
        group = h.create_group(self.name)
        # Save the object to the group
        self.saveToHDF5Handle(group)
        # Close file
        h.close()
        pass
    def saveToHDF5Handle(self, handle):
        """Save the properties of the object to the hdf5 handle.
        Args:
            handle (:obj:`h5py.Group`): Handle used to store the object properties
        """
        handle["line_number"] = np.array([self.line_number],dtype=int)
        ar = []
        ar.append(self.line_type_name.encode("ascii"))
        handle["line_type_name"] = np.asarray(ar)
        handle["line_type_index"] = np.array([self.line_type_index],dtype=int)
        handle["unstretched_length"] = np.array([self.unstretched_length],dtype=float)
        handle["anchor_node_number"] = np.array([self.anchor_node_number],dtype=int)
        handle["fairlead_node_number"] = np.array([self.fairlead_node_number],dtype=int)
        asciiList = [n.encode("ascii", "ignore") for n in self.flags]
        handle["flags"] = asciiList
        ar = []
        ar.append(self.name.encode("ascii"))
        handle["name"] = np.asarray(ar)
        ar = []
        ar.append(self.description.encode("ascii"))
        handle["description"] = np.asarray(ar)
    #-------------------------
    # Load functions
    #-------------------------
    def loadJSON(self,name = None, filePath = None):
        """Load an instance of the object from JSON format file

        Args:
            name (:obj:`str`, optional): Name of the object to load.
            filePath (:obj:`str`, optional): Path of the JSON file to load. If None, the function looks for a file with name "name".json.

        The JSON file must contain a datastructure representing an instance of this object's class, as generated by e.g. the function :func:`~saveJSON`.

        """

        if not(name == None):
            self.name = name
        if (name == None) and not(filePath == None):
            self.name = ".".join(filePath.split(os.path.sep)[-1].split(".")[0:-1])
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".json"
            else:
                raise Exception("object needs name for loading.")
        if not(os.path.isfile(filePath)):
            raise Exception("file %s not found."%filePath)
        self._loadedItems = []
        f = open(filePath,"r")
        data = f.read()
        f.close()
        dd = json.loads(data)
        self.loadFromJSONDict(dd)
    def loadFromJSONDict(self, data):
        """Load an instance of the object from a dictionnary representing a JSON structure)

        Args:
            name (:obj:`collections.OrderedDict`): Dictionnary containing a JSON structure, as produced by e.g. :func:`json.loads`

        """

        varName = "line_number"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "line_type_name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "line_type_index"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "unstretched_length"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "anchor_node_number"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "fairlead_node_number"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "flags"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "description"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
    def loadHDF5(self,name=None, filePath=None):
        """Load an instance of the object from HDF5 format file
        Args:
            name (:obj:`str`, optional): Name of the object to load. Default is None.
            filePath (:obj:`str`, optional): Path of the HDF5 file to load. If None, the function looks for a file with name "self.name".h5.
        The HDF5 file must contain a datastructure representing an instance of this objects class, as generated by e.g. the function :func:`~saveHDF5`.
        """
        # Define filepath and name
        if not(name == None):
            self.name = name
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".h5"
            else:
                raise Exception("name is required for loading")
        # Open hdf5 file
        h = h5py.File(filePath,"r")
        group = h[self.name]
        # Save the object to the group
        self.loadFromHDF5Handle(group)
        # Close file
        h.close()
        pass
    def loadFromHDF5Handle(self, gr):
        """Load the properties of the object from a hdf5 handle.
        Args:
            gr (:obj:`h5py.Group`): Handle used to read the object properties from
        """
        if ("line_number" in list(gr.keys())):
            self.line_number = gr["line_number"][0]
        if ("line_type_name" in list(gr.keys())):
            self.line_type_name = gr["line_type_name"][0].decode("ascii")
        if ("line_type_index" in list(gr.keys())):
            self.line_type_index = gr["line_type_index"][0]
        if ("unstretched_length" in list(gr.keys())):
            self.unstretched_length = gr["unstretched_length"][0]
        if ("anchor_node_number" in list(gr.keys())):
            self.anchor_node_number = gr["anchor_node_number"][0]
        if ("fairlead_node_number" in list(gr.keys())):
            self.fairlead_node_number = gr["fairlead_node_number"][0]
        if ("flags" in list(gr.keys())):
            self.flags = [n.decode("ascii", "ignore") for n in gr["flags"][:]]
        if ("name" in list(gr.keys())):
            self.name = gr["name"][0].decode("ascii")
        if ("description" in list(gr.keys())):
            self.description = gr["description"][0].decode("ascii")
    #------------------------
    # is_set function
    #------------------------
    def is_set(self, varName): # pragma: no cover

        """Check if a given property of the object is set

        Args:
            varName (:obj:`str`): name of the property to check

        Returns:
            :obj:`bool`: True if the property is set, else False
        """

        if (isinstance(getattr(self,varName),list) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (isinstance(getattr(self,varName),np.ndarray) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (getattr(self,varName) != None):
            return True
        return False
    #------------------------
