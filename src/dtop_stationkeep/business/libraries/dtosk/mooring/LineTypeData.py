# This is the Station Keeping module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Neil Luxcey, Nicolas Michelet, Rocio Isorna
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import collections
import numpy as np
import os
import json
import h5py
#------------------------------------
# @ USER DEFINED IMPORTS START
# @ USER DEFINED IMPORTS END
#------------------------------------

class LineTypeData():

    """data model representing a line type in Map++
    """

    #------------
    # Constructor
    #------------
    def __init__(self,name=None):
#------------------------------------
# @ USER DEFINED DESCRIPTION START
# @ USER DEFINED DESCRIPTION END
#------------------------------------

        self._line_type_name=''
        self._material='undefined'
        self._type=''
        self._quality=''
        self._catalogue_id='none'
        self._diameter=0.0
        self._diameter_fls=0.0
        self._weight_in_air=0.0
        self._weight_in_water=0.0
        self._ea=0.0
        self._cb=1.0
        self._mbl=0.0
        self._mbl_uls=0.0
        self._cost_per_meter=0.0
        self._ad=0.0
        self._m=0.0
        self._failure_rate_repair=0.0
        self._failure_rate_replacement=0.0
        self._name='none'
        self._description=''
        if not(name == None): # pragma: no cover
            self._name = name

#------------------------------------
# @ USER DEFINED PROPERTIES START
# @ USER DEFINED PROPERTIES END
#------------------------------------

#------------------------------------
# @ USER DEFINED METHODS START

    def compute_fls_area(self):
        # Fetch diameter_fls, if defined. Else use diameter new.
        if self.diameter_fls==0.0:
            self.diameter_fls=self.diameter
        diameter_fls = self.diameter_fls

        # Compute cross section area
        if self.type == 'chain':
            cross_section_area_fls = 2*np.pi*diameter_fls**2/4
        else:
            cross_section_area_fls = np.pi*diameter_fls**2/4

        return cross_section_area_fls, diameter_fls

    def define_from_catalogue_index(self,catalogue,idx):
        self.line_type_name = catalogue.catalogue_id[idx]
        self.material = catalogue.material[idx]
        self.type = catalogue.type[idx]
        self.quality = catalogue.quality[idx]
        self.catalogue_id = catalogue.catalogue_id[idx]
        self.diameter = catalogue.diameter[idx]
        self.weight_in_air = catalogue.weight_in_air[idx]
        self.weight_in_water = catalogue.weight_in_water[idx]
        self.ea = catalogue.ea[idx]
        self.cb=1.0
        self.mbl = catalogue.mbl[idx]
        self.cost_per_meter = catalogue.cost_per_meter[idx]
        self.ad = catalogue.ad[idx]
        self.m = catalogue.m[idx]
        self.failure_rate_repair = catalogue.failure_rate_repair[idx]
        self.failure_rate_replacement = catalogue.failure_rate_replacement[idx]
        self.name = catalogue.catalogue_id[idx]
        self.description='line type defined from catalogue'

    def equivalent_diameter(self, sea_water_density = None):

        # Compute equivalent line diameter that will be used in map++ to compute buoyancy
        if sea_water_density is None:
            sea_water_density = 1025.0

        equivalent_line_diameter = (4*(self.weight_in_air - self.weight_in_water)/(np.pi*sea_water_density))**0.5

        return equivalent_line_diameter

    def write_to_string_list(self):

        # Init list
        finput = []

        # Compute equivalent diameter (map++ uses weight_in_air and equivalent diameter to compute buoyancy)
        d = self.equivalent_diameter()

        # Unused parameters
        c_int_damp = 0.0
        ca = 0.0
        cdn = 0.0
        cdt = 0.0

        # Populate
        finput.append('{}  {: >8.4f}  {: >8.2f}  {: >8.2f}  {: >8.2f}  {: >8.2f}  {: >8.2f}  {: >8.2f}  {: >8.2f}'.format(self.line_type_name, d ,self.weight_in_air,self.ea,self.cb,c_int_damp,ca,cdn,cdt))

        return finput

# @ USER DEFINED METHODS END
#------------------------------------

    #------------
    # Get functions
    #------------
    @ property
    def line_type_name(self): # pragma: no cover
        """str: Line type name
        """
        return self._line_type_name
    #------------
    @ property
    def material(self): # pragma: no cover
        """str: Material of the line
        """
        return self._material
    #------------
    @ property
    def type(self): # pragma: no cover
        """str: Type of line: chain, rope, polyester
        """
        return self._type
    #------------
    @ property
    def quality(self): # pragma: no cover
        """str: line quality (used for chain) : R3,R4
        """
        return self._quality
    #------------
    @ property
    def catalogue_id(self): # pragma: no cover
        """str: Catalogue id
        """
        return self._catalogue_id
    #------------
    @ property
    def diameter(self): # pragma: no cover
        """float: Line diameter [m]. For chains: nominal diameter. []
        """
        return self._diameter
    #------------
    @ property
    def diameter_fls(self): # pragma: no cover
        """float: Line diameter used for stress computation in FLS analysis [m]. Used to take into account the effect of corrosion. For chains: nominal diameter. []
        """
        return self._diameter_fls
    #------------
    @ property
    def weight_in_air(self): # pragma: no cover
        """float: Mass density in air [kg/m] []
        """
        return self._weight_in_air
    #------------
    @ property
    def weight_in_water(self): # pragma: no cover
        """float: Mass density in water [kg/m] []
        """
        return self._weight_in_water
    #------------
    @ property
    def ea(self): # pragma: no cover
        """float: Element axial stiffness [N] []
        """
        return self._ea
    #------------
    @ property
    def cb(self): # pragma: no cover
        """float: Cable/seabed friction coefficient [-] []
        """
        return self._cb
    #------------
    @ property
    def mbl(self): # pragma: no cover
        """float: Breaking load [N] []
        """
        return self._mbl
    #------------
    @ property
    def mbl_uls(self): # pragma: no cover
        """float: Minimum Breaking load used for ULS analysis[N]. Used to take into account the effect of corrosion. []
        """
        return self._mbl_uls
    #------------
    @ property
    def cost_per_meter(self): # pragma: no cover
        """float: Cost per meter in euros []
        """
        return self._cost_per_meter
    #------------
    @ property
    def ad(self): # pragma: no cover
        """float: intercept parameter of the S-N curve, where the stress is given in [MPa] []
        """
        return self._ad
    #------------
    @ property
    def m(self): # pragma: no cover
        """float: slope of the S-N curve []
        """
        return self._m
    #------------
    @ property
    def failure_rate_repair(self): # pragma: no cover
        """double: failure rate repair of the line
        """
        return self._failure_rate_repair
    #------------
    @ property
    def failure_rate_replacement(self): # pragma: no cover
        """double: failure rate replacement of the line
        """
        return self._failure_rate_replacement
    #------------
    @ property
    def name(self): # pragma: no cover
        """str: name of the instance object
        """
        return self._name
    #------------
    @ property
    def description(self): # pragma: no cover
        """str: description of the instance object
        """
        return self._description
    #------------
    #------------
    # Set functions
    #------------
    @ line_type_name.setter
    def line_type_name(self,val): # pragma: no cover
        self._line_type_name=str(val)
    #------------
    @ material.setter
    def material(self,val): # pragma: no cover
        self._material=str(val)
    #------------
    @ type.setter
    def type(self,val): # pragma: no cover
        self._type=str(val)
    #------------
    @ quality.setter
    def quality(self,val): # pragma: no cover
        self._quality=str(val)
    #------------
    @ catalogue_id.setter
    def catalogue_id(self,val): # pragma: no cover
        self._catalogue_id=str(val)
    #------------
    @ diameter.setter
    def diameter(self,val): # pragma: no cover
        self._diameter=float(val)
    #------------
    @ diameter_fls.setter
    def diameter_fls(self,val): # pragma: no cover
        self._diameter_fls=float(val)
    #------------
    @ weight_in_air.setter
    def weight_in_air(self,val): # pragma: no cover
        self._weight_in_air=float(val)
    #------------
    @ weight_in_water.setter
    def weight_in_water(self,val): # pragma: no cover
        self._weight_in_water=float(val)
    #------------
    @ ea.setter
    def ea(self,val): # pragma: no cover
        self._ea=float(val)
    #------------
    @ cb.setter
    def cb(self,val): # pragma: no cover
        self._cb=float(val)
    #------------
    @ mbl.setter
    def mbl(self,val): # pragma: no cover
        self._mbl=float(val)
    #------------
    @ mbl_uls.setter
    def mbl_uls(self,val): # pragma: no cover
        self._mbl_uls=float(val)
    #------------
    @ cost_per_meter.setter
    def cost_per_meter(self,val): # pragma: no cover
        self._cost_per_meter=float(val)
    #------------
    @ ad.setter
    def ad(self,val): # pragma: no cover
        self._ad=float(val)
    #------------
    @ m.setter
    def m(self,val): # pragma: no cover
        self._m=float(val)
    #------------
    @ failure_rate_repair.setter
    def failure_rate_repair(self,val): # pragma: no cover
        self._failure_rate_repair=float(val)
    #------------
    @ failure_rate_replacement.setter
    def failure_rate_replacement(self,val): # pragma: no cover
        self._failure_rate_replacement=float(val)
    #------------
    @ name.setter
    def name(self,val): # pragma: no cover
        self._name=str(val)
    #------------
    @ description.setter
    def description(self,val): # pragma: no cover
        self._description=str(val)
    #------------
    #-------------------------
    # Representation functions
    #-------------------------
    def type_rep(self): # pragma: no cover
        """Generate a representation of the object type

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains the representation of the object type
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtosk:mooring:LineTypeData"
        rep["name"] = self.name
        rep["description"] = self.description
        return rep
    def prop_rep(self, short = False, deep = True):

        """Generate a representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains a representation of the object properties
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtosk:mooring:LineTypeData"
        rep["name"] = self.name
        rep["description"] = self.description
        if self.is_set("line_type_name"):
            rep["line_type_name"] = self.line_type_name
        if self.is_set("material"):
            rep["material"] = self.material
        if self.is_set("type"):
            rep["type"] = self.type
        if self.is_set("quality"):
            rep["quality"] = self.quality
        if self.is_set("catalogue_id"):
            rep["catalogue_id"] = self.catalogue_id
        if self.is_set("diameter"):
            rep["diameter"] = self.diameter
        if self.is_set("diameter_fls"):
            rep["diameter_fls"] = self.diameter_fls
        if self.is_set("weight_in_air"):
            rep["weight_in_air"] = self.weight_in_air
        if self.is_set("weight_in_water"):
            rep["weight_in_water"] = self.weight_in_water
        if self.is_set("ea"):
            rep["ea"] = self.ea
        if self.is_set("cb"):
            rep["cb"] = self.cb
        if self.is_set("mbl"):
            rep["mbl"] = self.mbl
        if self.is_set("mbl_uls"):
            rep["mbl_uls"] = self.mbl_uls
        if self.is_set("cost_per_meter"):
            rep["cost_per_meter"] = self.cost_per_meter
        if self.is_set("ad"):
            rep["ad"] = self.ad
        if self.is_set("m"):
            rep["m"] = self.m
        if self.is_set("failure_rate_repair"):
            rep["failure_rate_repair"] = self.failure_rate_repair
        if self.is_set("failure_rate_replacement"):
            rep["failue_rate_replacement"] = self.failure_rate_replacement
        if self.is_set("name"):
            rep["name"] = self.name
        if self.is_set("description"):
            rep["description"] = self.description
        return rep
    #-------------------------
    # Save functions
    #-------------------------
    def json_rep(self, short=False, deep=True):

        """Generate a JSON representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`str`: string that contains a JSON representation of the object properties
        """

        return ( json.dumps(self.prop_rep(short=short, deep=deep),indent=4, separators=(",",": ")))
    def saveJSON(self, fileName=None):
        """Save the instance of the object to JSON format file

        Args:
            fileName (:obj:`str`, optional): Name of the JSON file, included extension. Defaults is None. If None, the name of the JSON file will be self.name.json. It can also contain an absolute or relative path.

        """

        if fileName==None:
            fileName=self.name + ".json"
        f=open(fileName, "w")
        f.write(self.json_rep())
        f.write("\n")
        f.close()

    def saveHDF5(self,filePath=None):
        """Save the instance of the object to HDF5 format file
        Args:
            filePath (:obj:`str`, optional): Name of the HDF5 file, included extension. Defaults is None. If None, the name of the HDF5 file will be "self.name".h5. It can also contain an absolute or relative path.
        """
        # Define filepath
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".h5"
            else:
                raise Exception("name is required for saving")
        # Open hdf5 file
        h = h5py.File(filePath,"w")
        group = h.create_group(self.name)
        # Save the object to the group
        self.saveToHDF5Handle(group)
        # Close file
        h.close()
        pass
    def saveToHDF5Handle(self, handle):
        """Save the properties of the object to the hdf5 handle.
        Args:
            handle (:obj:`h5py.Group`): Handle used to store the object properties
        """
        ar = []
        ar.append(self.line_type_name.encode("ascii"))
        handle["line_type_name"] = np.asarray(ar)
        ar = []
        ar.append(self.material.encode("ascii"))
        handle["material"] = np.asarray(ar)
        ar = []
        ar.append(self.type.encode("ascii"))
        handle["type"] = np.asarray(ar)
        ar = []
        ar.append(self.quality.encode("ascii"))
        handle["quality"] = np.asarray(ar)
        ar = []
        ar.append(self.catalogue_id.encode("ascii"))
        handle["catalogue_id"] = np.asarray(ar)
        handle["diameter"] = np.array([self.diameter],dtype=float)
        handle["diameter_fls"] = np.array([self.diameter_fls],dtype=float)
        handle["weight_in_air"] = np.array([self.weight_in_air],dtype=float)
        handle["weight_in_water"] = np.array([self.weight_in_water],dtype=float)
        handle["ea"] = np.array([self.ea],dtype=float)
        handle["cb"] = np.array([self.cb],dtype=float)
        handle["mbl"] = np.array([self.mbl],dtype=float)
        handle["mbl_uls"] = np.array([self.mbl_uls],dtype=float)
        handle["cost_per_meter"] = np.array([self.cost_per_meter],dtype=float)
        handle["ad"] = np.array([self.ad],dtype=float)
        handle["m"] = np.array([self.m],dtype=float)
        handle["failure_rate_repair"] = np.array([self.failure_rate_repair],dtype=float)
        handle["failure_rate_replacement"] = np.array([self.failure_rate_replacement],dtype=float)
        ar = []
        ar.append(self.name.encode("ascii"))
        handle["name"] = np.asarray(ar)
        ar = []
        ar.append(self.description.encode("ascii"))
        handle["description"] = np.asarray(ar)
    #-------------------------
    # Load functions
    #-------------------------
    def loadJSON(self,name = None, filePath = None):
        """Load an instance of the object from JSON format file

        Args:
            name (:obj:`str`, optional): Name of the object to load.
            filePath (:obj:`str`, optional): Path of the JSON file to load. If None, the function looks for a file with name "name".json.

        The JSON file must contain a datastructure representing an instance of this object's class, as generated by e.g. the function :func:`~saveJSON`.

        """

        if not(name == None):
            self.name = name
        if (name == None) and not(filePath == None):
            self.name = ".".join(filePath.split(os.path.sep)[-1].split(".")[0:-1])
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".json"
            else:
                raise Exception("object needs name for loading.")
        if not(os.path.isfile(filePath)):
            raise Exception("file %s not found."%filePath)
        self._loadedItems = []
        f = open(filePath,"r")
        data = f.read()
        f.close()
        dd = json.loads(data)
        self.loadFromJSONDict(dd)
    def loadFromJSONDict(self, data):
        """Load an instance of the object from a dictionnary representing a JSON structure)

        Args:
            name (:obj:`collections.OrderedDict`): Dictionnary containing a JSON structure, as produced by e.g. :func:`json.loads`

        """

        varName = "line_type_name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "material"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "type"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "quality"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "catalogue_id"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "diameter"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "diameter_fls"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "weight_in_air"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "weight_in_water"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "ea"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "cb"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "mbl"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "mbl_uls"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "cost_per_meter"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "ad"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "m"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "failure_rate_repair"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "failure_rate_replacement"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "description"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
    def loadHDF5(self,name=None, filePath=None):
        """Load an instance of the object from HDF5 format file
        Args:
            name (:obj:`str`, optional): Name of the object to load. Default is None.
            filePath (:obj:`str`, optional): Path of the HDF5 file to load. If None, the function looks for a file with name "self.name".h5.
        The HDF5 file must contain a datastructure representing an instance of this objects class, as generated by e.g. the function :func:`~saveHDF5`.
        """
        # Define filepath and name
        if not(name == None):
            self.name = name
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".h5"
            else:
                raise Exception("name is required for loading")
        # Open hdf5 file
        h = h5py.File(filePath,"r")
        group = h[self.name]
        # Save the object to the group
        self.loadFromHDF5Handle(group)
        # Close file
        h.close()
        pass
    def loadFromHDF5Handle(self, gr):
        """Load the properties of the object from a hdf5 handle.
        Args:
            gr (:obj:`h5py.Group`): Handle used to read the object properties from
        """
        if ("line_type_name" in list(gr.keys())):
            self.line_type_name = gr["line_type_name"][0].decode("ascii")
        if ("material" in list(gr.keys())):
            self.material = gr["material"][0].decode("ascii")
        if ("type" in list(gr.keys())):
            self.type = gr["type"][0].decode("ascii")
        if ("quality" in list(gr.keys())):
            self.quality = gr["quality"][0].decode("ascii")
        if ("catalogue_id" in list(gr.keys())):
            self.catalogue_id = gr["catalogue_id"][0].decode("ascii")
        if ("diameter" in list(gr.keys())):
            self.diameter = gr["diameter"][0]
        if ("diameter_fls" in list(gr.keys())):
            self.diameter_fls = gr["diameter_fls"][0]
        if ("weight_in_air" in list(gr.keys())):
            self.weight_in_air = gr["weight_in_air"][0]
        if ("weight_in_water" in list(gr.keys())):
            self.weight_in_water = gr["weight_in_water"][0]
        if ("ea" in list(gr.keys())):
            self.ea = gr["ea"][0]
        if ("cb" in list(gr.keys())):
            self.cb = gr["cb"][0]
        if ("mbl" in list(gr.keys())):
            self.mbl = gr["mbl"][0]
        if ("mbl_uls" in list(gr.keys())):
            self.mbl_uls = gr["mbl_uls"][0]
        if ("cost_per_meter" in list(gr.keys())):
            self.cost_per_meter = gr["cost_per_meter"][0]
        if ("ad" in list(gr.keys())):
            self.ad = gr["ad"][0]
        if ("m" in list(gr.keys())):
            self.m = gr["m"][0]
        if ("failure_rate_repair" in list(gr.keys())):
            self.failure_rate_repair = gr["failure_rate_repair"][0]
        if ("failure_rate_replacement" in list(gr.keys())):
            self.failure_rate_replacement = gr["failure_rate_replacement"][0]
        if ("name" in list(gr.keys())):
            self.name = gr["name"][0].decode("ascii")
        if ("description" in list(gr.keys())):
            self.description = gr["description"][0].decode("ascii")
    #------------------------
    # is_set function
    #------------------------
    def is_set(self, varName): # pragma: no cover

        """Check if a given property of the object is set

        Args:
            varName (:obj:`str`): name of the property to check

        Returns:
            :obj:`bool`: True if the property is set, else False
        """

        if (isinstance(getattr(self,varName),list) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (isinstance(getattr(self,varName),np.ndarray) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (getattr(self,varName) != None):
            return True
        return False
    #------------------------
