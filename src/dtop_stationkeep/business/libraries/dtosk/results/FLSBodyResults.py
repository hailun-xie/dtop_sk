# This is the Station Keeping module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Neil Luxcey, Nicolas Michelet, Rocio Isorna
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import collections
import numpy as np
import os
import json
import h5py
#------------------------------------
# @ USER DEFINED IMPORTS START
# @ USER DEFINED IMPORTS END
#------------------------------------

class FLSBodyResults():

    """data model representing FLS results for one body
    """

    #------------
    # Constructor
    #------------
    def __init__(self,name=None):
#------------------------------------
# @ USER DEFINED DESCRIPTION START
# @ USER DEFINED DESCRIPTION END
#------------------------------------

        self._mooring_pretension=np.zeros(shape=(1,1), dtype=float)
        self._total_offset=np.zeros(shape=(1), dtype=float)
        self._total_position=np.zeros(shape=(1,1), dtype=float)
        self._steady_position=np.zeros(shape=(1,1), dtype=float)
        self._steady_force=np.zeros(shape=(1,1), dtype=float)
        self._steady_offset=np.zeros(shape=(1), dtype=float)
        self._steady_mooring_force=np.zeros(shape=(1,1,1,1), dtype=float)
        self._steady_mooring_tension=np.zeros(shape=(1,1,1), dtype=float)
        self._dynamic_offset=np.zeros(shape=(1), dtype=float)
        self._motion_spectrum=np.zeros(shape=(1,1,1), dtype=float)
        self._omega=np.zeros(shape=(1), dtype=float)
        self._damping_matrix=np.zeros(shape=(1,1), dtype=float)
        self._mooring_force=np.zeros(shape=(1,1,1,1), dtype=float)
        self._mooring_tension=np.zeros(shape=(1,1,1), dtype=float)
        self._mooring_touchdown=np.zeros(shape=(1,1), dtype=float)
        self._env_damage_lifetime=np.zeros(shape=(1,1), dtype=float)
        self._env_damage_one_year=np.zeros(shape=(1,1), dtype=float)
        self._damage_lifetime=np.zeros(shape=(1), dtype=float)
        self._damage_one_year=np.zeros(shape=(1), dtype=float)
        self._fls_criteria_1=np.zeros(shape=(1), dtype=float)
        self._fls_criteria_2=np.zeros(shape=(1), dtype=float)
        self._env_proba=np.zeros(shape=(1), dtype=float)
        self._env_stress_std=np.zeros(shape=(1,1), dtype=float)
        self._env_stress_range=np.zeros(shape=(1,1), dtype=float)
        self._cdf_stress_range=np.zeros(shape=(1,1), dtype=float)
        self._cdf=np.zeros(shape=(1,1), dtype=float)
        self._ad=np.zeros(shape=(1), dtype=float)
        self._m=np.zeros(shape=(1), dtype=float)
        self._n_cycles_lifetime=np.zeros(shape=(1), dtype=float)
        self._gamma_f=np.zeros(shape=(1), dtype=float)
        self._n_years_lifetime=0.0
        self._name='none'
        self._description=''
        if not(name == None): # pragma: no cover
            self._name = name

#------------------------------------
# @ USER DEFINED PROPERTIES START
# @ USER DEFINED PROPERTIES END
#------------------------------------

#------------------------------------
# @ USER DEFINED METHODS START
# @ USER DEFINED METHODS END
#------------------------------------

    #------------
    # Get functions
    #------------
    @ property
    def mooring_pretension(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:shape=(nlines,2) dim(*,*) []
        """
        return self._mooring_pretension
    #------------
    @ property
    def total_offset(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:shape=(len(environment_array)) dim(*) []
        """
        return self._total_offset
    #------------
    @ property
    def total_position(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:shape=(len(environment_array),6) dim(*,*) []
        """
        return self._total_position
    #------------
    @ property
    def steady_position(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:shape=(len(environment_array),6) dim(*,*) []
        """
        return self._steady_position
    #------------
    @ property
    def steady_force(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:shape=(len(environment_array),6) dim(*,*) []
        """
        return self._steady_force
    #------------
    @ property
    def steady_offset(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:shape=(len(environment_array)) dim(*) []
        """
        return self._steady_offset
    #------------
    @ property
    def steady_mooring_force(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:shape=(len(environment_array),len(body.mooring_system_force.line_properties),2,3) dim(*,*,*,*) []
        """
        return self._steady_mooring_force
    #------------
    @ property
    def steady_mooring_tension(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:shape=(len(environment_array),len(body.mooring_system_force.line_properties),2) dim(*,*,*) []
        """
        return self._steady_mooring_tension
    #------------
    @ property
    def dynamic_offset(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:shape=(len(environment_array)) dim(*) []
        """
        return self._dynamic_offset
    #------------
    @ property
    def motion_spectrum(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:shape=(len(environment_array),n_omega,6) dim(*,*,*) []
        """
        return self._motion_spectrum
    #------------
    @ property
    def omega(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:shape=(n_omega) dim(*) []
        """
        return self._omega
    #------------
    @ property
    def damping_matrix(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:shape=(len(environment_array),6) dim(*,*) []
        """
        return self._damping_matrix
    #------------
    @ property
    def mooring_force(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:shape=(len(environment_array),len(body.mooring_system_force.line_properties),2,3) dim(*,*,*,*) []
        """
        return self._mooring_force
    #------------
    @ property
    def mooring_tension(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:shape=(len(environment_array),len(body.mooring_system_force.line_properties),2) dim(*,*,*) []
        """
        return self._mooring_tension
    #------------
    @ property
    def mooring_touchdown(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:shape=(len(environment_array),len(body.mooring_system_force.line_properties)) dim(*,*) []
        """
        return self._mooring_touchdown
    #------------
    @ property
    def env_damage_lifetime(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:shape=(len(environment_array),len(body.mooring_system_force.line_properties)) dim(*,*) []
        """
        return self._env_damage_lifetime
    #------------
    @ property
    def env_damage_one_year(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:shape=(len(environment_array),len(body.mooring_system_force.line_properties)) dim(*,*) []
        """
        return self._env_damage_one_year
    #------------
    @ property
    def damage_lifetime(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:shape=(len(body.mooring_system_force.line_properties)) dim(*) []
        """
        return self._damage_lifetime
    #------------
    @ property
    def damage_one_year(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:shape=(len(body.mooring_system_force.line_properties)) dim(*) []
        """
        return self._damage_one_year
    #------------
    @ property
    def fls_criteria_1(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`: 1-gamma*dc shape=(len(body.mooring_system_force.line_properties)) dim(*) []
        """
        return self._fls_criteria_1
    #------------
    @ property
    def fls_criteria_2(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:1/dc shape=(len(body.mooring_system_force.line_properties)) dim(*) []
        """
        return self._fls_criteria_2
    #------------
    @ property
    def env_proba(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:shape=(len(environment_array)) dim(*) []
        """
        return self._env_proba
    #------------
    @ property
    def env_stress_std(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:shape=(len(environment_array),len(body.mooring_system_force.line_properties)) dim(*,*) []
        """
        return self._env_stress_std
    #------------
    @ property
    def env_stress_range(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:shape=(len(environment_array),len(body.mooring_system_force.line_properties)) dim(*,*) []
        """
        return self._env_stress_range
    #------------
    @ property
    def cdf_stress_range(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:shape=(len(stress_range),len(body.mooring_system_force.line_properties)) dim(*,*) []
        """
        return self._cdf_stress_range
    #------------
    @ property
    def cdf(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:shape=(len(stress_range),len(body.mooring_system_force.line_properties)) dim(*,*) []
        """
        return self._cdf
    #------------
    @ property
    def ad(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`: ad value of SN-curve shape=(len(body.mooring_system_force.line_properties)) dim(*) []
        """
        return self._ad
    #------------
    @ property
    def m(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`: m value of SN-curve shape=(len(body.mooring_system_force.line_properties)) dim(*) []
        """
        return self._m
    #------------
    @ property
    def n_cycles_lifetime(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`: Number of stress cycles during lifetime shape=(len(body.mooring_system_force.line_properties)) dim(*) []
        """
        return self._n_cycles_lifetime
    #------------
    @ property
    def gamma_f(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:Safety factor for fatigue analysis, for each line segment dim(*) []
        """
        return self._gamma_f
    #------------
    @ property
    def n_years_lifetime(self): # pragma: no cover
        """float: Lifetime of the mooring system in years []
        """
        return self._n_years_lifetime
    #------------
    @ property
    def name(self): # pragma: no cover
        """str: name of the instance object
        """
        return self._name
    #------------
    @ property
    def description(self): # pragma: no cover
        """str: description of the instance object
        """
        return self._description
    #------------
    #------------
    # Set functions
    #------------
    @ mooring_pretension.setter
    def mooring_pretension(self,val): # pragma: no cover
        self._mooring_pretension=val
    #------------
    @ total_offset.setter
    def total_offset(self,val): # pragma: no cover
        self._total_offset=val
    #------------
    @ total_position.setter
    def total_position(self,val): # pragma: no cover
        self._total_position=val
    #------------
    @ steady_position.setter
    def steady_position(self,val): # pragma: no cover
        self._steady_position=val
    #------------
    @ steady_force.setter
    def steady_force(self,val): # pragma: no cover
        self._steady_force=val
    #------------
    @ steady_offset.setter
    def steady_offset(self,val): # pragma: no cover
        self._steady_offset=val
    #------------
    @ steady_mooring_force.setter
    def steady_mooring_force(self,val): # pragma: no cover
        self._steady_mooring_force=val
    #------------
    @ steady_mooring_tension.setter
    def steady_mooring_tension(self,val): # pragma: no cover
        self._steady_mooring_tension=val
    #------------
    @ dynamic_offset.setter
    def dynamic_offset(self,val): # pragma: no cover
        self._dynamic_offset=val
    #------------
    @ motion_spectrum.setter
    def motion_spectrum(self,val): # pragma: no cover
        self._motion_spectrum=val
    #------------
    @ omega.setter
    def omega(self,val): # pragma: no cover
        self._omega=val
    #------------
    @ damping_matrix.setter
    def damping_matrix(self,val): # pragma: no cover
        self._damping_matrix=val
    #------------
    @ mooring_force.setter
    def mooring_force(self,val): # pragma: no cover
        self._mooring_force=val
    #------------
    @ mooring_tension.setter
    def mooring_tension(self,val): # pragma: no cover
        self._mooring_tension=val
    #------------
    @ mooring_touchdown.setter
    def mooring_touchdown(self,val): # pragma: no cover
        self._mooring_touchdown=val
    #------------
    @ env_damage_lifetime.setter
    def env_damage_lifetime(self,val): # pragma: no cover
        self._env_damage_lifetime=val
    #------------
    @ env_damage_one_year.setter
    def env_damage_one_year(self,val): # pragma: no cover
        self._env_damage_one_year=val
    #------------
    @ damage_lifetime.setter
    def damage_lifetime(self,val): # pragma: no cover
        self._damage_lifetime=val
    #------------
    @ damage_one_year.setter
    def damage_one_year(self,val): # pragma: no cover
        self._damage_one_year=val
    #------------
    @ fls_criteria_1.setter
    def fls_criteria_1(self,val): # pragma: no cover
        self._fls_criteria_1=val
    #------------
    @ fls_criteria_2.setter
    def fls_criteria_2(self,val): # pragma: no cover
        self._fls_criteria_2=val
    #------------
    @ env_proba.setter
    def env_proba(self,val): # pragma: no cover
        self._env_proba=val
    #------------
    @ env_stress_std.setter
    def env_stress_std(self,val): # pragma: no cover
        self._env_stress_std=val
    #------------
    @ env_stress_range.setter
    def env_stress_range(self,val): # pragma: no cover
        self._env_stress_range=val
    #------------
    @ cdf_stress_range.setter
    def cdf_stress_range(self,val): # pragma: no cover
        self._cdf_stress_range=val
    #------------
    @ cdf.setter
    def cdf(self,val): # pragma: no cover
        self._cdf=val
    #------------
    @ ad.setter
    def ad(self,val): # pragma: no cover
        self._ad=val
    #------------
    @ m.setter
    def m(self,val): # pragma: no cover
        self._m=val
    #------------
    @ n_cycles_lifetime.setter
    def n_cycles_lifetime(self,val): # pragma: no cover
        self._n_cycles_lifetime=val
    #------------
    @ gamma_f.setter
    def gamma_f(self,val): # pragma: no cover
        self._gamma_f=val
    #------------
    @ n_years_lifetime.setter
    def n_years_lifetime(self,val): # pragma: no cover
        self._n_years_lifetime=float(val)
    #------------
    @ name.setter
    def name(self,val): # pragma: no cover
        self._name=str(val)
    #------------
    @ description.setter
    def description(self,val): # pragma: no cover
        self._description=str(val)
    #------------
    #-------------------------
    # Representation functions
    #-------------------------
    def type_rep(self): # pragma: no cover
        """Generate a representation of the object type

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains the representation of the object type
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtosk:results:FLSBodyResults"
        rep["name"] = self.name
        rep["description"] = self.description
        return rep
    def prop_rep(self, short = False, deep = True):

        """Generate a representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains a representation of the object properties
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtosk:results:FLSBodyResults"
        rep["name"] = self.name
        rep["description"] = self.description
        if self.is_set("mooring_pretension"):
            if (short):
                rep["mooring_pretension"] = str(self.mooring_pretension.shape)
            else:
                try:
                    rep["mooring_pretension"] = self.mooring_pretension.tolist()
                except:
                    rep["mooring_pretension"] = self.mooring_pretension
        if self.is_set("total_offset"):
            if (short):
                rep["total_offset"] = str(self.total_offset.shape)
            else:
                try:
                    rep["total_offset"] = self.total_offset.tolist()
                except:
                    rep["total_offset"] = self.total_offset
        if self.is_set("total_position"):
            if (short):
                rep["total_position"] = str(self.total_position.shape)
            else:
                try:
                    rep["total_position"] = self.total_position.tolist()
                except:
                    rep["total_position"] = self.total_position
        if self.is_set("steady_position"):
            if (short):
                rep["steady_position"] = str(self.steady_position.shape)
            else:
                try:
                    rep["steady_position"] = self.steady_position.tolist()
                except:
                    rep["steady_position"] = self.steady_position
        if self.is_set("steady_force"):
            if (short):
                rep["steady_force"] = str(self.steady_force.shape)
            else:
                try:
                    rep["steady_force"] = self.steady_force.tolist()
                except:
                    rep["steady_force"] = self.steady_force
        if self.is_set("steady_offset"):
            if (short):
                rep["steady_offset"] = str(self.steady_offset.shape)
            else:
                try:
                    rep["steady_offset"] = self.steady_offset.tolist()
                except:
                    rep["steady_offset"] = self.steady_offset
        if self.is_set("steady_mooring_force"):
            if (short):
                rep["steady_mooring_force"] = str(self.steady_mooring_force.shape)
            else:
                try:
                    rep["steady_mooring_force"] = self.steady_mooring_force.tolist()
                except:
                    rep["steady_mooring_force"] = self.steady_mooring_force
        if self.is_set("steady_mooring_tension"):
            if (short):
                rep["steady_mooring_tension"] = str(self.steady_mooring_tension.shape)
            else:
                try:
                    rep["steady_mooring_tension"] = self.steady_mooring_tension.tolist()
                except:
                    rep["steady_mooring_tension"] = self.steady_mooring_tension
        if self.is_set("dynamic_offset"):
            if (short):
                rep["dynamic_offset"] = str(self.dynamic_offset.shape)
            else:
                try:
                    rep["dynamic_offset"] = self.dynamic_offset.tolist()
                except:
                    rep["dynamic_offset"] = self.dynamic_offset
        if self.is_set("motion_spectrum"):
            if (short):
                rep["motion_spectrum"] = str(self.motion_spectrum.shape)
            else:
                try:
                    rep["motion_spectrum"] = self.motion_spectrum.tolist()
                except:
                    rep["motion_spectrum"] = self.motion_spectrum
        if self.is_set("omega"):
            if (short):
                rep["omega"] = str(self.omega.shape)
            else:
                try:
                    rep["omega"] = self.omega.tolist()
                except:
                    rep["omega"] = self.omega
        if self.is_set("damping_matrix"):
            if (short):
                rep["damping_matrix"] = str(self.damping_matrix.shape)
            else:
                try:
                    rep["damping_matrix"] = self.damping_matrix.tolist()
                except:
                    rep["damping_matrix"] = self.damping_matrix
        if self.is_set("mooring_force"):
            if (short):
                rep["mooring_force"] = str(self.mooring_force.shape)
            else:
                try:
                    rep["mooring_force"] = self.mooring_force.tolist()
                except:
                    rep["mooring_force"] = self.mooring_force
        if self.is_set("mooring_tension"):
            if (short):
                rep["mooring_tension"] = str(self.mooring_tension.shape)
            else:
                try:
                    rep["mooring_tension"] = self.mooring_tension.tolist()
                except:
                    rep["mooring_tension"] = self.mooring_tension
        if self.is_set("mooring_touchdown"):
            if (short):
                rep["mooring_touchdown"] = str(self.mooring_touchdown.shape)
            else:
                try:
                    rep["mooring_touchdown"] = self.mooring_touchdown.tolist()
                except:
                    rep["mooring_touchdown"] = self.mooring_touchdown
        if self.is_set("env_damage_lifetime"):
            if (short):
                rep["env_damage_lifetime"] = str(self.env_damage_lifetime.shape)
            else:
                try:
                    rep["env_damage_lifetime"] = self.env_damage_lifetime.tolist()
                except:
                    rep["env_damage_lifetime"] = self.env_damage_lifetime
        if self.is_set("env_damage_one_year"):
            if (short):
                rep["env_damage_one_year"] = str(self.env_damage_one_year.shape)
            else:
                try:
                    rep["env_damage_one_year"] = self.env_damage_one_year.tolist()
                except:
                    rep["env_damage_one_year"] = self.env_damage_one_year
        if self.is_set("damage_lifetime"):
            if (short):
                rep["damage_lifetime"] = str(self.damage_lifetime.shape)
            else:
                try:
                    rep["damage_lifetime"] = self.damage_lifetime.tolist()
                except:
                    rep["damage_lifetime"] = self.damage_lifetime
        if self.is_set("damage_one_year"):
            if (short):
                rep["damage_one_year"] = str(self.damage_one_year.shape)
            else:
                try:
                    rep["damage_one_year"] = self.damage_one_year.tolist()
                except:
                    rep["damage_one_year"] = self.damage_one_year
        if self.is_set("fls_criteria_1"):
            if (short):
                rep["fls_criteria_1"] = str(self.fls_criteria_1.shape)
            else:
                try:
                    rep["fls_criteria_1"] = self.fls_criteria_1.tolist()
                except:
                    rep["fls_criteria_1"] = self.fls_criteria_1
        if self.is_set("fls_criteria_2"):
            if (short):
                rep["fls_criteria_2"] = str(self.fls_criteria_2.shape)
            else:
                try:
                    rep["fls_criteria_2"] = self.fls_criteria_2.tolist()
                except:
                    rep["fls_criteria_2"] = self.fls_criteria_2
        if self.is_set("env_proba"):
            if (short):
                rep["env_proba"] = str(self.env_proba.shape)
            else:
                try:
                    rep["env_proba"] = self.env_proba.tolist()
                except:
                    rep["env_proba"] = self.env_proba
        if self.is_set("env_stress_std"):
            if (short):
                rep["env_stress_std"] = str(self.env_stress_std.shape)
            else:
                try:
                    rep["env_stress_std"] = self.env_stress_std.tolist()
                except:
                    rep["env_stress_std"] = self.env_stress_std
        if self.is_set("env_stress_range"):
            if (short):
                rep["env_stress_range"] = str(self.env_stress_range.shape)
            else:
                try:
                    rep["env_stress_range"] = self.env_stress_range.tolist()
                except:
                    rep["env_stress_range"] = self.env_stress_range
        if self.is_set("cdf_stress_range"):
            if (short):
                rep["cdf_stress_range"] = str(self.cdf_stress_range.shape)
            else:
                try:
                    rep["cdf_stress_range"] = self.cdf_stress_range.tolist()
                except:
                    rep["cdf_stress_range"] = self.cdf_stress_range
        if self.is_set("cdf"):
            if (short):
                rep["cdf"] = str(self.cdf.shape)
            else:
                try:
                    rep["cdf"] = self.cdf.tolist()
                except:
                    rep["cdf"] = self.cdf
        if self.is_set("ad"):
            if (short):
                rep["ad"] = str(self.ad.shape)
            else:
                try:
                    rep["ad"] = self.ad.tolist()
                except:
                    rep["ad"] = self.ad
        if self.is_set("m"):
            if (short):
                rep["m"] = str(self.m.shape)
            else:
                try:
                    rep["m"] = self.m.tolist()
                except:
                    rep["m"] = self.m
        if self.is_set("n_cycles_lifetime"):
            if (short):
                rep["n_cycles_lifetime"] = str(self.n_cycles_lifetime.shape)
            else:
                try:
                    rep["n_cycles_lifetime"] = self.n_cycles_lifetime.tolist()
                except:
                    rep["n_cycles_lifetime"] = self.n_cycles_lifetime
        if self.is_set("gamma_f"):
            if (short):
                rep["gamma_f"] = str(self.gamma_f.shape)
            else:
                try:
                    rep["gamma_f"] = self.gamma_f.tolist()
                except:
                    rep["gamma_f"] = self.gamma_f
        if self.is_set("n_years_lifetime"):
            rep["n_years_lifetime"] = self.n_years_lifetime
        if self.is_set("name"):
            rep["name"] = self.name
        if self.is_set("description"):
            rep["description"] = self.description
        return rep
    #-------------------------
    # Save functions
    #-------------------------
    def json_rep(self, short=False, deep=True):

        """Generate a JSON representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`str`: string that contains a JSON representation of the object properties
        """

        return ( json.dumps(self.prop_rep(short=short, deep=deep),indent=4, separators=(",",": ")))
    def saveJSON(self, fileName=None):
        """Save the instance of the object to JSON format file

        Args:
            fileName (:obj:`str`, optional): Name of the JSON file, included extension. Defaults is None. If None, the name of the JSON file will be self.name.json. It can also contain an absolute or relative path.

        """

        if fileName==None:
            fileName=self.name + ".json"
        f=open(fileName, "w")
        f.write(self.json_rep())
        f.write("\n")
        f.close()

    def saveHDF5(self,filePath=None):
        """Save the instance of the object to HDF5 format file
        Args:
            filePath (:obj:`str`, optional): Name of the HDF5 file, included extension. Defaults is None. If None, the name of the HDF5 file will be "self.name".h5. It can also contain an absolute or relative path.
        """
        # Define filepath
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".h5"
            else:
                raise Exception("name is required for saving")
        # Open hdf5 file
        h = h5py.File(filePath,"w")
        group = h.create_group(self.name)
        # Save the object to the group
        self.saveToHDF5Handle(group)
        # Close file
        h.close()
        pass
    def saveToHDF5Handle(self, handle):
        """Save the properties of the object to the hdf5 handle.
        Args:
            handle (:obj:`h5py.Group`): Handle used to store the object properties
        """
        handle["mooring_pretension"] = np.array(self.mooring_pretension,dtype=float)
        handle["total_offset"] = np.array(self.total_offset,dtype=float)
        handle["total_position"] = np.array(self.total_position,dtype=float)
        handle["steady_position"] = np.array(self.steady_position,dtype=float)
        handle["steady_force"] = np.array(self.steady_force,dtype=float)
        handle["steady_offset"] = np.array(self.steady_offset,dtype=float)
        handle["steady_mooring_force"] = np.array(self.steady_mooring_force,dtype=float)
        handle["steady_mooring_tension"] = np.array(self.steady_mooring_tension,dtype=float)
        handle["dynamic_offset"] = np.array(self.dynamic_offset,dtype=float)
        handle["motion_spectrum"] = np.array(self.motion_spectrum,dtype=float)
        handle["omega"] = np.array(self.omega,dtype=float)
        handle["damping_matrix"] = np.array(self.damping_matrix,dtype=float)
        handle["mooring_force"] = np.array(self.mooring_force,dtype=float)
        handle["mooring_tension"] = np.array(self.mooring_tension,dtype=float)
        handle["mooring_touchdown"] = np.array(self.mooring_touchdown,dtype=float)
        handle["env_damage_lifetime"] = np.array(self.env_damage_lifetime,dtype=float)
        handle["env_damage_one_year"] = np.array(self.env_damage_one_year,dtype=float)
        handle["damage_lifetime"] = np.array(self.damage_lifetime,dtype=float)
        handle["damage_one_year"] = np.array(self.damage_one_year,dtype=float)
        handle["fls_criteria_1"] = np.array(self.fls_criteria_1,dtype=float)
        handle["fls_criteria_2"] = np.array(self.fls_criteria_2,dtype=float)
        handle["env_proba"] = np.array(self.env_proba,dtype=float)
        handle["env_stress_std"] = np.array(self.env_stress_std,dtype=float)
        handle["env_stress_range"] = np.array(self.env_stress_range,dtype=float)
        handle["cdf_stress_range"] = np.array(self.cdf_stress_range,dtype=float)
        handle["cdf"] = np.array(self.cdf,dtype=float)
        handle["ad"] = np.array(self.ad,dtype=float)
        handle["m"] = np.array(self.m,dtype=float)
        handle["n_cycles_lifetime"] = np.array(self.n_cycles_lifetime,dtype=float)
        handle["gamma_f"] = np.array(self.gamma_f,dtype=float)
        handle["n_years_lifetime"] = np.array([self.n_years_lifetime],dtype=float)
        ar = []
        ar.append(self.name.encode("ascii"))
        handle["name"] = np.asarray(ar)
        ar = []
        ar.append(self.description.encode("ascii"))
        handle["description"] = np.asarray(ar)
    #-------------------------
    # Load functions
    #-------------------------
    def loadJSON(self,name = None, filePath = None):
        """Load an instance of the object from JSON format file

        Args:
            name (:obj:`str`, optional): Name of the object to load.
            filePath (:obj:`str`, optional): Path of the JSON file to load. If None, the function looks for a file with name "name".json.

        The JSON file must contain a datastructure representing an instance of this object's class, as generated by e.g. the function :func:`~saveJSON`.

        """

        if not(name == None):
            self.name = name
        if (name == None) and not(filePath == None):
            self.name = ".".join(filePath.split(os.path.sep)[-1].split(".")[0:-1])
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".json"
            else:
                raise Exception("object needs name for loading.")
        if not(os.path.isfile(filePath)):
            raise Exception("file %s not found."%filePath)
        self._loadedItems = []
        f = open(filePath,"r")
        data = f.read()
        f.close()
        dd = json.loads(data)
        self.loadFromJSONDict(dd)
    def loadFromJSONDict(self, data):
        """Load an instance of the object from a dictionnary representing a JSON structure)

        Args:
            name (:obj:`collections.OrderedDict`): Dictionnary containing a JSON structure, as produced by e.g. :func:`json.loads`

        """

        varName = "mooring_pretension"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "total_offset"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "total_position"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "steady_position"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "steady_force"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "steady_offset"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "steady_mooring_force"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "steady_mooring_tension"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "dynamic_offset"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "motion_spectrum"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "omega"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "damping_matrix"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "mooring_force"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "mooring_tension"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "mooring_touchdown"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "env_damage_lifetime"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "env_damage_one_year"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "damage_lifetime"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "damage_one_year"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "fls_criteria_1"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "fls_criteria_2"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "env_proba"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "env_stress_std"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "env_stress_range"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "cdf_stress_range"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "cdf"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "ad"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "m"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "n_cycles_lifetime"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "gamma_f"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "n_years_lifetime"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "description"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
    def loadHDF5(self,name=None, filePath=None):
        """Load an instance of the object from HDF5 format file
        Args:
            name (:obj:`str`, optional): Name of the object to load. Default is None.
            filePath (:obj:`str`, optional): Path of the HDF5 file to load. If None, the function looks for a file with name "self.name".h5.
        The HDF5 file must contain a datastructure representing an instance of this objects class, as generated by e.g. the function :func:`~saveHDF5`.
        """
        # Define filepath and name
        if not(name == None):
            self.name = name
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".h5"
            else:
                raise Exception("name is required for loading")
        # Open hdf5 file
        h = h5py.File(filePath,"r")
        group = h[self.name]
        # Save the object to the group
        self.loadFromHDF5Handle(group)
        # Close file
        h.close()
        pass
    def loadFromHDF5Handle(self, gr):
        """Load the properties of the object from a hdf5 handle.
        Args:
            gr (:obj:`h5py.Group`): Handle used to read the object properties from
        """
        if ("mooring_pretension" in list(gr.keys())):
            self.mooring_pretension = gr["mooring_pretension"][:][:]
        if ("total_offset" in list(gr.keys())):
            self.total_offset = gr["total_offset"][:]
        if ("total_position" in list(gr.keys())):
            self.total_position = gr["total_position"][:][:]
        if ("steady_position" in list(gr.keys())):
            self.steady_position = gr["steady_position"][:][:]
        if ("steady_force" in list(gr.keys())):
            self.steady_force = gr["steady_force"][:][:]
        if ("steady_offset" in list(gr.keys())):
            self.steady_offset = gr["steady_offset"][:]
        if ("steady_mooring_force" in list(gr.keys())):
            self.steady_mooring_force = gr["steady_mooring_force"][:][:][:][:]
        if ("steady_mooring_tension" in list(gr.keys())):
            self.steady_mooring_tension = gr["steady_mooring_tension"][:][:][:]
        if ("dynamic_offset" in list(gr.keys())):
            self.dynamic_offset = gr["dynamic_offset"][:]
        if ("motion_spectrum" in list(gr.keys())):
            self.motion_spectrum = gr["motion_spectrum"][:][:][:]
        if ("omega" in list(gr.keys())):
            self.omega = gr["omega"][:]
        if ("damping_matrix" in list(gr.keys())):
            self.damping_matrix = gr["damping_matrix"][:][:]
        if ("mooring_force" in list(gr.keys())):
            self.mooring_force = gr["mooring_force"][:][:][:][:]
        if ("mooring_tension" in list(gr.keys())):
            self.mooring_tension = gr["mooring_tension"][:][:][:]
        if ("mooring_touchdown" in list(gr.keys())):
            self.mooring_touchdown = gr["mooring_touchdown"][:][:]
        if ("env_damage_lifetime" in list(gr.keys())):
            self.env_damage_lifetime = gr["env_damage_lifetime"][:][:]
        if ("env_damage_one_year" in list(gr.keys())):
            self.env_damage_one_year = gr["env_damage_one_year"][:][:]
        if ("damage_lifetime" in list(gr.keys())):
            self.damage_lifetime = gr["damage_lifetime"][:]
        if ("damage_one_year" in list(gr.keys())):
            self.damage_one_year = gr["damage_one_year"][:]
        if ("fls_criteria_1" in list(gr.keys())):
            self.fls_criteria_1 = gr["fls_criteria_1"][:]
        if ("fls_criteria_2" in list(gr.keys())):
            self.fls_criteria_2 = gr["fls_criteria_2"][:]
        if ("env_proba" in list(gr.keys())):
            self.env_proba = gr["env_proba"][:]
        if ("env_stress_std" in list(gr.keys())):
            self.env_stress_std = gr["env_stress_std"][:][:]
        if ("env_stress_range" in list(gr.keys())):
            self.env_stress_range = gr["env_stress_range"][:][:]
        if ("cdf_stress_range" in list(gr.keys())):
            self.cdf_stress_range = gr["cdf_stress_range"][:][:]
        if ("cdf" in list(gr.keys())):
            self.cdf = gr["cdf"][:][:]
        if ("ad" in list(gr.keys())):
            self.ad = gr["ad"][:]
        if ("m" in list(gr.keys())):
            self.m = gr["m"][:]
        if ("n_cycles_lifetime" in list(gr.keys())):
            self.n_cycles_lifetime = gr["n_cycles_lifetime"][:]
        if ("gamma_f" in list(gr.keys())):
            self.gamma_f = gr["gamma_f"][:]
        if ("n_years_lifetime" in list(gr.keys())):
            self.n_years_lifetime = gr["n_years_lifetime"][0]
        if ("name" in list(gr.keys())):
            self.name = gr["name"][0].decode("ascii")
        if ("description" in list(gr.keys())):
            self.description = gr["description"][0].decode("ascii")
    #------------------------
    # is_set function
    #------------------------
    def is_set(self, varName): # pragma: no cover

        """Check if a given property of the object is set

        Args:
            varName (:obj:`str`): name of the property to check

        Returns:
            :obj:`bool`: True if the property is set, else False
        """

        if (isinstance(getattr(self,varName),list) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (isinstance(getattr(self,varName),np.ndarray) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (getattr(self,varName) != None):
            return True
        return False
    #------------------------
