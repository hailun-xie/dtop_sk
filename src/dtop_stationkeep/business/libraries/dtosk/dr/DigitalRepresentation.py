# This is the Station Keeping module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Neil Luxcey, Nicolas Michelet, Rocio Isorna
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import collections
import numpy as np
import os
import json
import h5py
from . import DRStationKeeping
from . import DRMooringLine
from . import DRMooringLineSegment
from . import DRFoundation
from . import DRNode
#------------------------------------
# @ USER DEFINED IMPORTS START
# @ USER DEFINED IMPORTS END
#------------------------------------

class DigitalRepresentation():

    """Digital representation from the SK-module
    """

    #------------
    # Constructor
    #------------
    def __init__(self,name=None):
#------------------------------------
# @ USER DEFINED DESCRIPTION START
# @ USER DEFINED DESCRIPTION END
#------------------------------------

        self._station_keeping=[]
        self._mooring_line=[]
        self._mooring_line_segment=[]
        self._foundations=[]
        self._node=[]
        self._name='none'
        self._description=''
        if not(name == None): # pragma: no cover
            self._name = name

#------------------------------------
# @ USER DEFINED PROPERTIES START
# @ USER DEFINED PROPERTIES END
#------------------------------------

#------------------------------------
# @ USER DEFINED METHODS START
    def populate(self,farm):

        # Set name
        self.name = 'SK-DigitalRepresentation'
        array_id = '1'

        # Loop over each device
        for idx in range(0,len(farm.body_array)):

            # Add one station keeping node per device
            self.station_keeping.append(DRStationKeeping.DRStationKeeping())
            self.station_keeping[idx].id = array_id + '_' + str(idx+1)
            self.station_keeping[idx].hierarchical.part_of_array.value=array_id # There is always one array in DTO+

            # Add the mooring lines to this station keeping system
            if farm.body_array[idx].positioning_type == 'moored':

                # Loop over each mooring line
                [ml_of_segment,ml_of_node,nml] = farm.body_array[idx].mooring_system_force.identify_mooring_line_hierarchy()
                iml_start = len(self.mooring_line)
                for iml in range(0,nml):

                    # Index of mooring line
                    iml_idx = iml_start + iml

                    # Add the mooring line
                    self.mooring_line.append(DRMooringLine.DRMooringLine())
                    self.mooring_line[iml_idx].id = self.station_keeping[idx].id + '_' + str(iml+1)
                    self.mooring_line[iml_idx].hierarchical.part_of_station_keeping.value=self.station_keeping[idx].id
                    self.station_keeping[idx].hierarchical.mooring_line.value.append(self.mooring_line[iml_idx].id)

                # Loop over each node
                inode_start = len(self.node)
                ifound = 0
                for inode in range(0,len(farm.body_array[idx].mooring_system_force.node_properties)):

                    # Index of node
                    inode_idx = inode_start + inode

                    # Add the node
                    self.node.append(DRNode.DRNode())
                    self.node[inode_idx].id = self.station_keeping[idx].id + '_' + str(inode+1)
                    self.node[inode_idx].hierarchical.part_of_station_keeping.value=self.station_keeping[idx].id
                    self.station_keeping[idx].hierarchical.node.value.append(self.node[inode_idx].id)

                    if farm.body_array[idx].mooring_system_force.node_properties[inode].type == 'Vessel':
                        self.node[inode_idx].location.reference_system.value= 'vessel'
                    else:
                        self.node[inode_idx].location.reference_system.value= 'farm'
                    self.node[inode_idx].location.position.value=farm.body_array[idx].mooring_system_force.node_properties[inode].position
                    self.node[inode_idx].location.position.unit='m'

                    # Check if this node is connected to an anchor
                    if farm.body_array[idx].mooring_system_force.node_properties[inode].anchor_required:
                        # Add an anchor to the foundation list
                        self.foundations.append(DRFoundation.DRFoundation())
                        ifound_idx = len(self.foundations)-1
                        ifound = ifound+1
                        self.foundations[ifound_idx].id = self.station_keeping[idx].id + '_' + str(ifound)
                        # add hierarchical connections
                        self.foundations[ifound_idx].hierarchical.part_of_station_keeping.value=self.station_keeping[idx].id
                        self.foundations[ifound_idx].hierarchical.node.value.append(self.node[inode_idx].id)
                        # add properties
                        self.foundations[ifound_idx].properties.populate(farm.body_array[idx].results.design_results.anchor_and_foundation)

                # Loop over each line segment
                iline_start = len(self.mooring_line_segment)
                for iline in range(0,len(farm.body_array[idx].mooring_system_force.line_properties)):

                    # Index of line segment
                    iline_idx = iline_start + iline

                    # Add the line segment
                    self.mooring_line_segment.append(DRMooringLineSegment.DRMooringLineSegment())
                    self.mooring_line_segment[iline_idx].id = self.station_keeping[idx].id + '_' + str(iline+1)

                    # Fill parent mooring line
                    ml_index = ml_of_segment[iline]-1 + iml_start
                    self.mooring_line_segment[iline_idx].hierarchical.part_of_mooring_line.value=self.mooring_line[ml_index].id
                    self.mooring_line[ml_index].hierarchical.mooring_line_segment.value.append(self.mooring_line_segment[iline_idx].id)

                    # Fill children nodes
                    index_anchor = farm.body_array[idx].mooring_system_force.line_properties[iline].anchor_node_number-1
                    index_fairlead = farm.body_array[idx].mooring_system_force.line_properties[iline].fairlead_node_number-1
                    self.mooring_line_segment[iline_idx].hierarchical.node.value=[self.node[inode_start + index_anchor].id,self.node[inode_start + index_fairlead].id]

                    # Fill properties
                    line = farm.body_array[idx].mooring_system_force.line_properties[iline]
                    line_type_index = farm.body_array[idx].mooring_system_force.line_properties[iline].line_type_index
                    line_type = farm.body_array[idx].mooring_system_force.line_dictionnary[line_type_index]
                    self.mooring_line_segment[iline_idx].properties.populate(line,line_type)
                    self.mooring_line_segment[iline_idx].assessment.populate(farm.body_array[idx].results.uls_results,iline)

            elif farm.body_array[idx].positioning_type == 'fixed':

                # Add the ofudnation to the foundation list
                self.foundations.append(DRFoundation.DRFoundation())
                ifound_idx = len(self.foundations)-1
                self.foundations[ifound_idx].id = self.station_keeping[idx].id + '_1'
                # add hierarchical connections (foundation of fixed structure is connected directly to the structure, not to a node)
                self.foundations[ifound_idx].hierarchical.part_of_station_keeping.value=self.station_keeping[idx].id
                # add properties
                self.foundations[ifound_idx].properties.populate(farm.body_array[idx].results.design_results.anchor_and_foundation)
            # Now fill the children of the stationkeeping
            # self.station_keeping[idx].hierarchical.foundations=TaggedStringArray.TaggedStringArray()



# @ USER DEFINED METHODS END
#------------------------------------

    #------------
    # Get functions
    #------------
    @ property
    def station_keeping(self): # pragma: no cover
        """:obj:`list` of :obj:`~.DRStationKeeping.DRStationKeeping`: station keeping
        """
        return self._station_keeping
    #------------
    @ property
    def mooring_line(self): # pragma: no cover
        """:obj:`list` of :obj:`~.DRMooringLine.DRMooringLine`: mooring line
        """
        return self._mooring_line
    #------------
    @ property
    def mooring_line_segment(self): # pragma: no cover
        """:obj:`list` of :obj:`~.DRMooringLineSegment.DRMooringLineSegment`: mooring line segment
        """
        return self._mooring_line_segment
    #------------
    @ property
    def foundations(self): # pragma: no cover
        """:obj:`list` of :obj:`~.DRFoundation.DRFoundation`: foundation
        """
        return self._foundations
    #------------
    @ property
    def node(self): # pragma: no cover
        """:obj:`list` of :obj:`~.DRNode.DRNode`: node
        """
        return self._node
    #------------
    @ property
    def name(self): # pragma: no cover
        """str: name of the instance object
        """
        return self._name
    #------------
    @ property
    def description(self): # pragma: no cover
        """str: description of the instance object
        """
        return self._description
    #------------
    #------------
    # Set functions
    #------------
    @ station_keeping.setter
    def station_keeping(self,val): # pragma: no cover
        self._station_keeping=val
    #------------
    @ mooring_line.setter
    def mooring_line(self,val): # pragma: no cover
        self._mooring_line=val
    #------------
    @ mooring_line_segment.setter
    def mooring_line_segment(self,val): # pragma: no cover
        self._mooring_line_segment=val
    #------------
    @ foundations.setter
    def foundations(self,val): # pragma: no cover
        self._foundations=val
    #------------
    @ node.setter
    def node(self,val): # pragma: no cover
        self._node=val
    #------------
    @ name.setter
    def name(self,val): # pragma: no cover
        self._name=str(val)
    #------------
    @ description.setter
    def description(self,val): # pragma: no cover
        self._description=str(val)
    #------------
    #-------------------------
    # Representation functions
    #-------------------------
    def type_rep(self): # pragma: no cover
        """Generate a representation of the object type

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains the representation of the object type
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtosk:dr:DigitalRepresentation"
        rep["name"] = self.name
        rep["description"] = self.description
        return rep
    def prop_rep(self, short = False, deep = True):

        """Generate a representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains a representation of the object properties
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtosk:dr:DigitalRepresentation"
        rep["name"] = self.name
        rep["description"] = self.description
        if self.is_set("station_keeping"):
            rep["station_keeping"] = []
            for i in range(0,len(self.station_keeping)):
                if (short and not(deep)):
                    itemType = self.station_keeping[i].type_rep()
                    rep["station_keeping"].append( itemType )
                else:
                    rep["station_keeping"].append( self.station_keeping[i].prop_rep(short, deep) )
        else:
            rep["station_keeping"] = []
        if self.is_set("mooring_line"):
            rep["mooring_line"] = []
            for i in range(0,len(self.mooring_line)):
                if (short and not(deep)):
                    itemType = self.mooring_line[i].type_rep()
                    rep["mooring_line"].append( itemType )
                else:
                    rep["mooring_line"].append( self.mooring_line[i].prop_rep(short, deep) )
        else:
            rep["mooring_line"] = []
        if self.is_set("mooring_line_segment"):
            rep["mooring_line_segment"] = []
            for i in range(0,len(self.mooring_line_segment)):
                if (short and not(deep)):
                    itemType = self.mooring_line_segment[i].type_rep()
                    rep["mooring_line_segment"].append( itemType )
                else:
                    rep["mooring_line_segment"].append( self.mooring_line_segment[i].prop_rep(short, deep) )
        else:
            rep["mooring_line_segment"] = []
        if self.is_set("foundations"):
            rep["foundations"] = []
            for i in range(0,len(self.foundations)):
                if (short and not(deep)):
                    itemType = self.foundations[i].type_rep()
                    rep["foundations"].append( itemType )
                else:
                    rep["foundations"].append( self.foundations[i].prop_rep(short, deep) )
        else:
            rep["foundations"] = []
        if self.is_set("node"):
            rep["node"] = []
            for i in range(0,len(self.node)):
                if (short and not(deep)):
                    itemType = self.node[i].type_rep()
                    rep["node"].append( itemType )
                else:
                    rep["node"].append( self.node[i].prop_rep(short, deep) )
        else:
            rep["node"] = []
        if self.is_set("name"):
            rep["name"] = self.name
        if self.is_set("description"):
            rep["description"] = self.description
        return rep
    #-------------------------
    # Save functions
    #-------------------------
    def json_rep(self, short=False, deep=True):

        """Generate a JSON representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`str`: string that contains a JSON representation of the object properties
        """

        return ( json.dumps(self.prop_rep(short=short, deep=deep),indent=4, separators=(",",": ")))
    def saveJSON(self, fileName=None):
        """Save the instance of the object to JSON format file

        Args:
            fileName (:obj:`str`, optional): Name of the JSON file, included extension. Defaults is None. If None, the name of the JSON file will be self.name.json. It can also contain an absolute or relative path.

        """

        if fileName==None:
            fileName=self.name + ".json"
        f=open(fileName, "w")
        f.write(self.json_rep())
        f.write("\n")
        f.close()

    def saveHDF5(self,filePath=None):
        """Save the instance of the object to HDF5 format file
        Args:
            filePath (:obj:`str`, optional): Name of the HDF5 file, included extension. Defaults is None. If None, the name of the HDF5 file will be "self.name".h5. It can also contain an absolute or relative path.
        """
        # Define filepath
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".h5"
            else:
                raise Exception("name is required for saving")
        # Open hdf5 file
        h = h5py.File(filePath,"w")
        group = h.create_group(self.name)
        # Save the object to the group
        self.saveToHDF5Handle(group)
        # Close file
        h.close()
        pass
    def saveToHDF5Handle(self, handle):
        """Save the properties of the object to the hdf5 handle.
        Args:
            handle (:obj:`h5py.Group`): Handle used to store the object properties
        """
        subgroup = handle.create_group("station_keeping")
        for idx in range(0,len(self.station_keeping)):
            subgroup1 = subgroup.create_group("station_keeping" + "_" + str(idx))
            self.station_keeping[idx].saveToHDF5Handle(subgroup1)
        subgroup = handle.create_group("mooring_line")
        for idx in range(0,len(self.mooring_line)):
            subgroup1 = subgroup.create_group("mooring_line" + "_" + str(idx))
            self.mooring_line[idx].saveToHDF5Handle(subgroup1)
        subgroup = handle.create_group("mooring_line_segment")
        for idx in range(0,len(self.mooring_line_segment)):
            subgroup1 = subgroup.create_group("mooring_line_segment" + "_" + str(idx))
            self.mooring_line_segment[idx].saveToHDF5Handle(subgroup1)
        subgroup = handle.create_group("foundations")
        for idx in range(0,len(self.foundations)):
            subgroup1 = subgroup.create_group("foundations" + "_" + str(idx))
            self.foundations[idx].saveToHDF5Handle(subgroup1)
        subgroup = handle.create_group("node")
        for idx in range(0,len(self.node)):
            subgroup1 = subgroup.create_group("node" + "_" + str(idx))
            self.node[idx].saveToHDF5Handle(subgroup1)
        ar = []
        ar.append(self.name.encode("ascii"))
        handle["name"] = np.asarray(ar)
        ar = []
        ar.append(self.description.encode("ascii"))
        handle["description"] = np.asarray(ar)
    #-------------------------
    # Load functions
    #-------------------------
    def loadJSON(self,name = None, filePath = None):
        """Load an instance of the object from JSON format file

        Args:
            name (:obj:`str`, optional): Name of the object to load.
            filePath (:obj:`str`, optional): Path of the JSON file to load. If None, the function looks for a file with name "name".json.

        The JSON file must contain a datastructure representing an instance of this object's class, as generated by e.g. the function :func:`~saveJSON`.

        """

        if not(name == None):
            self.name = name
        if (name == None) and not(filePath == None):
            self.name = ".".join(filePath.split(os.path.sep)[-1].split(".")[0:-1])
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".json"
            else:
                raise Exception("object needs name for loading.")
        if not(os.path.isfile(filePath)):
            raise Exception("file %s not found."%filePath)
        self._loadedItems = []
        f = open(filePath,"r")
        data = f.read()
        f.close()
        dd = json.loads(data)
        self.loadFromJSONDict(dd)
    def loadFromJSONDict(self, data):
        """Load an instance of the object from a dictionnary representing a JSON structure)

        Args:
            name (:obj:`collections.OrderedDict`): Dictionnary containing a JSON structure, as produced by e.g. :func:`json.loads`

        """

        varName = "station_keeping"
        try :
            if data[varName] != None:
                self.station_keeping=[]
                for i in range(0,len(data[varName])):
                    self.station_keeping.append(DRStationKeeping.DRStationKeeping())
                    self.station_keeping[i].loadFromJSONDict(data[varName][i])
            else:
                self.station_keeping = []
        except :
            pass
        varName = "mooring_line"
        try :
            if data[varName] != None:
                self.mooring_line=[]
                for i in range(0,len(data[varName])):
                    self.mooring_line.append(DRMooringLine.DRMooringLine())
                    self.mooring_line[i].loadFromJSONDict(data[varName][i])
            else:
                self.mooring_line = []
        except :
            pass
        varName = "mooring_line_segment"
        try :
            if data[varName] != None:
                self.mooring_line_segment=[]
                for i in range(0,len(data[varName])):
                    self.mooring_line_segment.append(DRMooringLineSegment.DRMooringLineSegment())
                    self.mooring_line_segment[i].loadFromJSONDict(data[varName][i])
            else:
                self.mooring_line_segment = []
        except :
            pass
        varName = "foundations"
        try :
            if data[varName] != None:
                self.foundations=[]
                for i in range(0,len(data[varName])):
                    self.foundations.append(DRFoundation.DRFoundation())
                    self.foundations[i].loadFromJSONDict(data[varName][i])
            else:
                self.foundations = []
        except :
            pass
        varName = "node"
        try :
            if data[varName] != None:
                self.node=[]
                for i in range(0,len(data[varName])):
                    self.node.append(DRNode.DRNode())
                    self.node[i].loadFromJSONDict(data[varName][i])
            else:
                self.node = []
        except :
            pass
        varName = "name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "description"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
    def loadHDF5(self,name=None, filePath=None):
        """Load an instance of the object from HDF5 format file
        Args:
            name (:obj:`str`, optional): Name of the object to load. Default is None.
            filePath (:obj:`str`, optional): Path of the HDF5 file to load. If None, the function looks for a file with name "self.name".h5.
        The HDF5 file must contain a datastructure representing an instance of this objects class, as generated by e.g. the function :func:`~saveHDF5`.
        """
        # Define filepath and name
        if not(name == None):
            self.name = name
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".h5"
            else:
                raise Exception("name is required for loading")
        # Open hdf5 file
        h = h5py.File(filePath,"r")
        group = h[self.name]
        # Save the object to the group
        self.loadFromHDF5Handle(group)
        # Close file
        h.close()
        pass
    def loadFromHDF5Handle(self, gr):
        """Load the properties of the object from a hdf5 handle.
        Args:
            gr (:obj:`h5py.Group`): Handle used to read the object properties from
        """
        if ("station_keeping" in list(gr.keys())):
            subgroup = gr["station_keeping"]
            self.station_keeping=[]
            for idx in range(0,len(subgroup.keys())):
                ssubgroup = subgroup["station_keeping"+"_"+str(idx)]
                self.station_keeping.append(DRStationKeeping.DRStationKeeping())
                self.station_keeping[idx].loadFromHDF5Handle(ssubgroup)
        if ("mooring_line" in list(gr.keys())):
            subgroup = gr["mooring_line"]
            self.mooring_line=[]
            for idx in range(0,len(subgroup.keys())):
                ssubgroup = subgroup["mooring_line"+"_"+str(idx)]
                self.mooring_line.append(DRMooringLine.DRMooringLine())
                self.mooring_line[idx].loadFromHDF5Handle(ssubgroup)
        if ("mooring_line_segment" in list(gr.keys())):
            subgroup = gr["mooring_line_segment"]
            self.mooring_line_segment=[]
            for idx in range(0,len(subgroup.keys())):
                ssubgroup = subgroup["mooring_line_segment"+"_"+str(idx)]
                self.mooring_line_segment.append(DRMooringLineSegment.DRMooringLineSegment())
                self.mooring_line_segment[idx].loadFromHDF5Handle(ssubgroup)
        if ("foundations" in list(gr.keys())):
            subgroup = gr["foundations"]
            self.foundations=[]
            for idx in range(0,len(subgroup.keys())):
                ssubgroup = subgroup["foundations"+"_"+str(idx)]
                self.foundations.append(DRFoundation.DRFoundation())
                self.foundations[idx].loadFromHDF5Handle(ssubgroup)
        if ("node" in list(gr.keys())):
            subgroup = gr["node"]
            self.node=[]
            for idx in range(0,len(subgroup.keys())):
                ssubgroup = subgroup["node"+"_"+str(idx)]
                self.node.append(DRNode.DRNode())
                self.node[idx].loadFromHDF5Handle(ssubgroup)
        if ("name" in list(gr.keys())):
            self.name = gr["name"][0].decode("ascii")
        if ("description" in list(gr.keys())):
            self.description = gr["description"][0].decode("ascii")
    #------------------------
    # is_set function
    #------------------------
    def is_set(self, varName): # pragma: no cover

        """Check if a given property of the object is set

        Args:
            varName (:obj:`str`): name of the property to check

        Returns:
            :obj:`bool`: True if the property is set, else False
        """

        if (isinstance(getattr(self,varName),list) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (isinstance(getattr(self,varName),np.ndarray) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (getattr(self,varName) != None):
            return True
        return False
    #------------------------
