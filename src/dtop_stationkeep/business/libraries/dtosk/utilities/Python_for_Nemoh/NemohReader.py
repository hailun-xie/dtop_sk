# This is the Station Keeping module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Neil Luxcey, Nicolas Michelet, Rocio Isorna
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

# Class definition for NemohReader.
# Purpose: Reopen a Nemoh instance from its name by reading the stored files.
# Written by: C.Spraul
#
import os
import numpy as np
from .Nemoh import Nemoh
from .MeshReader import MeshReader

class NemohReader( Nemoh ):
    """
    Purpose: Reopen a NemohLauncher instance from its name by reading the stored files.
    """
    def __init__(self, name):
        """
        Inputs:
            - name  : string, used to find the directory / files where the mesh and calculation results are stored.
        """
        self.name = name
        self.mesh = MeshReader( self.name )
        #
        solver, depth, nw, w_min, w_max, ndir, dir_min, dir_max = self.read_Nemoh_input_file()
        self.solver = solver
        self.depth = depth
        self.nw = nw
        self.ndir = ndir
        self.w = np.linspace(w_min, w_max, self.nw)
        self.dir = np.linspace(dir_min, dir_max, self.ndir)
        #
        A, B, Fe = self.read_results()
        self.A = A
        self.B = B
        self.Fe = Fe
    
    def read_Nemoh_input_file( self ):
        # input.txt
        fid = open(self.name + os.sep + 'input.txt','r')
        fid.readline()
        solver = int( fid.read(1) )
        fid.close()
        # Nemoh.cal file
        fid = open(self.name + os.sep + 'Nemoh.cal','r')
        n=1
        textline = []
        for line in fid:
            textline.append( line )
            if n == 4: 
                depth = float( line[:8] )
            if (n == 9+18*self.mesh.nBodies):
                nw = int( line[:4] )
                w_min = float( line[5:11] )
                w_max = float( line[12:18] )
            if (n == 10+18*self.mesh.nBodies):
                ndir = int( line[:4] )
                dir_min = float( line[5:11] )
                dir_max = float( line[12:18] )
            n+=1
        fid.close()
        return solver, depth, nw, w_min, w_max, ndir, dir_min, dir_max
    


