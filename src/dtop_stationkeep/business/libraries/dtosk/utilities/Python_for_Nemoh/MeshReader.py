# This is the Station Keeping module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Neil Luxcey, Nicolas Michelet, Rocio Isorna
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

# Class definition for MeshReader.
# Purpose: re-open existing meshes for Nemoh calcultations.
# Written by: C.Spraul
#
import os
import numpy as np
from .Mesh import Mesh


class MeshReader( Mesh ):
    """
    A class to read an existing mesh from its directory.
    (As created using either Mesh or AxiMesh)
    """
    def __init__(self, name):
        """
        inputs:
            - name: string, used to find the directory / files where the mesh and calculation results are stored.
        """
        self.name = name
        filepath = os.path.join(self.name,'mesh','mesh.dat')
        with open(filepath,'r') as fid:
            self.sym = bool( int( fid.readline()[-2] ) )
        #
        rho, g, nBodies, nn, nf, CoG = self.read_Nemoh_input_file()
        self.rho = rho
        self.g = g
        self.nBodies = nBodies
        self.nn = nn
        self.nf = nf
        self.CoG = CoG
        #
        mass, inertia, KH, CoB, displacement, WPA = self.read_hydrostatic()
        self.mass = mass
        self.inertia = inertia
        self.KH = KH
        self.CoB = CoB
        self.displacement = displacement
        self.WPA = WPA
        #
        self.description_mesh = self.read_description_mesh()
        #
        self.translation = np.zeros((3))
        self.rotation = np.zeros((3))
        for i in range(3,len(name)):
            if name[i-3:i] == '_T_':
                print( 'Translation in '+ name )
                j=i
                X = ''
                while name[j] != '_':
                    X += name[j]
                    j+=1
                X = float( X )
                j+=1
                Y = ''
                while name[j] != '_':
                    Y += name[j]
                    j+=1
                Y = float( Y )
                j+=1
                Z = ''
                while j<len(name) and name[j] != '_':
                    Z += name[j]
                    j+=1
                Z = float( Z )
                self.translation = np.array([X, Y, Z])
                print( self.translation )
        for i in range(3,len(name)):
            if name[i-3:i] == '_R_':
                print( 'Rotation in '+ name )
                j=i
                RX = ''
                while name[j] != '_':
                    RX += name[j]
                    j+=1
                RX = float( RX )
                j+=1
                RY = ''
                while name[j] != '_':
                    RY += name[j]
                    j+=1
                RY = float( RY )
                j+=1
                RZ = ''
                while j<len(name) and name[j] != '_':
                    RZ += name[j]
                    j+=1
                RZ = float( RZ )
                self.rotation = np.array([RX, RY, RZ])
                print( self.rotation )
    
    def read_Nemoh_input_file(self):
        #
        fid = open(self.name + os.sep + 'Nemoh.cal','r')
        for i in range(6):
            fid.readline()
        nBodies = int( fid.read(8) )
        fid.close()
        #
        fid = open( self.name + os.sep +'Nemoh.cal','r')
        fid.readline()
        rho = float( fid.read(8) )
        fid.readline()
        g = float( fid.read(8) )
        fid.readline()
        for i in range(3):
            fid.readline()
        nBodies = int( fid.read(8) )
        fid.readline()
        for i in range(2):
            fid.readline()
        nn = int( fid.read(6) )
        fid.read(1)
        nf = int( fid.read(6) )
        fid.readline()
        for i in range(4):
            fid.readline()
        fid.read(12)
        xG = float( fid.read(6) )
        fid.read(1)
        yG = float( fid.read(6) )
        fid.read(1)
        zG = float( fid.read(6) )
        fid.close()
        CoG = np.array([xG, yG, zG])
        #
        return rho, g, nBodies, nn, nf, CoG
    
    def read_description_mesh( self ):
        fid = open( self.name + os.sep +'mesh'+ os.sep +'Description_Full.tec','r')
        line = fid.readline()
        nn = int( line[12:20] )
        nf = int( line[28:36] )
        nodes = np.zeros((nn,3))
        for i in range(nn):
            line = fid.readline()[:-1]
            line = np.fromstring(line, dtype=float, sep=' ')
            nodes[i,:] = line[:3]
        quad = np.zeros((nf, 4), dtype=int)
        for i in range(nf):
            line = fid.readline()[:-1]
            line = np.fromstring(line, dtype=int, sep=' ')
            quad[i,:] = line[:]-1
        fid.close()
        #
        description_mesh = np.zeros((nf, 4, 3))
        for i in range(nf):
            description_mesh[i,:,:] = nodes[quad[i,:]]
        return description_mesh