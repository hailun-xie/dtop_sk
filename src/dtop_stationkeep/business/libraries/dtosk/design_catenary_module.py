# This is the Station Keeping module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Neil Luxcey, Nicolas Michelet, Rocio Isorna
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import numpy as np 
from  .mooring.Designer import Designer
from  .winching_module import winch_line_for_pretension
from  .frequency_analysis_module import compute_eigenperiod,compute_max_dynamic_offset,run_frequency_analysis,compute_tension_from_offset_position
import copy
import matplotlib.pyplot as plt
import logging
import sys
logger = logging.getLogger() # grabs root logger

def design_catenary(body,environment_array,water_depth,weather_direction,catalogue_chains):
	"""THis is the summary2

	Args:
		body (:class:`~dtop_stationkeep.business.libraries.dtosk.entity.body`): [description]
		environment_array (:class:`~dtop_stationkeep.business.libraries.dtosk.entity.Body`): [description]
		water_depth (float): [description]
		weather_direction ([type]): [description]
		catalogue_chains (:class:`~dtop_stationkeep.business.libraries.dtosk.catalogue.Lines`): [description]
	"""
	"""
	Methods:
		blablabal
	"""
	# Init
	water_density = environment_array[0].current.water_density

	logger.info('----------------------')
	logger.info('Design catenary system')
	logger.info('----------------------')

	# Iteration based on static analysis
	design_catenary_static(body,environment_array,water_depth,weather_direction,catalogue_chains)

	# Iteration based on ULS analysis
	design_catenary_uls(body,environment_array,water_depth,weather_direction,catalogue_chains)

	logger.info('Final diameter: ' + str(body.mooring_system_force.line_dictionnary[0].diameter) + ' m  , with MBL = ' + str(body.mooring_system_force.line_dictionnary[0].mbl/1000) + 'kN' )
	logger.info('Final number of lines: ' + str(len(body.mooring_system_force.line_properties)))

	# Reduce the mooring radius by 95% of the touchdown
	body.position = copy.deepcopy(body.initial_position)
	body.reduce_mooring_radius(body.results.uls_results.mooring_touchdown.min(),water_density)

	# Update tension at anchors due to reduced touchdown length
	[mooring_force,mooring_tension,mooring_touchdown,tension_versus_mbl] = compute_tension_from_offset_position(body,body.results.uls_results.total_position)
	body.results.uls_results.mooring_force=copy.deepcopy(mooring_force)
	body.results.uls_results.mooring_tension=copy.deepcopy(mooring_tension)
	body.results.uls_results.mooring_touchdown=copy.deepcopy(mooring_touchdown)
	body.results.uls_results.tension_versus_mbl=copy.deepcopy(tension_versus_mbl)

	# Put the mooring system back to the initial position
	body.position = copy.deepcopy(body.initial_position)
	body.mooring_system_force.solve_mooring_system(body.position)

	return

def design_catenary_static_init(body,environment_array,water_depth,weather_direction,catalogue_chains):
	"""[summary]

	Args:
		body ([type]): [description]
		environment_array ([type]): [description]
		water_depth ([type]): [description]
		weather_direction ([type]): [description]
		catalogue_chains ([type]): [description]

	Raises:
		Exception: [description]

	Returns:
		[type]: [description]
	"""

	# Init
	water_density = environment_array[0].current.water_density
	pretension_coeff_min = body.mooring_design_criteria.pretension_coeff_min
	length_increment = body.mooring_design_criteria.length_increment
	offset_max = body.mooring_design_criteria.offset_max

	# Initiate design of the mooring system for each device
	logger.info('-- Initiate design')
	designer = Designer()
	body.mooring_system_force = designer.initiate_mooring_system(body,environment_array,water_depth,catalogue_chains)
	winch_line_for_pretension(body,pretension_coeff_min,length_increment,water_depth, water_density)



	# Get estimate of offset due to 1st order wave motion (rao_offset), without mooring
	logger.info('-- Check value of maximum offset input')
	K_mooring = np.zeros(shape=(6,6),dtype=float)
	dynamic_offset_max = compute_max_dynamic_offset(body,environment_array,K_mooring)

	# Compute steady_offset_max = total_offset_max - rao_offset_max
	steady_offset_max = offset_max - dynamic_offset_max
	logger.info('Offset max: ' + str(offset_max))
	logger.info('Dynamic Offset max: ' + f"{dynamic_offset_max:.2f}")
	logger.info('Steady Offset max: ' + f"{steady_offset_max:.2f}")
	if steady_offset_max <= 0.0:
		raise Exception("Error: dynamic offset is larger than the specified maximum total offset. Consider increasing the value of the maximum total offset. Maximum dynamic offset is: " + str(dynamic_offset_max) + 'm')

	# Increase diameter so that offset due to steady forces are less than steady_offset_max
	additional_line_needed = designer.refine_mooring_system_for_steady_offset(steady_offset_max,body,environment_array,catalogue_chains,water_depth,pretension_coeff_min,length_increment)

	return additional_line_needed

def design_catenary_static(body,environment_array,water_depth,weather_direction,catalogue_chains):

	additional_line_needed = design_catenary_static_init(body,environment_array,water_depth,weather_direction,catalogue_chains)

	while additional_line_needed:
		# Error if we have reached the maximum number of lines
		if body.mooring_design_criteria.nlines_max == body.mooring_design_criteria.nlines:
			raise Exception('Error: maximum number of lines has been reached without finding an appropriate mooring design')
		else:
			logger.info('-- Restart the design with an additional line: ' + str(body.mooring_design_criteria.nlines + 1) + ' lines.')
			# Add one line and start over
			body.mooring_design_criteria.nlines = body.mooring_design_criteria.nlines + 1
			additional_line_needed = design_catenary_static_init(body,environment_array,water_depth,weather_direction,catalogue_chains)

def design_catenary_uls_init(body,environment_array,water_depth,weather_direction,catalogue_chains):

	# Init
	water_density = environment_array[0].current.water_density
	pretension_coeff_min = body.mooring_design_criteria.pretension_coeff_min
	length_increment = body.mooring_design_criteria.length_increment
	Tmin = body.mooring_design_criteria.Tmin
	designer = Designer()

	# Compute eigenperiod
	logger.info('-- Check eigenperiod of the system')
	eigenperiod = compute_eigenperiod(body)
	if (eigenperiod[0] < Tmin) or (eigenperiod[1] < Tmin):
		raise Exception("Error:  obtained eigenperiods in surge and/or sway are smaller than Tmin : " + str(eigenperiod[0:2]) + ". The system is too stiff. Increase target maximum total offset.")
	logger.info('Minimum eigenperiod OK: ' + '(limit set to ' + str(Tmin) + 's by the user)')
	logger.info('Surge ' + f"{eigenperiod[0]:.2f}" +'s ')
	logger.info('Sway ' + f"{eigenperiod[1]:.2f}" +'s ')

	# Compute motion response and tension in line for each environment, results stored in body.results
	run_frequency_analysis(body,weather_direction,environment_array,water_depth)


def design_catenary_uls(body,environment_array,water_depth,weather_direction,catalogue_chains):

	# Init
	water_density = environment_array[0].current.water_density
	pretension_coeff_min = body.mooring_design_criteria.pretension_coeff_min
	length_increment = body.mooring_design_criteria.length_increment
	Tmin = body.mooring_design_criteria.Tmin
	designer = Designer()
	design_catenary_uls_init(body,environment_array,water_depth,weather_direction,catalogue_chains)

	# Increase line diameter until Tmax * SF < MBL
	logger.info('-- Increase line diameter to cope with ULS criteria')
	logger.info('Max tension/MBL: ' + str(body.results.uls_results.tension_versus_mbl))
	while (body.results.uls_results.tension_versus_mbl>1.0).any():
		additional_line_needed = designer.increase_segment_diameter(body.mooring_system_force,catalogue_chains)
		if additional_line_needed:
			# Error if we have reached the maximum number of lines
			if body.mooring_design_criteria.nlines_max == body.mooring_design_criteria.nlines:
				raise Exception('Error: maximum number of lines has been reached without finding an appropriate mooring design. Try to (1) decrease minimum pretension, (2) increase maximum number of lines, (3) increase the maximum offset.')
			else:
				logger.info('-- Restart the design with an additional line: ' + str(body.mooring_design_criteria.nlines + 1) + ' lines.')
				# Add one line and start over
				body.mooring_design_criteria.nlines = body.mooring_design_criteria.nlines + 1
				design_catenary_static(body,environment_array,water_depth,weather_direction,catalogue_chains)
				design_catenary_uls_init(body,environment_array,water_depth,weather_direction,catalogue_chains)
		else:

			# Update line type name for each line properties (we know here that all lines are of the same line type)
			for idl in range(0,len(body.mooring_system_force.line_properties)):
				body.mooring_system_force.line_properties[idl].line_type_name = body.mooring_system_force.line_dictionnary[0].line_type_name

			# Initiate calculation
			body.mooring_system_force.initiate_calculation(water_depth,water_density,body.initial_position)

			# Winch the lines to get acceptable pretension
			winch_line_for_pretension(body,pretension_coeff_min,length_increment,water_depth,water_density)
			
			# ULS analysis
			design_catenary_uls_init(body,environment_array,water_depth,weather_direction,catalogue_chains)

	logger.info('Selected line diameter: ' + str(body.mooring_system_force.line_dictionnary[0].diameter) + 'm')
	

