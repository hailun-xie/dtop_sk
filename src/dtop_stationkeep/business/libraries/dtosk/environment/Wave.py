# This is the Station Keeping module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Neil Luxcey, Nicolas Michelet, Rocio Isorna
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import collections
import numpy as np
import os
import json
import h5py
#------------------------------------
# @ USER DEFINED IMPORTS START
from scipy import optimize
from ..global_parameters import acceleration_of_gravity
from .. linear_wave_module import compute_wave_number
# @ USER DEFINED IMPORTS END
#------------------------------------

class Wave():

    """data model representing wave condition
    """

    #------------
    # Constructor
    #------------
    def __init__(self,name=None):
#------------------------------------
# @ USER DEFINED DESCRIPTION START
# @ USER DEFINED DESCRIPTION END
#------------------------------------

        self._hs=0.0
        self._tp=0.0
        self._direction=0.0
        self._gamma=3.3
        self._water_density=1025.0
        self._name='none'
        self._description=''
        if not(name == None): # pragma: no cover
            self._name = name

#------------------------------------
# @ USER DEFINED PROPERTIES START
# @ USER DEFINED PROPERTIES END
#------------------------------------

#------------------------------------
# @ USER DEFINED METHODS START

    def omega_peak(self):

        omega_peak = 2 * np.pi / self.tp

        return omega_peak

    def wave_number(self,water_depth):

        wavenumber = compute_wave_number(self.omega_peak(),water_depth)

        return wavenumber

    def wave_length(self,water_depth):

        wave_length = 2.0 * np.pi / self.wave_number(water_depth)

        return wave_length

    def hmax(self,water_depth):

        # Bandwidth DNV-RP-C205, 3.5.9.4
        bandwidth = (-0.000191 * self.gamma ** 3.0 + 0.00488 * self.gamma ** 2.0 - 0.0525 * self.gamma - 0.605)

        # Tz
        tz = self.tp / 1.4049

        # Maximum wave height DNV-RP-C205, 3.5.11.5, with t = 3hours
        hmax = 0.5 * self.hs * np.sqrt((1.0 - bandwidth) * np.log(3.0 * 3600 / tz))

        # Breaking wave height limit DNV-RP-C205, 3.4.6.1
        wave_length = self.wave_length(water_depth)
        hb = wave_length * 0.142 * np.tanh(2.0 * np.pi * water_depth / wave_length)

        # Limit hmax by hb
        hmax=min(hmax,hb)

        return hmax

    def vmax(self,water_depth,z):

        # Init
        h = water_depth
        omega = self.omega_peak()

        # Get wave number
        k = self.wave_number(h)

        # Wave amplitude max
        amp = 0.5*self.hmax(h)

        # Compute water particle acceleration
        umax = omega * amp * np.cosh(k*(z+h))/np.sinh(k*h)

        return umax

# @ USER DEFINED METHODS END
#------------------------------------

    #------------
    # Get functions
    #------------
    @ property
    def hs(self): # pragma: no cover
        """float: significant wave height [m]
        """
        return self._hs
    #------------
    @ property
    def tp(self): # pragma: no cover
        """float: peak period [s]
        """
        return self._tp
    #------------
    @ property
    def direction(self): # pragma: no cover
        """float: wave direction in {n}. Going-to convention. [rad]
        """
        return self._direction
    #------------
    @ property
    def gamma(self): # pragma: no cover
        """float: peak shape parameter [-]
        """
        return self._gamma
    #------------
    @ property
    def water_density(self): # pragma: no cover
        """float: Mass density of sea water [kg/m3]
        """
        return self._water_density
    #------------
    @ property
    def name(self): # pragma: no cover
        """str: name of the instance object
        """
        return self._name
    #------------
    @ property
    def description(self): # pragma: no cover
        """str: description of the instance object
        """
        return self._description
    #------------
    #------------
    # Set functions
    #------------
    @ hs.setter
    def hs(self,val): # pragma: no cover
        self._hs=float(val)
    #------------
    @ tp.setter
    def tp(self,val): # pragma: no cover
        self._tp=float(val)
    #------------
    @ direction.setter
    def direction(self,val): # pragma: no cover
        self._direction=float(val)
    #------------
    @ gamma.setter
    def gamma(self,val): # pragma: no cover
        self._gamma=float(val)
    #------------
    @ water_density.setter
    def water_density(self,val): # pragma: no cover
        self._water_density=float(val)
    #------------
    @ name.setter
    def name(self,val): # pragma: no cover
        self._name=str(val)
    #------------
    @ description.setter
    def description(self,val): # pragma: no cover
        self._description=str(val)
    #------------
    #-------------------------
    # Representation functions
    #-------------------------
    def type_rep(self): # pragma: no cover
        """Generate a representation of the object type

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains the representation of the object type
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtosk:environment:Wave"
        rep["name"] = self.name
        rep["description"] = self.description
        return rep
    def prop_rep(self, short = False, deep = True):

        """Generate a representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains a representation of the object properties
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtosk:environment:Wave"
        rep["name"] = self.name
        rep["description"] = self.description
        if self.is_set("hs"):
            rep["hs"] = self.hs
        if self.is_set("tp"):
            rep["tp"] = self.tp
        if self.is_set("direction"):
            rep["direction"] = self.direction
        if self.is_set("gamma"):
            rep["gamma"] = self.gamma
        if self.is_set("water_density"):
            rep["water_density"] = self.water_density
        if self.is_set("name"):
            rep["name"] = self.name
        if self.is_set("description"):
            rep["description"] = self.description
        return rep
    #-------------------------
    # Save functions
    #-------------------------
    def json_rep(self, short=False, deep=True):

        """Generate a JSON representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`str`: string that contains a JSON representation of the object properties
        """

        return ( json.dumps(self.prop_rep(short=short, deep=deep),indent=4, separators=(",",": ")))
    def saveJSON(self, fileName=None):
        """Save the instance of the object to JSON format file

        Args:
            fileName (:obj:`str`, optional): Name of the JSON file, included extension. Defaults is None. If None, the name of the JSON file will be self.name.json. It can also contain an absolute or relative path.

        """

        if fileName==None:
            fileName=self.name + ".json"
        f=open(fileName, "w")
        f.write(self.json_rep())
        f.write("\n")
        f.close()

    def saveHDF5(self,filePath=None):
        """Save the instance of the object to HDF5 format file
        Args:
            filePath (:obj:`str`, optional): Name of the HDF5 file, included extension. Defaults is None. If None, the name of the HDF5 file will be "self.name".h5. It can also contain an absolute or relative path.
        """
        # Define filepath
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".h5"
            else:
                raise Exception("name is required for saving")
        # Open hdf5 file
        h = h5py.File(filePath,"w")
        group = h.create_group(self.name)
        # Save the object to the group
        self.saveToHDF5Handle(group)
        # Close file
        h.close()
        pass
    def saveToHDF5Handle(self, handle):
        """Save the properties of the object to the hdf5 handle.
        Args:
            handle (:obj:`h5py.Group`): Handle used to store the object properties
        """
        handle["hs"] = np.array([self.hs],dtype=float)
        handle["tp"] = np.array([self.tp],dtype=float)
        handle["direction"] = np.array([self.direction],dtype=float)
        handle["gamma"] = np.array([self.gamma],dtype=float)
        handle["water_density"] = np.array([self.water_density],dtype=float)
        ar = []
        ar.append(self.name.encode("ascii"))
        handle["name"] = np.asarray(ar)
        ar = []
        ar.append(self.description.encode("ascii"))
        handle["description"] = np.asarray(ar)
    #-------------------------
    # Load functions
    #-------------------------
    def loadJSON(self,name = None, filePath = None):
        """Load an instance of the object from JSON format file

        Args:
            name (:obj:`str`, optional): Name of the object to load.
            filePath (:obj:`str`, optional): Path of the JSON file to load. If None, the function looks for a file with name "name".json.

        The JSON file must contain a datastructure representing an instance of this object's class, as generated by e.g. the function :func:`~saveJSON`.

        """

        if not(name == None):
            self.name = name
        if (name == None) and not(filePath == None):
            self.name = ".".join(filePath.split(os.path.sep)[-1].split(".")[0:-1])
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".json"
            else:
                raise Exception("object needs name for loading.")
        if not(os.path.isfile(filePath)):
            raise Exception("file %s not found."%filePath)
        self._loadedItems = []
        f = open(filePath,"r")
        data = f.read()
        f.close()
        dd = json.loads(data)
        self.loadFromJSONDict(dd)
    def loadFromJSONDict(self, data):
        """Load an instance of the object from a dictionnary representing a JSON structure)

        Args:
            name (:obj:`collections.OrderedDict`): Dictionnary containing a JSON structure, as produced by e.g. :func:`json.loads`

        """

        varName = "hs"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "tp"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "direction"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "gamma"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "water_density"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "description"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
    def loadHDF5(self,name=None, filePath=None):
        """Load an instance of the object from HDF5 format file
        Args:
            name (:obj:`str`, optional): Name of the object to load. Default is None.
            filePath (:obj:`str`, optional): Path of the HDF5 file to load. If None, the function looks for a file with name "self.name".h5.
        The HDF5 file must contain a datastructure representing an instance of this objects class, as generated by e.g. the function :func:`~saveHDF5`.
        """
        # Define filepath and name
        if not(name == None):
            self.name = name
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".h5"
            else:
                raise Exception("name is required for loading")
        # Open hdf5 file
        h = h5py.File(filePath,"r")
        group = h[self.name]
        # Save the object to the group
        self.loadFromHDF5Handle(group)
        # Close file
        h.close()
        pass
    def loadFromHDF5Handle(self, gr):
        """Load the properties of the object from a hdf5 handle.
        Args:
            gr (:obj:`h5py.Group`): Handle used to read the object properties from
        """
        if ("hs" in list(gr.keys())):
            self.hs = gr["hs"][0]
        if ("tp" in list(gr.keys())):
            self.tp = gr["tp"][0]
        if ("direction" in list(gr.keys())):
            self.direction = gr["direction"][0]
        if ("gamma" in list(gr.keys())):
            self.gamma = gr["gamma"][0]
        if ("water_density" in list(gr.keys())):
            self.water_density = gr["water_density"][0]
        if ("name" in list(gr.keys())):
            self.name = gr["name"][0].decode("ascii")
        if ("description" in list(gr.keys())):
            self.description = gr["description"][0].decode("ascii")
    #------------------------
    # is_set function
    #------------------------
    def is_set(self, varName): # pragma: no cover

        """Check if a given property of the object is set

        Args:
            varName (:obj:`str`): name of the property to check

        Returns:
            :obj:`bool`: True if the property is set, else False
        """

        if (isinstance(getattr(self,varName),list) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (isinstance(getattr(self,varName),np.ndarray) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (getattr(self,varName) != None):
            return True
        return False
    #------------------------
