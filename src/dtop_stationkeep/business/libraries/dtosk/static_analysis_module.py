# This is the Station Keeping module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Neil Luxcey, Nicolas Michelet, Rocio Isorna
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


import numpy as np 
from scipy import optimize
from .global_parameters import acceleration_of_gravity
from .solver.Static import Static
from .linear_wave_module import compute_jonswap_spectrum
from .forceModel.FirstOrderWaveForce import FirstOrderWaveForce
from scipy import interpolate
from .rotation_module import rotation_about_z
import copy
import matplotlib.pyplot as plt
from .frequency_analysis_module import compute_tension_from_offset_position


def run_static_analysis_for_master_structure(body,body_array,beta,environment_array,water_depth):

	# Init
	static_solver = Static()

	# Init the variables to store the results
	# Results from initial position
	mooring_pretension=np.zeros(shape=(len(body.mooring_system_force.line_properties),2), dtype=float)
	# Results from equilibrium calculation (with applied steady forces from wind, current and mean wave drift)
	steady_position=np.zeros(shape=(len(beta),len(environment_array),6), dtype=float)
	steady_force=np.zeros(shape=(len(beta),len(environment_array),6), dtype=float)
	steady_mooring_force=np.zeros(shape=(len(beta),len(environment_array),len(body.mooring_system_force.line_properties),2,3), dtype=float)
	steady_mooring_tension=np.zeros(shape=(len(beta),len(environment_array),len(body.mooring_system_force.line_properties),2), dtype=float)
	steady_offset = np.zeros(shape=(len(beta),len(environment_array)),dtype=float)
	# Results from postprocessing (estimating the maxima)
	mooring_force = np.zeros(shape=(len(beta),len(environment_array),len(body.mooring_system_force.line_properties),2,3),dtype=float)
	mooring_tension = np.zeros(shape=(len(beta),len(environment_array),len(body.mooring_system_force.line_properties),2),dtype=float)
	mooring_touchdown = np.zeros(shape=(len(beta),len(environment_array),len(body.mooring_system_force.line_properties)),dtype=float)
	total_position = np.zeros(shape=(len(beta),len(environment_array),6),dtype=float)
	total_offset = np.zeros(shape=(len(beta),len(environment_array)),dtype=float)
	dynamic_offset = np.zeros(shape=(len(beta),len(environment_array)),dtype=float)

	# Compute equilibrium without excitation forces (initial position)
	[xmean,eq_found] = static_solver.compute_equilibrium(body)
	if not eq_found:
		raise Exception("Error in run_frequency_analysis: initial equilibrium not found")
	mooring_pretension = body.mooring_system_force.get_tension()

	# Compute equilibrium with steady forces from connected bodies
	steady_force = gather_steady_force_in_n(body_array,beta,environment_array)
	for ibeta in range(0,len(beta)):
		for ienv in range(0,len(environment_array)):
			body.set_constant_force(copy.deepcopy(steady_force[ibeta,ienv,:]))
			[xmean,eq_found] = static_solver.compute_equilibrium(body)
			body.set_constant_force(copy.deepcopy(steady_force[ibeta,ienv,:])*0.0) # Set constant force back to zero
			
			# Register equilibrium position
			steady_position[ibeta,ienv,0:6] = copy.deepcopy(xmean)
			steady_offset[ibeta,ienv] = np.linalg.norm(xmean[0:2]-body.initial_position[0:2])

			# Register tension in each line segment
			body.position = copy.deepcopy(xmean)
			[steady_mooring_force[ibeta,ienv,:,:,:],steady_mooring_tension[ibeta,ienv,:,:],dum1,dum2] = body.compute_tension_in_all_lines()
			body.position = copy.deepcopy(body.initial_position)

	# Compute equilibrium with maximum forces from connected bodies
	total_force = gather_total_force_in_n(body_array,beta,environment_array,water_depth)
	for ibeta in range(0,len(beta)):
		for ienv in range(0,len(environment_array)):
			body.set_constant_force(copy.deepcopy(total_force[ibeta,ienv,:]))
			[xmean,eq_found] = static_solver.compute_equilibrium(body)
			body.set_constant_force(copy.deepcopy(total_force[ibeta,ienv,:])*0.0) # Set constant force back to zero

			# Register equilibrium position
			total_position[ibeta,ienv,0:6] = copy.deepcopy(xmean)
			total_offset[ibeta,ienv] = np.linalg.norm(xmean[0:2]-body.initial_position[0:2])
			dynamic_offset[ibeta,ienv] =  total_offset[ibeta,ienv] - steady_offset[ibeta,ienv]

	# Register results in body.results
	body.results.uls_results.total_position=copy.deepcopy(total_position)
	body.results.uls_results.total_offset=copy.deepcopy(total_offset)
	body.results.uls_results.steady_offset=copy.deepcopy(steady_offset)
	body.results.uls_results.dynamic_offset=copy.deepcopy(dynamic_offset)
	body.results.uls_results.mooring_pretension=copy.deepcopy(mooring_pretension)
	body.results.uls_results.steady_position=copy.deepcopy(steady_position)
	body.results.uls_results.steady_force=copy.deepcopy(steady_force)
	body.results.uls_results.steady_mooring_force=copy.deepcopy(steady_mooring_force)
	body.results.uls_results.steady_mooring_tension=copy.deepcopy(steady_mooring_tension)

	# Compute max tension in mooring line segment
	[mooring_force,mooring_tension,mooring_touchdown,tension_versus_mbl] = compute_tension_from_offset_position(body,total_position)
	body.results.uls_results.mooring_force=copy.deepcopy(mooring_force)
	body.results.uls_results.mooring_tension=copy.deepcopy(mooring_tension)
	body.results.uls_results.mooring_touchdown=copy.deepcopy(mooring_touchdown)
	body.results.uls_results.tension_versus_mbl=copy.deepcopy(tension_versus_mbl)

	# Mirror some inputs used for the analysis
	body.results.uls_results.safety_factor = copy.deepcopy(body.mooring_design_criteria.safety_factor)

	# Get MBL used for ULS analysis
	body.results.uls_results.mbl_uls=body.get_mbl_uls_all_lines()

	# Put the mooring system back to the initial position
	body.position = copy.deepcopy(body.initial_position)
	body.mooring_system_force.solve_mooring_system(body.position)

	pass

def run_static_analysis_for_fixed_master_structure(body,body_array,beta,environment_array,water_depth):

	# Init
	forces_on_fixed_structure = np.zeros(shape=(len(beta),len(environment_array),6),dtype=float)

	# Compute equilibrium with steady forces from connected bodies
	total_force = gather_total_force_in_n(body_array,beta,environment_array,water_depth)
	
	# Add weight from master_structure
	total_force[:,:,2] = total_force[:,:,2] - body.mass*9.81

	body.results.uls_results.forces_on_fixed_structure=copy.deepcopy(total_force)


def gather_total_force_in_n(body_array,beta,environment_array,water_depth):

	# Init
	total_force_cplx = np.zeros(shape=(len(beta),len(environment_array),6), dtype=complex)
	total_force = np.zeros(shape=(len(beta),len(environment_array),6), dtype=float)
	force_in_n = np.zeros(shape=(6), dtype=complex)

	# Gather total_force as a complex value, including space_lag due to device position (space lag is only applied on the dynamic component)
	for ibeta in range(0,len(beta)):
		for ienv in range(0,len(environment_array)):
			for ibdy in range(0,len(body_array)):
				if body_array[ibdy].positioning_reference == 'masterstructure':

					for iline in range(0,len(body_array[ibdy].mooring_system_force.line_properties)):

						# Fetch index of anchor and fairlead
						id_anc = body_array[ibdy].mooring_system_force.line_properties[iline].anchor_node_number-1
						id_fair = body_array[ibdy].mooring_system_force.line_properties[iline].fairlead_node_number-1

						# Accumulate force  for each 'fix' node
						if body_array[ibdy].mooring_system_force.node_properties[id_anc].type == 'Fix':

							# Fetch position of the force application point
							pos = body_array[ibdy].mooring_system_force.node_properties[id_anc].position

							# Compute phase as a complex number, assuming that the maximum forces is due to a wave at period Tp
							k = environment_array[ienv].wave.wave_number(water_depth)
							kx = k*np.cos(beta[ibeta])
							ky = k*np.sin(beta[ibeta])
							space_lag = np.exp( (1j)*kx*pos[0] + (1j)*ky*pos[1] )

							# Compute force and moments
							f_steady = -body_array[ibdy].results.uls_results.steady_mooring_force[ibeta,ienv,iline,0,:]
							f_total = -body_array[ibdy].results.uls_results.mooring_force[ibeta,ienv,iline,0,:]
							f_dynamic = f_total - f_steady
							force_in_n[0:3] = f_steady + f_dynamic*space_lag
							force_in_n[3:6] = np.cross(pos,force_in_n[0:3])

							# Increment total force
							total_force_cplx[ibeta,ienv,:] = total_force_cplx[ibeta,ienv,0:6] + force_in_n[0:6]

						elif body_array[ibdy].mooring_system_force.node_properties[id_fair].type == 'Fix': # pragma: no cover

							# Fetch position of the force application point
							pos = body_array[ibdy].mooring_system_force.node_properties[id_fair].position

							# Compute phase as a complex number, assuming that the maximum forces is due to a wave at period Tp
							k = environment_array[ienv].wave.wave_number(water_depth)
							kx = k*np.cos(beta[ibeta])
							ky = k*np.sin(beta[ibeta])
							space_lag = np.exp( (1j)*kx*pos[0] + (1j)*ky*pos[1] )

							# Compute force and moments
							f_steady = -body_array[ibdy].results.uls_results.steady_mooring_force[ibeta,ienv,iline,1,:]
							f_total = -body_array[ibdy].results.uls_results.mooring_force[ibeta,ienv,iline,1,:]
							f_dynamic = f_total - f_steady
							force_in_n[0:3] = f_steady + f_dynamic*space_lag
							force_in_n[3:6] = np.cross(pos,force_in_n[0:3])

							# Increment total force
							total_force_cplx[ibeta,ienv,:] = total_force_cplx[ibeta,ienv,0:6] + force_in_n[0:6]
	
	# Compute force amplitude
	for ibeta in range(0,len(beta)):
		for ienv in range(0,len(environment_array)):
			for idof in range(0,6):
				total_force[ibeta,ienv,idof] = np.abs(total_force_cplx[ibeta,ienv,idof])

	return total_force

def gather_steady_force_in_n(body_array,beta,environment_array):

	# Init
	steady_force = np.zeros(shape=(len(beta),len(environment_array),6), dtype=float)
	force_in_n = np.zeros(shape=(6), dtype=float)

	# Gather steady_force
	for ibeta in range (0,len(beta)):
		for ienv in range(0,len(environment_array)):
			for ibdy in range(0,len(body_array)):
				if body_array[ibdy].positioning_reference == 'masterstructure':
					for iline in range(0,len(body_array[ibdy].mooring_system_force.line_properties)):

						# Fetch index of anchor and fairlead
						id_anc = body_array[ibdy].mooring_system_force.line_properties[iline].anchor_node_number-1
						id_fair = body_array[ibdy].mooring_system_force.line_properties[iline].fairlead_node_number-1

						# Accumulate force  for each 'fix' node
						if body_array[ibdy].mooring_system_force.node_properties[id_anc].type == 'Fix':

							# Compute force and moments
							pos = body_array[ibdy].mooring_system_force.node_properties[id_anc].position
							force_in_n[0:3] = -body_array[ibdy].results.uls_results.steady_mooring_force[ibeta,ienv,iline,0,:]
							force_in_n[3:6] = np.cross(pos,force_in_n[0:3])

							# Increment total force
							steady_force[ibeta,ienv,:] = steady_force[ibeta,ienv,0:6] + force_in_n[0:6]

						elif body_array[ibdy].mooring_system_force.node_properties[id_fair].type == 'Fix': # pragma: no cover

							# Compute force and moments
							pos = body_array[ibdy].mooring_system_force.node_properties[id_fair].position
							force_in_n[0:3] = -body_array[ibdy].results.uls_results.steady_mooring_force[ibeta,ienv,iline,1,:]
							force_in_n[3:6] = np.cross(pos,force_in_n[0:3])

							# Increment total force
							steady_force[ibeta,ienv,:] = steady_force[ibeta,ienv,0:6] + force_in_n[0:6]

	return steady_force

def compute_forces_on_fixed_structure(body,beta,environment_array,water_depth):

	# Init
	forces_on_fixed_structure = np.zeros(shape=(len(beta),len(environment_array),6),dtype=float)
	w = np.linspace(2*np.pi/50.0, 2*np.pi/3.0, num=100)

	# Loop over all the dirctions and environmental conditions
	for idx in range(0,len(environment_array)):
		
		# Compute wave spectrum
		Seta = compute_jonswap_spectrum(environment_array[idx].wave.hs,environment_array[idx].wave.tp,w)

		for idir in range(0,len(beta)):
			
			# Set colinear direction for current/wind/wave
			environment_array[idx].set_colinear_direction(beta[idir])

			# Compute static forces in {b}
			forces_on_fixed_structure[idir,idx,:] = body.compute_static_force(current = environment_array[idx].current, wind = environment_array[idx].wind, wave = environment_array[idx].wave, water_depth = water_depth, include_drag_due_to_wave_particle_velocity = True)

	body.results.uls_results.forces_on_fixed_structure=copy.deepcopy(forces_on_fixed_structure)

	return forces_on_fixed_structure



