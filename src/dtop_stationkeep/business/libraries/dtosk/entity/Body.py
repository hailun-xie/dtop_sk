import collections
import numpy as np
import os
import json
import h5py
from ..forceModel import GravityForce
from ..forceModel import HydrostaticForce
from ..mooring import Mooring
from ..forceModel import ConstantForce
from ..forceModel import RotorForce
from . import BodyPart
from ..forceModel import WindForce
from ..forceModel import CurrentForce
from ..forceModel import MeanWaveDriftForce
from ..mooring import MooringDesignCriteria
from ..results import BodyResults
#------------------------------------
# @ USER DEFINED IMPORTS START
from ..utilities.Python_for_Nemoh.NemohReader import NemohReader
import copy
import logging
logger = logging.getLogger()
# @ USER DEFINED IMPORTS END
#------------------------------------

class Body():

    """data model representing a body
    """

    #------------
    # Constructor
    #------------
    def __init__(self,name=None):
#------------------------------------
# @ USER DEFINED DESCRIPTION START
# @ USER DEFINED DESCRIPTION END
#------------------------------------

        self._machine_type=''
        self._submerged_volume=0.0
        self._mass=0.0
        self._mass_matrix=np.zeros(shape=(6,6), dtype=float)
        self._cog_position=np.zeros(shape=(3), dtype=float)
        self._rotation_center_in_b=np.zeros(shape=(3), dtype=float)
        self._initial_position=np.zeros(shape=(6), dtype=float)
        self._position=np.zeros(shape=(6), dtype=float)
        self._positioning_type='fixed'
        self._positioning_reference='seabed'
        self._gravity_force=GravityForce.GravityForce()
        self._gravity_force.description = 'gravity force model'
        self._hydrostatic_force=HydrostaticForce.HydrostaticForce()
        self._hydrostatic_force.description = 'hydrostatic force model'
        self._mooring_system_force=Mooring.Mooring()
        self._mooring_system_force.description = 'mooring force model'
        self._constant_force=ConstantForce.ConstantForce()
        self._constant_force.description = 'constant force model'
        self._rotor_force=[]
        self._body_parts=[]
        self._wind_force=WindForce.WindForce()
        self._wind_force.description = 'wind force model'
        self._current_force=CurrentForce.CurrentForce()
        self._current_force.description = 'current force model'
        self._mean_wave_drift_force=MeanWaveDriftForce.MeanWaveDriftForce()
        self._mean_wave_drift_force.description = 'wave drift force model'
        self._active_dof_static=np.zeros(shape=(6), dtype=int)
        self._active_dof_dynamic=np.zeros(shape=(6), dtype=int)
        self._hdb_source='nemoh_run'
        self._critical_damping_coefficient=np.zeros(shape=(6), dtype=float)
        self._mooring_design_criteria=MooringDesignCriteria.MooringDesignCriteria()
        self._mooring_design_criteria.description = 'Criteria for mooring design'
        self._results=BodyResults.BodyResults()
        self._results.description = 'Results from the different analyses'
        self._water_depth=0.0
        self._name='none'
        self._description=''
        if not(name == None): # pragma: no cover
            self._name = name

#------------------------------------
# @ USER DEFINED PROPERTIES START
        self.active_dof_static[:]=1
        self.active_dof_dynamic[:]=1
        self.A = 0.0 # Added mass matrix
        self.B = 0.0 # Linear damping matrix
        self.Fe = 0.0 # Excitation transfer function
        self.w = 0.0 # Frequency array (omega, rad/s)
        self.dir = 0.0 # direction array in rad
# @ USER DEFINED PROPERTIES END
#------------------------------------

#------------------------------------
# @ USER DEFINED METHODS START

    def populate_simple_buoyancy_force(self, rho_water):

        self.constant_force.force_in_n = np.array([0,0,self.submerged_volume*rho_water*self.gravity_force.accelerationOfGravity,0,0,0],dtype = float)

        pass

    def populate_steady_force_model(self,steady_force_model,water_depth):

        if steady_force_model.method == 'main_dimensions':

            ####################
            # Build wind model #
            ####################

            self.body_parts.append(BodyPart.BodyPart())
            self.body_parts[0].mean_wave_drift_force_applied = False

            if steady_force_model.device_dry_profile == 'cylinder':

                # Cylinder size
                self.body_parts[0].geometry_type='cylinder'
                self.body_parts[0].cylinder.length=steady_force_model.device_dry_height
                self.body_parts[0].cylinder.diameter=steady_force_model.device_dry_width

            elif steady_force_model.device_dry_profile == 'rectangle':

                # Rectangular box size
                self.body_parts[0].geometry_type='rectangle'
                self.body_parts[0].rectangle.dx=steady_force_model.device_dry_width
                self.body_parts[0].rectangle.dy=steady_force_model.device_dry_width
                self.body_parts[0].rectangle.dz=steady_force_model.device_dry_height

            # Body part position
            if self.positioning_type=='moored':
                self.body_parts[0].position_in_b[2]=steady_force_model.device_dry_height/2
            elif self.positioning_type=='fixed':
                self.body_parts[0].position_in_b[2]=steady_force_model.device_dry_height/2 + water_depth
            logger.info('Dry part placed at z_b=' + str(self.body_parts[0].position_in_b[2]) + 'm')

            ###########################################
            # Build current and mean wave drift model #
            ###########################################

            self.body_parts.append(BodyPart.BodyPart())

            if steady_force_model.device_wet_profile == 'cylinder':

                # Cylinder size
                self.body_parts[1].geometry_type='cylinder'
                self.body_parts[1].cylinder.length=steady_force_model.device_wet_height
                self.body_parts[1].cylinder.diameter=steady_force_model.device_wet_width

            elif steady_force_model.device_dry_profile == 'rectangle':

                # Rectangular box size
                self.body_parts[1].geometry_type='rectangle'
                self.body_parts[1].rectangle.dx=steady_force_model.device_wet_width
                self.body_parts[1].rectangle.dy=steady_force_model.device_wet_width
                self.body_parts[1].rectangle.dz=steady_force_model.device_wet_height

            # Body part position
            if self.positioning_type=='moored':
                self.body_parts[1].position_in_b[2]=-steady_force_model.device_wet_height/2
            elif self.positioning_type=='fixed':
                self.body_parts[1].position_in_b[2]=steady_force_model.device_wet_height/2
                if steady_force_model.device_wet_height<water_depth:
                    self.body_parts[1].mean_wave_drift_force_applied = False
            logger.info('Wet part placed at z_b=' + str(self.body_parts[1].position_in_b[2]) + 'm')

        elif steady_force_model.method == 'from_file':
            raise Exception("Steady force model coefficients read from file: not implemented yet")
        else:
            raise Exception("Unknown steady force model method : " + str(steady_force_model.method))


    def populate_from_nemoh_run(self,path_to_nemoh_result_folder,nemoh_scale_factor):

        # Read Nemoh results
        nemoh = NemohReader( path_to_nemoh_result_folder )

        # Populate body mass, mass matrix and hydrostatic
        self.mass_matrix = copy.deepcopy(nemoh.mesh.inertia)
        self.mass = self.mass_matrix[0,0]
        self.cog_position = copy.deepcopy(nemoh.mesh.CoG)
        self.hydrostatic_force.stiffness_matrix = copy.deepcopy(nemoh.mesh.KH)
        self.gravity_force.include_gravity = False # Restoring due to gravity is included in the stiffness matrix - Checked

        # Populate body hydrodynamics
        self.A = copy.deepcopy(np.transpose(nemoh.A,(2,0,1)))
        self.B = copy.deepcopy(np.transpose(nemoh.B,(2,0,1)))
        self.Fe = copy.deepcopy(np.transpose(np.conj(nemoh.Fe),(1,0,2)))

        # Populate frequency and direction array
        self.hdb_source = 'nemoh_run'
        self.dir = copy.deepcopy(nemoh.dir*np.pi/180.0)
        self.w = copy.deepcopy(nemoh.w)

        # Scaling if necessary
        if nemoh_scale_factor != 1:

            # Mass matrix
            self.mass_matrix[0:3,0:3] = nemoh_scale_factor**3*self.mass_matrix[0:3,0:3]
            self.mass_matrix[3:6,0:3] = nemoh_scale_factor**4*self.mass_matrix[3:6,0:3]
            self.mass_matrix[0:3,3:6] = nemoh_scale_factor**4*self.mass_matrix[0:3,3:6]
            self.mass_matrix[3:6,3:6] = nemoh_scale_factor**5*self.mass_matrix[3:6,3:6]

            # CoG position
            self.cog_position = nemoh_scale_factor*self.cog_position

            # Hydrostatic stiffness matrix
            self.hydrostatic_force.stiffness_matrix[0:3,0:3] = nemoh_scale_factor**2*self.hydrostatic_force.stiffness_matrix[0:3,0:3]
            self.hydrostatic_force.stiffness_matrix[3:6,0:3] = nemoh_scale_factor**3*self.hydrostatic_force.stiffness_matrix[3:6,0:3]
            self.hydrostatic_force.stiffness_matrix[0:3,3:6] = nemoh_scale_factor**3*self.hydrostatic_force.stiffness_matrix[0:3,3:6]
            self.hydrostatic_force.stiffness_matrix[3:6,3:6] = nemoh_scale_factor**4*self.hydrostatic_force.stiffness_matrix[3:6,3:6]

            # Added mass
            for i in range(len(self.A)):
                self.A[i,0:3,0:3] = nemoh_scale_factor**3*self.A[i,0:3,0:3]
                self.A[i,3:6,0:3] = nemoh_scale_factor**4*self.A[i,3:6,0:3]
                self.A[i,0:3,3:6] = nemoh_scale_factor**4*self.A[i,0:3,3:6]
                self.A[i,3:6,3:6] = nemoh_scale_factor**5*self.A[i,3:6,3:6]

            # Radiation damping
            for i in range(len(self.B)):
                self.B[i,0:3,0:3] = nemoh_scale_factor**2.5*self.B[i,0:3,0:3]
                self.B[i,3:6,0:3] = nemoh_scale_factor**3.5*self.B[i,3:6,0:3]
                self.B[i,0:3,3:6] = nemoh_scale_factor**3.5*self.B[i,0:3,3:6]
                self.B[i,3:6,3:6] = nemoh_scale_factor**4.5*self.B[i,3:6,3:6]

            # Excitation Forces
            for i in range(len(Fe)):
                self.Fe[i,:,0:3] = nemoh_scale_factor**2*self.Fe[i,:,0:3]
                self.Fe[i,:,3:6] = nemoh_scale_factor**3*self.Fe[i,:,3:6]

            # Pulsation
            self.w = nemoh_scale_factor**(-0.5)*self.w

        pass

    def populate_from_mc_module_outputs(self,hydro_data_MC_module):

        # Populate body hydrodynamics
        self.A = hydro_data_MC_module.added_mass
        self.B = hydro_data_MC_module.radiation_damping
        self.Fe = hydro_data_MC_module.excitation_force_real + (1j)*hydro_data_MC_module.excitation_force_imag
        self.gravity_force.include_gravity = False # Restoring due to gravity is included in the stiffness matrix (checked with Francesco)
        self.B_fitting = hydro_data_MC_module.fitting_damping
        self.K_fitting = hydro_data_MC_module.fitting_stiffness
        self.tp_fitting_array = np.unique(hydro_data_MC_module.tp_capture_width)
        self.hs_fitting_array = np.unique(hydro_data_MC_module.hs_capture_width)
        self.dir_fitting_array = np.unique(hydro_data_MC_module.wave_angle_capture_width)
        self.B_add = hydro_data_MC_module.additional_damping
        self.K_add = hydro_data_MC_module.additional_stiffness
        self.shared_dof = hydro_data_MC_module.shared_dof
        self.velocity_transformation_matrix = hydro_data_MC_module.velocity_transformation_matrix
        
        # Remove the transportation to COG since it is done by SK (This is done because we will need COB from MC, but it is not available in the results of MC)
        if (self.shared_dof[0] == 1 and self.shared_dof[4] == 1):
            idx1=0
            idx2=sum(self.shared_dof[0:4]) - 1
            self.velocity_transformation_matrix[idx1,idx2] = 0
        if (self.shared_dof[1] == 1 and self.shared_dof[3] == 1):
            idx1=sum(self.shared_dof[0:1]) - 1
            idx2 = sum(self.shared_dof[0:3]) - 1
            self.velocity_transformation_matrix[idx1,idx2] = 0


        self.pto_damping = hydro_data_MC_module.pto_damping
        self.hydrostatic_matrix_gendof = hydro_data_MC_module.hydrostatic_matrix #generalized dofs

        self.active_dof_static = copy.deepcopy(self.shared_dof)
        self.active_dof_dynamic = copy.deepcopy(self.shared_dof)

        # Detect if more than one body is defined per device. If it is the case, solve the static problem only in horizontal plane
        if sum(self.shared_dof)<self.hydrostatic_matrix_gendof.shape[0]:
            self.active_dof_static[2:4]=0
        elif sum(self.shared_dof)>self.hydrostatic_matrix_gendof.shape[0]:
            raise Exception('Inconsistent definition of shared_dof and hydrodynamic/hydrostatic matrices size')


        # Populate frequency and direction array
        self.hdb_source = 'mc_module'
        self.dir = copy.deepcopy(hydro_data_MC_module.direction)
        self.w = copy.deepcopy(hydro_data_MC_module.frequency)


    def set_constant_force(self, Fext_n):
        self.constant_force.force_in_n = copy.deepcopy(Fext_n)
        pass

    def compute_static_force_for_relevant_dof(self,current = None, wind = None, wave = None, water_depth = 100.0):
        ndof = sum(self.active_dof_static)
        force = np.zeros(shape=(ndof),dtype=float)
        force_6 = self.compute_static_force(current = current, wind = wind, wave = wave, water_depth = water_depth)
        counter = 0
        for idx in range(0,6):
            if self.active_dof_static[idx]==1:
                force[counter] = force_6[idx]
                counter = counter+1

        return force


    def compute_static_force(self,current = None, wind = None, wave = None, water_depth = 100.0, include_mooring = True, include_drag_due_to_wave_particle_velocity = False,include_hydrostatic = True):
        # Compute static force in b
        force = np.zeros(shape=(6),dtype=float)

        # compute gravity force
        force=self.gravity_force.compute_force(self.position,self.cog_position,self.mass,self.rotation_center_in_b)
        # print('gravity')
        # print(self.gravity_force.compute_force(self.position,self.cog_position,self.mass))

        # compute hydrostatic force
        if include_hydrostatic:
            force=force+self.hydrostatic_force.compute_force(self.position,self.rotation_center_in_b)
            # print('hydrostatic')
            # print(self.hydrostatic_force.compute_force(self.position))

        # compute mooring system force
        if include_mooring:
            if self.mooring_system_force.is_initiated:
                force=force+self.mooring_system_force.compute_force(self.position,self.rotation_center_in_b)
                # print('mooring')
                # print(self.mooring_system_force.compute_force(self.position)

        # compute constant force
        force=force+self.constant_force.compute_force(self.position,self.rotation_center_in_b)

        # compute rotor forces, if defined
        for idx in range (0,len(self.rotor_force)):
            if not current is None:
                force = force + self.rotor_force[idx].compute_force(self.position,self.rotation_center_in_b,current,water_depth,wave)
                # print('rotor')
                # print(self.rotor_force[idx].compute_force(self.position,current,water_depth))

        # compute wind force
        if not wind is None:
            force = force + self.wind_force.compute_force(self.position,self.body_parts,self.rotation_center_in_b,wind)
            # print('wind')
            # print(self.wind_force.compute_force(self.position,self.body_parts,wind))

        # compute current force
        if not current is None:
            if include_drag_due_to_wave_particle_velocity:
                force = force + self.current_force.compute_force(self.position,self.body_parts,self.rotation_center_in_b,current,water_depth,wave=wave)
                # print('current force = ' + str(self.current_force.compute_force(self.position,self.body_parts,current,water_depth,wave=wave)))
            else:
                force = force + self.current_force.compute_force(self.position,self.body_parts,self.rotation_center_in_b,current,water_depth)
                # print('current force = ' + str(self.current_force.compute_force(self.position,self.body_parts,current,water_depth)))

        # Compute mean wave drift force
        if not wave is None:
            force = force + self.mean_wave_drift_force.compute_force(self.positioning_type,self.position,self.body_parts,self.rotation_center_in_b,wave,water_depth)
            # print('drfit')
            # print(self.mean_wave_drift_force.compute_force(self.positioning_type,self.position,self.body_parts,wave,water_depth))

        return force

    def get_linear_stiffness(self):

        # stiffness from mooring
        k=self.mooring_system_force.compute_linear_stiffness(self.position)

        # stiffness from hydrostatic
        k=k+self.hydrostatic_force.stiffness_matrix

        return k

    def compute_mooring_stiffness_by_difference(self):
        # Ouput : Stiffness matrix from mooring in {br} cs.
        if self.hdb_source == 'mc_module':
            rotation_center_in_b = self.cog_position # buoyancy center
        else:
            rotation_center_in_b = np.array([0.0,0.0,0.0]) # all is defined related to [0,0,0] in Nemoh
        # Save position
        pos_init=np.array(self.position)
        pos_temp=np.array(self.position)

        # stiffness from all the mooring force only
        delta=np.array([0.1,0.1,0.1,0.1*np.pi/180.0,0.1*np.pi/180.0,0.1*np.pi/180.0])
        k=np.zeros(shape=(6,6),dtype=float)

        for i in range(0,6):

            # +delta
            pos_temp=np.array(pos_init)
            if i==0:
                pos_temp[0]=pos_init[0] + np.cos(pos_init[5])*delta[0]
                pos_temp[1]=pos_init[1] + np.sin(pos_init[5])*delta[0]
            elif i==1:
                pos_temp[0]=pos_init[0] - np.sin(pos_init[5])*delta[1]
                pos_temp[1]=pos_init[1] + np.cos(pos_init[5])*delta[1]
            else:
                pos_temp[i]=pos_init[i]+delta[i]

            self.position=pos_temp
            f2=self.mooring_system_force.compute_force(self.position,rotation_center_in_b)

            # -delta
            pos_temp=np.array(pos_init)
            if i==0:
                pos_temp[0]=pos_init[0] - np.cos(pos_init[5])*delta[0]
                pos_temp[1]=pos_init[1] - np.sin(pos_init[5])*delta[0]
            elif i==1:
                pos_temp[0]=pos_init[0] + np.sin(pos_init[5])*delta[1]
                pos_temp[1]=pos_init[1] - np.cos(pos_init[5])*delta[1]
            else:
                pos_temp[i]=pos_init[i]-delta[i]

            self.position=pos_temp
            f1=self.mooring_system_force.compute_force(self.position,rotation_center_in_b)

            # Compute k
            k[:,i]=-(f2-f1)/(2*delta[i])

        # Restore position
        self.position=np.array(pos_init)

        return k

    def compute_linear_stiffness_by_difference(self,current = None, wind = None, wave = None, water_depth = 100.0):

        # Save position
        pos_init=np.array(self.position)
        pos_temp=np.array(self.position)

        # stiffness from all the static forces
        delta=np.array([0.1,0.1,0.1,0.1*np.pi/180.0,0.1*np.pi/180.0,0.1*np.pi/180.0])
        k=np.zeros(shape=(6,6),dtype=float)

        for i in range(0,6):
            pos_temp=np.array(pos_init)
            pos_temp[i]=pos_init[i]+delta[i]
            self.position=pos_temp
            f2=self.compute_static_force(current = current, wind = wind, wave = wave, water_depth = water_depth)
            pos_temp=np.array(pos_init)
            pos_temp[i]=pos_init[i]-delta[i]
            self.position=pos_temp
            f1=self.compute_static_force(current = current, wind = wind, wave = wave, water_depth = water_depth)
            k[:,i]=-(f2-f1)/(2*delta[i])

        # Restore position
        self.position=np.array(pos_init)

        return k

    def set_position_relevant_dof(self,x):
        counter = -1
        for idx in range(0,6):
            if self.active_dof_static[idx]==1:
                counter = counter+1
                self.position[idx] = x[counter]


    def compute_linear_stiffness_by_difference_for_relevant_dof(self,current = None, wind = None, wave = None, water_depth = 100.0):

        k_6 = self.compute_linear_stiffness_by_difference(current = current, wind = wind, wave = wave, water_depth = water_depth)
        ndof = sum(self.active_dof_static)
        k= np.zeros(shape=(ndof,ndof),dtype=float)
        counter1 = -1
        counter2 = -1
        for idx1 in range(0,6):
            if self.active_dof_static[idx1]==1:
                counter1 = counter1+1
                counter2 = -1
                for idx2 in range(0,6):
                    if self.active_dof_static[idx2]==1:
                        counter2 = counter2+1
                        k[counter1,counter2] = k_6[idx1,idx2]
        return k

    def compute_tension_in_all_lines(self):

        # Init
        force = np.zeros(shape=(len(self.mooring_system_force.line_properties),2,3),dtype=float)
        tension = np.zeros(shape=(len(self.mooring_system_force.line_properties),2),dtype=float)
        tension_vs_mbl = np.zeros(shape=(len(self.mooring_system_force.line_properties)),dtype=float)
        Lb = np.zeros(shape=(len(self.mooring_system_force.line_properties)),dtype=float)
        SF = self.mooring_design_criteria.safety_factor # Safety factor

        # Solve the mooring system at body position
        self.mooring_system_force.solve_mooring_system(self.position)

        # Get MBL values for ULS analysis
        mbl_uls = self.get_mbl_uls_all_lines()

        # Get force and tension of all segments
        for ilin in range(0,len(self.mooring_system_force.line_properties)):
            # Anchor
            force[ilin,0,:] = self.mooring_system_force.get_anchor_force_in_n(ilin)
            tension[ilin,0] = np.linalg.norm(force[ilin,0,:])
            # Fairlead
            force[ilin,1,:] = self.mooring_system_force.get_fairlead_force_in_n(ilin)
            tension[ilin,1] = np.linalg.norm(force[ilin,1,:])
            # Touchdown length
            [dum1,Lb[ilin],dum2,dum3] = self.mooring_system_force.get_line_geometry_data(ilin)
            # Tension relative to mbl
            tension_vs_mbl[ilin] = SF * max(tension[ilin,0],tension[ilin,1]) / mbl_uls[ilin]

        return force,tension,Lb,tension_vs_mbl

    def get_mbl_uls_all_lines(self):

        # Init
        mbl_uls = np.zeros(shape=(len(self.mooring_system_force.line_properties)),dtype=float)

        # Fetch MBL value for ULS analysis
        for ilin in range(0,len(self.mooring_system_force.line_properties)):
            idtyp = self.mooring_system_force.line_properties[ilin].line_type_index
            if self.mooring_system_force.line_dictionnary[idtyp].mbl_uls==0.0:
                self.mooring_system_force.line_dictionnary[idtyp].mbl_uls=self.mooring_system_force.line_dictionnary[idtyp].mbl
            mbl_uls[ilin] = self.mooring_system_force.line_dictionnary[idtyp].mbl_uls

        return mbl_uls

    def get_diameter_fls_all_lines(self):

        # Init
        diameter_fls = np.zeros(shape=(len(self.mooring_system_force.line_properties)),dtype=float)
        cross_section_area_fls = np.zeros(shape=(len(self.mooring_system_force.line_properties)),dtype=float)

        # Fetch diameter value for FLS analysis
        for ilin in range(0,len(self.mooring_system_force.line_properties)):

            # Fetch line type
            idtyp = self.mooring_system_force.line_properties[ilin].line_type_index

            [cross_section_area_fls[ilin], diameter_fls[ilin] ] = self.mooring_system_force.line_dictionnary[idtyp].compute_fls_area()

        return diameter_fls,cross_section_area_fls

    def populate_mooring_design(self):

        # Mooring design main properties
        self.results.design_results.mooring.line_dictionnary=copy.deepcopy(self.mooring_system_force.line_dictionnary)
        self.results.design_results.mooring.node_properties=copy.deepcopy(self.mooring_system_force.node_properties)
        self.results.design_results.mooring.line_properties=copy.deepcopy(self.mooring_system_force.line_properties)
        self.results.design_results.mooring.solver_options=copy.deepcopy(self.mooring_system_force.solver_options)

        # Line segment cost
        self.results.design_results.mooring.line_cost = np.zeros(shape=(len(self.mooring_system_force.line_properties)),dtype=float)
        total_mooring_line_cost = 0.0
        for iline in range(0,len(self.mooring_system_force.line_properties)):
            iline_type_index = self.mooring_system_force.line_properties[iline].line_type_index
            cost_per_meter = self.mooring_system_force.line_dictionnary[iline_type_index].cost_per_meter
            self.results.design_results.mooring.line_cost[iline] = self.mooring_system_force.line_properties[iline].unstretched_length*cost_per_meter
            total_mooring_line_cost = total_mooring_line_cost + self.results.design_results.mooring.line_cost[iline]

        # Ancillaries
        self.mooring_design_criteria.ancillaries.compute_cost(total_mooring_line_cost,self.name)
        self.results.design_results.mooring.ancillaries = copy.deepcopy(self.mooring_design_criteria.ancillaries)

    def get_position_relevant_dof(self):
        ndof = sum(self.active_dof_static)
        xpos=np.zeros(shape=(ndof),dtype=float)
        counter = -1
        for idx in range(0,6):
            if self.active_dof_static[idx]==1:
                counter = counter+1
                xpos[counter] = self.position[idx]
        return xpos

    def plot(self,ax):

        # Init
        xlma=np.array([0.0])
        xlmi=np.array([0.0])
        ylma=np.array([0.0])
        ylmi=np.array([0.0])
        zlma=np.array([0.0])
        zlmi=np.array([0.0])

        # Plot body geometry
        for idx in range(0,len(self.body_parts)):
            [xlma_t, xlmi_t, ylma_t, ylmi_t, zlma_t, zlmi_t] = self.body_parts[idx].plot(self.position,ax)
            xlma = np.append(xlma,xlma_t.max())
            ylma =np.append(ylma,ylma_t.max())
            zlma =np.append(zlma,zlma_t.max())
            xlmi =np.append(xlmi,xlmi_t.min())
            ylmi =np.append(ylmi,ylmi_t.min())
            zlmi =np.append(zlmi,zlmi_t.min())

        # Plot mooring system
        if len(self.mooring_system_force.line_properties)>0:
            [xlma_t, xlmi_t, ylma_t, ylmi_t, zlma_t, zlmi_t] = self.mooring_system_force.plot_mooring_lines(ax)
            xlma = np.append(xlma,xlma_t.max())
            ylma =np.append(ylma,ylma_t.max())
            zlma =np.append(zlma,zlma_t.max())
            xlmi =np.append(xlmi,xlmi_t.min())
            ylmi =np.append(ylmi,ylmi_t.min())
            zlmi =np.append(zlmi,zlmi_t.min())

        # Plot rotor if present
        if len(self.rotor_force)>0:
            for idx in range(0,len(self.rotor_force)):
                [xlma_t, xlmi_t, ylma_t, ylmi_t, zlma_t, zlmi_t] = self.rotor_force[idx].plot(self.position,ax)
                xlma = np.append(xlma,xlma_t.max())
                ylma =np.append(ylma,ylma_t.max())
                zlma =np.append(zlma,zlma_t.max())
                xlmi =np.append(xlmi,xlmi_t.min())
                ylmi =np.append(ylmi,ylmi_t.min())
                zlmi =np.append(zlmi,zlmi_t.min())

        return xlma, xlmi, ylma, ylmi, zlma, zlmi

    def reduce_mooring_radius(self,mooring_touchdown_min,water_density):

        # Init
        perc = 0.95

        # Loop over the anchors
        ls = self.mooring_system_force.anchor_position_in_n.shape
        for idx in range (0,ls[0]):

            # Compute vector from anchor to body
            AB = - self.mooring_system_force.anchor_position_in_n[idx,0:2] + self.initial_position[0:2]

            # Compute vector from anchor to new anchor
            AA1 = AB/np.linalg.norm(AB)*mooring_touchdown_min*perc

            # Compute new anchor position in {n}
            self.mooring_system_force.anchor_position_in_n[idx,0:2] = self.mooring_system_force.anchor_position_in_n[idx,0:2] + AA1

        # Same loop for the nodes that have anchors (should be exactly the same in practice)
        for idx in range(0,len(self.mooring_system_force.node_properties)):
            if (self.mooring_system_force.node_properties[idx].type == 'Fix' and self.mooring_system_force.node_properties[idx].anchor_required):

                # Compute vector from anchor to body
                AB = - self.mooring_system_force.node_properties[idx].position[0:2] + self.initial_position[0:2]

                # Compute vector from anchor to new anchor
                AA1 = AB/np.linalg.norm(AB)*mooring_touchdown_min*perc

                self.mooring_system_force.node_properties[idx].position[0:2] = self.mooring_system_force.node_properties[idx].position[0:2] + AA1
        # Reduce the length of each line
        for ilin in range(0,len(self.mooring_system_force.line_properties)):
            self.mooring_system_force.line_properties[ilin].unstretched_length = self.mooring_system_force.line_properties[ilin].unstretched_length-mooring_touchdown_min*perc

        # Reduce the mooring radius value
        self.mooring_system_force.mooring_radius = self.mooring_system_force.mooring_radius - mooring_touchdown_min*perc

        # Initiate pymap for the mooring design
        self.mooring_system_force.initiate_calculation(-self.mooring_system_force.anchor_position_in_n[0,2],water_density,vessel_position = self.initial_position)

    def compute_design_force_at_anchor(self):

        # Init
        design_force_at_anchor = np.zeros(shape=(3),dtype = float)

        # Make list of nodes that need an anchor
        node_list = []
        for inode in range(0,len(self.mooring_system_force.node_properties)):
            if self.mooring_system_force.node_properties[inode].anchor_required:
                node_list.append(inode)
        nnodes = len(node_list)

        # Fetch mooring force on each node (sum the contribution of each line)
        d_line = 0.0
        if nnodes>0:
            force_node = np.zeros(shape=(nnodes,3),dtype=float)
            for inode in range(0,nnodes):
                for iline in range(0,len(self.mooring_system_force.line_properties)):
                    idx=-1
                    if self.mooring_system_force.line_properties[iline].anchor_node_number-1 == node_list[inode]:
                        idx = self.mooring_system_force.line_properties[iline].anchor_node_number-1
                        idxp=0
                    elif self.mooring_system_force.line_properties[iline].fairlead_node_number-1 == node_list[inode]:
                        idx = self.mooring_system_force.line_properties[iline].fairlead_node_number-1
                        idxp=1
                    if idx>=0:
                        fx = np.sqrt(self.results.uls_results.mooring_force[:,:,iline,idxp,0].max() ** 2 + self.results.uls_results.mooring_force[:,:,iline,idxp,1].max() **2 )
                        fy = 0.0
                        fz = self.results.uls_results.mooring_force[:,:,iline,idxp,2].max()
                        if self.mooring_design_criteria.mooring_type == 'catenary':
                            tmax = np.sqrt(fx**2 + fy**2 + fz**2)
                            itype = self.mooring_system_force.line_properties[iline].line_type_index
                            mbl = self.mooring_system_force.line_dictionnary[itype].mbl
                            proof_load_coeff = self.mooring_design_criteria.proof_load_coeff
                            if tmax < mbl*proof_load_coeff:
                                fx = mbl*proof_load_coeff
                                fy = 0.0
                                fz = 0.0
                        force_node[inode,0] = fx + force_node[inode,0]
                        force_node[inode,1] = fy + force_node[inode,1]
                        force_node[inode,2] = fz + force_node[inode,2]
                        itype = self.mooring_system_force.line_properties[iline].line_type_index
                        d_line = max(d_line,self.mooring_system_force.line_dictionnary[itype].diameter)

            # Fetch the maximum load condition
            design_force_at_anchor[0] = force_node[:,0].max()
            design_force_at_anchor[1] = force_node[:,1].max()
            design_force_at_anchor[2] = force_node[:,2].max()
        else:
            design_force_at_anchor = None

        return design_force_at_anchor,nnodes,d_line

# @ USER DEFINED METHODS END
#------------------------------------

    #------------
    # Get functions
    #------------
    @ property
    def machine_type(self): # pragma: no cover
        """str: tec or wec
        """
        return self._machine_type
    #------------
    @ property
    def submerged_volume(self): # pragma: no cover
        """float: device submerged volume. Only used for bottom fixed devices to compute the weight in water. [m3]
        """
        return self._submerged_volume
    #------------
    @ property
    def mass(self): # pragma: no cover
        """float: none []
        """
        return self._mass
    #------------
    @ property
    def mass_matrix(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:none dim(6,6) []
        """
        return self._mass_matrix
    #------------
    @ property
    def cog_position(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:none dim(3) []
        """
        return self._cog_position
    #------------
    @ property
    def rotation_center_in_b(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:none dim(3) []
        """
        return self._rotation_center_in_b
    #------------
    @ property
    def initial_position(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:Position of the body in n. 3 translations, 3 rotations as defined in the input dim(6) []
        """
        return self._initial_position
    #------------
    @ property
    def position(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:Position of the body in n. 3 translations, 3 rotations dim(6) []
        """
        return self._position
    #------------
    @ property
    def positioning_type(self): # pragma: no cover
        """str: fixed or moored
        """
        return self._positioning_type
    #------------
    @ property
    def positioning_reference(self): # pragma: no cover
        """str: seabed or masterstructure
        """
        return self._positioning_reference
    #------------
    @ property
    def gravity_force(self): # pragma: no cover
        """:obj:`~.GravityForce.GravityForce`: gravity force model
        """
        return self._gravity_force
    #------------
    @ property
    def hydrostatic_force(self): # pragma: no cover
        """:obj:`~.HydrostaticForce.HydrostaticForce`: hydrostatic force model
        """
        return self._hydrostatic_force
    #------------
    @ property
    def mooring_system_force(self): # pragma: no cover
        """:obj:`~.Mooring.Mooring`: mooring force model
        """
        return self._mooring_system_force
    #------------
    @ property
    def constant_force(self): # pragma: no cover
        """:obj:`~.ConstantForce.ConstantForce`: constant force model
        """
        return self._constant_force
    #------------
    @ property
    def rotor_force(self): # pragma: no cover
        """:obj:`list` of :obj:`~.RotorForce.RotorForce`: rotor force model
        """
        return self._rotor_force
    #------------
    @ property
    def body_parts(self): # pragma: no cover
        """:obj:`list` of :obj:`~.BodyPart.BodyPart`: geometrical parts of the body. Produce wind force and current force
        """
        return self._body_parts
    #------------
    @ property
    def wind_force(self): # pragma: no cover
        """:obj:`~.WindForce.WindForce`: wind force model
        """
        return self._wind_force
    #------------
    @ property
    def current_force(self): # pragma: no cover
        """:obj:`~.CurrentForce.CurrentForce`: current force model
        """
        return self._current_force
    #------------
    @ property
    def mean_wave_drift_force(self): # pragma: no cover
        """:obj:`~.MeanWaveDriftForce.MeanWaveDriftForce`: wave drift force model
        """
        return self._mean_wave_drift_force
    #------------
    @ property
    def active_dof_static(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`int`:active dofs for static analysis , 1 for active dof, 0 for inactive dof []
        """
        return self._active_dof_static
    #------------
    @ property
    def active_dof_dynamic(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`int`:active dofs for dynamic analysis , 1 for active dof, 0 for inactive dof []
        """
        return self._active_dof_dynamic
    #------------
    @ property
    def hdb_source(self): # pragma: no cover
        """str: flag that indicates how to fetch the hydrodynamic data. nemoh_run or mc_module
        """
        return self._hdb_source
    #------------
    @ property
    def critical_damping_coefficient(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:Additional damping in terms of coefficient of critical damping. (1.0 is critical damping) dim(6) []
        """
        return self._critical_damping_coefficient
    #------------
    @ property
    def mooring_design_criteria(self): # pragma: no cover
        """:obj:`~.MooringDesignCriteria.MooringDesignCriteria`: Criteria for mooring design
        """
        return self._mooring_design_criteria
    #------------
    @ property
    def results(self): # pragma: no cover
        """:obj:`~.BodyResults.BodyResults`: Results from the different analyses
        """
        return self._results
    #------------
    @ property
    def water_depth(self): # pragma: no cover
        """float: Water depth at the body initial position []
        """
        return self._water_depth
    #------------
    @ property
    def name(self): # pragma: no cover
        """str: name of the instance object
        """
        return self._name
    #------------
    @ property
    def description(self): # pragma: no cover
        """str: description of the instance object
        """
        return self._description
    #------------
    #------------
    # Set functions
    #------------
    @ machine_type.setter
    def machine_type(self,val): # pragma: no cover
        self._machine_type=str(val)
    #------------
    @ submerged_volume.setter
    def submerged_volume(self,val): # pragma: no cover
        self._submerged_volume=float(val)
    #------------
    @ mass.setter
    def mass(self,val): # pragma: no cover
        self._mass=float(val)
    #------------
    @ mass_matrix.setter
    def mass_matrix(self,val): # pragma: no cover
        self._mass_matrix=val
    #------------
    @ cog_position.setter
    def cog_position(self,val): # pragma: no cover
        self._cog_position=val
    #------------
    @ rotation_center_in_b.setter
    def rotation_center_in_b(self,val): # pragma: no cover
        self._rotation_center_in_b=val
    #------------
    @ initial_position.setter
    def initial_position(self,val): # pragma: no cover
        self._initial_position=val
    #------------
    @ position.setter
    def position(self,val): # pragma: no cover
        self._position=val
    #------------
    @ positioning_type.setter
    def positioning_type(self,val): # pragma: no cover
        self._positioning_type=str(val)
    #------------
    @ positioning_reference.setter
    def positioning_reference(self,val): # pragma: no cover
        self._positioning_reference=str(val)
    #------------
    @ gravity_force.setter
    def gravity_force(self,val): # pragma: no cover
        self._gravity_force=val
    #------------
    @ hydrostatic_force.setter
    def hydrostatic_force(self,val): # pragma: no cover
        self._hydrostatic_force=val
    #------------
    @ mooring_system_force.setter
    def mooring_system_force(self,val): # pragma: no cover
        self._mooring_system_force=val
    #------------
    @ constant_force.setter
    def constant_force(self,val): # pragma: no cover
        self._constant_force=val
    #------------
    @ rotor_force.setter
    def rotor_force(self,val): # pragma: no cover
        self._rotor_force=val
    #------------
    @ body_parts.setter
    def body_parts(self,val): # pragma: no cover
        self._body_parts=val
    #------------
    @ wind_force.setter
    def wind_force(self,val): # pragma: no cover
        self._wind_force=val
    #------------
    @ current_force.setter
    def current_force(self,val): # pragma: no cover
        self._current_force=val
    #------------
    @ mean_wave_drift_force.setter
    def mean_wave_drift_force(self,val): # pragma: no cover
        self._mean_wave_drift_force=val
    #------------
    @ active_dof_static.setter
    def active_dof_static(self,val): # pragma: no cover
        self._active_dof_static=val
    #------------
    @ active_dof_dynamic.setter
    def active_dof_dynamic(self,val): # pragma: no cover
        self._active_dof_dynamic=val
    #------------
    @ hdb_source.setter
    def hdb_source(self,val): # pragma: no cover
        self._hdb_source=str(val)
    #------------
    @ critical_damping_coefficient.setter
    def critical_damping_coefficient(self,val): # pragma: no cover
        self._critical_damping_coefficient=val
    #------------
    @ mooring_design_criteria.setter
    def mooring_design_criteria(self,val): # pragma: no cover
        self._mooring_design_criteria=val
    #------------
    @ results.setter
    def results(self,val): # pragma: no cover
        self._results=val
    #------------
    @ water_depth.setter
    def water_depth(self,val): # pragma: no cover
        self._water_depth=float(val)
    #------------
    @ name.setter
    def name(self,val): # pragma: no cover
        self._name=str(val)
    #------------
    @ description.setter
    def description(self,val): # pragma: no cover
        self._description=str(val)
    #------------
    #-------------------------
    # Representation functions
    #-------------------------
    def type_rep(self): # pragma: no cover
        """Generate a representation of the object type

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains the representation of the object type
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtosk:entity:Body"
        rep["name"] = self.name
        rep["description"] = self.description
        return rep
    def prop_rep(self, short = False, deep = True):

        """Generate a representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains a representation of the object properties
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtosk:entity:Body"
        rep["name"] = self.name
        rep["description"] = self.description
        if self.is_set("machine_type"):
            rep["machine_type"] = self.machine_type
        if self.is_set("submerged_volume"):
            rep["submerged_volume"] = self.submerged_volume
        if self.is_set("mass"):
            rep["mass"] = self.mass
        if self.is_set("mass_matrix"):
            if (short):
                rep["mass_matrix"] = str(self.mass_matrix.shape)
            else:
                try:
                    rep["mass_matrix"] = self.mass_matrix.tolist()
                except:
                    rep["mass_matrix"] = self.mass_matrix
        if self.is_set("cog_position"):
            if (short):
                rep["cog_position"] = str(self.cog_position.shape)
            else:
                try:
                    rep["cog_position"] = self.cog_position.tolist()
                except:
                    rep["cog_position"] = self.cog_position
        if self.is_set("rotation_center_in_b"):
            if (short):
                rep["rotation_center_in_b"] = str(self.rotation_center_in_b.shape)
            else:
                try:
                    rep["rotation_center_in_b"] = self.rotation_center_in_b.tolist()
                except:
                    rep["rotation_center_in_b"] = self.rotation_center_in_b
        if self.is_set("initial_position"):
            if (short):
                rep["initial_position"] = str(self.initial_position.shape)
            else:
                try:
                    rep["initial_position"] = self.initial_position.tolist()
                except:
                    rep["initial_position"] = self.initial_position
        if self.is_set("position"):
            if (short):
                rep["position"] = str(self.position.shape)
            else:
                try:
                    rep["position"] = self.position.tolist()
                except:
                    rep["position"] = self.position
        if self.is_set("positioning_type"):
            rep["positioning_type"] = self.positioning_type
        if self.is_set("positioning_reference"):
            rep["positioning_reference"] = self.positioning_reference
        if self.is_set("gravity_force"):
            if (short and not(deep)):
                rep["gravity_force"] = self.gravity_force.type_rep()
            else:
                rep["gravity_force"] = self.gravity_force.prop_rep(short, deep)
        if self.is_set("hydrostatic_force"):
            if (short and not(deep)):
                rep["hydrostatic_force"] = self.hydrostatic_force.type_rep()
            else:
                rep["hydrostatic_force"] = self.hydrostatic_force.prop_rep(short, deep)
        if self.is_set("mooring_system_force"):
            if (short and not(deep)):
                rep["mooring_system_force"] = self.mooring_system_force.type_rep()
            else:
                rep["mooring_system_force"] = self.mooring_system_force.prop_rep(short, deep)
        if self.is_set("constant_force"):
            if (short and not(deep)):
                rep["constant_force"] = self.constant_force.type_rep()
            else:
                rep["constant_force"] = self.constant_force.prop_rep(short, deep)
        if self.is_set("rotor_force"):
            rep["rotor_force"] = []
            for i in range(0,len(self.rotor_force)):
                if (short and not(deep)):
                    itemType = self.rotor_force[i].type_rep()
                    rep["rotor_force"].append( itemType )
                else:
                    rep["rotor_force"].append( self.rotor_force[i].prop_rep(short, deep) )
        else:
            rep["rotor_force"] = []
        if self.is_set("body_parts"):
            rep["body_parts"] = []
            for i in range(0,len(self.body_parts)):
                if (short and not(deep)):
                    itemType = self.body_parts[i].type_rep()
                    rep["body_parts"].append( itemType )
                else:
                    rep["body_parts"].append( self.body_parts[i].prop_rep(short, deep) )
        else:
            rep["body_parts"] = []
        if self.is_set("wind_force"):
            if (short and not(deep)):
                rep["wind_force"] = self.wind_force.type_rep()
            else:
                rep["wind_force"] = self.wind_force.prop_rep(short, deep)
        if self.is_set("current_force"):
            if (short and not(deep)):
                rep["current_force"] = self.current_force.type_rep()
            else:
                rep["current_force"] = self.current_force.prop_rep(short, deep)
        if self.is_set("mean_wave_drift_force"):
            if (short and not(deep)):
                rep["mean_wave_drift_force"] = self.mean_wave_drift_force.type_rep()
            else:
                rep["mean_wave_drift_force"] = self.mean_wave_drift_force.prop_rep(short, deep)
        if self.is_set("active_dof_static"):
            if (short):
                rep["active_dof_static"] = str(self.active_dof_static.shape)
            else:
                try:
                    rep["active_dof_static"] = self.active_dof_static.tolist()
                except:
                    rep["active_dof_static"] = self.active_dof_static
        if self.is_set("active_dof_dynamic"):
            if (short):
                rep["active_dof_dynamic"] = str(self.active_dof_dynamic.shape)
            else:
                try:
                    rep["active_dof_dynamic"] = self.active_dof_dynamic.tolist()
                except:
                    rep["active_dof_dynamic"] = self.active_dof_dynamic
        if self.is_set("hdb_source"):
            rep["hdb_source"] = self.hdb_source
        if self.is_set("critical_damping_coefficient"):
            if (short):
                rep["critical_damping_coefficient"] = str(self.critical_damping_coefficient.shape)
            else:
                try:
                    rep["critical_damping_coefficient"] = self.critical_damping_coefficient.tolist()
                except:
                    rep["critical_damping_coefficient"] = self.critical_damping_coefficient
        if self.is_set("mooring_design_criteria"):
            if (short and not(deep)):
                rep["mooring_design_criteria"] = self.mooring_design_criteria.type_rep()
            else:
                rep["mooring_design_criteria"] = self.mooring_design_criteria.prop_rep(short, deep)
        if self.is_set("results"):
            if (short and not(deep)):
                rep["results"] = self.results.type_rep()
            else:
                rep["results"] = self.results.prop_rep(short, deep)
        if self.is_set("water_depth"):
            rep["water_depth"] = self.water_depth
        if self.is_set("name"):
            rep["name"] = self.name
        if self.is_set("description"):
            rep["description"] = self.description
        return rep
    #-------------------------
    # Save functions
    #-------------------------
    def json_rep(self, short=False, deep=True):

        """Generate a JSON representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`str`: string that contains a JSON representation of the object properties
        """

        return ( json.dumps(self.prop_rep(short=short, deep=deep),indent=4, separators=(",",": ")))
    def saveJSON(self, fileName=None):
        """Save the instance of the object to JSON format file

        Args:
            fileName (:obj:`str`, optional): Name of the JSON file, included extension. Defaults is None. If None, the name of the JSON file will be self.name.json. It can also contain an absolute or relative path.

        """

        if fileName==None:
            fileName=self.name + ".json"
        f=open(fileName, "w")
        f.write(self.json_rep())
        f.write("\n")
        f.close()

    def saveHDF5(self,filePath=None):
        """Save the instance of the object to HDF5 format file
        Args:
            filePath (:obj:`str`, optional): Name of the HDF5 file, included extension. Defaults is None. If None, the name of the HDF5 file will be "self.name".h5. It can also contain an absolute or relative path.
        """
        # Define filepath
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".h5"
            else:
                raise Exception("name is required for saving")
        # Open hdf5 file
        h = h5py.File(filePath,"w")
        group = h.create_group(self.name)
        # Save the object to the group
        self.saveToHDF5Handle(group)
        # Close file
        h.close()
        pass
    def saveToHDF5Handle(self, handle):
        """Save the properties of the object to the hdf5 handle.
        Args:
            handle (:obj:`h5py.Group`): Handle used to store the object properties
        """
        ar = []
        ar.append(self.machine_type.encode("ascii"))
        handle["machine_type"] = np.asarray(ar)
        handle["submerged_volume"] = np.array([self.submerged_volume],dtype=float)
        handle["mass"] = np.array([self.mass],dtype=float)
        handle["mass_matrix"] = np.array(self.mass_matrix,dtype=float)
        handle["cog_position"] = np.array(self.cog_position,dtype=float)
        handle["rotation_center_in_b"] = np.array(self.rotation_center_in_b,dtype=float)
        handle["initial_position"] = np.array(self.initial_position,dtype=float)
        handle["position"] = np.array(self.position,dtype=float)
        ar = []
        ar.append(self.positioning_type.encode("ascii"))
        handle["positioning_type"] = np.asarray(ar)
        ar = []
        ar.append(self.positioning_reference.encode("ascii"))
        handle["positioning_reference"] = np.asarray(ar)
        subgroup = handle.create_group("gravity_force")
        self.gravity_force.saveToHDF5Handle(subgroup)
        subgroup = handle.create_group("hydrostatic_force")
        self.hydrostatic_force.saveToHDF5Handle(subgroup)
        subgroup = handle.create_group("mooring_system_force")
        self.mooring_system_force.saveToHDF5Handle(subgroup)
        subgroup = handle.create_group("constant_force")
        self.constant_force.saveToHDF5Handle(subgroup)
        subgroup = handle.create_group("rotor_force")
        for idx in range(0,len(self.rotor_force)):
            subgroup1 = subgroup.create_group("rotor_force" + "_" + str(idx))
            self.rotor_force[idx].saveToHDF5Handle(subgroup1)
        subgroup = handle.create_group("body_parts")
        for idx in range(0,len(self.body_parts)):
            subgroup1 = subgroup.create_group("body_parts" + "_" + str(idx))
            self.body_parts[idx].saveToHDF5Handle(subgroup1)
        subgroup = handle.create_group("wind_force")
        self.wind_force.saveToHDF5Handle(subgroup)
        subgroup = handle.create_group("current_force")
        self.current_force.saveToHDF5Handle(subgroup)
        subgroup = handle.create_group("mean_wave_drift_force")
        self.mean_wave_drift_force.saveToHDF5Handle(subgroup)
        handle["active_dof_static"] = np.array(self.active_dof_static,dtype=int)
        handle["active_dof_dynamic"] = np.array(self.active_dof_dynamic,dtype=int)
        ar = []
        ar.append(self.hdb_source.encode("ascii"))
        handle["hdb_source"] = np.asarray(ar)
        handle["critical_damping_coefficient"] = np.array(self.critical_damping_coefficient,dtype=float)
        subgroup = handle.create_group("mooring_design_criteria")
        self.mooring_design_criteria.saveToHDF5Handle(subgroup)
        subgroup = handle.create_group("results")
        self.results.saveToHDF5Handle(subgroup)
        handle["water_depth"] = np.array([self.water_depth],dtype=float)
        ar = []
        ar.append(self.name.encode("ascii"))
        handle["name"] = np.asarray(ar)
        ar = []
        ar.append(self.description.encode("ascii"))
        handle["description"] = np.asarray(ar)
    #-------------------------
    # Load functions
    #-------------------------
    def loadJSON(self,name = None, filePath = None):
        """Load an instance of the object from JSON format file

        Args:
            name (:obj:`str`, optional): Name of the object to load.
            filePath (:obj:`str`, optional): Path of the JSON file to load. If None, the function looks for a file with name "name".json.

        The JSON file must contain a datastructure representing an instance of this object's class, as generated by e.g. the function :func:`~saveJSON`.

        """

        if not(name == None):
            self.name = name
        if (name == None) and not(filePath == None):
            self.name = ".".join(filePath.split(os.path.sep)[-1].split(".")[0:-1])
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".json"
            else:
                raise Exception("object needs name for loading.")
        if not(os.path.isfile(filePath)):
            raise Exception("file %s not found."%filePath)
        self._loadedItems = []
        f = open(filePath,"r")
        data = f.read()
        f.close()
        dd = json.loads(data)
        self.loadFromJSONDict(dd)
    def loadFromJSONDict(self, data):
        """Load an instance of the object from a dictionnary representing a JSON structure)

        Args:
            name (:obj:`collections.OrderedDict`): Dictionnary containing a JSON structure, as produced by e.g. :func:`json.loads`

        """

        varName = "machine_type"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "submerged_volume"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "mass"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "mass_matrix"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "cog_position"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "rotation_center_in_b"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "initial_position"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "position"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "positioning_type"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "positioning_reference"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "gravity_force"
        try :
            if data[varName] != None:
                self.gravity_force=GravityForce.GravityForce()
                self.gravity_force.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "hydrostatic_force"
        try :
            if data[varName] != None:
                self.hydrostatic_force=HydrostaticForce.HydrostaticForce()
                self.hydrostatic_force.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "mooring_system_force"
        try :
            if data[varName] != None:
                self.mooring_system_force=Mooring.Mooring()
                self.mooring_system_force.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "constant_force"
        try :
            if data[varName] != None:
                self.constant_force=ConstantForce.ConstantForce()
                self.constant_force.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "rotor_force"
        try :
            if data[varName] != None:
                self.rotor_force=[]
                for i in range(0,len(data[varName])):
                    self.rotor_force.append(RotorForce.RotorForce())
                    self.rotor_force[i].loadFromJSONDict(data[varName][i])
            else:
                self.rotor_force = []
        except :
            pass
        varName = "body_parts"
        try :
            if data[varName] != None:
                self.body_parts=[]
                for i in range(0,len(data[varName])):
                    self.body_parts.append(BodyPart.BodyPart())
                    self.body_parts[i].loadFromJSONDict(data[varName][i])
            else:
                self.body_parts = []
        except :
            pass
        varName = "wind_force"
        try :
            if data[varName] != None:
                self.wind_force=WindForce.WindForce()
                self.wind_force.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "current_force"
        try :
            if data[varName] != None:
                self.current_force=CurrentForce.CurrentForce()
                self.current_force.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "mean_wave_drift_force"
        try :
            if data[varName] != None:
                self.mean_wave_drift_force=MeanWaveDriftForce.MeanWaveDriftForce()
                self.mean_wave_drift_force.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "active_dof_static"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "active_dof_dynamic"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "hdb_source"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "critical_damping_coefficient"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "mooring_design_criteria"
        try :
            if data[varName] != None:
                self.mooring_design_criteria=MooringDesignCriteria.MooringDesignCriteria()
                self.mooring_design_criteria.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "results"
        try :
            if data[varName] != None:
                self.results=BodyResults.BodyResults()
                self.results.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "water_depth"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "description"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
    def loadHDF5(self,name=None, filePath=None):
        """Load an instance of the object from HDF5 format file
        Args:
            name (:obj:`str`, optional): Name of the object to load. Default is None.
            filePath (:obj:`str`, optional): Path of the HDF5 file to load. If None, the function looks for a file with name "self.name".h5.
        The HDF5 file must contain a datastructure representing an instance of this objects class, as generated by e.g. the function :func:`~saveHDF5`.
        """
        # Define filepath and name
        if not(name == None):
            self.name = name
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".h5"
            else:
                raise Exception("name is required for loading")
        # Open hdf5 file
        h = h5py.File(filePath,"r")
        group = h[self.name]
        # Save the object to the group
        self.loadFromHDF5Handle(group)
        # Close file
        h.close()
        pass
    def loadFromHDF5Handle(self, gr):
        """Load the properties of the object from a hdf5 handle.
        Args:
            gr (:obj:`h5py.Group`): Handle used to read the object properties from
        """
        if ("machine_type" in list(gr.keys())):
            self.machine_type = gr["machine_type"][0].decode("ascii")
        if ("submerged_volume" in list(gr.keys())):
            self.submerged_volume = gr["submerged_volume"][0]
        if ("mass" in list(gr.keys())):
            self.mass = gr["mass"][0]
        if ("mass_matrix" in list(gr.keys())):
            self.mass_matrix = gr["mass_matrix"][:][:]
        if ("cog_position" in list(gr.keys())):
            self.cog_position = gr["cog_position"][:]
        if ("rotation_center_in_b" in list(gr.keys())):
            self.rotation_center_in_b = gr["rotation_center_in_b"][:]
        if ("initial_position" in list(gr.keys())):
            self.initial_position = gr["initial_position"][:]
        if ("position" in list(gr.keys())):
            self.position = gr["position"][:]
        if ("positioning_type" in list(gr.keys())):
            self.positioning_type = gr["positioning_type"][0].decode("ascii")
        if ("positioning_reference" in list(gr.keys())):
            self.positioning_reference = gr["positioning_reference"][0].decode("ascii")
        if ("gravity_force" in list(gr.keys())):
            subgroup = gr["gravity_force"]
            self.gravity_force.loadFromHDF5Handle(subgroup)
        if ("hydrostatic_force" in list(gr.keys())):
            subgroup = gr["hydrostatic_force"]
            self.hydrostatic_force.loadFromHDF5Handle(subgroup)
        if ("mooring_system_force" in list(gr.keys())):
            subgroup = gr["mooring_system_force"]
            self.mooring_system_force.loadFromHDF5Handle(subgroup)
        if ("constant_force" in list(gr.keys())):
            subgroup = gr["constant_force"]
            self.constant_force.loadFromHDF5Handle(subgroup)
        if ("rotor_force" in list(gr.keys())):
            subgroup = gr["rotor_force"]
            self.rotor_force=[]
            for idx in range(0,len(subgroup.keys())):
                ssubgroup = subgroup["rotor_force"+"_"+str(idx)]
                self.rotor_force.append(RotorForce.RotorForce())
                self.rotor_force[idx].loadFromHDF5Handle(ssubgroup)
        if ("body_parts" in list(gr.keys())):
            subgroup = gr["body_parts"]
            self.body_parts=[]
            for idx in range(0,len(subgroup.keys())):
                ssubgroup = subgroup["body_parts"+"_"+str(idx)]
                self.body_parts.append(BodyPart.BodyPart())
                self.body_parts[idx].loadFromHDF5Handle(ssubgroup)
        if ("wind_force" in list(gr.keys())):
            subgroup = gr["wind_force"]
            self.wind_force.loadFromHDF5Handle(subgroup)
        if ("current_force" in list(gr.keys())):
            subgroup = gr["current_force"]
            self.current_force.loadFromHDF5Handle(subgroup)
        if ("mean_wave_drift_force" in list(gr.keys())):
            subgroup = gr["mean_wave_drift_force"]
            self.mean_wave_drift_force.loadFromHDF5Handle(subgroup)
        if ("active_dof_static" in list(gr.keys())):
            self.active_dof_static = gr["active_dof_static"][:]
        if ("active_dof_dynamic" in list(gr.keys())):
            self.active_dof_dynamic = gr["active_dof_dynamic"][:]
        if ("hdb_source" in list(gr.keys())):
            self.hdb_source = gr["hdb_source"][0].decode("ascii")
        if ("critical_damping_coefficient" in list(gr.keys())):
            self.critical_damping_coefficient = gr["critical_damping_coefficient"][:]
        if ("mooring_design_criteria" in list(gr.keys())):
            subgroup = gr["mooring_design_criteria"]
            self.mooring_design_criteria.loadFromHDF5Handle(subgroup)
        if ("results" in list(gr.keys())):
            subgroup = gr["results"]
            self.results.loadFromHDF5Handle(subgroup)
        if ("water_depth" in list(gr.keys())):
            self.water_depth = gr["water_depth"][0]
        if ("name" in list(gr.keys())):
            self.name = gr["name"][0].decode("ascii")
        if ("description" in list(gr.keys())):
            self.description = gr["description"][0].decode("ascii")
    #------------------------
    # is_set function
    #------------------------
    def is_set(self, varName): # pragma: no cover

        """Check if a given property of the object is set

        Args:
            varName (:obj:`str`): name of the property to check

        Returns:
            :obj:`bool`: True if the property is set, else False
        """

        if (isinstance(getattr(self,varName),list) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (isinstance(getattr(self,varName),np.ndarray) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (getattr(self,varName) != None):
            return True
        return False
    #------------------------
