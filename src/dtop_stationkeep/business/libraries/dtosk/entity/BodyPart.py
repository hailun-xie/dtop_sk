# This is the Station Keeping module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Neil Luxcey, Nicolas Michelet, Rocio Isorna
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import collections
import numpy as np
import os
import json
import h5py
from .geometry import RectangularCuboid
from .geometry import Cylinder
#------------------------------------
# @ USER DEFINED IMPORTS START
from ..geometry_module import plot_cylinder_in_n,plot_rectangle_in_n
from ..geometry_module import get_cylinder_intersection
from ..rotation_module import Rab
# @ USER DEFINED IMPORTS END
#------------------------------------

class BodyPart():

    """data model used to represent a geometrical part of a body. Used for wind force and current force
    """

    #------------
    # Constructor
    #------------
    def __init__(self,name=None):
#------------------------------------
# @ USER DEFINED DESCRIPTION START
# @ USER DEFINED DESCRIPTION END
#------------------------------------

        self._position_in_b=np.zeros(shape=(6), dtype=float)
        self._geometry_type='cylinder'
        self._rectangle=RectangularCuboid.RectangularCuboid()
        self._rectangle.description = 'a rectangular cuboid of which volume center is at body part origin'
        self._cylinder=Cylinder.Cylinder()
        self._cylinder.description = 'a cylinder of which volume center is at body part origin, and cylinder axis is vertical, i.e. along z-axis of the body part.'
        self._surface_roughness=0.0
        self._n_strip=1.0
        self._name='none'
        self._description=''
        if not(name == None): # pragma: no cover
            self._name = name

#------------------------------------
# @ USER DEFINED PROPERTIES START
        self.mean_wave_drift_force_applied = True
# @ USER DEFINED PROPERTIES END
#------------------------------------

#------------------------------------
# @ USER DEFINED METHODS START
    def plot(self,body_position_in_n,ax):

        xlma = np.array([0.0])
        xlmi = np.array([0.0])
        ylma = np.array([0.0])
        ylmi = np.array([0.0])
        zlma = np.array([0.0])
        zlmi = np.array([0.0])

        if self.geometry_type == 'cylinder':

            # Compute end point positions in n
            [pi_n, intersection_exists, pp1_n, pp2_n, pp_n] = get_cylinder_intersection(body_position_in_n,self.position_in_b,self.cylinder)

            # Plot cylinder
            plot_cylinder_in_n(pp1_n,pp2_n,self.cylinder.diameter,ax)
            xlma[0] = max(pp1_n[0],pp2_n[0],self.cylinder.diameter)
            xlmi[0] = min(pp1_n[0],pp2_n[0],-self.cylinder.diameter)
            ylma[0] = max(pp1_n[1],pp2_n[1],self.cylinder.diameter)
            ylmi[0] = min(pp1_n[1],pp2_n[1],-self.cylinder.diameter)
            zlma[0] = max(pp1_n[2],pp2_n[2])
            zlmi[0] = min(pp1_n[2],pp2_n[2])

        elif self.geometry_type == 'rectangle':
        # Assumption : we assume self.position_in_b[3:6] == 0.0

            # Compute position of rectangle in n
            position_in_n = np.zeros(shape=(6),dtype=float)
            position_in_n[3:6] = body_position_in_n[3:6]
            Rbn = Rab(body_position_in_n[3:6])
            position_in_n[0:3] = body_position_in_n[0:3] + Rbn.dot(self.position_in_b[0:3].T).T

            # Plot rectangle
            plot_rectangle_in_n(position_in_n,self.rectangle.dx/2,self.rectangle.dy/2,self.rectangle.dz/2,ax)
            xlma[0] = position_in_n[0] + self.rectangle.dx
            xlmi[0] = position_in_n[0] - self.rectangle.dx
            ylma[0] = position_in_n[1] + self.rectangle.dy
            ylmi[0] = position_in_n[1] - self.rectangle.dy
            zlma[0] = position_in_n[2] + self.rectangle.dz
            zlmi[0] = position_in_n[2] - self.rectangle.dz

        return xlma, xlmi, ylma, ylmi, zlma, zlmi
# @ USER DEFINED METHODS END
#------------------------------------

    #------------
    # Get functions
    #------------
    @ property
    def position_in_b(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:Position of the body part in {b} dim(6) []
        """
        return self._position_in_b
    #------------
    @ property
    def geometry_type(self): # pragma: no cover
        """str: Profile type. rectangle or cylinder
        """
        return self._geometry_type
    #------------
    @ property
    def rectangle(self): # pragma: no cover
        """:obj:`~.RectangularCuboid.RectangularCuboid`: a rectangular cuboid of which volume center is at body part origin
        """
        return self._rectangle
    #------------
    @ property
    def cylinder(self): # pragma: no cover
        """:obj:`~.Cylinder.Cylinder`: a cylinder of which volume center is at body part origin, and cylinder axis is vertical, i.e. along z-axis of the body part.
        """
        return self._cylinder
    #------------
    @ property
    def surface_roughness(self): # pragma: no cover
        """float: surface roughness [m] []
        """
        return self._surface_roughness
    #------------
    @ property
    def n_strip(self): # pragma: no cover
        """float: number of strips to descritize each body part where current force is applied []
        """
        return self._n_strip
    #------------
    @ property
    def name(self): # pragma: no cover
        """str: name of the instance object
        """
        return self._name
    #------------
    @ property
    def description(self): # pragma: no cover
        """str: description of the instance object
        """
        return self._description
    #------------
    #------------
    # Set functions
    #------------
    @ position_in_b.setter
    def position_in_b(self,val): # pragma: no cover
        self._position_in_b=val
    #------------
    @ geometry_type.setter
    def geometry_type(self,val): # pragma: no cover
        self._geometry_type=str(val)
    #------------
    @ rectangle.setter
    def rectangle(self,val): # pragma: no cover
        self._rectangle=val
    #------------
    @ cylinder.setter
    def cylinder(self,val): # pragma: no cover
        self._cylinder=val
    #------------
    @ surface_roughness.setter
    def surface_roughness(self,val): # pragma: no cover
        self._surface_roughness=float(val)
    #------------
    @ n_strip.setter
    def n_strip(self,val): # pragma: no cover
        self._n_strip=float(val)
    #------------
    @ name.setter
    def name(self,val): # pragma: no cover
        self._name=str(val)
    #------------
    @ description.setter
    def description(self,val): # pragma: no cover
        self._description=str(val)
    #------------
    #-------------------------
    # Representation functions
    #-------------------------
    def type_rep(self): # pragma: no cover
        """Generate a representation of the object type

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains the representation of the object type
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtosk:entity:BodyPart"
        rep["name"] = self.name
        rep["description"] = self.description
        return rep
    def prop_rep(self, short = False, deep = True):

        """Generate a representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains a representation of the object properties
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtosk:entity:BodyPart"
        rep["name"] = self.name
        rep["description"] = self.description
        if self.is_set("position_in_b"):
            if (short):
                rep["position_in_b"] = str(self.position_in_b.shape)
            else:
                try:
                    rep["position_in_b"] = self.position_in_b.tolist()
                except:
                    rep["position_in_b"] = self.position_in_b
        if self.is_set("geometry_type"):
            rep["geometry_type"] = self.geometry_type
        if self.is_set("rectangle"):
            if (short and not(deep)):
                rep["rectangle"] = self.rectangle.type_rep()
            else:
                rep["rectangle"] = self.rectangle.prop_rep(short, deep)
        if self.is_set("cylinder"):
            if (short and not(deep)):
                rep["cylinder"] = self.cylinder.type_rep()
            else:
                rep["cylinder"] = self.cylinder.prop_rep(short, deep)
        if self.is_set("surface_roughness"):
            rep["surface_roughness"] = self.surface_roughness
        if self.is_set("n_strip"):
            rep["n_strip"] = self.n_strip
        if self.is_set("name"):
            rep["name"] = self.name
        if self.is_set("description"):
            rep["description"] = self.description
        return rep
    #-------------------------
    # Save functions
    #-------------------------
    def json_rep(self, short=False, deep=True):

        """Generate a JSON representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`str`: string that contains a JSON representation of the object properties
        """

        return ( json.dumps(self.prop_rep(short=short, deep=deep),indent=4, separators=(",",": ")))
    def saveJSON(self, fileName=None):
        """Save the instance of the object to JSON format file

        Args:
            fileName (:obj:`str`, optional): Name of the JSON file, included extension. Defaults is None. If None, the name of the JSON file will be self.name.json. It can also contain an absolute or relative path.

        """

        if fileName==None:
            fileName=self.name + ".json"
        f=open(fileName, "w")
        f.write(self.json_rep())
        f.write("\n")
        f.close()

    def saveHDF5(self,filePath=None):
        """Save the instance of the object to HDF5 format file
        Args:
            filePath (:obj:`str`, optional): Name of the HDF5 file, included extension. Defaults is None. If None, the name of the HDF5 file will be "self.name".h5. It can also contain an absolute or relative path.
        """
        # Define filepath
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".h5"
            else:
                raise Exception("name is required for saving")
        # Open hdf5 file
        h = h5py.File(filePath,"w")
        group = h.create_group(self.name)
        # Save the object to the group
        self.saveToHDF5Handle(group)
        # Close file
        h.close()
        pass
    def saveToHDF5Handle(self, handle):
        """Save the properties of the object to the hdf5 handle.
        Args:
            handle (:obj:`h5py.Group`): Handle used to store the object properties
        """
        handle["position_in_b"] = np.array(self.position_in_b,dtype=float)
        ar = []
        ar.append(self.geometry_type.encode("ascii"))
        handle["geometry_type"] = np.asarray(ar)
        subgroup = handle.create_group("rectangle")
        self.rectangle.saveToHDF5Handle(subgroup)
        subgroup = handle.create_group("cylinder")
        self.cylinder.saveToHDF5Handle(subgroup)
        handle["surface_roughness"] = np.array([self.surface_roughness],dtype=float)
        handle["n_strip"] = np.array([self.n_strip],dtype=float)
        ar = []
        ar.append(self.name.encode("ascii"))
        handle["name"] = np.asarray(ar)
        ar = []
        ar.append(self.description.encode("ascii"))
        handle["description"] = np.asarray(ar)
    #-------------------------
    # Load functions
    #-------------------------
    def loadJSON(self,name = None, filePath = None):
        """Load an instance of the object from JSON format file

        Args:
            name (:obj:`str`, optional): Name of the object to load.
            filePath (:obj:`str`, optional): Path of the JSON file to load. If None, the function looks for a file with name "name".json.

        The JSON file must contain a datastructure representing an instance of this object's class, as generated by e.g. the function :func:`~saveJSON`.

        """

        if not(name == None):
            self.name = name
        if (name == None) and not(filePath == None):
            self.name = ".".join(filePath.split(os.path.sep)[-1].split(".")[0:-1])
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".json"
            else:
                raise Exception("object needs name for loading.")
        if not(os.path.isfile(filePath)):
            raise Exception("file %s not found."%filePath)
        self._loadedItems = []
        f = open(filePath,"r")
        data = f.read()
        f.close()
        dd = json.loads(data)
        self.loadFromJSONDict(dd)
    def loadFromJSONDict(self, data):
        """Load an instance of the object from a dictionnary representing a JSON structure)

        Args:
            name (:obj:`collections.OrderedDict`): Dictionnary containing a JSON structure, as produced by e.g. :func:`json.loads`

        """

        varName = "position_in_b"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "geometry_type"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "rectangle"
        try :
            if data[varName] != None:
                self.rectangle=RectangularCuboid.RectangularCuboid()
                self.rectangle.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "cylinder"
        try :
            if data[varName] != None:
                self.cylinder=Cylinder.Cylinder()
                self.cylinder.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "surface_roughness"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "n_strip"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "description"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
    def loadHDF5(self,name=None, filePath=None):
        """Load an instance of the object from HDF5 format file
        Args:
            name (:obj:`str`, optional): Name of the object to load. Default is None.
            filePath (:obj:`str`, optional): Path of the HDF5 file to load. If None, the function looks for a file with name "self.name".h5.
        The HDF5 file must contain a datastructure representing an instance of this objects class, as generated by e.g. the function :func:`~saveHDF5`.
        """
        # Define filepath and name
        if not(name == None):
            self.name = name
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".h5"
            else:
                raise Exception("name is required for loading")
        # Open hdf5 file
        h = h5py.File(filePath,"r")
        group = h[self.name]
        # Save the object to the group
        self.loadFromHDF5Handle(group)
        # Close file
        h.close()
        pass
    def loadFromHDF5Handle(self, gr):
        """Load the properties of the object from a hdf5 handle.
        Args:
            gr (:obj:`h5py.Group`): Handle used to read the object properties from
        """
        if ("position_in_b" in list(gr.keys())):
            self.position_in_b = gr["position_in_b"][:]
        if ("geometry_type" in list(gr.keys())):
            self.geometry_type = gr["geometry_type"][0].decode("ascii")
        if ("rectangle" in list(gr.keys())):
            subgroup = gr["rectangle"]
            self.rectangle.loadFromHDF5Handle(subgroup)
        if ("cylinder" in list(gr.keys())):
            subgroup = gr["cylinder"]
            self.cylinder.loadFromHDF5Handle(subgroup)
        if ("surface_roughness" in list(gr.keys())):
            self.surface_roughness = gr["surface_roughness"][0]
        if ("n_strip" in list(gr.keys())):
            self.n_strip = gr["n_strip"][0]
        if ("name" in list(gr.keys())):
            self.name = gr["name"][0].decode("ascii")
        if ("description" in list(gr.keys())):
            self.description = gr["description"][0].decode("ascii")
    #------------------------
    # is_set function
    #------------------------
    def is_set(self, varName): # pragma: no cover

        """Check if a given property of the object is set

        Args:
            varName (:obj:`str`): name of the property to check

        Returns:
            :obj:`bool`: True if the property is set, else False
        """

        if (isinstance(getattr(self,varName),list) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (isinstance(getattr(self,varName),np.ndarray) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (getattr(self,varName) != None):
            return True
        return False
    #------------------------
