# This is the Station Keeping module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Neil Luxcey, Nicolas Michelet, Rocio Isorna
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import collections 
import numpy as np 
import os
import pandas as pd
try: 
    import json 
except: 
    print('WARNING: json is not installed.') 
from FoundationMatrixSuitability import FoundationMatrixSuitability
#------------------------------------------------------------------------------------------------
## Pile foundation design test
#
#-------------------------------------------------------------------------------------------------

drag = FoundationMatrixSuitability()

a, b, c, d = drag.suit_matrix()


s =a[a['Foundation_type'] == "shallow"].index.values.astype(int)[0]

ss= c ['steep'][s] + d ['stiff_clay'][s]


 


#inputs from user or other modules


#print(design)

print(a)
print(b)
print(c)
print(d)
print(s)
print('total is',ss)
