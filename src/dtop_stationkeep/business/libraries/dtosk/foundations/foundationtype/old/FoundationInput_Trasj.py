# This is the Station Keeping module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Neil Luxcey, Nicolas Michelet, Rocio Isorna
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import collections
import numpy as np
import os
try:
    import json
except:
    print('WARNING: json is not installed.')
from ....dtosk import DeviceGeometry
#------------------------------------
# @ USER DEFINED IMPORTS START
import pandas as pd
# @ USER DEFINED IMPORTS END
#------------------------------------

class FoundationInput():

    #------------
    # Constructor
    #------------
    def __init__(self,name=None):
        self._soil_type=''
        self._soil_slope=0.0
        self._slope_class=''
        self._foundation_preference=''
        self._seabed_connection_type=''
        self._geometry_pref=''
        self._soil_classification=''
        self._int_friction_angle=0.0
        self._u_shear_strenght=0.0
        self._sand_dr=0
        self._main_load_direction=''
        self._geometry=DeviceGeometry.DeviceGeometry()
        self._name='none'
        self._description=''
        if not(name == None):
            self._name = name

#------------------------------------
# @ USER DEFINED PROPERTIES START

        self.loads_ap = np.array([])
        self.slope_class=''
        self.dic_soil_properties = json.load(open('foundationtype\\soil_properties.json'))
# @ USER DEFINED PROPERTIES END
#------------------------------------

#------------------------------------
# @ USER DEFINED METHODS START


    def soil_properties(self):

        self.int_friction_angle = self.dic_soil_properties["friction_angle"][self.soil_type]
        self.u_shear_strenght = self.dic_soil_properties["uss"][self.soil_type]
        self.sand_dr = self.dic_soil_properties["relative_density"][self.soil_type]
        self.g_b = self.dic_soil_properties["bouyant_weight"][self.soil_type]

        '''Fonction to classify the soil : cohessive or cohesionless. This will determine the methology that will be used for the design'''

        if self.soil_type == "very_soft_clay" or self.soil_type == "soft_clay" or self.soil_type == "firm_clay" or self.soil_type == "stiff_clay" or self.soil_type == "very_stiff_clay" or self.soil_type == "hard_clay":
            self.soil_classification = "cohessive"
        elif self.soil_type == "very_loose_sand" or self.soil_type == "loose_sand" or self.soil_type == "medium_dense_sand" or self.soil_type == "dense_sand" or self.soil_type == "very_dense_sand" or self.soil_type == "gravels_pebbles":
            self.soil_classification = "cohesionless"
        else:
            self.soil_classification = "not possible classification"

    def main_load(self):

        if (self.loads_ap[0] + self.loads_ap[5] + self.loads_ap[6]) == 0:
            self.main_load_direction = 'horizontal'
        elif self.loads_ap[0] != 0 and (self.loads_ap[5] + self.loads_ap[6]) == 0:
            self.main_load_direction = 'uplift'
        else:
            self.main_load_direction = 'gravitary'

    def soil_slope_type(self):

        if self.soil_slope < 10 :
            self.slope_class = 'moderate'
        elif self.soil_slope >= 10 :
            self.slope_class = 'steep'

    def foundation_geometry(self):

        if self.seabed_connection_type == 'moored':
            self.geometry_pref = 'rectangular'
        elif self.seabed_connection_type == 'fixed':
            self.geometry_pref = self.geometry_pref


    def results(self):
        self.soil_properties()
        self.main_load()
        self.soil_slope_type()
        self.foundation_geometry()

        soil_properties = pd.DataFrame({"Soil classification": [self.soil_classification], "Soil type": [self.soil_type], "Bouyant weight [kg/m3]": [self.g_b], "Friction angle [°]": [self.int_friction_angle], "U_shear strength [Pa]":[self.u_shear_strenght]})

        print(soil_properties)

# @ USER DEFINED METHODS END
#------------------------------------

    #------------
    # Get functions
    #------------
    @ property
    def soil_type(self):
        return self._soil_type
    #------------
    @ property
    def soil_slope(self):
        return self._soil_slope
    #------------
    @ property
    def slope_class(self):
        return self._slope_class
    #------------
    @ property
    def foundation_preference(self):
        return self._foundation_preference
    #------------
    @ property
    def seabed_connection_type(self):
        return self._seabed_connection_type
    #------------
    @ property
    def geometry_pref(self):
        return self._geometry_pref
    #------------
    @ property
    def soil_classification(self):
        return self._soil_classification
    #------------
    @ property
    def int_friction_angle(self):
        return self._int_friction_angle
    #------------
    @ property
    def u_shear_strenght(self):
        return self._u_shear_strenght
    #------------
    @ property
    def sand_dr(self):
        return self._sand_dr
    #------------
    @ property
    def main_load_direction(self):
        return self._main_load_direction
    #------------
    @ property
    def geometry(self):
        return self._geometry
    #------------
    @ property
    def name(self):
        return self._name
    #------------
    @ property
    def description(self):
        return self._description
    #------------
    #------------
    # Set functions
    #------------
    @ soil_type.setter
    def soil_type(self,val):
        self._soil_type=str(val)
    #------------
    @ soil_slope.setter
    def soil_slope(self,val):
        self._soil_slope=float(val)
    #------------
    @ slope_class.setter
    def slope_class(self,val):
        self._slope_class=str(val)
    #------------
    @ foundation_preference.setter
    def foundation_preference(self,val):
        self._foundation_preference=str(val)
    #------------
    @ seabed_connection_type.setter
    def seabed_connection_type(self,val):
        self._seabed_connection_type=str(val)
    #------------
    @ geometry_pref.setter
    def geometry_pref(self,val):
        self._geometry_pref=str(val)
    #------------
    @ soil_classification.setter
    def soil_classification(self,val):
        self._soil_classification=str(val)
    #------------
    @ int_friction_angle.setter
    def int_friction_angle(self,val):
        self._int_friction_angle=float(val)
    #------------
    @ u_shear_strenght.setter
    def u_shear_strenght(self,val):
        self._u_shear_strenght=float(val)
    #------------
    @ sand_dr.setter
    def sand_dr(self,val):
        self._sand_dr=float(val)
    #------------
    @ main_load_direction.setter
    def main_load_direction(self,val):
        self._main_load_direction=str(val)
    #------------
    @ geometry.setter
    def geometry(self,val):
        self._geometry=val
    #------------
    @ name.setter
    def name(self,val):
        self._name=str(val)
    #------------
    @ description.setter
    def description(self,val):
        self._description=str(val)
    #------------
    #-------------------------
    # Representation functions
    #-------------------------
    def type_rep(self):
        rep = collections.OrderedDict()
        rep["__type__"] = "foundations:FoundationType"
        rep["name"] = self.name
        rep["description"] = self.description
        return rep
    def prop_rep(self, short = False, deep = True):
        rep = collections.OrderedDict()
        rep["__type__"] = "foundations:FoundationType"
        rep["name"] = self.name
        rep["description"] = self.description
        if self.is_set("soil_type"):
            rep["soil_type"] = self.soil_type
        if self.is_set("soil_slope"):
            rep["soil_slope"] = self.soil_slope
        if self.is_set("slope_class"):
            rep["slope_class"] = self.slope_class
        if self.is_set("foundation_preference"):
            rep["foundation_preference"] = self.foundation_preference
        if self.is_set("seabed_connection_type"):
            rep["seabed_connection_type"] = self.seabed_connection_type
        if self.is_set("geometry_pref"):
            rep["geometry_pref"] = self.geometry_pref
        if self.is_set("soil_classification"):
            rep["soil_classification"] = self.soil_classification
        if self.is_set("int_friction_angle"):
            rep["int_friction_angle"] = self.int_friction_angle
        if self.is_set("u_shear_strenght"):
            rep["u_shear_strenght"] = self.u_shear_strenght
        if self.is_set("sand_dr"):
            rep["sand_dr"] = self.sand_dr
        if self.is_set("main_load_direction"):
            rep["main_load_direction"] = self.main_load_direction
        if self.is_set("geometry"):
            if (short and not(deep)):
                rep["geometry"] = self.geometry.type_rep()
            else:
                rep["geometry"] = self.geometry.prop_rep(short, deep)
        if self.is_set("name"):
            rep["name"] = self.name
        if self.is_set("description"):
            rep["description"] = self.description
        return rep
    #-------------------------
    # Save functions
    #-------------------------
    def json_rep(self, short=False, deep=True):
        return ( json.dumps(self.prop_rep(short=short, deep=deep),indent=4, separators=(",",": ")))
    def saveJSON(self, fileName=None):
        if fileName==None:
            fileName=self.name + ".json"
        f=open(fileName, "w")
        f.write(self.json_rep())
        f.close()
    #-------------------------
    # Load functions
    #-------------------------
    def loadJSON(self,name = None, filePath = None):
        if not(name == None):
            self.name = name
        if (name == None) and not(filePath == None):
            self.name = ".".join(filePath.split(os.path.sep)[-1].split(".")[0:-1])
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".json"
            else:
                raise Exception("object needs name for loading.")
        if not(os.path.isfile(filePath)):
            raise Exception("file %s not found."%filePath)
        self._loadedItems = []
        f = open(filePath,"r")
        data = f.read()
        f.close()
        dd = json.loads(data)
        self.loadFromJSONDict(dd)
    def loadFromJSONDict(self, data):
        varName = "soil_type"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "soil_slope"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "slope_class"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "foundation_preference"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "seabed_connection_type"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "geometry_pref"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "soil_classification"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "int_friction_angle"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "u_shear_strenght"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "sand_dr"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "main_load_direction"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "geometry"
        try :
            if data[varName] != None:
                self.geometry=DeviceGeometry.DeviceGeometry()
                self.geometry.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "description"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
    #------------------------
    # is_set function
    #------------------------
    def is_set(self, varName):
        if (isinstance(getattr(self,varName),list) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (isinstance(getattr(self,varName),np.ndarray) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (getattr(self,varName) != None):
            return True
        return False
    #------------------------
