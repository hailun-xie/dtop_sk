# This is the Station Keeping module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Neil Luxcey, Nicolas Michelet, Rocio Isorna
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import collections 
import numpy as np 
import os
try: 
    import json 
except: 
    print('WARNING: json is not installed.') 
from foundationdesign.PileFoundation import PileFoundation
#------------------------------------------------------------------------------------------------
## Pile foundation design test
#
#-------------------------------------------------------------------------------------------------

pile = PileFoundation()

#inputs from user or other modules

pile.int_friction_angle = 37
pile.u_shear_strenght = 0
pile.sand_dr = 0.9
pile.SF = 1
pile.g_b = 1020
pile.seabed_connection_type = "moored"
pile.tip_type = 'open_end'
pile.dist_attachment = 0
pile.loads_from_mooring = [0.27e+06, 0, 3.46e+06, 0, 0, 0, 0 ]  
pile.deflection_crit = 5



#def desing_input_from_mooring (V_min, V_max, H_x, H_y, M_x, M_y, z_ap):


#print(update_inputs_mooring)
#print(uls_loads)

#design = pile.lateral_design()
#design_b = pile.axial_design()
designa = pile.design()
pile.steel_stress_analysis()
results = pile.outputs()


#print(design)

print(designa)
print(results[0])
print(pile.deflection_crit, pile.n_h, pile.sand_dr, pile.k1, pile.int_friction_angle_d, pile.lat_cap, pile.tension_capacity, pile.comp_capacity)
print(pile.skin_friction_comp, pile.skin_friction_tension)
print(pile.cohessionless_sf_comp, pile.cohessionless_sf_tension)
print(np.tan(pile.int_friction_angle_d_rad))
print(pile.mid_overburden_pressure)

