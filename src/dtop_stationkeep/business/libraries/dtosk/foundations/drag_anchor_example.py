# This is the Station Keeping module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Neil Luxcey, Nicolas Michelet, Rocio Isorna
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import collections 
import numpy as np 
import os
import pandas as pd
try: 
    import json 
except: 
    print('WARNING: json is not installed.') 
from foundationdesign.DragAnchors import DragAnchors
from foundationdesign.AnchorCatalogue import AnchorCatalogue
#------------------------------------------------------------------------------------------------
## Pile foundation design test
#
'''
- int_friction_angle: internal friction angle of the soil in °
- u_shear_strenght: undrained shear strength in Pa
'''
#-------------------------------------------------------------------------------------------------
'''object anchor and catalogue'''
anchor = DragAnchors()
#inputs from user or other modules
'''inputs from SC or user'''
anchor.int_friction_angle = 38
anchor.u_shear_strenght = 0

anchor.loads_from_mooring = [0, 0, 1000000, 0, 0, 0, 0]

#Load design##

design = anchor.design()
general, bom = anchor.outputs()
a= anchor.soil_type
print(a)
print(general)
print(bom)



