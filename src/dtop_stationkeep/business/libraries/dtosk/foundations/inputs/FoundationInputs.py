# This is the Station Keeping module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Neil Luxcey, Nicolas Michelet, Rocio Isorna
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import collections
import numpy as np
import os
import json
import h5py
#------------------------------------
# @ USER DEFINED IMPORTS START
import pandas as pd
from .SoilProperties import SoilProperties
# @ USER DEFINED IMPORTS END
#------------------------------------

class FoundationInputs():

    """data model for input data needed to perform the foundation design
    """

    #------------
    # Constructor
    #------------
    def __init__(self,name=None):
#------------------------------------
# @ USER DEFINED DESCRIPTION START
# @ USER DEFINED DESCRIPTION END
#------------------------------------

        self._soil_type=''
        self._user_foundation_pref=''
        self._soil_properties_sf=''
        self._soil_properties_sf_value=1.3
        self._foundation_material='concrete'
        self._foundation_material_density=2400
        self._soil_type_def=''
        self._undrained_shear_strength=0.0
        self._int_friction_angle=0.0
        self._sand_dr=0
        self._g_b=0.0
        self._soil_slope=0
        self._pile_tip='open_end'
        self._pile_length_above_seabed=0.0
        self._deflection_criteria_for_pile=0.0
        self._slope_class=''
        self._seabed_connection_type=''
        self._geometry_pref=''
        self._main_load_direction=''
        self._loads_from_mooring=np.zeros(shape=(1), dtype=float)
        self._dist_attachment_pa=0
        self._name='none'
        self._description=''
        if not(name == None): # pragma: no cover
            self._name = name

#------------------------------------
# @ USER DEFINED PROPERTIES START
        self.soil_catalogue=SoilProperties()
        self.device_geometry={}
        self.loads_ap = np.array([])
        self.slope_class=''
        self.dic_soil_properties = self.soil_catalogue.soil_properties_cat()
        self.index =0
# @ USER DEFINED PROPERTIES END
#------------------------------------

#------------------------------------
# @ USER DEFINED METHODS START


    def soil_properties(self):

        if self.soil_type != 'custom':

            self.index = self.dic_soil_properties[self.dic_soil_properties["Soil_type"] == self.soil_type].index.values
            self.int_friction_angle = self.dic_soil_properties["Friction_angle [°]"][self.index]
            self.undrained_shear_strength= self.dic_soil_properties["Uss [Pa]"][self.index]
            self.sand_dr = self.dic_soil_properties["Dr"][self.index]
            self.g_b = self.dic_soil_properties["Bouyant_weight [N/m3]"][self.index]
            self.soil_type_def = self.dic_soil_properties["soil_type_def"][self.index]

        if self.soil_properties_sf == 'default':
            self.soil_properties_sf_value = 1.3

        int_friction_angle_rad = np.deg2rad(self.int_friction_angle)

        self.int_friction_angle_d = np.rad2deg(np.arctan(np.tan(int_friction_angle_rad) / self.soil_properties_sf_value))
        self.u_shear_strenght_d = self.undrained_shear_strength / self.soil_properties_sf_value


    def main_load(self):

        if (self.loads_ap[0] + self.loads_ap[4] + self.loads_ap[5]) == 0:
            self.main_load_direction = 'horizontal'
        elif self.loads_ap[0] != 0 and (self.loads_ap[4] + self.loads_ap[5]) == 0:
            self.main_load_direction = 'uplift'
        else:
            self.main_load_direction = 'gravitary'

    def soil_slope_type(self):

        if self.soil_slope < 10 :
            self.slope_class = 'moderate'
        elif self.soil_slope >= 10 :
            self.slope_class = 'steep'


    def geometry_definition(self):
        if self.geometry_pref == '':
            if self.seabed_connection_type == 'moored':
                self.geometry_pref= 'cylinder'
            elif self.seabed_connection_type == 'fixed':
                self.geometry_pref = self.device_geometry['geometry']

    def piles_inputs(self):

        if self.pile_tip == '':
            self.pile_tip = 'open_end'
        elif self.pile_tip == 'open_end' or self.pile_tip=='closed_end':
            self.pile_tip = self.pile_tip
        else:
            self.pile_tip = 'open_end'

        if self.seabed_connection_type == "fixed":
            if self.deflection_criteria_for_pile != 5:
                self.deflection_criteria_for_pile = self.deflection_criteria_for_pile

        elif self.seabed_connection_type == "moored":
            if self.deflection_criteria_for_pile != 10:
                self.deflection_criteria_for_pile = self.deflection_criteria_for_pile

        if self.seabed_connection_type == 'fixed':
            self.dist_attachment_pa = 0
        elif self.seabed_connection_type == 'moored':
            self.dist_attachment_pa = self.dist_attachment_pa

    def outputs_for_design(self):

        self.soil_properties()
        self.main_load()
        self.soil_slope_type()
        self.geometry_definition()
        self.piles_inputs()

        soil_properties = pd.DataFrame({"Soil classification": [self.soil_type_def], "Soil type": [self.soil_type], "Buoyant weight [N/m3]": [self.g_b], "Friction angle [°]": [self.int_friction_angle], "U_shear strength [Pa]":[self.undrained_shear_strength], "Dr":[self.sand_dr]})


# @ USER DEFINED METHODS END
#------------------------------------

    #------------
    # Get functions
    #------------
    @ property
    def soil_type(self): # pragma: no cover
        """str: none
        """
        return self._soil_type
    #------------
    @ property
    def user_foundation_pref(self): # pragma: no cover
        """str: foundation user preference for fixed systems, gravity or pile, for moored systems, gravity, pile, drag anchor, suction caisson, foundation can be determine by dto using evaluation when soil is different from user
        """
        return self._user_foundation_pref
    #------------
    @ property
    def soil_properties_sf(self): # pragma: no cover
        """str: user or default, default value is 1.3 (DNV)
        """
        return self._soil_properties_sf
    #------------
    @ property
    def soil_properties_sf_value(self): # pragma: no cover
        """float: default value is 1.3 from DNV []
        """
        return self._soil_properties_sf_value
    #------------
    @ property
    def foundation_material(self): # pragma: no cover
        """str: user definition only for gravity solution
        """
        return self._foundation_material
    #------------
    @ property
    def foundation_material_density(self): # pragma: no cover
        """float: user definiton only for gravity foundation default value is for concrete  []
        """
        return self._foundation_material_density
    #------------
    @ property
    def soil_type_def(self): # pragma: no cover
        """str: user definition only for soil_type equal to user, cohesive or cohesionless 
        """
        return self._soil_type_def
    #------------
    @ property
    def undrained_shear_strength(self): # pragma: no cover
        """float: user definition only for soil_type_def equal to cohesive in Pa []
        """
        return self._undrained_shear_strength
    #------------
    @ property
    def int_friction_angle(self): # pragma: no cover
        """float: angle de frottement interne du soil in °, user value required if soil_type_def equal to cohesionless []
        """
        return self._int_friction_angle
    #------------
    @ property
    def sand_dr(self): # pragma: no cover
        """float: relative density of sand no units, user value required if soil_type_def equal to cohesionless  []
        """
        return self._sand_dr
    #------------
    @ property
    def g_b(self): # pragma: no cover
        """float: soil bouyant weight in kgm-3, user definition only for soil_type equal to user  []
        """
        return self._g_b
    #------------
    @ property
    def soil_slope(self): # pragma: no cover
        """float: soil slope, user definition from site characterisation []
        """
        return self._soil_slope
    #------------
    @ property
    def pile_tip(self): # pragma: no cover
        """str: pile tip description, user value required when user_foundation_pref equal to pile, open_end or closed_end 
        """
        return self._pile_tip
    #------------
    @ property
    def pile_length_above_seabed(self): # pragma: no cover
        """float: Pile length above seabed (typically >=0 for a pile foundation, =0 for a pile anchor). []
        """
        return self._pile_length_above_seabed
    #------------
    @ property
    def deflection_criteria_for_pile(self): # pragma: no cover
        """float: deflection criterium for pile design, user value required when user_foundation_pref equal to pile []
        """
        return self._deflection_criteria_for_pile
    #------------
    @ property
    def slope_class(self): # pragma: no cover
        """str: soil slope, steep >= 10° or moderate < 10°
        """
        return self._slope_class
    #------------
    @ property
    def seabed_connection_type(self): # pragma: no cover
        """str: fixed or moored
        """
        return self._seabed_connection_type
    #------------
    @ property
    def geometry_pref(self): # pragma: no cover
        """str: geometry of the device; rectangular, lx, ly or cylinder, lr
        """
        return self._geometry_pref
    #------------
    @ property
    def main_load_direction(self): # pragma: no cover
        """str: main load direction: gravity, uplift, horizontal
        """
        return self._main_load_direction
    #------------
    @ property
    def loads_from_mooring(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:loads vector and application point dim(*) []
        """
        return self._loads_from_mooring
    #------------
    @ property
    def dist_attachment_pa(self): # pragma: no cover
        """float: distance of the pile load attachement point above the seafloor default value 0 []
        """
        return self._dist_attachment_pa
    #------------
    @ property
    def name(self): # pragma: no cover
        """str: name of the instance object
        """
        return self._name
    #------------
    @ property
    def description(self): # pragma: no cover
        """str: description of the instance object
        """
        return self._description
    #------------
    #------------
    # Set functions
    #------------
    @ soil_type.setter
    def soil_type(self,val): # pragma: no cover
        self._soil_type=str(val)
    #------------
    @ user_foundation_pref.setter
    def user_foundation_pref(self,val): # pragma: no cover
        self._user_foundation_pref=str(val)
    #------------
    @ soil_properties_sf.setter
    def soil_properties_sf(self,val): # pragma: no cover
        self._soil_properties_sf=str(val)
    #------------
    @ soil_properties_sf_value.setter
    def soil_properties_sf_value(self,val): # pragma: no cover
        self._soil_properties_sf_value=float(val)
    #------------
    @ foundation_material.setter
    def foundation_material(self,val): # pragma: no cover
        self._foundation_material=str(val)
    #------------
    @ foundation_material_density.setter
    def foundation_material_density(self,val): # pragma: no cover
        self._foundation_material_density=float(val)
    #------------
    @ soil_type_def.setter
    def soil_type_def(self,val): # pragma: no cover
        self._soil_type_def=str(val)
    #------------
    @ undrained_shear_strength.setter
    def undrained_shear_strength(self,val): # pragma: no cover
        self._undrained_shear_strength=float(val)
    #------------
    @ int_friction_angle.setter
    def int_friction_angle(self,val): # pragma: no cover
        self._int_friction_angle=float(val)
    #------------
    @ sand_dr.setter
    def sand_dr(self,val): # pragma: no cover
        self._sand_dr=float(val)
    #------------
    @ g_b.setter
    def g_b(self,val): # pragma: no cover
        self._g_b=float(val)
    #------------
    @ soil_slope.setter
    def soil_slope(self,val): # pragma: no cover
        self._soil_slope=float(val)
    #------------
    @ pile_tip.setter
    def pile_tip(self,val): # pragma: no cover
        self._pile_tip=str(val)
    #------------
    @ pile_length_above_seabed.setter
    def pile_length_above_seabed(self,val): # pragma: no cover
        self._pile_length_above_seabed=float(val)
    #------------
    @ deflection_criteria_for_pile.setter
    def deflection_criteria_for_pile(self,val): # pragma: no cover
        self._deflection_criteria_for_pile=float(val)
    #------------
    @ slope_class.setter
    def slope_class(self,val): # pragma: no cover
        self._slope_class=str(val)
    #------------
    @ seabed_connection_type.setter
    def seabed_connection_type(self,val): # pragma: no cover
        self._seabed_connection_type=str(val)
    #------------
    @ geometry_pref.setter
    def geometry_pref(self,val): # pragma: no cover
        self._geometry_pref=str(val)
    #------------
    @ main_load_direction.setter
    def main_load_direction(self,val): # pragma: no cover
        self._main_load_direction=str(val)
    #------------
    @ loads_from_mooring.setter
    def loads_from_mooring(self,val): # pragma: no cover
        self._loads_from_mooring=val
    #------------
    @ dist_attachment_pa.setter
    def dist_attachment_pa(self,val): # pragma: no cover
        self._dist_attachment_pa=float(val)
    #------------
    @ name.setter
    def name(self,val): # pragma: no cover
        self._name=str(val)
    #------------
    @ description.setter
    def description(self,val): # pragma: no cover
        self._description=str(val)
    #------------
    #-------------------------
    # Representation functions
    #-------------------------
    def type_rep(self): # pragma: no cover
        """Generate a representation of the object type

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains the representation of the object type
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtosk:foundations:inputs:FoundationInputs"
        rep["name"] = self.name
        rep["description"] = self.description
        return rep
    def prop_rep(self, short = False, deep = True):

        """Generate a representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains a representation of the object properties
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtosk:foundations:inputs:FoundationInputs"
        rep["name"] = self.name
        rep["description"] = self.description
        if self.is_set("soil_type"):
            rep["soil_type"] = self.soil_type
        if self.is_set("user_foundation_pref"):
            rep["user_foundation_pref"] = self.user_foundation_pref
        if self.is_set("soil_properties_sf"):
            rep["soil_properties_sf"] = self.soil_properties_sf
        if self.is_set("soil_properties_sf_value"):
            rep["soil_properties_sf_value"] = self.soil_properties_sf_value
        if self.is_set("foundation_material"):
            rep["foundation_material"] = self.foundation_material
        if self.is_set("foundation_material_density"):
            rep["foundation_material_density"] = self.foundation_material_density
        if self.is_set("soil_type_def"):
            rep["soil_type_def"] = self.soil_type_def
        if self.is_set("undrained_shear_strength"):
            rep["undrained_shear_strength"] = self.undrained_shear_strength
        if self.is_set("int_friction_angle"):
            rep["int_friction_angle"] = self.int_friction_angle
        if self.is_set("sand_dr"):
            rep["sand_dr"] = self.sand_dr
        if self.is_set("g_b"):
            rep["g_b"] = self.g_b
        if self.is_set("soil_slope"):
            rep["soil_slope"] = self.soil_slope
        if self.is_set("pile_tip"):
            rep["pile_tip"] = self.pile_tip
        if self.is_set("pile_length_above_seabed"):
            rep["pile_length_above_seabed"] = self.pile_length_above_seabed
        if self.is_set("deflection_criteria_for_pile"):
            rep["deflection_criteria_for_pile"] = self.deflection_criteria_for_pile
        if self.is_set("slope_class"):
            rep["slope_class"] = self.slope_class
        if self.is_set("seabed_connection_type"):
            rep["seabed_connection_type"] = self.seabed_connection_type
        if self.is_set("geometry_pref"):
            rep["geometry_pref"] = self.geometry_pref
        if self.is_set("main_load_direction"):
            rep["main_load_direction"] = self.main_load_direction
        if self.is_set("loads_from_mooring"):
            if (short):
                rep["loads_from_mooring"] = str(self.loads_from_mooring.shape)
            else:
                try:
                    rep["loads_from_mooring"] = self.loads_from_mooring.tolist()
                except:
                    rep["loads_from_mooring"] = self.loads_from_mooring
        if self.is_set("dist_attachment_pa"):
            rep["dist_attachment_pa"] = self.dist_attachment_pa
        if self.is_set("name"):
            rep["name"] = self.name
        if self.is_set("description"):
            rep["description"] = self.description
        return rep
    #-------------------------
    # Save functions
    #-------------------------
    def json_rep(self, short=False, deep=True):

        """Generate a JSON representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`str`: string that contains a JSON representation of the object properties
        """

        return ( json.dumps(self.prop_rep(short=short, deep=deep),indent=4, separators=(",",": ")))
    def saveJSON(self, fileName=None):
        """Save the instance of the object to JSON format file

        Args:
            fileName (:obj:`str`, optional): Name of the JSON file, included extension. Defaults is None. If None, the name of the JSON file will be self.name.json. It can also contain an absolute or relative path.

        """

        if fileName==None:
            fileName=self.name + ".json"
        f=open(fileName, "w")
        f.write(self.json_rep())
        f.write("\n")
        f.close()

    def saveHDF5(self,filePath=None):
        """Save the instance of the object to HDF5 format file
        Args:
            filePath (:obj:`str`, optional): Name of the HDF5 file, included extension. Defaults is None. If None, the name of the HDF5 file will be "self.name".h5. It can also contain an absolute or relative path.
        """
        # Define filepath
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".h5"
            else:
                raise Exception("name is required for saving")
        # Open hdf5 file
        h = h5py.File(filePath,"w")
        group = h.create_group(self.name)
        # Save the object to the group
        self.saveToHDF5Handle(group)
        # Close file
        h.close()
        pass
    def saveToHDF5Handle(self, handle):
        """Save the properties of the object to the hdf5 handle.
        Args:
            handle (:obj:`h5py.Group`): Handle used to store the object properties
        """
        ar = []
        ar.append(self.soil_type.encode("ascii"))
        handle["soil_type"] = np.asarray(ar)
        ar = []
        ar.append(self.user_foundation_pref.encode("ascii"))
        handle["user_foundation_pref"] = np.asarray(ar)
        ar = []
        ar.append(self.soil_properties_sf.encode("ascii"))
        handle["soil_properties_sf"] = np.asarray(ar)
        handle["soil_properties_sf_value"] = np.array([self.soil_properties_sf_value],dtype=float)
        ar = []
        ar.append(self.foundation_material.encode("ascii"))
        handle["foundation_material"] = np.asarray(ar)
        handle["foundation_material_density"] = np.array([self.foundation_material_density],dtype=float)
        ar = []
        ar.append(self.soil_type_def.encode("ascii"))
        handle["soil_type_def"] = np.asarray(ar)
        handle["undrained_shear_strength"] = np.array([self.undrained_shear_strength],dtype=float)
        handle["int_friction_angle"] = np.array([self.int_friction_angle],dtype=float)
        handle["sand_dr"] = np.array([self.sand_dr],dtype=float)
        handle["g_b"] = np.array([self.g_b],dtype=float)
        handle["soil_slope"] = np.array([self.soil_slope],dtype=float)
        ar = []
        ar.append(self.pile_tip.encode("ascii"))
        handle["pile_tip"] = np.asarray(ar)
        handle["pile_length_above_seabed"] = np.array([self.pile_length_above_seabed],dtype=float)
        handle["deflection_criteria_for_pile"] = np.array([self.deflection_criteria_for_pile],dtype=float)
        ar = []
        ar.append(self.slope_class.encode("ascii"))
        handle["slope_class"] = np.asarray(ar)
        ar = []
        ar.append(self.seabed_connection_type.encode("ascii"))
        handle["seabed_connection_type"] = np.asarray(ar)
        ar = []
        ar.append(self.geometry_pref.encode("ascii"))
        handle["geometry_pref"] = np.asarray(ar)
        ar = []
        ar.append(self.main_load_direction.encode("ascii"))
        handle["main_load_direction"] = np.asarray(ar)
        handle["loads_from_mooring"] = np.array(self.loads_from_mooring,dtype=float)
        handle["dist_attachment_pa"] = np.array([self.dist_attachment_pa],dtype=float)
        ar = []
        ar.append(self.name.encode("ascii"))
        handle["name"] = np.asarray(ar)
        ar = []
        ar.append(self.description.encode("ascii"))
        handle["description"] = np.asarray(ar)
    #-------------------------
    # Load functions
    #-------------------------
    def loadJSON(self,name = None, filePath = None):
        """Load an instance of the object from JSON format file

        Args:
            name (:obj:`str`, optional): Name of the object to load.
            filePath (:obj:`str`, optional): Path of the JSON file to load. If None, the function looks for a file with name "name".json.

        The JSON file must contain a datastructure representing an instance of this object's class, as generated by e.g. the function :func:`~saveJSON`.

        """

        if not(name == None):
            self.name = name
        if (name == None) and not(filePath == None):
            self.name = ".".join(filePath.split(os.path.sep)[-1].split(".")[0:-1])
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".json"
            else:
                raise Exception("object needs name for loading.")
        if not(os.path.isfile(filePath)):
            raise Exception("file %s not found."%filePath)
        self._loadedItems = []
        f = open(filePath,"r")
        data = f.read()
        f.close()
        dd = json.loads(data)
        self.loadFromJSONDict(dd)
    def loadFromJSONDict(self, data):
        """Load an instance of the object from a dictionnary representing a JSON structure)

        Args:
            name (:obj:`collections.OrderedDict`): Dictionnary containing a JSON structure, as produced by e.g. :func:`json.loads`

        """

        varName = "soil_type"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "user_foundation_pref"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "soil_properties_sf"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "soil_properties_sf_value"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "foundation_material"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "foundation_material_density"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "soil_type_def"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "undrained_shear_strength"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "int_friction_angle"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "sand_dr"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "g_b"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "soil_slope"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "pile_tip"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "pile_length_above_seabed"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "deflection_criteria_for_pile"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "slope_class"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "seabed_connection_type"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "geometry_pref"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "main_load_direction"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "loads_from_mooring"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "dist_attachment_pa"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "description"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
    def loadHDF5(self,name=None, filePath=None):
        """Load an instance of the object from HDF5 format file
        Args:
            name (:obj:`str`, optional): Name of the object to load. Default is None.
            filePath (:obj:`str`, optional): Path of the HDF5 file to load. If None, the function looks for a file with name "self.name".h5.
        The HDF5 file must contain a datastructure representing an instance of this objects class, as generated by e.g. the function :func:`~saveHDF5`.
        """
        # Define filepath and name
        if not(name == None):
            self.name = name
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".h5"
            else:
                raise Exception("name is required for loading")
        # Open hdf5 file
        h = h5py.File(filePath,"r")
        group = h[self.name]
        # Save the object to the group
        self.loadFromHDF5Handle(group)
        # Close file
        h.close()
        pass
    def loadFromHDF5Handle(self, gr):
        """Load the properties of the object from a hdf5 handle.
        Args:
            gr (:obj:`h5py.Group`): Handle used to read the object properties from
        """
        if ("soil_type" in list(gr.keys())):
            self.soil_type = gr["soil_type"][0].decode("ascii")
        if ("user_foundation_pref" in list(gr.keys())):
            self.user_foundation_pref = gr["user_foundation_pref"][0].decode("ascii")
        if ("soil_properties_sf" in list(gr.keys())):
            self.soil_properties_sf = gr["soil_properties_sf"][0].decode("ascii")
        if ("soil_properties_sf_value" in list(gr.keys())):
            self.soil_properties_sf_value = gr["soil_properties_sf_value"][0]
        if ("foundation_material" in list(gr.keys())):
            self.foundation_material = gr["foundation_material"][0].decode("ascii")
        if ("foundation_material_density" in list(gr.keys())):
            self.foundation_material_density = gr["foundation_material_density"][0]
        if ("soil_type_def" in list(gr.keys())):
            self.soil_type_def = gr["soil_type_def"][0].decode("ascii")
        if ("undrained_shear_strength" in list(gr.keys())):
            self.undrained_shear_strength = gr["undrained_shear_strength"][0]
        if ("int_friction_angle" in list(gr.keys())):
            self.int_friction_angle = gr["int_friction_angle"][0]
        if ("sand_dr" in list(gr.keys())):
            self.sand_dr = gr["sand_dr"][0]
        if ("g_b" in list(gr.keys())):
            self.g_b = gr["g_b"][0]
        if ("soil_slope" in list(gr.keys())):
            self.soil_slope = gr["soil_slope"][0]
        if ("pile_tip" in list(gr.keys())):
            self.pile_tip = gr["pile_tip"][0].decode("ascii")
        if ("pile_length_above_seabed" in list(gr.keys())):
            self.pile_length_above_seabed = gr["pile_length_above_seabed"][0]
        if ("deflection_criteria_for_pile" in list(gr.keys())):
            self.deflection_criteria_for_pile = gr["deflection_criteria_for_pile"][0]
        if ("slope_class" in list(gr.keys())):
            self.slope_class = gr["slope_class"][0].decode("ascii")
        if ("seabed_connection_type" in list(gr.keys())):
            self.seabed_connection_type = gr["seabed_connection_type"][0].decode("ascii")
        if ("geometry_pref" in list(gr.keys())):
            self.geometry_pref = gr["geometry_pref"][0].decode("ascii")
        if ("main_load_direction" in list(gr.keys())):
            self.main_load_direction = gr["main_load_direction"][0].decode("ascii")
        if ("loads_from_mooring" in list(gr.keys())):
            self.loads_from_mooring = gr["loads_from_mooring"][:]
        if ("dist_attachment_pa" in list(gr.keys())):
            self.dist_attachment_pa = gr["dist_attachment_pa"][0]
        if ("name" in list(gr.keys())):
            self.name = gr["name"][0].decode("ascii")
        if ("description" in list(gr.keys())):
            self.description = gr["description"][0].decode("ascii")
    #------------------------
    # is_set function
    #------------------------
    def is_set(self, varName): # pragma: no cover

        """Check if a given property of the object is set

        Args:
            varName (:obj:`str`): name of the property to check

        Returns:
            :obj:`bool`: True if the property is set, else False
        """

        if (isinstance(getattr(self,varName),list) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (isinstance(getattr(self,varName),np.ndarray) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (getattr(self,varName) != None):
            return True
        return False
    #------------------------
