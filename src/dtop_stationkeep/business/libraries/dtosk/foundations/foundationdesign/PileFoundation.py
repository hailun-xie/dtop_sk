import collections
import numpy as np
import os
import json
import h5py
#------------------------------------
# @ USER DEFINED IMPORTS START
from scipy import interpolate, optimize
from .parameters_for_piles import x_phi, y_fi, y_nq, y_qi
from ...entity import BodyPart
from ...forceModel import CurrentForce
from ...forceModel import MeanWaveDriftForce
import matplotlib.pyplot as plt
import pandas as pd
import copy
import logging
logger = logging.getLogger() # grabs root logger
pd.options.display.float_format = '{:,.2g}'.format
# @ USER DEFINED IMPORTS END
#------------------------------------

class PileFoundation():

    """data model for input data needed to perform the pile foundation design
    """

    #------------
    # Constructor
    #------------
    def __init__(self,name=None):
#------------------------------------
# @ USER DEFINED DESCRIPTION START
# @ USER DEFINED DESCRIPTION END
#------------------------------------

        self._z_ap=0.0
        self._moor_V_min=0.0
        self._moor_V_max=0.0
        self._moor_H_x=0.0
        self._moor_H_y=0.0
        self._moor_M_x=0.0
        self._moor_M_y=0.0
        self._u_shear_strenght_d=0.0
        self._int_friction_angle_d=0.0
        self._n_iterations_max=500
        self._material_sf=1.3
        self._d_ext=0.2
        self._w_t=0.005
        self._plength=0
        self._pile_length_above_seabed=0.0
        self._total_length=0
        self._Pu=0.0
        self._user_deflection_crit=0
        self._deflection_crit=0.0
        self._seabed_connection_type=''
        self._y_max=0.0
        self._k_pile_soil=0.0
        self._I=0.0
        self._soil_type=0.0
        self._sand_dr=0
        self._z_max=0
        self._lat_cap=0
        self._dist_attachment=0
        self._SF=1
        self._pile_bouyant_weight=0.0
        self._pile_weight=0.0
        self._pile_vol=0.0
        self._pile_section=0.0
        self._d_steel=7850
        self._footprint=0.0
        self._mid_overburden_pressure=0.0
        self._g_b=0.0
        self._sf_tension=0.0
        self._sf_comp=0.0
        self._pile_burried_surface=0.0
        self._pile_surface=0.0
        self._comp_capacity=0.0
        self._tension_capacity=0.0
        self._tip_type=''
        self._Mmax=0.0
        self._section_stress=0.0
        self._pile_sec_modulus=0.0
        self._criteria_lateral_capacity=False
        self._criteria_axial_tension_capacity=False
        self._criteria_axial_compression_capacity=False
        self._criteria_steel_stress=False
        self._S_steel=0.0
        self._uls_res_lateral_load=0.0
        self._uls_tension=0.0
        self._uls_compression=0.0
        self._name='none'
        self._description=''
        if not(name == None): # pragma: no cover
            self._name = name

#------------------------------------
# @ USER DEFINED PROPERTIES START
        self.loads_from_mooring = np.zeros(shape=(1), dtype=float)
        self.water_density = 1025
        self.g= 9.81
        self.uls_res_lateral_load = 0
        self.uls_res_moment = 0
        self.max_plength = 60
        self.E_steel= 200e9
        self.S_steel= (250e6/self.material_sf)
        self.int_friction_angle_d_rad = 0
        self.uls_tension = 0
        self.uls_compression = 0
        self.i = 0
        self.section_tension=0
        self.depth = []
        self._Am= []
        self._Bm= []
        self.cohessionless_sf_comp=0
        self.cohessionless_sf_tension=0
        self.soil= ''

        # Current/Wave force model on wet part of the pile
        self.uls_env = []
        self.water_depth = 100.0
        self.pile_wet_part = BodyPart.BodyPart()
        self.pile_wet_part.geometry_type='cylinder'
        self.pile_wet_part.cylinder.length=self.pile_length_above_seabed
        self.pile_wet_part.position_in_b[2]=self.pile_length_above_seabed/2
        self.pile_wet_part.cylinder.diameter=self.d_ext
        self.pile_wet_part.mean_wave_drift_force_applied = False

# @ USER DEFINED PROPERTIES END
#------------------------------------

#------------------------------------
# @ USER DEFINED METHODS START


    def desing_loads_from_mooring(self,ff = np.zeros(shape=(6),dtype=float)):

        # External loads due to the device
        f_ext = copy.deepcopy(self.loads_from_mooring[0:6])

        # Add External loads due to the part of the pile above seabed (to be conservative, we add its contribution in the same direction as the existing external load)
        if np.linalg.norm(f_ext[2:4])>0:
            f_ext[2:4] = f_ext[2:4] + f_ext[2:4]/np.linalg.norm(f_ext[2:4])*np.linalg.norm(ff[0:2])
        else:
            f_ext[2:4] = copy.deepcopy(ff[0:2])
        if np.linalg.norm(f_ext[4:6])>0:
            f_ext[4:6] = f_ext[4:6] + f_ext[4:6]/np.linalg.norm(f_ext[4:6])*np.linalg.norm(ff[3:5])
        else:
            f_ext[4:6] = copy.deepcopy(ff[3:5])


        self.moor_V_min = copy.deepcopy(f_ext[0])
        self.moor_V_max = copy.deepcopy(f_ext[1])
        self.moor_H_x = copy.deepcopy(f_ext[2])
        self.moor_H_y = copy.deepcopy(f_ext[3])
        self.moor_M_x = copy.deepcopy(f_ext[4])
        self.moor_M_y = copy.deepcopy(f_ext[5])
        self.z_ap = self.loads_from_mooring[6]

        self.uls_tension = -min(self.moor_V_min,0)
        self.uls_compression = self.moor_V_max
        self.uls_res_lateral_load = np.sqrt((self._moor_H_x) ** 2 + (self.moor_H_y) ** 2)
        self.uls_res_moment = (self.uls_res_lateral_load * self.z_ap) + self.moor_M_x + self.moor_M_y

        self.int_friction_angle_d_rad = np.deg2rad(self.int_friction_angle_d)

        return self.moor_V_min, self.moor_V_max, self.moor_H_y, self.uls_res_lateral_load, self.uls_res_moment

    def soil_class(self):
        if self.u_shear_strenght_d > 0:
            self.soil = 'cohesive'
        elif self.int_friction_angle_d > 0:
            self.soil = 'cohesionless'

        return self.int_friction_angle_d, self.u_shear_strenght_d, self.soil


    def pile_stiffness(self):
        self.I = (np.pi * (self.d_ext / 2) ** 3) * self.w_t

        if self.soil == 'cohesionless':
            if  (0 <= self.sand_dr) and  (self.sand_dr < 0.5) :
                n_h = (1.41659715e+03 * (self.deflection_crit ** 4)) - (4.19757211e+04  * (self.deflection_crit ** 3)) + (4.47334169e+05 * (self.deflection_crit ** 2)) - (2.19605786e+06 * self.deflection_crit) + 5.70161654e+06

            elif (0.5 <= self.sand_dr)  and (self.sand_dr < 0.65) :
                n_h = (2.37985919e+02 * self.deflection_crit ** 6) + (-9.39641512e+03 * self.deflection_crit ** 5) + (1.50296991e+05 * self.deflection_crit ** 4) + (-1.24591529e+06 * self.deflection_crit ** 3) + (5.66448546e+06 * self.deflection_crit**2) + (-1.38860911e+07 * self.deflection_crit) + 1.91984502e+07

            elif 0.65 <= self.sand_dr and self.sand_dr < 0.85 :
                n_h = (-3.65613783e+03 * self.deflection_crit ** 5) + (1.10721983e+05 * self.deflection_crit ** 4) + (-1.28455272e+06 * self.deflection_crit ** 3) + (7.20353489e+06 * self.deflection_crit ** 2) + (-2.06775963e+07 * self.deflection_crit) + 3.28674871e+07

            elif 0.85 <= self.sand_dr :
                n_h = (1.01587750e+03 * self.deflection_crit ** 6) + (-3.65514168e+04 * self.deflection_crit ** 5) + (5.26657079e+05 * self.deflection_crit ** 4) + (-3.88964035e+06 * self.deflection_crit ** 3) + (1.57272485e+07 * self.deflection_crit ** 2) + (-3.52489035e+07 * self.deflection_crit) + 4.77865815e+07

        elif self.soil == 'cohesive' :
            if self.u_shear_strenght_d < 33000:
                k1 = (1.03028046e-04 * self.deflection_crit ** 6) + (-7.18889009e-03 * self.deflection_crit ** 5) + (1.99553451e-01 * self.deflection_crit ** 4) + (-2.80656849e+00 * self.deflection_crit ** 3) + (2.10942306e+01 * self.deflection_crit ** 2) + (-8.22955338e+01 * self.deflection_crit) + 1.60093964e+02

            else:
                k1 = (1.94922004e-04 * self.deflection_crit ** 6) + (-1.35052465e-02 * self.deflection_crit ** 5) + (3.71043691e-01 * self.deflection_crit ** 4) + (-5.15788655e+00 * self.deflection_crit ** 3) + (3.85732171e+01 * self.deflection_crit ** 2) + (-1.54196182e+02 * self.deflection_crit) + 3.11176262e+02

            n_h = self.u_shear_strenght_d * k1 / self.d_ext

        else:
            raise Exception('Error from pile_stiffness: soil should be cohesive or cohesionless. Found: ' + str(self.soil))

        self.k_pile_soil = (self.E_steel * self.I / n_h) ** 0.2

        return self.k_pile_soil, n_h

    def init_length (self):
        if self.seabed_connection_type == 'fixed':
            self.plength = min (max (3 * self.k_pile_soil, 5 * self.d_ext), 6 * self.d_ext)
        if self.seabed_connection_type == 'moored':
            self.plength = min (max (3 * self.k_pile_soil, 5 * self.d_ext), 10 * self.d_ext)

    def increase_section_incrementaly (self, n_iterations):
        if n_iterations - 1 == 0:
            self.d_ext = self.d_ext
            self.w_t = self.w_t
        else:
            if self.d_ext < 10 and self.w_t < 0.8 :
                self.d_ext += 0.10
                self.w_t += 0.002
                self.pile_stiffness()
                self.init_length()

            elif self.d_ext < 10 and self.w_t >= 0.8 :
                self.d_ext += 0.10
                self.pile_stiffness()
                self.init_length()
            elif self.d_ext > 10 and self.w_t >= 0.8 :
                self.plength *= 1.10



    def pile_properties (self):

        int_diam = self.d_ext - (2 * self.w_t)
        self.pile_section = (np.pi / 4) * ((self.d_ext ** 2)  - (int_diam** 2))
        self.footprint = (np.pi / 4) * self.d_ext ** 2
        self.pile_vol = self.pile_section * (self.plength + self.pile_length_above_seabed)
        self.pile_weight = self.pile_vol * self.d_steel * self.g
        self.pile_bouyant_weight = self.pile_weight - (self.pile_vol * self.water_density * self.g)
        self.pile_burried_surface = np.pi * (self.d_ext) * self.plength # Surface below seabed (used for friction resistance calculation)
        self.pile_surface = np.pi * (self.d_ext) * self.pile_length_above_seabed # Surface above seabed (used for Environmental Results later)
        self.pile_sec_modulus = np.pi * (self.d_ext**4 - int_diam**4) / (32 * self.d_ext)
        self.total_length = self.plength + self.pile_length_above_seabed

        rotation_center_in_b = np.array([0,0,0])

        # Update external loads due to current/wave forces on the pile part which is above the seabed
        # First update the dimensions of the part of the pile that is above seabed
        if self.pile_length_above_seabed>0:
            current_force = CurrentForce.CurrentForce()
            mean_wave_drift_force = MeanWaveDriftForce.MeanWaveDriftForce()
            position = np.zeros(shape=(6))
            position[2] = -self.water_depth
            self.pile_wet_part.cylinder.length=self.pile_length_above_seabed
            self.pile_wet_part.position_in_b[2]=self.pile_length_above_seabed/2
            self.pile_wet_part.cylinder.diameter=self.d_ext
            body_parts=[]
            body_parts.append(self.pile_wet_part)
            if self.pile_length_above_seabed>=self.water_depth:
                self.pile_wet_part.mean_wave_drift_force_applied = True
            # Compute the maximum force
            ff = np.zeros(shape=(6),dtype=float)
            for idx in range(0,len(self.uls_env)):

                ff_0 = np.zeros(shape=(6),dtype=float)

                # compute current force
                ff_0 = ff_0 + current_force.compute_force(position,body_parts,rotation_center_in_b,self.uls_env[idx].current,self.water_depth,wave=self.uls_env[idx].wave)

                # Compute mean wave drift force
                ff_0 = ff_0 + mean_wave_drift_force.compute_force('fixed',position,body_parts,rotation_center_in_b,self.uls_env[idx].wave,self.water_depth)

                if np.linalg.norm(ff_0[0:3])>np.linalg.norm(ff[0:3]):
                    ff = copy.deepcopy(ff_0)

            self.desing_loads_from_mooring(ff=ff)


    def compute_lateral_capacity (self):

        self.z_max = self.plength / self.k_pile_soil
        A_y = (-1.50403666e-01 * (self.z_max ** 5)) + (2.88499281e+00 * (self.z_max ** 4)) + (-2.19138178e+01 * (self.z_max ** 3)) + (8.23887001e+01 * (self.z_max ** 2)) + (-1.53392529e+02 * self.z_max) + 1.15953722e+02
        B_y = (-0.12403947 * (self.z_max ** 5)) + (2.34580504 * (self.z_max ** 4)) - (-17.56178296 * (self.z_max ** 3)) + (65.10317608 * (self.z_max ** 2)) - (-119.69087269 * self.z_max) + 89.24742881

        self.lat_cap = (self.deflection_crit * self.d_ext / 100) * (self.E_steel * self.I) / ((A_y * (self.k_pile_soil ** 3)) + (self.dist_attachment * B_y * (self.k_pile_soil ** 2)))

    def compute_skin_friction (self):

        self.mid_overburden_pressure = (self.g_b) * self.plength / 2

        if self.soil == 'cohesive':
            consolidation = self.u_shear_strenght_d / self.mid_overburden_pressure

            if 0 < consolidation and consolidation <= 0.4:
                self.cohessive_sf = min((self.mid_overburden_pressure * (0.468 - 0.052 * np.log(self.plength * 3.28084 / 2))), self.u_shear_strenght_d)

            elif 0.4 < consolidation and consolidation < 2 :
                self.cohessive_sf = self.u_shear_strenght_d * (0.458 - 0.155* np.log(consolidation))

            elif 2 <= consolidation :
                self.cohessive_sf = 0.351 * self.u_shear_strenght_d

            elif consolidation == 0:
                self.cohessive_sf = 0

            self.sf_comp =  self.cohessive_sf
            self.sf_tension =  self.cohessive_sf

            return consolidation

        elif self.soil == 'cohesionless':
            '''Table 2.3 from DNV-C30.4, limits for axial resistance of piles
            x_phi = friction angle[°], y_fi = unit friction resistance [Pa], y_nq = bearing capacity, y_qi = unit bearing capacity [Pa]'''

            f_int_fi = interpolate.interp1d(x_phi, y_fi)
            fi = f_int_fi(self.int_friction_angle_d)

            self.sf_comp = min (0.7 * self.mid_overburden_pressure * np.tan(self.int_friction_angle_d_rad), fi)
            self.sf_tension = min (0.5 * self.mid_overburden_pressure * np.tan(self.int_friction_angle_d_rad), fi)


    def compute_axial_capacity (self):
        friction_resistance = self.sf_tension * self.pile_burried_surface

        if self.soil == 'cohesive':
            u_bearing_capacity =  9 * self.u_shear_strenght_d
        elif self.soil == 'cohesionless':
            f_int_nq = interpolate.interp1d(x_phi, y_nq)
            nq = f_int_nq(self.int_friction_angle_d)
            f_int_qi = interpolate.interp1d(x_phi, y_qi)
            qi = f_int_qi(self.int_friction_angle_d)
            u_bearing_capacity = min( self.g_b * self.plength * nq, qi)

        if self.tip_type == 'closed_end':
            bearing_capacity = u_bearing_capacity * self.footprint
        if self.tip_type == 'open_end':
            bearing_capacity = u_bearing_capacity * self.pile_section

        self.tension_capacity = friction_resistance + self.pile_bouyant_weight
        self.comp_capacity = friction_resistance + bearing_capacity

    def steel_stress_analysis (self):
        n_points = 20
        c = self.plength/self.k_pile_soil
        z = np.zeros(n_points)
        zT = np.zeros(n_points)
        Am = np.zeros(n_points)
        Bm = np.zeros(n_points)
        self.M = np.zeros(n_points)

        for x in range(n_points):
            z [x] = (self.plength / n_points) * x
            zT [x] = z [x] / self.k_pile_soil
            c = self.plength/self.k_pile_soil

            if c <= 2.5:
                Am [x] = (-0.6402529752 * zT[x]**6) + (3.6839568177 * zT[x]**5) - (7.5804427718 * zT[x]**4) + (6.8685134440 * zT[x]**3) - (3.4657048331 * zT[x]**2) + (1.6527674956 * zT[x]) - 0.0189586227
                Bm [x] = (-5.3921700461E-01 * zT[x]**6) + (3.0944286116 * zT[x]**5) + (- 6.2693024058 * zT[x]**4) + (5.411842863 * zT[x]**3) + (-2.2762773298 * zT[x]**2) + (3.0018593665E-01 * zT[x]) + 9.8183942334E-01
                self.M [x] = (Am[x] * self.uls_res_lateral_load * self.k_pile_soil) + (Bm[x] * self.uls_res_moment)

            if 2.5 < c and c <= 3.5:
                Am [x] = (1.6225749946E-02 * zT[x]**6) - (1.3700358515E-01 * zT[x]**5) + (4.8565922398E-01 * zT[x]**4) - (7.8993439236E-01 * zT[x]**3) + (2.3251983961E-02 * zT[x]**2) + (1.0988092898 * zT[x]) - 1.4009640881E-03
                Bm [x] = (4.0570357487E-03  * zT[x]**6) + (-7.7698740787E-02 * zT[x]**5) + (4.3562272034E-01 * zT[x]**4) + (-9.4294602975E-01 * zT[x]**3) + (6.2370272841E-01 * zT[x]**2) + (-2.0135492039E-01 * zT[x]) - 1.0064126638E+00
                self.M [x] = (Am[x] * self.uls_res_lateral_load * self.k_pile_soil) + (Bm[x] * self.uls_res_moment)

            if 3.5 < c and c <= 4.5:
                Am [x] = (-5.2737946549E-04 * zT[x]**6) - (2.1298733041E-03 * zT[x]**5) + (5.8197014597E-02 * zT[x]**4) + (-1.5952872968E-01 * zT[x]**3) - (3.0947340794E-01 * zT[x]**2) + (1.1537818913* zT[x]) - 4.3525821395E-03
                Bm [x] = (5.3415496641E-03 * zT[x]**6) + (-7.2226001912E-02 * zT[x]**5) + (3.6012640456E-01 * zT[x]**4) + (-7.5431969167E-01 * zT[x]**3) + (4.8556251482E-01 * zT[x]**2) - (1.4978681591E-01* zT[x]) + 1.0015414439
                self.M [x] = (Am[x] * self.uls_res_lateral_load * self.k_pile_soil) + (Bm[x] * self.uls_res_moment)

            if 4.5 < c and c <= 7.5:
                Am [x] = (5.8370066554E-05 * zT[x]**6) - (9.9905657446E-03 * zT[x]**5) + (9.6969941642E-02 * zT[x]**4) - (2.4495278076E-01 * zT[x]**3) - (2.2638210304E-01 * zT[x]**2) + (1.1276926067 * zT[x]) - 9.2510766822E-03
                Bm [x] = (3.5736684785E-03 * zT[x]**6) + (- 4.8254259779E-02 * zT[x]**5) + (2.3211987345E-01 * zT[x]**4) + (-4.1754189929E-01 * zT[x]**3) + (-4.4037432830E-02 * zT[x]**2) + (8.6928087034E-02 * zT[x]) + 9.7111486015E-01
                self.M [x] = (Am[x] * self.uls_res_lateral_load * self.k_pile_soil) + (Bm[x] * self.uls_res_moment)

            if 7.5 < c :
                Am[x] = (6.4893793080E-04 * zT[x]**6) + (-1.7920613342E-02 * zT[x]**5) + (1.3595766551E-01 * zT[x]**4) + (-3.3339955942E-01 * zT[x]**3) + (-1.3147563076E-01 * zT[x]**2) + (1.0825784217 * zT[x]) - 9.0550247446E-04
                Bm[x] = (3.6941779329E-03 * zT[x]**6) + (-4.9146377790E-02 * zT[x]**5) + (2.3414996413E-01 * zT[x]**4) + (-4.2728061084E-01 * zT[x]**3) + (8.5503060085E-02 * zT[x]**2) + (3.2166038527E-02 * zT[x]) + 9.8076647021E-01
                self.M [x] = (Am[x] * self.uls_res_lateral_load * self.k_pile_soil) + (Bm[x] * self.uls_res_moment)

        self.Mmax = np.amax(self.M)
        self.section_stress = (self.uls_tension/self.pile_section) + (self.Mmax / self.pile_sec_modulus)
        self.depth = z

    def update_pile(self):
        self.pile_properties()
        self.pile_stiffness()
        self.y_max = (self.deflection_crit / 100) * self.d_ext
        self.compute_lateral_capacity()
        self.compute_skin_friction()
        self.compute_axial_capacity()
        self.steel_stress_analysis()

    def assess(self):


        logger.info('-----------------------------')
        logger.info('Assessment of pile foundation')
        logger.info('-----------------------------')

        self.desing_loads_from_mooring()
        self.soil_class()
        self.update_pile()

        if (self.lat_cap / self.SF) >= self.uls_res_lateral_load:
            self.criteria_lateral_capacity = True
        else:
            self.criteria_lateral_capacity = False

        if ((self.tension_capacity ) / self.SF) >= self.uls_tension:
            self.criteria_axial_tension_capacity = True
        else:
            self.criteria_axial_tension_capacity = False

        if (self.comp_capacity / self.SF) >= (self.uls_compression + (self.pile_bouyant_weight)):
            self.criteria_axial_compression_capacity = True
        else:
            self.criteria_axial_compression_capacity = True

        if self.S_steel >= self.section_stress:
            self.criteria_steel_stress = True
        else:
            self.criteria_steel_stress = False

        logger.info('Final lateral capacity: ' + str(self.lat_cap / self.SF) + str(' > ') + str(self.uls_res_lateral_load) + ' : ' + str(self.criteria_lateral_capacity))
        logger.info('Final axial tension capacity: ' + str((self.tension_capacity ) / self.SF) + str(' > ') + str(self.uls_tension) + ' : ' + str(self.criteria_axial_tension_capacity))
        logger.info('Final axial compression capacity: ' + str(self.comp_capacity / self.SF) + str(' > ') + str(self.uls_compression + (self.pile_bouyant_weight)) + ' : ' + str(self.criteria_axial_compression_capacity))
        logger.info('Final steel stress criterion: ' + str(self.S_steel) + str(' > ') + str(self.section_stress) + ' : ' + str(self.criteria_steel_stress))

        logger.info('Final design:')
        logger.info('Diameter [m]: ' + str(self.d_ext))
        logger.info('Thickness [m]: ' + str(self.w_t))
        logger.info('Burried length [m]: ' + str(self.plength))
        logger.info('Total length [m]: ' + str(self.total_length))
        logger.info('Mass [kg]: ' + str(self.pile_weight/9.81))

    def design (self):
        n_iterations = 0
        self.desing_loads_from_mooring()
        self.soil_class()
        self.pile_stiffness()
        self.init_length()
        self.pile_properties()
        self.compute_lateral_capacity()
        self.y_max = (self.deflection_crit / 100) * self.d_ext
        self.compute_skin_friction()
        self.compute_axial_capacity()
        self.steel_stress_analysis()

        logger.info('--------------------------------')
        logger.info('Design of pile foundation')
        logger.info('--------------------------------')
        logger.info('Applied load Vmin, Vmax : ' + str(self.loads_from_mooring[0:2]))
        logger.info('Applied load Hx, Hy : ' + str(self.loads_from_mooring[2:4]))
        logger.info('Applied load Mx, My : ' + str(self.loads_from_mooring[4:6]))
        logger.info('Applied load vertical position zap : ' + str(self.loads_from_mooring[6]))
        logger.info('Safety factor: ' + str(self.SF))
        logger.info('Initial lateral capacity: ' + str(self.lat_cap / self.SF) + str(' vs ') + str(self.uls_res_lateral_load))
        logger.info('Initial axial tension capacity: ' + str((self.tension_capacity ) / self.SF) + str(' vs ') + str(self.uls_tension))
        logger.info('Initial axial compression capacity: ' + str(self.comp_capacity / self.SF) + str(' vs ') + str(self.uls_compression + (self.pile_bouyant_weight)))
        logger.info('Initial steel stress criterion: ' + str(self.S_steel) + str(' vs ') + str(self.section_stress))

        logger.info('Proceed with lateral capacity design...')
        n0=copy.deepcopy(n_iterations)
        while (self.lat_cap / self.SF) < self.uls_res_lateral_load :
            if n_iterations > self.n_iterations_max :
                message_a = "dimensioning not achieved, maximal number of iterations achieved - lateral design"
                raise Exception(message_a)
                break
            else:
                n_iterations = n_iterations + 1
                self.increase_section_incrementaly(n_iterations)
            self.update_pile()
        logger.info('Done. (' + str(n_iterations-n0) + ' iterations)')

        logger.info('Proceed with axial tension design...')
        n0=copy.deepcopy(n_iterations)
        while ((self.tension_capacity ) / self.SF) < self.uls_tension :
            if n_iterations > self.n_iterations_max :
                message_a = "dimensioning not achieved, maximal number of iterations achieved - axial tension design "
                raise Exception(message_a)
                break
            else:
                n_iterations = n_iterations + 1
                self.increase_section_incrementaly(n_iterations)
            self.update_pile()
        logger.info('Done. (' + str(n_iterations-n0) + ' iterations)')

        logger.info('Proceed with axial compression design...')
        n0=copy.deepcopy(n_iterations)
        while (self.comp_capacity / self.SF) < (self.uls_compression + (self.pile_bouyant_weight)):
            if n_iterations > self.n_iterations_max :
                message_a = "dimensioning not achieved, maximal number of iterations achieved - axial compression design "
                raise Exception(message_a)
                break
            else:
                n_iterations = n_iterations + 1
                self.increase_section_incrementaly(n_iterations)
            self.update_pile()
        logger.info('Done. (' + str(n_iterations-n0) + ' iterations)')

        logger.info('Proceed with steel stress capacity design...')
        n0=copy.deepcopy(n_iterations)
        while self.S_steel < self.section_stress :
            if n_iterations > self.n_iterations_max :
                message_a = "dimensioning not achieved, maximal number of iterations achieved - Steel Stress"
                raise Exception(message_a)
                break
            else:
                n_iterations = n_iterations + 1
                self.increase_section_incrementaly(n_iterations)
            self.update_pile()
        logger.info('Done. (' + str(n_iterations-n0) + ' iterations)')

        self.assess()

        return n_iterations


    def outputs(self):
        bom_esa = pd.DataFrame ({"Materials": ["steel"],
                             "Weight [N]": [self.pile_weight],
                             "Volume [m3]" : [self.pile_vol],
                             "Footprint [m2]": [self.footprint]})

        pile_properties = pd.DataFrame ({"Diameter [m]": [self.d_ext], "width [m]": [self.w_t], "Burried length [m]": [self.plength], "Total length [m]": [self.total_length], "y_max [m]": [self.y_max], "def_crit [%]":[self.deflection_crit],
                                        "Inertia_moment [m4]": [self.I], "Stiffness[m]" : [self.k_pile_soil], 'Slenderness': [self.plength / self.d_ext]})

        pile_resistance = pd.DataFrame ({"Lateral_capacity [N]": [self.lat_cap],
                                        "uls_lat [N]": [self.uls_res_lateral_load],
                                        "skin_friction_tension [N/m2]": [self.sf_tension],
                                        "skin_friction_compression [N/m2]": [self.sf_comp],
                                        "tension_capacity [N]": [self.tension_capacity],
                                        "comp_capacity [N]": [self.comp_capacity],
                                        "uls_tension [N]":[self.uls_tension],
                                        "uls_comp [N]":[self.uls_compression + (self.pile_bouyant_weight)],
                                        "steel_stress [N]":[self.section_stress] })


        # plt.plot(self.M, self.depth)
        # plt.gca().invert_yaxis()
        # plt.xlabel('Moment [Nm]')
        # plt.ylabel('Depth [m]')
        # plt.show()
        # plt.savefig('moment_diagram.png')

        # pile_resistance.to_csv()
        # pile_properties.to_csv()
        # bom_esa.to_csv()

        return pile_properties, pile_resistance, bom_esa

# @ USER DEFINED METHODS END
#------------------------------------

    #------------
    # Get functions
    #------------
    @ property
    def z_ap(self): # pragma: no cover
        """float: high of application point of loads []
        """
        return self._z_ap
    #------------
    @ property
    def moor_V_min(self): # pragma: no cover
        """float: maximal moroing vertical upward load transfert to foundation []
        """
        return self._moor_V_min
    #------------
    @ property
    def moor_V_max(self): # pragma: no cover
        """float: maximal mooring vertical downard load transfert to foundation []
        """
        return self._moor_V_max
    #------------
    @ property
    def moor_H_x(self): # pragma: no cover
        """float: maximal mooring horizontal x load transfert to foundation []
        """
        return self._moor_H_x
    #------------
    @ property
    def moor_H_y(self): # pragma: no cover
        """float: maximal mooring horizontal y load transfert to foundation []
        """
        return self._moor_H_y
    #------------
    @ property
    def moor_M_x(self): # pragma: no cover
        """float: maximal mooring horizontal x load transfert to foundation []
        """
        return self._moor_M_x
    #------------
    @ property
    def moor_M_y(self): # pragma: no cover
        """float: maximal mooring horizontal y load transfert to foundation []
        """
        return self._moor_M_y
    #------------
    @ property
    def u_shear_strenght_d(self): # pragma: no cover
        """float: undrained shear strenght value for design []
        """
        return self._u_shear_strenght_d
    #------------
    @ property
    def int_friction_angle_d(self): # pragma: no cover
        """float: internal friction angle for design []
        """
        return self._int_friction_angle_d
    #------------
    @ property
    def n_iterations_max(self): # pragma: no cover
        """float: none []
        """
        return self._n_iterations_max
    #------------
    @ property
    def material_sf(self): # pragma: no cover
        """float: none []
        """
        return self._material_sf
    #------------
    @ property
    def d_ext(self): # pragma: no cover
        """float: none []
        """
        return self._d_ext
    #------------
    @ property
    def w_t(self): # pragma: no cover
        """float: none []
        """
        return self._w_t
    #------------
    @ property
    def plength(self): # pragma: no cover
        """float: none []
        """
        return self._plength
    #------------
    @ property
    def pile_length_above_seabed(self): # pragma: no cover
        """float: Pile length above seabed (typically >=0 for a pile foundation, =0 for a pile anchor). []
        """
        return self._pile_length_above_seabed
    #------------
    @ property
    def total_length(self): # pragma: no cover
        """float: none []
        """
        return self._total_length
    #------------
    @ property
    def Pu(self): # pragma: no cover
        """float: none []
        """
        return self._Pu
    #------------
    @ property
    def user_deflection_crit(self): # pragma: no cover
        """float: none []
        """
        return self._user_deflection_crit
    #------------
    @ property
    def deflection_crit(self): # pragma: no cover
        """float: none []
        """
        return self._deflection_crit
    #------------
    @ property
    def seabed_connection_type(self): # pragma: no cover
        """str: fixed or moored
        """
        return self._seabed_connection_type
    #------------
    @ property
    def y_max(self): # pragma: no cover
        """float: maximal deflection []
        """
        return self._y_max
    #------------
    @ property
    def k_pile_soil(self): # pragma: no cover
        """float: pile soil stiffnss []
        """
        return self._k_pile_soil
    #------------
    @ property
    def I(self): # pragma: no cover
        """float: moment of inertia of pile []
        """
        return self._I
    #------------
    @ property
    def soil_type(self): # pragma: no cover
        """float: type of soil []
        """
        return self._soil_type
    #------------
    @ property
    def sand_dr(self): # pragma: no cover
        """float: moment of inertia of pile []
        """
        return self._sand_dr
    #------------
    @ property
    def z_max(self): # pragma: no cover
        """float: depth coefficient []
        """
        return self._z_max
    #------------
    @ property
    def lat_cap(self): # pragma: no cover
        """float: lateral capacity of pile []
        """
        return self._lat_cap
    #------------
    @ property
    def dist_attachment(self): # pragma: no cover
        """float: distance of the pile load attachement point above the seafloor default value 0 []
        """
        return self._dist_attachment
    #------------
    @ property
    def SF(self): # pragma: no cover
        """float: safety factor applied to pile capcity []
        """
        return self._SF
    #------------
    @ property
    def pile_bouyant_weight(self): # pragma: no cover
        """float: pile obuyant weight []
        """
        return self._pile_bouyant_weight
    #------------
    @ property
    def pile_weight(self): # pragma: no cover
        """float: pile weight []
        """
        return self._pile_weight
    #------------
    @ property
    def pile_vol(self): # pragma: no cover
        """float: pile volume of steel []
        """
        return self._pile_vol
    #------------
    @ property
    def pile_section(self): # pragma: no cover
        """float: pile volume of steel []
        """
        return self._pile_section
    #------------
    @ property
    def d_steel(self): # pragma: no cover
        """float: steel density kgm-3 []
        """
        return self._d_steel
    #------------
    @ property
    def footprint(self): # pragma: no cover
        """float: foundation footprint []
        """
        return self._footprint
    #------------
    @ property
    def mid_overburden_pressure(self): # pragma: no cover
        """float: overburden pressure at midpoint of the foundation []
        """
        return self._mid_overburden_pressure
    #------------
    @ property
    def g_b(self): # pragma: no cover
        """float: bouyant soil unit wieght kgm-3 []
        """
        return self._g_b
    #------------
    @ property
    def sf_tension(self): # pragma: no cover
        """float: skin friction per meter for tension []
        """
        return self._sf_tension
    #------------
    @ property
    def sf_comp(self): # pragma: no cover
        """float: skin friction per meter for compression []
        """
        return self._sf_comp
    #------------
    @ property
    def pile_burried_surface(self): # pragma: no cover
        """float: pile burried surface area of the pile below seafloor []
        """
        return self._pile_burried_surface
    #------------
    @ property
    def pile_surface(self): # pragma: no cover
        """float: pile surface area of over the seafloor []
        """
        return self._pile_surface
    #------------
    @ property
    def comp_capacity(self): # pragma: no cover
        """float: pile compression capacity []
        """
        return self._comp_capacity
    #------------
    @ property
    def tension_capacity(self): # pragma: no cover
        """float: pile tension capacity []
        """
        return self._tension_capacity
    #------------
    @ property
    def tip_type(self): # pragma: no cover
        """str: open_end or closed_end
        """
        return self._tip_type
    #------------
    @ property
    def Mmax(self): # pragma: no cover
        """float: maximal moment on the pile []
        """
        return self._Mmax
    #------------
    @ property
    def section_stress(self): # pragma: no cover
        """float: section stress []
        """
        return self._section_stress
    #------------
    @ property
    def pile_sec_modulus(self): # pragma: no cover
        """float: section modulus []
        """
        return self._pile_sec_modulus
    #------------
    @ property
    def criteria_lateral_capacity(self): # pragma: no cover
        """bool: Lateral capacity criteria. True or False []
        """
        return self._criteria_lateral_capacity
    #------------
    @ property
    def criteria_axial_tension_capacity(self): # pragma: no cover
        """bool: Axial tension capacity. True or False []
        """
        return self._criteria_axial_tension_capacity
    #------------
    @ property
    def criteria_axial_compression_capacity(self): # pragma: no cover
        """bool: Axial compression capacity. True or False []
        """
        return self._criteria_axial_compression_capacity
    #------------
    @ property
    def criteria_steel_stress(self): # pragma: no cover
        """bool: Steel stress criteria. True or False []
        """
        return self._criteria_steel_stress
    #------------
    @ property
    def S_steel(self): # pragma: no cover
        """float: Steel stress capacity []
        """
        return self._S_steel
    #------------
    @ property
    def uls_res_lateral_load(self): # pragma: no cover
        """float: Calculated lateral load []
        """
        return self._uls_res_lateral_load
    #------------
    @ property
    def uls_tension(self): # pragma: no cover
        """float: Calculated tension []
        """
        return self._uls_tension
    #------------
    @ property
    def uls_compression(self): # pragma: no cover
        """float: Calculated compression []
        """
        return self._uls_compression
    #------------
    @ property
    def name(self): # pragma: no cover
        """str: name of the instance object
        """
        return self._name
    #------------
    @ property
    def description(self): # pragma: no cover
        """str: description of the instance object
        """
        return self._description
    #------------
    #------------
    # Set functions
    #------------
    @ z_ap.setter
    def z_ap(self,val): # pragma: no cover
        self._z_ap=float(val)
    #------------
    @ moor_V_min.setter
    def moor_V_min(self,val): # pragma: no cover
        self._moor_V_min=float(val)
    #------------
    @ moor_V_max.setter
    def moor_V_max(self,val): # pragma: no cover
        self._moor_V_max=float(val)
    #------------
    @ moor_H_x.setter
    def moor_H_x(self,val): # pragma: no cover
        self._moor_H_x=float(val)
    #------------
    @ moor_H_y.setter
    def moor_H_y(self,val): # pragma: no cover
        self._moor_H_y=float(val)
    #------------
    @ moor_M_x.setter
    def moor_M_x(self,val): # pragma: no cover
        self._moor_M_x=float(val)
    #------------
    @ moor_M_y.setter
    def moor_M_y(self,val): # pragma: no cover
        self._moor_M_y=float(val)
    #------------
    @ u_shear_strenght_d.setter
    def u_shear_strenght_d(self,val): # pragma: no cover
        self._u_shear_strenght_d=float(val)
    #------------
    @ int_friction_angle_d.setter
    def int_friction_angle_d(self,val): # pragma: no cover
        self._int_friction_angle_d=float(val)
    #------------
    @ n_iterations_max.setter
    def n_iterations_max(self,val): # pragma: no cover
        self._n_iterations_max=float(val)
    #------------
    @ material_sf.setter
    def material_sf(self,val): # pragma: no cover
        self._material_sf=float(val)
    #------------
    @ d_ext.setter
    def d_ext(self,val): # pragma: no cover
        self._d_ext=float(val)
    #------------
    @ w_t.setter
    def w_t(self,val): # pragma: no cover
        self._w_t=float(val)
    #------------
    @ plength.setter
    def plength(self,val): # pragma: no cover
        self._plength=float(val)
    #------------
    @ pile_length_above_seabed.setter
    def pile_length_above_seabed(self,val): # pragma: no cover
        self._pile_length_above_seabed=float(val)
    #------------
    @ total_length.setter
    def total_length(self,val): # pragma: no cover
        self._total_length=float(val)
    #------------
    @ Pu.setter
    def Pu(self,val): # pragma: no cover
        self._Pu=float(val)
    #------------
    @ user_deflection_crit.setter
    def user_deflection_crit(self,val): # pragma: no cover
        self._user_deflection_crit=float(val)
    #------------
    @ deflection_crit.setter
    def deflection_crit(self,val): # pragma: no cover
        self._deflection_crit=float(val)
    #------------
    @ seabed_connection_type.setter
    def seabed_connection_type(self,val): # pragma: no cover
        self._seabed_connection_type=str(val)
    #------------
    @ y_max.setter
    def y_max(self,val): # pragma: no cover
        self._y_max=float(val)
    #------------
    @ k_pile_soil.setter
    def k_pile_soil(self,val): # pragma: no cover
        self._k_pile_soil=float(val)
    #------------
    @ I.setter
    def I(self,val): # pragma: no cover
        self._I=float(val)
    #------------
    @ soil_type.setter
    def soil_type(self,val): # pragma: no cover
        self._soil_type=float(val)
    #------------
    @ sand_dr.setter
    def sand_dr(self,val): # pragma: no cover
        self._sand_dr=float(val)
    #------------
    @ z_max.setter
    def z_max(self,val): # pragma: no cover
        self._z_max=float(val)
    #------------
    @ lat_cap.setter
    def lat_cap(self,val): # pragma: no cover
        self._lat_cap=float(val)
    #------------
    @ dist_attachment.setter
    def dist_attachment(self,val): # pragma: no cover
        self._dist_attachment=float(val)
    #------------
    @ SF.setter
    def SF(self,val): # pragma: no cover
        self._SF=float(val)
    #------------
    @ pile_bouyant_weight.setter
    def pile_bouyant_weight(self,val): # pragma: no cover
        self._pile_bouyant_weight=float(val)
    #------------
    @ pile_weight.setter
    def pile_weight(self,val): # pragma: no cover
        self._pile_weight=float(val)
    #------------
    @ pile_vol.setter
    def pile_vol(self,val): # pragma: no cover
        self._pile_vol=float(val)
    #------------
    @ pile_section.setter
    def pile_section(self,val): # pragma: no cover
        self._pile_section=float(val)
    #------------
    @ d_steel.setter
    def d_steel(self,val): # pragma: no cover
        self._d_steel=float(val)
    #------------
    @ footprint.setter
    def footprint(self,val): # pragma: no cover
        self._footprint=float(val)
    #------------
    @ mid_overburden_pressure.setter
    def mid_overburden_pressure(self,val): # pragma: no cover
        self._mid_overburden_pressure=float(val)
    #------------
    @ g_b.setter
    def g_b(self,val): # pragma: no cover
        self._g_b=float(val)
    #------------
    @ sf_tension.setter
    def sf_tension(self,val): # pragma: no cover
        self._sf_tension=float(val)
    #------------
    @ sf_comp.setter
    def sf_comp(self,val): # pragma: no cover
        self._sf_comp=float(val)
    #------------
    @ pile_burried_surface.setter
    def pile_burried_surface(self,val): # pragma: no cover
        self._pile_burried_surface=float(val)
    #------------
    @ pile_surface.setter
    def pile_surface(self,val): # pragma: no cover
        self._pile_surface=float(val)
    #------------
    @ comp_capacity.setter
    def comp_capacity(self,val): # pragma: no cover
        self._comp_capacity=float(val)
    #------------
    @ tension_capacity.setter
    def tension_capacity(self,val): # pragma: no cover
        self._tension_capacity=float(val)
    #------------
    @ tip_type.setter
    def tip_type(self,val): # pragma: no cover
        self._tip_type=str(val)
    #------------
    @ Mmax.setter
    def Mmax(self,val): # pragma: no cover
        self._Mmax=float(val)
    #------------
    @ section_stress.setter
    def section_stress(self,val): # pragma: no cover
        self._section_stress=float(val)
    #------------
    @ pile_sec_modulus.setter
    def pile_sec_modulus(self,val): # pragma: no cover
        self._pile_sec_modulus=float(val)
    #------------
    @ criteria_lateral_capacity.setter
    def criteria_lateral_capacity(self,val): # pragma: no cover
        self._criteria_lateral_capacity=val
    #------------
    @ criteria_axial_tension_capacity.setter
    def criteria_axial_tension_capacity(self,val): # pragma: no cover
        self._criteria_axial_tension_capacity=val
    #------------
    @ criteria_axial_compression_capacity.setter
    def criteria_axial_compression_capacity(self,val): # pragma: no cover
        self._criteria_axial_compression_capacity=val
    #------------
    @ criteria_steel_stress.setter
    def criteria_steel_stress(self,val): # pragma: no cover
        self._criteria_steel_stress=val
    #------------
    @ S_steel.setter
    def S_steel(self,val): # pragma: no cover
        self._S_steel=float(val)
    #------------
    @ uls_res_lateral_load.setter
    def uls_res_lateral_load(self,val): # pragma: no cover
        self._uls_res_lateral_load=float(val)
    #------------
    @ uls_tension.setter
    def uls_tension(self,val): # pragma: no cover
        self._uls_tension=float(val)
    #------------
    @ uls_compression.setter
    def uls_compression(self,val): # pragma: no cover
        self._uls_compression=float(val)
    #------------
    @ name.setter
    def name(self,val): # pragma: no cover
        self._name=str(val)
    #------------
    @ description.setter
    def description(self,val): # pragma: no cover
        self._description=str(val)
    #------------
    #-------------------------
    # Representation functions
    #-------------------------
    def type_rep(self): # pragma: no cover
        """Generate a representation of the object type

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains the representation of the object type
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtosk:foundations:foundationdesign:PileFoundation"
        rep["name"] = self.name
        rep["description"] = self.description
        return rep
    def prop_rep(self, short = False, deep = True):

        """Generate a representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains a representation of the object properties
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtosk:foundations:foundationdesign:PileFoundation"
        rep["name"] = self.name
        rep["description"] = self.description
        if self.is_set("z_ap"):
            rep["z_ap"] = self.z_ap
        if self.is_set("moor_V_min"):
            rep["moor_V_min"] = self.moor_V_min
        if self.is_set("moor_V_max"):
            rep["moor_V_max"] = self.moor_V_max
        if self.is_set("moor_H_x"):
            rep["moor_H_x"] = self.moor_H_x
        if self.is_set("moor_H_y"):
            rep["moor_H_y"] = self.moor_H_y
        if self.is_set("moor_M_x"):
            rep["moor_M_x"] = self.moor_M_x
        if self.is_set("moor_M_y"):
            rep["moor_M_y"] = self.moor_M_y
        if self.is_set("u_shear_strenght_d"):
            rep["u_shear_strenght_d"] = self.u_shear_strenght_d
        if self.is_set("int_friction_angle_d"):
            rep["int_friction_angle_d"] = self.int_friction_angle_d
        if self.is_set("n_iterations_max"):
            rep["n_iterations_max"] = self.n_iterations_max
        if self.is_set("material_sf"):
            rep["material_sf"] = self.material_sf
        if self.is_set("d_ext"):
            rep["d_ext"] = self.d_ext
        if self.is_set("w_t"):
            rep["w_t"] = self.w_t
        if self.is_set("plength"):
            rep["plength"] = self.plength
        if self.is_set("pile_length_above_seabed"):
            rep["pile_length_above_seabed"] = self.pile_length_above_seabed
        if self.is_set("total_length"):
            rep["total_length"] = self.total_length
        if self.is_set("Pu"):
            rep["Pu"] = self.Pu
        if self.is_set("user_deflection_crit"):
            rep["user_deflection_crit"] = self.user_deflection_crit
        if self.is_set("deflection_crit"):
            rep["deflection_crit"] = self.deflection_crit
        if self.is_set("seabed_connection_type"):
            rep["seabed_connection_type"] = self.seabed_connection_type
        if self.is_set("y_max"):
            rep["y_max"] = self.y_max
        if self.is_set("k_pile_soil"):
            rep["k_pile_soil"] = self.k_pile_soil
        if self.is_set("I"):
            rep["I"] = self.I
        if self.is_set("soil_type"):
            rep["soil_type"] = self.soil_type
        if self.is_set("sand_dr"):
            rep["sand_dr"] = self.sand_dr
        if self.is_set("z_max"):
            rep["z_max"] = self.z_max
        if self.is_set("lat_cap"):
            rep["lat_cap"] = self.lat_cap
        if self.is_set("dist_attachment"):
            rep["dist_attachment"] = self.dist_attachment
        if self.is_set("SF"):
            rep["SF"] = self.SF
        if self.is_set("pile_bouyant_weight"):
            rep["pile_bouyant_weight"] = self.pile_bouyant_weight
        if self.is_set("pile_weight"):
            rep["pile_weight"] = self.pile_weight
        if self.is_set("pile_vol"):
            rep["pile_vol"] = self.pile_vol
        if self.is_set("pile_section"):
            rep["pile_section"] = self.pile_section
        if self.is_set("d_steel"):
            rep["d_steel"] = self.d_steel
        if self.is_set("footprint"):
            rep["footprint"] = self.footprint
        if self.is_set("mid_overburden_pressure"):
            rep["mid_overburden_pressure"] = self.mid_overburden_pressure
        if self.is_set("g_b"):
            rep["g_b"] = self.g_b
        if self.is_set("sf_tension"):
            rep["sf_tension"] = self.sf_tension
        if self.is_set("sf_comp"):
            rep["sf_comp"] = self.sf_comp
        if self.is_set("pile_burried_surface"):
            rep["pile_burried_surface"] = self.pile_burried_surface
        if self.is_set("pile_surface"):
            rep["pile_surface"] = self.pile_surface
        if self.is_set("comp_capacity"):
            rep["comp_capacity"] = self.comp_capacity
        if self.is_set("tension_capacity"):
            rep["tension_capacity"] = self.tension_capacity
        if self.is_set("tip_type"):
            rep["tip_type"] = self.tip_type
        if self.is_set("Mmax"):
            rep["Mmax"] = self.Mmax
        if self.is_set("section_stress"):
            rep["section_stress"] = self.section_stress
        if self.is_set("pile_sec_modulus"):
            rep["pile_sec_modulus"] = self.pile_sec_modulus
        if self.is_set("criteria_lateral_capacity"):
            rep["criteria_lateral_capacity"] = self.criteria_lateral_capacity
        if self.is_set("criteria_axial_tension_capacity"):
            rep["criteria_axial_tension_capacity"] = self.criteria_axial_tension_capacity
        if self.is_set("criteria_axial_compression_capacity"):
            rep["criteria_axial_compression_capacity"] = self.criteria_axial_compression_capacity
        if self.is_set("criteria_steel_stress"):
            rep["criteria_steel_stress"] = self.criteria_steel_stress
        if self.is_set("S_steel"):
            rep["S_steel"] = self.S_steel
        if self.is_set("uls_res_lateral_load"):
            rep["uls_res_lateral_load"] = self.uls_res_lateral_load
        if self.is_set("uls_tension"):
            rep["uls_tension"] = self.uls_tension
        if self.is_set("uls_compression"):
            rep["uls_compression"] = self.uls_compression
        if self.is_set("name"):
            rep["name"] = self.name
        if self.is_set("description"):
            rep["description"] = self.description
        return rep
    #-------------------------
    # Save functions
    #-------------------------
    def json_rep(self, short=False, deep=True):

        """Generate a JSON representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`str`: string that contains a JSON representation of the object properties
        """

        return ( json.dumps(self.prop_rep(short=short, deep=deep),indent=4, separators=(",",": ")))
    def saveJSON(self, fileName=None):
        """Save the instance of the object to JSON format file

        Args:
            fileName (:obj:`str`, optional): Name of the JSON file, included extension. Defaults is None. If None, the name of the JSON file will be self.name.json. It can also contain an absolute or relative path.

        """

        if fileName==None:
            fileName=self.name + ".json"
        f=open(fileName, "w")
        f.write(self.json_rep())
        f.write("\n")
        f.close()

    def saveHDF5(self,filePath=None):
        """Save the instance of the object to HDF5 format file
        Args:
            filePath (:obj:`str`, optional): Name of the HDF5 file, included extension. Defaults is None. If None, the name of the HDF5 file will be "self.name".h5. It can also contain an absolute or relative path.
        """
        # Define filepath
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".h5"
            else:
                raise Exception("name is required for saving")
        # Open hdf5 file
        h = h5py.File(filePath,"w")
        group = h.create_group(self.name)
        # Save the object to the group
        self.saveToHDF5Handle(group)
        # Close file
        h.close()
        pass
    def saveToHDF5Handle(self, handle):
        """Save the properties of the object to the hdf5 handle.
        Args:
            handle (:obj:`h5py.Group`): Handle used to store the object properties
        """
        handle["z_ap"] = np.array([self.z_ap],dtype=float)
        handle["moor_V_min"] = np.array([self.moor_V_min],dtype=float)
        handle["moor_V_max"] = np.array([self.moor_V_max],dtype=float)
        handle["moor_H_x"] = np.array([self.moor_H_x],dtype=float)
        handle["moor_H_y"] = np.array([self.moor_H_y],dtype=float)
        handle["moor_M_x"] = np.array([self.moor_M_x],dtype=float)
        handle["moor_M_y"] = np.array([self.moor_M_y],dtype=float)
        handle["u_shear_strenght_d"] = np.array([self.u_shear_strenght_d],dtype=float)
        handle["int_friction_angle_d"] = np.array([self.int_friction_angle_d],dtype=float)
        handle["n_iterations_max"] = np.array([self.n_iterations_max],dtype=float)
        handle["material_sf"] = np.array([self.material_sf],dtype=float)
        handle["d_ext"] = np.array([self.d_ext],dtype=float)
        handle["w_t"] = np.array([self.w_t],dtype=float)
        handle["plength"] = np.array([self.plength],dtype=float)
        handle["pile_length_above_seabed"] = np.array([self.pile_length_above_seabed],dtype=float)
        handle["total_length"] = np.array([self.total_length],dtype=float)
        handle["Pu"] = np.array([self.Pu],dtype=float)
        handle["user_deflection_crit"] = np.array([self.user_deflection_crit],dtype=float)
        handle["deflection_crit"] = np.array([self.deflection_crit],dtype=float)
        ar = []
        ar.append(self.seabed_connection_type.encode("ascii"))
        handle["seabed_connection_type"] = np.asarray(ar)
        handle["y_max"] = np.array([self.y_max],dtype=float)
        handle["k_pile_soil"] = np.array([self.k_pile_soil],dtype=float)
        handle["I"] = np.array([self.I],dtype=float)
        handle["soil_type"] = np.array([self.soil_type],dtype=float)
        handle["sand_dr"] = np.array([self.sand_dr],dtype=float)
        handle["z_max"] = np.array([self.z_max],dtype=float)
        handle["lat_cap"] = np.array([self.lat_cap],dtype=float)
        handle["dist_attachment"] = np.array([self.dist_attachment],dtype=float)
        handle["SF"] = np.array([self.SF],dtype=float)
        handle["pile_bouyant_weight"] = np.array([self.pile_bouyant_weight],dtype=float)
        handle["pile_weight"] = np.array([self.pile_weight],dtype=float)
        handle["pile_vol"] = np.array([self.pile_vol],dtype=float)
        handle["pile_section"] = np.array([self.pile_section],dtype=float)
        handle["d_steel"] = np.array([self.d_steel],dtype=float)
        handle["footprint"] = np.array([self.footprint],dtype=float)
        handle["mid_overburden_pressure"] = np.array([self.mid_overburden_pressure],dtype=float)
        handle["g_b"] = np.array([self.g_b],dtype=float)
        handle["sf_tension"] = np.array([self.sf_tension],dtype=float)
        handle["sf_comp"] = np.array([self.sf_comp],dtype=float)
        handle["pile_burried_surface"] = np.array([self.pile_burried_surface],dtype=float)
        handle["pile_surface"] = np.array([self.pile_surface],dtype=float)
        handle["comp_capacity"] = np.array([self.comp_capacity],dtype=float)
        handle["tension_capacity"] = np.array([self.tension_capacity],dtype=float)
        ar = []
        ar.append(self.tip_type.encode("ascii"))
        handle["tip_type"] = np.asarray(ar)
        handle["Mmax"] = np.array([self.Mmax],dtype=float)
        handle["section_stress"] = np.array([self.section_stress],dtype=float)
        handle["pile_sec_modulus"] = np.array([self.pile_sec_modulus],dtype=float)
        handle["criteria_lateral_capacity"] = np.array([self.criteria_lateral_capacity],dtype=bool)
        handle["criteria_axial_tension_capacity"] = np.array([self.criteria_axial_tension_capacity],dtype=bool)
        handle["criteria_axial_compression_capacity"] = np.array([self.criteria_axial_compression_capacity],dtype=bool)
        handle["criteria_steel_stress"] = np.array([self.criteria_steel_stress],dtype=bool)
        handle["S_steel"] = np.array([self.S_steel],dtype=float)
        handle["uls_res_lateral_load"] = np.array([self.uls_res_lateral_load],dtype=float)
        handle["uls_tension"] = np.array([self.uls_tension],dtype=float)
        handle["uls_compression"] = np.array([self.uls_compression],dtype=float)
        ar = []
        ar.append(self.name.encode("ascii"))
        handle["name"] = np.asarray(ar)
        ar = []
        ar.append(self.description.encode("ascii"))
        handle["description"] = np.asarray(ar)
    #-------------------------
    # Load functions
    #-------------------------
    def loadJSON(self,name = None, filePath = None):
        """Load an instance of the object from JSON format file

        Args:
            name (:obj:`str`, optional): Name of the object to load.
            filePath (:obj:`str`, optional): Path of the JSON file to load. If None, the function looks for a file with name "name".json.

        The JSON file must contain a datastructure representing an instance of this object's class, as generated by e.g. the function :func:`~saveJSON`.

        """

        if not(name == None):
            self.name = name
        if (name == None) and not(filePath == None):
            self.name = ".".join(filePath.split(os.path.sep)[-1].split(".")[0:-1])
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".json"
            else:
                raise Exception("object needs name for loading.")
        if not(os.path.isfile(filePath)):
            raise Exception("file %s not found."%filePath)
        self._loadedItems = []
        f = open(filePath,"r")
        data = f.read()
        f.close()
        dd = json.loads(data)
        self.loadFromJSONDict(dd)
    def loadFromJSONDict(self, data):
        """Load an instance of the object from a dictionnary representing a JSON structure)

        Args:
            name (:obj:`collections.OrderedDict`): Dictionnary containing a JSON structure, as produced by e.g. :func:`json.loads`

        """

        varName = "z_ap"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "moor_V_min"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "moor_V_max"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "moor_H_x"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "moor_H_y"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "moor_M_x"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "moor_M_y"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "u_shear_strenght_d"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "int_friction_angle_d"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "n_iterations_max"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "material_sf"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "d_ext"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "w_t"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "plength"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "pile_length_above_seabed"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "total_length"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "Pu"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "user_deflection_crit"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "deflection_crit"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "seabed_connection_type"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "y_max"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "k_pile_soil"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "I"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "soil_type"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "sand_dr"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "z_max"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "lat_cap"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "dist_attachment"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "SF"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "pile_bouyant_weight"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "pile_weight"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "pile_vol"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "pile_section"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "d_steel"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "footprint"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "mid_overburden_pressure"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "g_b"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "sf_tension"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "sf_comp"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "pile_burried_surface"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "pile_surface"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "comp_capacity"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "tension_capacity"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "tip_type"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "Mmax"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "section_stress"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "pile_sec_modulus"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "criteria_lateral_capacity"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "criteria_axial_tension_capacity"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "criteria_axial_compression_capacity"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "criteria_steel_stress"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "S_steel"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "uls_res_lateral_load"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "uls_tension"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "uls_compression"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "description"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
    def loadHDF5(self,name=None, filePath=None):
        """Load an instance of the object from HDF5 format file
        Args:
            name (:obj:`str`, optional): Name of the object to load. Default is None.
            filePath (:obj:`str`, optional): Path of the HDF5 file to load. If None, the function looks for a file with name "self.name".h5.
        The HDF5 file must contain a datastructure representing an instance of this objects class, as generated by e.g. the function :func:`~saveHDF5`.
        """
        # Define filepath and name
        if not(name == None):
            self.name = name
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".h5"
            else:
                raise Exception("name is required for loading")
        # Open hdf5 file
        h = h5py.File(filePath,"r")
        group = h[self.name]
        # Save the object to the group
        self.loadFromHDF5Handle(group)
        # Close file
        h.close()
        pass
    def loadFromHDF5Handle(self, gr):
        """Load the properties of the object from a hdf5 handle.
        Args:
            gr (:obj:`h5py.Group`): Handle used to read the object properties from
        """
        if ("z_ap" in list(gr.keys())):
            self.z_ap = gr["z_ap"][0]
        if ("moor_V_min" in list(gr.keys())):
            self.moor_V_min = gr["moor_V_min"][0]
        if ("moor_V_max" in list(gr.keys())):
            self.moor_V_max = gr["moor_V_max"][0]
        if ("moor_H_x" in list(gr.keys())):
            self.moor_H_x = gr["moor_H_x"][0]
        if ("moor_H_y" in list(gr.keys())):
            self.moor_H_y = gr["moor_H_y"][0]
        if ("moor_M_x" in list(gr.keys())):
            self.moor_M_x = gr["moor_M_x"][0]
        if ("moor_M_y" in list(gr.keys())):
            self.moor_M_y = gr["moor_M_y"][0]
        if ("u_shear_strenght_d" in list(gr.keys())):
            self.u_shear_strenght_d = gr["u_shear_strenght_d"][0]
        if ("int_friction_angle_d" in list(gr.keys())):
            self.int_friction_angle_d = gr["int_friction_angle_d"][0]
        if ("n_iterations_max" in list(gr.keys())):
            self.n_iterations_max = gr["n_iterations_max"][0]
        if ("material_sf" in list(gr.keys())):
            self.material_sf = gr["material_sf"][0]
        if ("d_ext" in list(gr.keys())):
            self.d_ext = gr["d_ext"][0]
        if ("w_t" in list(gr.keys())):
            self.w_t = gr["w_t"][0]
        if ("plength" in list(gr.keys())):
            self.plength = gr["plength"][0]
        if ("pile_length_above_seabed" in list(gr.keys())):
            self.pile_length_above_seabed = gr["pile_length_above_seabed"][0]
        if ("total_length" in list(gr.keys())):
            self.total_length = gr["total_length"][0]
        if ("Pu" in list(gr.keys())):
            self.Pu = gr["Pu"][0]
        if ("user_deflection_crit" in list(gr.keys())):
            self.user_deflection_crit = gr["user_deflection_crit"][0]
        if ("deflection_crit" in list(gr.keys())):
            self.deflection_crit = gr["deflection_crit"][0]
        if ("seabed_connection_type" in list(gr.keys())):
            self.seabed_connection_type = gr["seabed_connection_type"][0].decode("ascii")
        if ("y_max" in list(gr.keys())):
            self.y_max = gr["y_max"][0]
        if ("k_pile_soil" in list(gr.keys())):
            self.k_pile_soil = gr["k_pile_soil"][0]
        if ("I" in list(gr.keys())):
            self.I = gr["I"][0]
        if ("soil_type" in list(gr.keys())):
            self.soil_type = gr["soil_type"][0]
        if ("sand_dr" in list(gr.keys())):
            self.sand_dr = gr["sand_dr"][0]
        if ("z_max" in list(gr.keys())):
            self.z_max = gr["z_max"][0]
        if ("lat_cap" in list(gr.keys())):
            self.lat_cap = gr["lat_cap"][0]
        if ("dist_attachment" in list(gr.keys())):
            self.dist_attachment = gr["dist_attachment"][0]
        if ("SF" in list(gr.keys())):
            self.SF = gr["SF"][0]
        if ("pile_bouyant_weight" in list(gr.keys())):
            self.pile_bouyant_weight = gr["pile_bouyant_weight"][0]
        if ("pile_weight" in list(gr.keys())):
            self.pile_weight = gr["pile_weight"][0]
        if ("pile_vol" in list(gr.keys())):
            self.pile_vol = gr["pile_vol"][0]
        if ("pile_section" in list(gr.keys())):
            self.pile_section = gr["pile_section"][0]
        if ("d_steel" in list(gr.keys())):
            self.d_steel = gr["d_steel"][0]
        if ("footprint" in list(gr.keys())):
            self.footprint = gr["footprint"][0]
        if ("mid_overburden_pressure" in list(gr.keys())):
            self.mid_overburden_pressure = gr["mid_overburden_pressure"][0]
        if ("g_b" in list(gr.keys())):
            self.g_b = gr["g_b"][0]
        if ("sf_tension" in list(gr.keys())):
            self.sf_tension = gr["sf_tension"][0]
        if ("sf_comp" in list(gr.keys())):
            self.sf_comp = gr["sf_comp"][0]
        if ("pile_burried_surface" in list(gr.keys())):
            self.pile_burried_surface = gr["pile_burried_surface"][0]
        if ("pile_surface" in list(gr.keys())):
            self.pile_surface = gr["pile_surface"][0]
        if ("comp_capacity" in list(gr.keys())):
            self.comp_capacity = gr["comp_capacity"][0]
        if ("tension_capacity" in list(gr.keys())):
            self.tension_capacity = gr["tension_capacity"][0]
        if ("tip_type" in list(gr.keys())):
            self.tip_type = gr["tip_type"][0].decode("ascii")
        if ("Mmax" in list(gr.keys())):
            self.Mmax = gr["Mmax"][0]
        if ("section_stress" in list(gr.keys())):
            self.section_stress = gr["section_stress"][0]
        if ("pile_sec_modulus" in list(gr.keys())):
            self.pile_sec_modulus = gr["pile_sec_modulus"][0]
        if ("criteria_lateral_capacity" in list(gr.keys())):
            self.criteria_lateral_capacity = gr["criteria_lateral_capacity"][0]
        if ("criteria_axial_tension_capacity" in list(gr.keys())):
            self.criteria_axial_tension_capacity = gr["criteria_axial_tension_capacity"][0]
        if ("criteria_axial_compression_capacity" in list(gr.keys())):
            self.criteria_axial_compression_capacity = gr["criteria_axial_compression_capacity"][0]
        if ("criteria_steel_stress" in list(gr.keys())):
            self.criteria_steel_stress = gr["criteria_steel_stress"][0]
        if ("S_steel" in list(gr.keys())):
            self.S_steel = gr["S_steel"][0]
        if ("uls_res_lateral_load" in list(gr.keys())):
            self.uls_res_lateral_load = gr["uls_res_lateral_load"][0]
        if ("uls_tension" in list(gr.keys())):
            self.uls_tension = gr["uls_tension"][0]
        if ("uls_compression" in list(gr.keys())):
            self.uls_compression = gr["uls_compression"][0]
        if ("name" in list(gr.keys())):
            self.name = gr["name"][0].decode("ascii")
        if ("description" in list(gr.keys())):
            self.description = gr["description"][0].decode("ascii")
    #------------------------
    # is_set function
    #------------------------
    def is_set(self, varName): # pragma: no cover

        """Check if a given property of the object is set

        Args:
            varName (:obj:`str`): name of the property to check

        Returns:
            :obj:`bool`: True if the property is set, else False
        """

        if (isinstance(getattr(self,varName),list) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (isinstance(getattr(self,varName),np.ndarray) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (getattr(self,varName) != None):
            return True
        return False
    #------------------------
