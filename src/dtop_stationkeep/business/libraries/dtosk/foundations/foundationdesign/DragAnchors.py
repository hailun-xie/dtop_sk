# This is the Station Keeping module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Neil Luxcey, Nicolas Michelet, Rocio Isorna
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import collections
import numpy as np
import os
import json
import h5py
#------------------------------------
# @ USER DEFINED IMPORTS START
import pandas as pd
import math
import logging
logger = logging.getLogger() # grabs root logger
pd.options.display.float_format = '{:,.4g}'.format
from ...catalogue.AnchorCatalogue import AnchorCatalogue
# @ USER DEFINED IMPORTS END
#------------------------------------

class DragAnchors():

    """data model for input data needed to perform the design of a Drag anchor
    """

    #------------
    # Constructor
    #------------
    def __init__(self,name=None):
#------------------------------------
# @ USER DEFINED DESCRIPTION START
# @ USER DEFINED DESCRIPTION END
#------------------------------------

        self._z_ap=0.0
        self._moor_V_min=0.0
        self._moor_V_max=0.0
        self._moor_H_x=0.0
        self._moor_H_y=0.0
        self._moor_M_x=0.0
        self._moor_M_y=0.0
        self._anchor_id=''
        self._soil_type=''
        self._u_shear_strenght_d=0.0
        self._int_friction_angle_d=0.0
        self._h_max=0.0
        self._uhc=0.0
        self._footprint=0.0
        self._weight=0.0
        self._dim_a=0.0
        self._dim_b=0.0
        self._dim_ef=0.0
        self._criteria_uhc=False
        self._effective_uhc=0.0
        self._SF=1.8
        self._name='none'
        self._description=''
        if not(name == None): # pragma: no cover
            self._name = name

#------------------------------------
# @ USER DEFINED PROPERTIES START
        #Definition of soil properties for design using SF from DNVJOS101

        self.loads_from_mooring=np.zeros(shape=(1), dtype=float)
        self.drag_cat= AnchorCatalogue()

        self.m_anchor = 0
        self.b_anchor = 0
        self.m_A = 0
        self.m_B = 0
        self.m_EF = 0
        self.b_A = 0
        self.b_B = 0
        self.b_EF = 0
        self.seabed_connection_type=''

# @ USER DEFINED PROPERTIES END
#------------------------------------

#------------------------------------
# @ USER DEFINED METHODS START

    def set_catalogue_anchors(self,catalogue_anchors):

        self.drag_cat = catalogue_anchors

    pass

    def desing_loads_from_mooring (self):

        self.moor_V_min = self.loads_from_mooring[0]
        self.moor_V_max = self.loads_from_mooring[1]
        self.moor_H_x = self.loads_from_mooring[2]
        self.moor_H_y = self.loads_from_mooring[3]
        self.moor_M_x = self.loads_from_mooring[4]
        self.moor_M_y = self.loads_from_mooring[5]
        self.z_ap = self.loads_from_mooring[6]
        self.h_max = (np.sqrt((self.moor_H_x ** 2) + (self.moor_H_y ** 2)))
        self.uhc = self.SF * self.h_max

        return self.moor_H_x, self.moor_H_y, self.h_max

    def manual_def_soil_type_and_anchor(self, catalogue_index):
        if self.int_friction_angle_d != 0:
            if self.int_friction_angle_d >= 30:
                self.soil_type='hard'
                self.m_anchor = self.drag_cat.m_stiff_soil_sand[catalogue_index]
                self.b_anchor = self.drag_cat.b_stiff_soil_sand[catalogue_index]

            elif ((25 < self.int_friction_angle_d) and (self.int_friction_angle_d <= 30)):
                self.soil_type='mid'
                self.m_anchor = self.drag_cat.m_mid_soil[catalogue_index]
                self.b_anchor = self.drag_cat.b_mid_soil[catalogue_index]

            elif (self.int_friction_angle_d <= 25 and self.u_shear_strenght_d <= 20e+03):
                self.soil_type='soft'
                self.m_anchor = self.drag_cat.m_soft_soil[catalogue_index]
                self.b_anchor = self.drag_cat.b_soft_soil[catalogue_index]

        if self.u_shear_strenght_d !=0:
            if self.u_shear_strenght_d > 75e+03:
                self.soil_type='hard'
                self.m_anchor = self.drag_cat.m_stiff_soil_sand[catalogue_index]
                self.b_anchor = self.drag_cat.b_stiff_soil_sand[catalogue_index]

            elif ((20e+03 < self.u_shear_strenght_d) and (self.u_shear_strenght_d <= 75e+03)):
                self.soil_type='mid'
                self.m_anchor = self.drag_cat.m_mid_soil[catalogue_index]
                self.b_anchor = self.drag_cat.b_mid_soil[catalogue_index]


            elif  self.u_shear_strenght_d <= 20e+03:
                self.soil_type='soft'
                self.m_anchor = self.drag_cat.m_soft_soil[catalogue_index]
                self.b_anchor = self.drag_cat.b_soft_soil[catalogue_index]

        self.anchor_id = self.drag_cat.catalogue_id[catalogue_index]
        self.m_A = self.drag_cat.m_A[catalogue_index]
        self.b_A = self.drag_cat.b_A[catalogue_index]
        self.m_B = self.drag_cat.m_B[catalogue_index]
        self.b_B = self.drag_cat.b_B[catalogue_index]
        self.m_EF = self.drag_cat.m_EF[catalogue_index]
        self.b_EF = self.drag_cat.b_EF[catalogue_index]

    def def_soil_type_and_anchor(self):

        if self.int_friction_angle_d != 0:
            if self.int_friction_angle_d >= 30:
                self.soil_type='hard'
                self.index = self.drag_cat.Rel_stiff_soil.index(min(self.drag_cat.Rel_stiff_soil))
                self.anchor_id = self.drag_cat.catalogue_id[self.index]
                self.m_anchor = self.drag_cat.m_stiff_soil_sand[self.index]
                self.b_anchor = self.drag_cat.b_stiff_soil_sand[self.index]

            elif ((25 < self.int_friction_angle_d) and (self.int_friction_angle_d <= 30)):
                self.soil_type='mid'
                self.index = self.drag_cat.Rel_mid_soil.index(min(self.drag_cat.Rel_mid_soil))
                self.m_anchor = self.drag_cat.m_mid_soil[self.index]
                self.b_anchor = self.drag_cat.b_mid_soil[self.index]

            elif (self.int_friction_angle_d <= 25 and self.u_shear_strenght_d <= 20e+03):
                self.soil_type='soft'
                self.index = self.drag_cat.Rel_soft_soil.index(min(self.drag_cat.Rel_soft_soil))
                self.m_anchor = self.drag_cat.m_soft_soil[self.index]
                self.b_anchor = self.drag_cat.b_soft_soil[self.index]

        if self.u_shear_strenght_d !=0:
            if self.u_shear_strenght_d > 75e+03:
                self.soil_type='hard'
                self.index = self.drag_cat.Rel_stiff_soil.index(min(self.drag_cat.Rel_stiff_soil))
                self.anchor_id = self.drag_cat.catalogue_id[self.index]
                self.m_anchor = self.drag_cat.m_stiff_soil_sand[self.index]
                self.b_anchor = self.drag_cat.b_stiff_soil_sand[self.index]

            elif ((20e+03 < self.u_shear_strenght_d) and (self.u_shear_strenght_d <= 75e+03)):
                self.soil_type='mid'
                self.index = self.drag_cat.Rel_mid_soil.index(min(self.drag_cat.Rel_mid_soil))
                self.m_anchor = self.drag_cat.m_mid_soil[self.index]
                self.b_anchor = self.drag_cat.b_mid_soil[self.index]


            elif  self.u_shear_strenght_d <= 20e+03:
                self.soil_type='soft'
                self.index = self.drag_cat.Rel_soft_soil.index(min(self.drag_cat.Rel_soft_soil))
                self.m_anchor = self.drag_cat.m_soft_soil[self.index]
                self.b_anchor = self.drag_cat.b_soft_soil[self.index]

        self.anchor_id = self.drag_cat.catalogue_id[self.index]
        self.m_A = self.drag_cat.m_A[self.index]
        self.b_A = self.drag_cat.b_A[self.index]
        self.m_B = self.drag_cat.m_B[self.index]
        self.b_B = self.drag_cat.b_B[self.index]
        self.m_EF = self.drag_cat.m_EF[self.index]
        self.b_EF = self.drag_cat.b_EF[self.index]


    def assess(self,catalogue_index = None):

        logger.info('-------------------------')
        logger.info('Assessment of drag anchor')
        logger.info('-------------------------')

        if not (catalogue_index is None):
            self.manual_def_soil_type_and_anchor(catalogue_index)

        logger.info('Drag anchor design coefficients:')
        logger.info('m = ' + str(self.m_anchor))
        logger.info('b = ' + str(self.b_anchor))
        logger.info('m_A = ' + str(self.m_A))
        logger.info('b_A = ' + str(self.b_A))
        logger.info('m_B = ' + str(self.m_B))
        logger.info('b_B = ' + str(self.b_B))
        logger.info('m_EF = ' + str(self.m_EF))
        logger.info('b_EF = ' + str(self.b_EF))
        logger.info('id = ' + str(self.anchor_id))

        self.desing_loads_from_mooring()
        self.effective_uhc = math.ceil(self.m_anchor * (self.weight)**(self.b_anchor))
        if self.effective_uhc >= self.uhc:
            self.criteria_uhc = True
        else:
            self.criteria_uhc = False
        self.dim_a = self.m_A * (self.weight)**(self.b_A)
        self.dim_b = self.m_B * (self.weight)**(self.b_B)
        self.dim_ef = self.m_EF * (self.weight)**(self.b_EF)

        self.footprint = self.dim_a * self.dim_b

        logger.info('Final ultimate holding capacity: ' + str(self.effective_uhc) + str(' vs ') + str(self.uhc))

        logger.info('Final design:')
        logger.info('Length - A [m]: ' + str(self.dim_a))
        logger.info('Width - B [m]: ' + str(self.dim_b))
        logger.info('Height - EF [m]: ' + str(self.dim_ef))
        logger.info('Mass [kg]: ' + str(self.weight))




    def design(self, foundation_type_selection, catalogue_index):

        logger.info('---------------------')
        logger.info('Design of drag anchor')
        logger.info('---------------------')

        logger.info('Applied load Vmin, Vmax : ' + str(self.loads_from_mooring[0:2]))
        logger.info('Applied load Hx, Hy : ' + str(self.loads_from_mooring[2:4]))
        logger.info('Applied load Mx, My : ' + str(self.loads_from_mooring[4:6]))
        logger.info('Applied load vertical position zap : ' + str(self.loads_from_mooring[6]))

        self.desing_loads_from_mooring()

        if foundation_type_selection == 'auto':
            self.def_soil_type_and_anchor()
        else:
            self.manual_def_soil_type_and_anchor(catalogue_index)

        self.weight = (self.uhc/self.m_anchor)**(1 / self.b_anchor) # This is the mass, not the weight! (ref. deliverable D5.6)
        self.dim_a = self.m_A * (self.weight)**(self.b_A)
        self.dim_b = self.m_B * (self.weight)**(self.b_B)
        self.dim_ef = self.m_EF * (self.weight)**(self.b_EF)

        self.footprint = self.dim_a * self.dim_b

        self.assess()


    def outputs(self):

        bom_esa = pd.DataFrame ({"Materials": ["steel"],
                             "Weight [kg]": [self.weight],
                             "Footprint [m2]": [self.footprint]})

        anchor_properties = pd.DataFrame ({"Type_of_anchor": [self.anchor_id],
                                         "Weight [kg]": [self.weight],
                                         "UHC [N]": [self.uhc],
                                         "Length - A [m]": [self.dim_a],
                                         "Width - B [m]": [self.dim_b],
                                         "Height - EF [m]": [self.dim_ef]})

        # anchor_properties.to_csv()

        # bom_esa.to_csv()

        return anchor_properties, bom_esa












# @ USER DEFINED METHODS END
#------------------------------------

    #------------
    # Get functions
    #------------
    @ property
    def z_ap(self): # pragma: no cover
        """float: high of application point of loads []
        """
        return self._z_ap
    #------------
    @ property
    def moor_V_min(self): # pragma: no cover
        """float: maximal moroing vertical upward load transfert to foundation []
        """
        return self._moor_V_min
    #------------
    @ property
    def moor_V_max(self): # pragma: no cover
        """float: maximal mooring vertical downard load transfert to foundation []
        """
        return self._moor_V_max
    #------------
    @ property
    def moor_H_x(self): # pragma: no cover
        """float: maximal mooring horizontal x load transfert to foundation []
        """
        return self._moor_H_x
    #------------
    @ property
    def moor_H_y(self): # pragma: no cover
        """float: maximal mooring horizontal y load transfert to foundation []
        """
        return self._moor_H_y
    #------------
    @ property
    def moor_M_x(self): # pragma: no cover
        """float: maximal mooring horizontal x load transfert to foundation []
        """
        return self._moor_M_x
    #------------
    @ property
    def moor_M_y(self): # pragma: no cover
        """float: maximal mooring horizontal y load transfert to foundation []
        """
        return self._moor_M_y
    #------------
    @ property
    def anchor_id(self): # pragma: no cover
        """str: catalogue
        """
        return self._anchor_id
    #------------
    @ property
    def soil_type(self): # pragma: no cover
        """str: soil type for anchor design
        """
        return self._soil_type
    #------------
    @ property
    def u_shear_strenght_d(self): # pragma: no cover
        """float: undrained shear strenght  []
        """
        return self._u_shear_strenght_d
    #------------
    @ property
    def int_friction_angle_d(self): # pragma: no cover
        """float: angle de frottement interne du soil in °  []
        """
        return self._int_friction_angle_d
    #------------
    @ property
    def h_max(self): # pragma: no cover
        """float: resultant horizontal load []
        """
        return self._h_max
    #------------
    @ property
    def uhc(self): # pragma: no cover
        """float: ultimate holding capacity target []
        """
        return self._uhc
    #------------
    @ property
    def footprint(self): # pragma: no cover
        """float: anchor surface []
        """
        return self._footprint
    #------------
    @ property
    def weight(self): # pragma: no cover
        """float: anchor weight []
        """
        return self._weight
    #------------
    @ property
    def dim_a(self): # pragma: no cover
        """float: length a []
        """
        return self._dim_a
    #------------
    @ property
    def dim_b(self): # pragma: no cover
        """float: width b []
        """
        return self._dim_b
    #------------
    @ property
    def dim_ef(self): # pragma: no cover
        """float: height ef []
        """
        return self._dim_ef
    #------------
    @ property
    def criteria_uhc(self): # pragma: no cover
        """bool: Ultimate holding capacity criterion. True or False. []
        """
        return self._criteria_uhc
    #------------
    @ property
    def effective_uhc(self): # pragma: no cover
        """float: Ultimate holding capacity value []
        """
        return self._effective_uhc
    #------------
    @ property
    def SF(self): # pragma: no cover
        """float: Ultimate holding capacity safety factor []
        """
        return self._SF
    #------------
    @ property
    def name(self): # pragma: no cover
        """str: name of the instance object
        """
        return self._name
    #------------
    @ property
    def description(self): # pragma: no cover
        """str: description of the instance object
        """
        return self._description
    #------------
    #------------
    # Set functions
    #------------
    @ z_ap.setter
    def z_ap(self,val): # pragma: no cover
        self._z_ap=float(val)
    #------------
    @ moor_V_min.setter
    def moor_V_min(self,val): # pragma: no cover
        self._moor_V_min=float(val)
    #------------
    @ moor_V_max.setter
    def moor_V_max(self,val): # pragma: no cover
        self._moor_V_max=float(val)
    #------------
    @ moor_H_x.setter
    def moor_H_x(self,val): # pragma: no cover
        self._moor_H_x=float(val)
    #------------
    @ moor_H_y.setter
    def moor_H_y(self,val): # pragma: no cover
        self._moor_H_y=float(val)
    #------------
    @ moor_M_x.setter
    def moor_M_x(self,val): # pragma: no cover
        self._moor_M_x=float(val)
    #------------
    @ moor_M_y.setter
    def moor_M_y(self,val): # pragma: no cover
        self._moor_M_y=float(val)
    #------------
    @ anchor_id.setter
    def anchor_id(self,val): # pragma: no cover
        self._anchor_id=str(val)
    #------------
    @ soil_type.setter
    def soil_type(self,val): # pragma: no cover
        self._soil_type=str(val)
    #------------
    @ u_shear_strenght_d.setter
    def u_shear_strenght_d(self,val): # pragma: no cover
        self._u_shear_strenght_d=float(val)
    #------------
    @ int_friction_angle_d.setter
    def int_friction_angle_d(self,val): # pragma: no cover
        self._int_friction_angle_d=float(val)
    #------------
    @ h_max.setter
    def h_max(self,val): # pragma: no cover
        self._h_max=float(val)
    #------------
    @ uhc.setter
    def uhc(self,val): # pragma: no cover
        self._uhc=float(val)
    #------------
    @ footprint.setter
    def footprint(self,val): # pragma: no cover
        self._footprint=float(val)
    #------------
    @ weight.setter
    def weight(self,val): # pragma: no cover
        self._weight=float(val)
    #------------
    @ dim_a.setter
    def dim_a(self,val): # pragma: no cover
        self._dim_a=float(val)
    #------------
    @ dim_b.setter
    def dim_b(self,val): # pragma: no cover
        self._dim_b=float(val)
    #------------
    @ dim_ef.setter
    def dim_ef(self,val): # pragma: no cover
        self._dim_ef=float(val)
    #------------
    @ criteria_uhc.setter
    def criteria_uhc(self,val): # pragma: no cover
        self._criteria_uhc=val
    #------------
    @ effective_uhc.setter
    def effective_uhc(self,val): # pragma: no cover
        self._effective_uhc=float(val)
    #------------
    @ SF.setter
    def SF(self,val): # pragma: no cover
        self._SF=float(val)
    #------------
    @ name.setter
    def name(self,val): # pragma: no cover
        self._name=str(val)
    #------------
    @ description.setter
    def description(self,val): # pragma: no cover
        self._description=str(val)
    #------------
    #-------------------------
    # Representation functions
    #-------------------------
    def type_rep(self): # pragma: no cover
        """Generate a representation of the object type

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains the representation of the object type
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtosk:foundations:foundationdesign:DragAnchors"
        rep["name"] = self.name
        rep["description"] = self.description
        return rep
    def prop_rep(self, short = False, deep = True):

        """Generate a representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains a representation of the object properties
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtosk:foundations:foundationdesign:DragAnchors"
        rep["name"] = self.name
        rep["description"] = self.description
        if self.is_set("z_ap"):
            rep["z_ap"] = self.z_ap
        if self.is_set("moor_V_min"):
            rep["moor_V_min"] = self.moor_V_min
        if self.is_set("moor_V_max"):
            rep["moor_V_max"] = self.moor_V_max
        if self.is_set("moor_H_x"):
            rep["moor_H_x"] = self.moor_H_x
        if self.is_set("moor_H_y"):
            rep["moor_H_y"] = self.moor_H_y
        if self.is_set("moor_M_x"):
            rep["moor_M_x"] = self.moor_M_x
        if self.is_set("moor_M_y"):
            rep["moor_M_y"] = self.moor_M_y
        if self.is_set("anchor_id"):
            rep["anchor_id"] = self.anchor_id
        if self.is_set("soil_type"):
            rep["soil_type"] = self.soil_type
        if self.is_set("u_shear_strenght_d"):
            rep["u_shear_strenght_d"] = self.u_shear_strenght_d
        if self.is_set("int_friction_angle_d"):
            rep["int_friction_angle_d"] = self.int_friction_angle_d
        if self.is_set("h_max"):
            rep["h_max"] = self.h_max
        if self.is_set("uhc"):
            rep["uhc"] = self.uhc
        if self.is_set("footprint"):
            rep["footprint"] = self.footprint
        if self.is_set("weight"):
            rep["weight"] = self.weight
        if self.is_set("dim_a"):
            rep["dim_a"] = self.dim_a
        if self.is_set("dim_b"):
            rep["dim_b"] = self.dim_b
        if self.is_set("dim_ef"):
            rep["dim_ef"] = self.dim_ef
        if self.is_set("criteria_uhc"):
            rep["criteria_uhc"] = self.criteria_uhc
        if self.is_set("effective_uhc"):
            rep["effective_uhc"] = self.effective_uhc
        if self.is_set("SF"):
            rep["SF"] = self.SF
        if self.is_set("name"):
            rep["name"] = self.name
        if self.is_set("description"):
            rep["description"] = self.description
        return rep
    #-------------------------
    # Save functions
    #-------------------------
    def json_rep(self, short=False, deep=True):

        """Generate a JSON representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`str`: string that contains a JSON representation of the object properties
        """

        return ( json.dumps(self.prop_rep(short=short, deep=deep),indent=4, separators=(",",": ")))
    def saveJSON(self, fileName=None):
        """Save the instance of the object to JSON format file

        Args:
            fileName (:obj:`str`, optional): Name of the JSON file, included extension. Defaults is None. If None, the name of the JSON file will be self.name.json. It can also contain an absolute or relative path.

        """

        if fileName==None:
            fileName=self.name + ".json"
        f=open(fileName, "w")
        f.write(self.json_rep())
        f.write("\n")
        f.close()

    def saveHDF5(self,filePath=None):
        """Save the instance of the object to HDF5 format file
        Args:
            filePath (:obj:`str`, optional): Name of the HDF5 file, included extension. Defaults is None. If None, the name of the HDF5 file will be "self.name".h5. It can also contain an absolute or relative path.
        """
        # Define filepath
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".h5"
            else:
                raise Exception("name is required for saving")
        # Open hdf5 file
        h = h5py.File(filePath,"w")
        group = h.create_group(self.name)
        # Save the object to the group
        self.saveToHDF5Handle(group)
        # Close file
        h.close()
        pass
    def saveToHDF5Handle(self, handle):
        """Save the properties of the object to the hdf5 handle.
        Args:
            handle (:obj:`h5py.Group`): Handle used to store the object properties
        """
        handle["z_ap"] = np.array([self.z_ap],dtype=float)
        handle["moor_V_min"] = np.array([self.moor_V_min],dtype=float)
        handle["moor_V_max"] = np.array([self.moor_V_max],dtype=float)
        handle["moor_H_x"] = np.array([self.moor_H_x],dtype=float)
        handle["moor_H_y"] = np.array([self.moor_H_y],dtype=float)
        handle["moor_M_x"] = np.array([self.moor_M_x],dtype=float)
        handle["moor_M_y"] = np.array([self.moor_M_y],dtype=float)
        ar = []
        ar.append(self.anchor_id.encode("ascii"))
        handle["anchor_id"] = np.asarray(ar)
        ar = []
        ar.append(self.soil_type.encode("ascii"))
        handle["soil_type"] = np.asarray(ar)
        handle["u_shear_strenght_d"] = np.array([self.u_shear_strenght_d],dtype=float)
        handle["int_friction_angle_d"] = np.array([self.int_friction_angle_d],dtype=float)
        handle["h_max"] = np.array([self.h_max],dtype=float)
        handle["uhc"] = np.array([self.uhc],dtype=float)
        handle["footprint"] = np.array([self.footprint],dtype=float)
        handle["weight"] = np.array([self.weight],dtype=float)
        handle["dim_a"] = np.array([self.dim_a],dtype=float)
        handle["dim_b"] = np.array([self.dim_b],dtype=float)
        handle["dim_ef"] = np.array([self.dim_ef],dtype=float)
        handle["criteria_uhc"] = np.array([self.criteria_uhc],dtype=bool)
        handle["effective_uhc"] = np.array([self.effective_uhc],dtype=float)
        handle["SF"] = np.array([self.SF],dtype=float)
        ar = []
        ar.append(self.name.encode("ascii"))
        handle["name"] = np.asarray(ar)
        ar = []
        ar.append(self.description.encode("ascii"))
        handle["description"] = np.asarray(ar)
    #-------------------------
    # Load functions
    #-------------------------
    def loadJSON(self,name = None, filePath = None):
        """Load an instance of the object from JSON format file

        Args:
            name (:obj:`str`, optional): Name of the object to load.
            filePath (:obj:`str`, optional): Path of the JSON file to load. If None, the function looks for a file with name "name".json.

        The JSON file must contain a datastructure representing an instance of this object's class, as generated by e.g. the function :func:`~saveJSON`.

        """

        if not(name == None):
            self.name = name
        if (name == None) and not(filePath == None):
            self.name = ".".join(filePath.split(os.path.sep)[-1].split(".")[0:-1])
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".json"
            else:
                raise Exception("object needs name for loading.")
        if not(os.path.isfile(filePath)):
            raise Exception("file %s not found."%filePath)
        self._loadedItems = []
        f = open(filePath,"r")
        data = f.read()
        f.close()
        dd = json.loads(data)
        self.loadFromJSONDict(dd)
    def loadFromJSONDict(self, data):
        """Load an instance of the object from a dictionnary representing a JSON structure)

        Args:
            name (:obj:`collections.OrderedDict`): Dictionnary containing a JSON structure, as produced by e.g. :func:`json.loads`

        """

        varName = "z_ap"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "moor_V_min"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "moor_V_max"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "moor_H_x"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "moor_H_y"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "moor_M_x"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "moor_M_y"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "anchor_id"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "soil_type"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "u_shear_strenght_d"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "int_friction_angle_d"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "h_max"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "uhc"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "footprint"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "weight"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "dim_a"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "dim_b"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "dim_ef"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "criteria_uhc"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "effective_uhc"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "SF"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "description"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
    def loadHDF5(self,name=None, filePath=None):
        """Load an instance of the object from HDF5 format file
        Args:
            name (:obj:`str`, optional): Name of the object to load. Default is None.
            filePath (:obj:`str`, optional): Path of the HDF5 file to load. If None, the function looks for a file with name "self.name".h5.
        The HDF5 file must contain a datastructure representing an instance of this objects class, as generated by e.g. the function :func:`~saveHDF5`.
        """
        # Define filepath and name
        if not(name == None):
            self.name = name
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".h5"
            else:
                raise Exception("name is required for loading")
        # Open hdf5 file
        h = h5py.File(filePath,"r")
        group = h[self.name]
        # Save the object to the group
        self.loadFromHDF5Handle(group)
        # Close file
        h.close()
        pass
    def loadFromHDF5Handle(self, gr):
        """Load the properties of the object from a hdf5 handle.
        Args:
            gr (:obj:`h5py.Group`): Handle used to read the object properties from
        """
        if ("z_ap" in list(gr.keys())):
            self.z_ap = gr["z_ap"][0]
        if ("moor_V_min" in list(gr.keys())):
            self.moor_V_min = gr["moor_V_min"][0]
        if ("moor_V_max" in list(gr.keys())):
            self.moor_V_max = gr["moor_V_max"][0]
        if ("moor_H_x" in list(gr.keys())):
            self.moor_H_x = gr["moor_H_x"][0]
        if ("moor_H_y" in list(gr.keys())):
            self.moor_H_y = gr["moor_H_y"][0]
        if ("moor_M_x" in list(gr.keys())):
            self.moor_M_x = gr["moor_M_x"][0]
        if ("moor_M_y" in list(gr.keys())):
            self.moor_M_y = gr["moor_M_y"][0]
        if ("anchor_id" in list(gr.keys())):
            self.anchor_id = gr["anchor_id"][0].decode("ascii")
        if ("soil_type" in list(gr.keys())):
            self.soil_type = gr["soil_type"][0].decode("ascii")
        if ("u_shear_strenght_d" in list(gr.keys())):
            self.u_shear_strenght_d = gr["u_shear_strenght_d"][0]
        if ("int_friction_angle_d" in list(gr.keys())):
            self.int_friction_angle_d = gr["int_friction_angle_d"][0]
        if ("h_max" in list(gr.keys())):
            self.h_max = gr["h_max"][0]
        if ("uhc" in list(gr.keys())):
            self.uhc = gr["uhc"][0]
        if ("footprint" in list(gr.keys())):
            self.footprint = gr["footprint"][0]
        if ("weight" in list(gr.keys())):
            self.weight = gr["weight"][0]
        if ("dim_a" in list(gr.keys())):
            self.dim_a = gr["dim_a"][0]
        if ("dim_b" in list(gr.keys())):
            self.dim_b = gr["dim_b"][0]
        if ("dim_ef" in list(gr.keys())):
            self.dim_ef = gr["dim_ef"][0]
        if ("criteria_uhc" in list(gr.keys())):
            self.criteria_uhc = gr["criteria_uhc"][0]
        if ("effective_uhc" in list(gr.keys())):
            self.effective_uhc = gr["effective_uhc"][0]
        if ("SF" in list(gr.keys())):
            self.SF = gr["SF"][0]
        if ("name" in list(gr.keys())):
            self.name = gr["name"][0].decode("ascii")
        if ("description" in list(gr.keys())):
            self.description = gr["description"][0].decode("ascii")
    #------------------------
    # is_set function
    #------------------------
    def is_set(self, varName): # pragma: no cover

        """Check if a given property of the object is set

        Args:
            varName (:obj:`str`): name of the property to check

        Returns:
            :obj:`bool`: True if the property is set, else False
        """

        if (isinstance(getattr(self,varName),list) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (isinstance(getattr(self,varName),np.ndarray) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (getattr(self,varName) != None):
            return True
        return False
    #------------------------
