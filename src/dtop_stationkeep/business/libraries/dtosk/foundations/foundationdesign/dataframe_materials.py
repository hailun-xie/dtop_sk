# This is the Station Keeping module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Neil Luxcey, Nicolas Michelet, Rocio Isorna
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import pandas as pd
import math
import numpy as np



df1 = pd.DataFrame ({ "Materials" : [ "ABS", "unalloyed_steel", "low_unalloyed_steel", "aluminum","concrete", "Brick", "copper", "reinforcing_steel", "fiberglass", "Synthetic_fiber", "cast_iron",
                                   "gravel", "oil", "brass", "Magnesium", "kraft_paper", "alkyd_paint", "lead", "Polybutadiene", "Polyethylene", "Polypropylene", "PVC", "epoxy_resin",
                                   "Sand", "Ceramic", "Zinc"],
                             "GWP_fabrication" : [ 4500, 2215, 3187, 17090.88, 426.3, 250, 6200, 2700, 2900, 6533,
                                                   3056.5, 5.5, 1380, 5700, 81800, 1100, 6000, 630, 3900, 2000,
                                                   2000, 2100, 6700, 4.6, 1100, 5800],
                             "GWP_treatment" : [0, -711.6, -966.6, -5709.1, 0, 0, -3200, 0, 0, -1743.6,
                                               -443.5, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                                0, 0, 0, 0, 0, 0],
                             "CED_fabrication " : [98600, 24209, 35777, 183551, 2982.1, 2700, 81500, 27100, 47100, 105733,
                                                   33242.5, 59, 86600, 77600, 289800, 18200, 76600, 9300, 98900, 76400,
                                                   74600, 51400, 133500, 61, 15900, 65500],
                             "CED_treatment ": [0, -4390.9, -7323.4, -56749.2, 0, 0, -41100, 0, 0, -8970,
                                                -6557.5, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                                0, 0, 0, 0, 0, 0],
                             "Density": [0, 7850, 0, 0, 2400, 0, 0, 7850, 0, 0, 7300, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]})

df2 = df1.set_index("Materials")

cooper_information = df2.loc["copper",:]
cooper_GWP_fabrication = df2.loc["copper", "GWP_fabrication"]

df1.to_csv(r'materials.csv')

data = pd.read_csv('materials.csv', index_col = "Materials", squeeze= True)

material = data.loc["concrete", "Density"]

#data.head()
#loc["concrete", "Density"]


print(df1)
print(df2)
print(cooper_information)
print(cooper_GWP_fabrication)
print(material)

###Biblio###
# concrete: wikipedia
# iron_cast: https://www.engineeringtoolbox.com/metal-alloys-densities-d_50.html
# reinforced_concrete: 
#