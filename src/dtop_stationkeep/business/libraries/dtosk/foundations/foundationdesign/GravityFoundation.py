# This is the Station Keeping module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Neil Luxcey, Nicolas Michelet, Rocio Isorna
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import collections
import numpy as np
import os
import json
import h5py
#------------------------------------
# @ USER DEFINED IMPORTS START
import math
import numpy as np
import pandas as pd
pd.options.display.float_format = '{:,.2g}'.format
import matplotlib.pyplot as plt
import logging
logger = logging.getLogger() # grabs root logger
# @ USER DEFINED IMPORTS END
#------------------------------------

class GravityFoundation():

    """data model for input data needed to perform the gravity foundation design
    """

    #------------
    # Constructor
    #------------
    def __init__(self,name=None):
#------------------------------------
# @ USER DEFINED DESCRIPTION START
# @ USER DEFINED DESCRIPTION END
#------------------------------------

        self._base_lx=0.0
        self._base_ly=0.0
        self._base_radius=0.0
        self._base_r1=0.0
        self._base_r2=0.0
        self._base_r3=0.0
        self._base_alpha01=0.0
        self._base_alpha12=0.0
        self._base_alpha23=0.0
        self._contact_points_mass=0.0
        self._device_radius=0.0
        self._u_shear_strenght=0.0
        self._bearing_capacity=0.0
        self._friction_resistance=0.0
        self._n_iterations_max=500
        self._int_friction_angle_d=0.0
        self._u_shear_strenght_d=0.0
        self._overturning_resitance_y=0.0
        self._overturning_resitance_x=0.0
        self._geometry_pref=''
        self._ecc_x=0.0
        self._ecc_y=0.0
        self._ecc_r=0.0
        self._eff_area=0.0
        self._material=''
        self._theta=0.0
        self._eff_lx=0.0
        self._eff_ly=0.0
        self._thickness=0.3
        self._material_density=0.0
        self._qc=0.0
        self._qq=0.0
        self._qg=0.0
        self._g_b=0.0
        self._sliding_resistance=0.0
        self._slope=0.0
        self._found_weight=0.0
        self._f_soil_surf=0.0
        self._V_min_load=0.0
        self._device_weight=0.0
        self._thickness_max=0.0
        self._Fn_bc=0.0
        self._Fn_sr=0.0
        self._Fh_sr=0.0
        self._SF=1
        self._stabm_r=0.0
        self._stabm_x=0.0
        self._stabm_y=0.0
        self._Om_r=0.0
        self._Om_x=0.0
        self._Om_y=0.0
        self._z_ap=0.0
        self._moor_V_min=0.0
        self._moor_V_max=0.0
        self._moor_H_x=0.0
        self._moor_H_y=0.0
        self._moor_M_x=0.0
        self._moor_M_y=0.0
        self._seabed_connection_type=''
        self._criteria_bearing_capacity=False
        self._criteria_stability=False
        self._criteria_sliding_resistance=False
        self._name='none'
        self._description=''
        if not(name == None): # pragma: no cover
            self._name = name

#------------------------------------
# @ USER DEFINED PROPERTIES START
        self.loads_from_mooring = np.array([])
        self.sand_dr =0
        self.water_density = 1025
        self.g= 9.81
        self.h_res=0
        self.found_bouy_weight = 0
        self.int_friction_angle_d_rad=0.0
        self.a = 0
        self.bearing_capacity = 1
        self.sliding_resistance = 1
        self.stabm_r = 0
        self.stabm_x = 0
        self.stabm_y = 0
        self.device_geometry={}
        self.material = 'concrete'
        self.material_density = 2400
        self.soil = ''
        self.convergence = pd.DataFrame()
# @ USER DEFINED PROPERTIES END
#------------------------------------

#------------------------------------
# @ USER DEFINED METHODS START

    def design_loads_from_mooring(self):
        self.moor_V_min = self.loads_from_mooring[0]
        self.moor_V_max = self.loads_from_mooring[1]
        self.moor_H_x = self.loads_from_mooring[2]
        self.moor_H_y = self.loads_from_mooring[3]
        self.moor_M_x = self.loads_from_mooring[4]
        self.moor_M_y = self.loads_from_mooring[5]
        self.z_ap = self.loads_from_mooring[6]
        self.h_res = np.sqrt((self.moor_H_x**2) + (self.moor_H_y**2))

        if self.moor_H_x == 0:
            self.theta = 1
        else:
            self.theta = np.arctan(self.moor_H_y / self.moor_H_x)

        if self.geometry_pref != 'contact_points':
            if self.seabed_connection_type == 'moored':
                self.geometry_pref = 'cylinder'
            elif self.seabed_connection_type == 'fixed':
                self.geometry_pref = self.device_geometry['geometry']

        if self.material != 'concrete':
            self.material = self.material
            self.material_density = self.material_density
        elif self.material == 'concrete':
            self.material_density = 2400

        self.slope = np.deg2rad(self.slope)

    def soil_properties(self):
        if self.u_shear_strenght_d != 0:
            self.soil = 'cohesive'
        elif self.int_friction_angle_d !=0 :
            self.soil = 'cohesionless'
            self.int_friction_angle_d_rad = np.deg2rad(self.int_friction_angle_d)
        return self.soil

    def init_dim_ecc_moored(self):
        if self.geometry_pref != 'contact_points':
            self.base_radius = 1
            self.f_weight()
            self.res_loads_design()
            self.ecc_r = abs(self.Om_r / self.found_bouy_weight)

            while ((2*self.base_radius) < (6 * self.ecc_r)):
                self.base_radius = self.base_radius + 0.25
                self.thickness = self.base_radius * 0.20
                self.f_weight()
                self.res_loads_design()
                self.ecc_r = abs(self.Om_r / self.found_bouy_weight)

            a = (self.moor_V_min + self.found_bouy_weight)
            while a < 1:
                self.base_radius = self.base_radius + 0.25
                self.thickness = self.base_radius * 0.20
                self.f_soil_surf = np.pi * (self.base_radius)**2
                foundation_volume = self.f_soil_surf * self.thickness
                self.found_weight = foundation_volume * (self.material_density * self.g)
                self.found_bouy_weight = foundation_volume * ((self.material_density - self.water_density) * self.g)
                self.res_loads_design()
                self.ecc_r = abs(self.Om_r / self.found_bouy_weight)
                a = (self.moor_V_min + self.found_bouy_weight)
            self.f_weight()
            logger.info('Initial diameter: ' +str(2*self.base_radius))
            logger.info('Initial thickness: ' + str(self.thickness))

    def init_dim_fixed(self):
        if self.geometry_pref == 'cylinder':
            self.ecc_r = np.sqrt((self.ecc_x**2) + (self.ecc_y**2))
            self.base_radius = round(max(self.ecc_r * 6, self.device_geometry['lr']),2)
            self.base_radius = round(max(self.base_radius, 1.0),2)

        elif self.geometry_pref == 'rectangular':
            self.base_lx = round(max(self.ecc_x * 6, self.device_geometry['lx']), 2)
            self.base_ly = round(max(self.ecc_y * 6, self.device_geometry['ly']), 2)

        elif self.geometry_pref == 'contact_points':
            self.base_r1 = self.base_r2 = self.base_r3 = 2
            self.base_alpha01 = 0
            self.base_alpha12 = self.base_alpha23 = 120
            self.contact_points_mass = 10000.0

    def init_eccentricity_fixed (self):
        if self.moor_V_max > 0.0:
            if self.geometry_pref == 'rectangular':
                self.ecc_x = self.moor_M_x / self.moor_V_max
                self.ecc_y = self.moor_M_y / self.moor_V_max

            elif self.geometry_pref == 'cylinder':
                self.ecc_r = abs(self.Om_r / self.moor_V_max)
        else:
            raise Exception("Error in foundation design: maximum downwards loads is negative (i.e. upwards). Increase the mass of the device.")

    def ecc_update(self):
        if self.geometry_pref == 'rectangular':
            self.ecc_x = self.moor_M_x / (self.moor_V_max + self.found_bouy_weight)
            self.ecc_y = self.moor_M_y / (self.moor_V_max + self.found_bouy_weight)

        elif self.geometry_pref == 'cylinder':
            self.ecc_r = abs(self.Om_r / (self.moor_V_max + self.found_bouy_weight))

    def f_weight(self):
        if self.geometry_pref == 'cylinder':
            self.f_soil_surf = np.pi * (self.base_radius)**2
            foundation_volume = self.f_soil_surf * self.thickness
            self.found_weight = foundation_volume * (self.material_density * self.g)
            self.found_bouy_weight = foundation_volume * ((self.material_density - self.water_density) * self.g)
        elif self.geometry_pref == 'rectangular':
            self.f_soil_surf = self.base_lx * self.base_ly
            foundation_volume = self.f_soil_surf * self.thickness
            self.found_weight = foundation_volume * (self.material_density * self.g)
            self.found_bouy_weight = foundation_volume * ((self.material_density - self.water_density) * self.g)
        elif self.geometry_pref == 'contact_points':
            foundation_volume = self.contact_points_mass / self.material_density
            self.found_weight = foundation_volume * (self.material_density * self.g)
            self.found_bouy_weight = foundation_volume * ((self.material_density - self.water_density) * self.g)

        return self.f_soil_surf, self.found_bouy_weight

    def effective_area(self):
        if self.geometry_pref == 'cylinder':
            if self.ecc_r/self.base_radius>1:
                self.eff_area = 0.0
                bc_eff = 0.0
                lc_eff = 0.0
                self.eff_lx = 0.0
                self.eff_ly = 0.0
            else:
                self.eff_area = 2 * (((self.base_radius**2) * (np.arccos(self.ecc_r/self.base_radius))) - (self.ecc_r * np.sqrt(self.base_radius**2 - self.ecc_r**2)))
                bc_eff = 2 * (self.base_radius - self.ecc_r)
                lc_eff = 2 * self.base_radius * np.sqrt(1 - (1 - (bc_eff / (2*self.base_radius)))**2)
                self.eff_lx = np.sqrt(self.eff_area * lc_eff / bc_eff)
                self.eff_ly = (np.sqrt(self.eff_area * lc_eff / bc_eff)) * bc_eff / lc_eff
            self.f_soil_surf = np.pi * (self.base_radius)**2

        elif self.geometry_pref == 'rectangular':
            self.eff_lx = (self.base_lx - (2 * self.ecc_y))
            self.eff_ly = (self.base_ly - (2 * self.ecc_x))
            self.eff_area = (self.eff_lx) * (self.eff_ly)
            self.f_soil_surf = self.base_lx * self.base_ly

    def res_loads_design(self):

        print('self.moor_V_max')
        print(self.moor_V_max)
        print('self.found_bouy_weight')
        print(self.found_bouy_weight)
        print('self.slope')
        print(self.slope)
        print('self.h_res')
        print(self.h_res)

        self.Fn_bc = ((self.moor_V_max + self.found_bouy_weight) * np.cos(self.slope)) - ((self.h_res * np.sin(self.slope)))
        self.Fh_sr = ((self.moor_V_max + self.found_bouy_weight) * np.sin(self.slope)) + (self.h_res * np.cos(self.slope)) #OK
        self.Fn_sr = ((self.moor_V_min + self.found_bouy_weight) * np.cos(self.slope)) - ((self.h_res * np.sin(self.slope))) #OK


        if self.geometry_pref == "cylinder":
            if self.seabed_connection_type == "fixed":
                self.Om_r = np.sqrt((self.moor_M_x**2 + self.moor_M_y**2)) + (self.h_res * (self.z_ap))
            elif self.seabed_connection_type == 'moored':
                M_x = self.thickness * self.moor_H_y
                M_y = self.thickness * self.moor_H_x
                self.Om_r = np.sqrt(M_x**2 + M_y**2)
        elif self.geometry_pref == "rectangular" or self.geometry_pref == "contact_points":
            self.Om_x = self.moor_M_x + (self.moor_H_y * self.z_ap)
            self.Om_y = self.moor_M_y + (self.moor_H_x * self.z_ap)

    def compute_bearing_capacity(self):
        ''' effectives length for dimension of the foundation '''
        b_eff = min(self.eff_lx, self.eff_ly)
        l_eff = max(self.eff_lx, self.eff_ly)
      #  self.eff_area = 2 * (((self.base_radius**2) * (np.arccos(self.ecc_r/self.base_radius))) - (self.ecc_r * np.sqrt(self.base_radius**2 - self.ecc_r**2)))
        #the foundation depth installation is considered = 0
        if self.soil == 'cohesive':
            Nc = 2 + np.pi
            if l_eff == 0.0:
                sc = 0.0
                ic = 0.0
            else:
                sc = 1 + (0.2 * b_eff / l_eff)
                ic = 0.5 + (0.5 * np.sqrt(1 - min(1,(self.h_res / (self.eff_area * self.u_shear_strenght_d)))))
            qd = self.u_shear_strenght_d * Nc * sc * ic
            self.bearing_capacity = self.eff_area * qd

        elif self.soil == 'cohesionless':
            # undrained shear strength, cu  = 0
            Nq =  (np.exp(np.pi * np.tan(self.int_friction_angle_d_rad))) * ((1 + np.sin(self.int_friction_angle_d_rad))/(1 - np.sin(self.int_friction_angle_d_rad)))
            Ng = (3/2) * (Nq - 1) * np.tan(self.int_friction_angle_d_rad)
            if l_eff == 0.0:
                sg = 0.0
            else:
                sg = max(0,min(1,1 - (0.4 * b_eff / l_eff)))
            ig = min(1,(1 - (self.h_res / (self.moor_V_max + self.found_bouy_weight)))**4)
            qd = ((1/2) * self.g_b * b_eff * Ng * sg * ig)
            self.bearing_capacity = self.eff_area * qd
            self.friction_resistance = self.Om_r / (self.moor_V_max + self.found_bouy_weight)
        ''' ultimate bearing capacity '''
        ### Depth factor are considered equal to 1, foundation_depth < width dc=dq=dg=1
        ### Hypothesis of inclination near horizontal bc=bq=bg=1, gc=gq=gg=1


    def compute_sliding_resistance(self):
        if self.soil == 'cohesive':
            self.sliding_resistance = (self.u_shear_strenght_d * self.eff_area )

        elif self.soil == 'cohesionless':
            self.sliding_resistance = (np.tan(self.int_friction_angle_d_rad) * self.Fn_sr)

    def compute_resist_moment(self):
        if self.geometry_pref == 'cylinder':
            self.stabm_r = self.Fn_sr * self.base_radius
        elif self.geometry_pref == 'rectangular':
            self.stabm_x = self.Fn_sr * (self.base_ly / 2)
            self.stabm_y = self.Fn_sr * (self.base_lx / 2)
        elif self.geometry_pref == 'contact_points':
            ###
            base_alpha01 = np.deg2rad(self.base_alpha01)
            base_alpha12 = np.deg2rad(self.base_alpha12)
            base_alpha23 = np.deg2rad(self.base_alpha23)
            x1 = self.base_r1*np.cos(base_alpha01)
            y1 = self.base_r1*np.sin(base_alpha01)
            x2 = self.base_r2*np.cos(base_alpha01 + base_alpha12)
            y2 = self.base_r2*np.sin(base_alpha01 + base_alpha12)
            x3 = self.base_r3*np.cos(base_alpha01 + base_alpha12 + base_alpha23)
            y3 = self.base_r3*np.sin(base_alpha01 + base_alpha12 + base_alpha23)
            X = np.array([x1,x2,x3])
            Y = np.array([y1,y2,y3])
            stab_temp_x = stab_temp_y = 0

            if self.Om_x <= 0:
                for yi in Y:
                    if yi >= 0:
                        stab_temp_x = stab_temp_x + self.Fn_sr * yi
                        self.stabm_x = stab_temp_x
            elif self.Om_x > 0:
                for yi in Y:
                    if yi < 0:
                        stab_temp_x = stab_temp_x + self.Fn_sr * yi
                        self.stabm_x = stab_temp_x

            if self.Om_y >= 0:
                for xi in X:
                    if xi >= 0:
                        stab_temp_y = stab_temp_y + self.Fn_sr * xi
                        self.stabm_y = stab_temp_y
            elif self.Om_y < 0:
                for xi in X:
                    if xi < 0:
                        stab_temp_y = stab_temp_y + self.Fn_sr * xi
                        self.stabm_y = stab_temp_y

    def increase_soil_surf(self):
        if self.geometry_pref == 'cylinder':
            self.base_radius = self.base_radius + 0.25
            self.thickness = self.base_radius * 0.20
        elif self.geometry_pref == 'rectangular':
            self.base_lx = self.base_lx + 0.25
            self.base_ly = self.base_ly + 0.25
            self.thickness = ((self.base_lx + self.base_ly)/2)*0.20

    def increase_sliding_res(self):
        if self.geometry_pref == 'cylinder':
            self.base_radius = self.base_radius + 0.25
            self.thickness = self.base_radius * 0.20
        elif self.geometry_pref == 'rectangular':
            self.base_lx = self.base_lx + 0.25
            self.base_ly = self.base_ly + 0.25
            self.thickness = ((self.base_lx + self.base_ly)/2)*0.20
        elif self.geometry_pref == 'contact_points':
            self.contact_points_mass = self.contact_points_mass*1.1

    def increase_mom_res_r(self):
        self.base_radius = self.base_radius + 0.25
        self.thickness = self.base_radius * 0.20
    def increase_mom_res_lx(self):
        self.base_lx = self.base_lx + 0.25
        self.thickness = ((self.base_lx + self.base_ly)/2)*0.20
    def increase_mom_res_ly(self):
        self.base_ly = self.base_ly + 0.25
        self.thickness = ((self.base_lx + self.base_ly)/2)*0.20
    def increase_mom_res_cp(self):
        self.base_r1 = self.base_r2 = self.base_r3 = self.base_r1 + 0.2
        self.contact_points_mass = self.contact_points_mass + 1000

    def update_dim(self):
        self.f_weight()
        self.res_loads_design()
        self.ecc_update()
        self.effective_area()
        self.compute_bearing_capacity()
        self.compute_sliding_resistance()
        self.compute_resist_moment()

    def assess(self):

        logger.info('--------------------------------')
        logger.info('Assessment of gravity foundation')
        logger.info('--------------------------------')

        self.design_loads_from_mooring()
        self.soil_properties()
        self.f_weight()
        self.update_dim()

        if (self.bearing_capacity / self.SF) >= self.Fn_bc:
            self.criteria_bearing_capacity = True
        elif self.geometry_pref == 'contact_points':
            self.criteria_bearing_capacity = True
            self.bearing_capacity = self.Fn_bc*self.SF*1.001
        else:
            self.criteria_bearing_capacity  = False

        if (self.sliding_resistance / self.SF) >= self.Fh_sr:
            self.criteria_sliding_resistance = True
        else:
            self.criteria_sliding_resistance = False

        if self.geometry_pref == 'cylinder':
            if (self.stabm_r / self.SF) >= self.Om_r:
                self.criteria_stability = True
            else:
                self.criteria_stability = False
        elif self.geometry_pref == 'rectangular' or self.geometry_pref == 'contact_points':
            if (abs(self.stabm_x) / self.SF) >= abs(self.Om_x) and (abs(self.stabm_y) / self.SF) >= abs(self.Om_y):
                self.criteria_stability = True
            else:
                self.criteria_stability = False

        if self.geometry_pref == 'contact_points':
            logger.info('Final bearing capacity: ' + 'Always True (rocky soil)')
        else:
            logger.info('Final bearing capacity: ' + str(self.bearing_capacity) + str(' vs ') + str(self.Fn_bc*self.SF))
        logger.info('Final sliding resistance: ' + str(self.sliding_resistance) + str(' vs ') + str(self.Fh_sr*self.SF))
        if self.geometry_pref == 'rectangular' or self.geometry_pref == 'contact_points':
            logger.info('Final x-stability: ' + str(abs(self.stabm_x)) + str(' vs ') + str(abs(self.Om_x*self.SF)))
            logger.info('Final y-stability: ' + str(abs(self.stabm_y)) + str(' vs ') + str(abs(self.Om_y*self.SF)))
        elif self.geometry_pref == 'cylinder':
            logger.info('Final stability: ' + str(self.stabm_r) + str(' vs ') + str(self.Om_r*self.SF))
        if self.geometry_pref != 'contact_points':
            logger.info('Effective area: ' + str(self.eff_area))

        logger.info('Final design:')
        if self.geometry_pref == 'rectangular':
            logger.info('Length-x [m]: ' + str(self.base_lx))
            logger.info('Length-y [m]: ' + str(self.base_ly))
            logger.info('Thickness [m]: ' + str(self.thickness))
            logger.info('Mass [kg]: ' + str(self.found_weight/9.81))
        elif self.geometry_pref == 'cylinder':
            logger.info('Diameter [m]: ' + str(2*self.base_radius))
            logger.info('Thickness [m]: ' + str(self.thickness))
            logger.info('Mass [kg]: ' + str(self.found_weight/9.81))
        elif self.geometry_pref == 'contact_points':
            logger.info('Radius r1 [m]: ' + str(self.base_r1))
            logger.info('Radius r2 [m]: ' + str(self.base_r2))
            logger.info('Radius r3 [m]: ' + str(self.base_r3))
            logger.info('Angle alpha01 [°]: ' + str(self.base_alpha01))
            logger.info('Angle alpha12 [°]: ' + str(self.base_alpha12))
            logger.info('Angle alpha23 [°]: ' + str(self.base_alpha23))
            logger.info('Mass [kg]: ' + str(self.contact_points_mass))

    def design(self):
        n_iterations = 0
        self.design_loads_from_mooring()
        self.soil_properties()
        if self.seabed_connection_type == 'moored':
            self.init_dim_ecc_moored()
            self.f_weight()
        elif self.seabed_connection_type == 'fixed':
            self.init_eccentricity_fixed()
            self.init_dim_fixed()
            self.f_weight()
        self.update_dim()
        a_list = []
        b_list = []
        c_list = []
        d_list = []
        e_list = []
        f_list = []
        g_list = []
        h_list = []
        i_list = []
        a_list.append(n_iterations)
        b_list.append(self.base_lx)
        c_list.append(self.base_ly)
        d_list.append(self.base_radius)
        e_list.append(self.thickness)
        f_list.append(self.bearing_capacity / self.Fn_bc)
        g_list.append(self.sliding_resistance / self.Fh_sr)
        h_list.append(self.base_r1)
        i_list.append(self.contact_points_mass)
        logger.info('--------------------------------')
        logger.info('Design of gravity foundation')
        logger.info('--------------------------------')
        logger.info('Applied load Vmin, Vmax : ' + str(self.loads_from_mooring[0:2]))
        logger.info('Applied load Hx, Hy : ' + str(self.loads_from_mooring[2:4]))
        logger.info('Applied load Mx, My : ' + str(self.loads_from_mooring[4:6]))
        logger.info('Applied load vertical position zap : ' + str(self.loads_from_mooring[6]))
        logger.info('Safety factor: ' + str(self.SF))
        if self.geometry_pref == 'contact_points':
            logger.info('Final bearing capacity: ' + 'Always True (rocky soil)')
        else:
            logger.info('Initial bearing capacity: ' + str(self.bearing_capacity) + str(' vs ') + str(self.Fn_bc*self.SF))
        logger.info('Initial sliding resistance: ' + str(self.sliding_resistance) + str(' vs ') + str(self.Fh_sr*self.SF))
        logger.info('Geometry: ' + self.geometry_pref)
        if self.geometry_pref == 'rectangular' or self.geometry_pref == 'contact_points':
            logger.info('Initial x-stability: ' + str(self.stabm_x) + str(' vs ') + str(self.Om_x*self.SF))
            logger.info('Initial y-stability: ' + str(self.stabm_y) + str(' vs ') + str(self.Om_y*self.SF))
        elif self.geometry_pref == 'cylinder':
            logger.info('Initial stability: ' + str(self.stabm_r) + str(' vs ') + str(self.Om_r*self.SF))

        while (self.bearing_capacity / self.SF) < self.Fn_bc and self.geometry_pref != 'contact_points':

            if n_iterations > self.n_iterations_max:
                message = "dimensioning not achieved, maximal number of iterations achieved bc"
                logger.info(message)
                break
            else:
                n_iterations = n_iterations + 1
                self.increase_soil_surf()
            self.update_dim()
            logger.info('Increasing foundation bearing capacity. New value: ' + str(self.bearing_capacity))
            a_list.append(n_iterations)
            b_list.append(self.base_lx)
            c_list.append(self.base_ly)
            d_list.append(self.base_radius)
            e_list.append(self.thickness)
            f_list.append(self.bearing_capacity / self.Fn_bc)
            g_list.append(self.sliding_resistance / self.Fh_sr)
            h_list.append(self.base_r1)
            i_list.append(self.contact_points_mass)

        if self.geometry_pref == 'cylinder':
            while (self.stabm_r / self.SF) < self.Om_r:

                if n_iterations > self.n_iterations_max:
                    message = "dimensioning not achieved, maximal number of iterations achieved Om"
                    logger.info(message)
                    break
                else:
                    n_iterations = n_iterations + 1
                    self.increase_mom_res_r()
                self.update_dim()
                logger.info('Increasing foundation stability. New value: ' + str(self.stabm_r))

        elif self.geometry_pref == 'rectangular':
            while (self.stabm_x / self.SF) < self.Om_x:

                if n_iterations > self.n_iterations_max:
                    message = "dimensioning not achieved, maximal number of iterations achieved Om"
                    logger.info(message)
                    break
                else:
                    n_iterations = n_iterations + 1
                    self.increase_mom_res_lx()
                self.update_dim()
                logger.info('Increasing foundation x-stability. New value: ' + str(self.stabm_x))
                a_list.append(n_iterations)
                b_list.append(self.base_lx)
                c_list.append(self.base_ly)
                d_list.append(self.base_radius)
                e_list.append(self.thickness)
                f_list.append(self.bearing_capacity / self.Fn_bc)
                g_list.append(self.sliding_resistance / self.Fh_sr)
                h_list.append(self.base_r1)
                i_list.append(self.contact_points_mass)

            while (self.stabm_y / self.SF) < self.Om_y:
                if n_iterations > self.n_iterations_max:
                    message = "dimensioning not achieved, maximal number of iterations achieved Om"
                    logger.info(message)
                    break
                else:
                    n_iterations = n_iterations + 1
                    self.increase_mom_res_ly()
                self.update_dim()
                logger.info('Increasing foundation y-stability. New value: ' + str(self.stabm_y))
                a_list.append(n_iterations)
                b_list.append(self.base_lx)
                c_list.append(self.base_ly)
                d_list.append(self.base_radius)
                e_list.append(self.thickness)
                f_list.append(self.bearing_capacity / self.Fn_bc)
                g_list.append(self.sliding_resistance / self.Fh_sr)
                h_list.append(self.base_r1)
                i_list.append(self.contact_points_mass)

        elif self.geometry_pref == 'contact_points':
            while (abs(self.stabm_x / self.SF)) < abs(self.Om_x) or (abs(self.stabm_y / self.SF)) < abs(self.Om_y):

                if n_iterations > self.n_iterations_max:
                    message = "dimensioning not achieved, maximal number of iterations achieved Om"
                    logger.info(message)
                    break
                else:
                    n_iterations = n_iterations + 1
                    self.increase_mom_res_cp()
                self.update_dim()
                logger.info('Increasing foundation x-stability. New value: ' + str(self.stabm_x))
                logger.info('Increasing foundation y-stability. New value: ' + str(self.stabm_y))
                a_list.append(n_iterations)
                b_list.append(self.base_lx)
                c_list.append(self.base_ly)
                d_list.append(self.base_radius)
                e_list.append(self.thickness)
                f_list.append(self.bearing_capacity / self.Fn_bc)
                g_list.append(self.sliding_resistance / self.Fh_sr)
                h_list.append(self.base_r1)
                i_list.append(self.contact_points_mass)

        while (self.sliding_resistance / self.SF) < self.Fh_sr:

            if n_iterations > self.n_iterations_max:
                message = "dimensioning not achieved, maximal number of iterations achieved sr"
                logger.info(message)
                break
            else:
                n_iterations = n_iterations + 1
                self.increase_sliding_res()
            self.update_dim()
            logger.info('Increasing foundation sliding resistance. New value: ' + str(self.sliding_resistance))
            a_list.append(n_iterations)
            b_list.append(self.base_lx)
            c_list.append(self.base_ly)
            d_list.append(self.base_radius)
            e_list.append(self.thickness)
            f_list.append(self.bearing_capacity / self.Fn_bc)
            g_list.append(self.sliding_resistance / self.Fh_sr)
            h_list.append(self.base_r1)
            i_list.append(self.contact_points_mass)

        self.assess()


        '''Dataframe with convergence information'''
        self.convergence =pd.DataFrame({'n_i' :a_list, 'lx [m]': b_list,  'ly [m]': c_list,'radius [m]': d_list, 'Thickness[m]': e_list, 'Vd/Vu': f_list, 'Hd/Hu': g_list, 'radius cp [m]': h_list, 'Mass [kg]': i_list })
        return "{:.2e}".format(self.Fn_bc), "{:.2e}".format(self.bearing_capacity), self.base_lx, self.base_ly, self.thickness, self.ecc_r, "{:.2e}".format(self.sliding_resistance), "{:.2e}".format(self.Fh_sr), self.base_r1, self.contact_points_mass

    def outputs (self):

        bom_esa = pd.DataFrame ({"Materials": [self.material],
                             "Density [kg/m3]":[self.material_density],
                             "Weight [N]": [self.found_weight],
                              "Footprint [m2]": [self.f_soil_surf]})

        if self.geometry_pref == 'rectangular':
            info = pd.DataFrame ({"Length x [m]" : [self.base_lx], "Length y [m]" : [self.base_ly], "Thickness [m]": [self.thickness], "ec [m]": [max(self.ecc_x, self.ecc_y)], "Bearing Capacity [m]" : [self.bearing_capacity],
                                "sliding resistance [m]" : [self.sliding_resistance], "stabm x [Nm]" : [self.stabm_x],"stabm y [Nm]" : [self.stabm_y],
                                "DL bc [N]" : [ self.Fn_bc ], "DL sr [N]" : [ self.Fn_sr], "om x [Nm]" : [ self.Om_x], "om y [Nm]" : [ self.Om_y]})

        elif self.geometry_pref =='cylinder':
            info = pd.DataFrame ({"Radius [m]" : [self.base_radius],"Thickness [m]": [self.thickness] , "ec [m]": [self._ecc_r], "Bearing Capacity [N]" : [self.bearing_capacity],
                                "sliding resistance [N]" : [self.sliding_resistance], "stabm r [Nm]" : [self.stabm_r],
                                "DL bc [N]" : [ self.Fn_bc ], "DL sr [N]" : [ self.Fh_sr], "om r [Nm]" : [ self.Om_r]})

        elif self.geometry_pref == 'contact_points':
            info = pd.DataFrame ({"Radius cp [m]" : [self.base_r1], "Mass [kg]" : [self.contact_points_mass],
                                "sliding resistance [m]" : [self.sliding_resistance], "stabm x [Nm]" : [self.stabm_x],"stabm y [Nm]" : [self.stabm_y],
                                "DL bc [N]" : [ self.Fn_bc ], "DL sr [N]" : [ self.Fn_sr], "om x [Nm]" : [ self.Om_x], "om y [Nm]" : [ self.Om_y]})

        return bom_esa, info

    def outputs_convergence(self):  # pragma: no cover
        palette = plt.get_cmap('Set1')
        if self.geometry_pref == 'rectangular':
            self.convergence.plot(kind = 'line', x='n_i', y = 'lx [m]', color = palette(1), label = 'lx')
            self.convergence.plot(kind = 'line', x='n_i', y = 'ly [m]', color = palette(2), label = 'ly')
            self.convergence.plot(kind = 'line', x='n_i', y = 'Thickness[m]', color = palette(3), label = 'Thickness')
            self.convergence.plot(kind = 'line', x='n_i', y = 'Hd/Hu', color= palette(4), label = 'Hd/Hu')
            self.convergence.plot(kind = 'line', x='n_i', y = 'Vd/Vu', color = palette(5), label = 'Vd/Vu')

            with pd.option_context('display.max_rows', None, 'display.max_columns', None ):
                print (self.convergence)
            plt.show()

        if self.geometry_pref == 'cylinder':
            self.convergence.plot(kind = 'line', x='n_i', y ='radius [m]', color = palette(1), label = 'radius')
            self.convergence.plot(kind = 'line', x='n_i', y = 'Thickness[m]', color = palette(3), label = 'Thickness')
            self.convergence.plot(kind = 'line', x='n_i', y = 'Hd/Hu', color= palette(4), label = 'Hd/Hu')
            self.convergence.plot(kind = 'line', x='n_i', y = 'Vd/Vu', color = palette(5), label = 'Vd/Vu')

            with pd.option_context('display.max_rows', None, 'display.max_columns', None ):
                print (self.convergence)
            plt.show()

        if self.geometry_pref == 'contact_points':
            self.convergence.plot(kind = 'line', x='n_i', y ='radius cp [m]', color = palette(1), label = 'radius cp')
            self.convergence.plot(kind = 'line', x='n_i', y = 'Mass [kg]', color = palette(3), label = 'Mass')
            self.convergence.plot(kind = 'line', x='n_i', y = 'Hd/Hu', color= palette(4), label = 'Hd/Hu')
            self.convergence.plot(kind = 'line', x='n_i', y = 'Vd/Vu', color = palette(5), label = 'Vd/Vu')

            with pd.option_context('display.max_rows', None, 'display.max_columns', None ):
                print (self.convergence)
            plt.show()


        return self.convergence


# @ USER DEFINED METHODS END
#------------------------------------

    #------------
    # Get functions
    #------------
    @ property
    def base_lx(self): # pragma: no cover
        """float: Width of the foundation - direction with less overturning moment []
        """
        return self._base_lx
    #------------
    @ property
    def base_ly(self): # pragma: no cover
        """float: length of the foundation - direction with the bigger overturning moment []
        """
        return self._base_ly
    #------------
    @ property
    def base_radius(self): # pragma: no cover
        """float: radius of the foundation for cylinder geometry []
        """
        return self._base_radius
    #------------
    @ property
    def base_r1(self): # pragma: no cover
        """float: radius of the first contact point foundation for contact points geometry [m]
        """
        return self._base_r1
    #------------
    @ property
    def base_r2(self): # pragma: no cover
        """float: radius of the second contact point foundation for contact points geometry [m]
        """
        return self._base_r2
    #------------
    @ property
    def base_r3(self): # pragma: no cover
        """float: radius of the third contact point foundation for contact points geometry [m]
        """
        return self._base_r3
    #------------
    @ property
    def base_alpha01(self): # pragma: no cover
        """float: Absolute angle between X-axis and first contact point foundation for contact points geometry [deg]
        """
        return self._base_alpha01
    #------------
    @ property
    def base_alpha12(self): # pragma: no cover
        """float: angle between the first and second contact points foundation for contact points geometry [deg]
        """
        return self._base_alpha12
    #------------
    @ property
    def base_alpha23(self): # pragma: no cover
        """float: angle between the second and third contact points foundation for contact points geometry [deg]
        """
        return self._base_alpha23
    #------------
    @ property
    def contact_points_mass(self): # pragma: no cover
        """float: mass of the contact points foundation []
        """
        return self._contact_points_mass
    #------------
    @ property
    def device_radius(self): # pragma: no cover
        """float: device raidus when cylinder geometry []
        """
        return self._device_radius
    #------------
    @ property
    def u_shear_strenght(self): # pragma: no cover
        """float: undrained shear strenght for cohessive soils []
        """
        return self._u_shear_strenght
    #------------
    @ property
    def bearing_capacity(self): # pragma: no cover
        """float: none []
        """
        return self._bearing_capacity
    #------------
    @ property
    def friction_resistance(self): # pragma: no cover
        """float: none []
        """
        return self._friction_resistance
    #------------
    @ property
    def n_iterations_max(self): # pragma: no cover
        """float: none []
        """
        return self._n_iterations_max
    #------------
    @ property
    def int_friction_angle_d(self): # pragma: no cover
        """float: internal friction angle for design []
        """
        return self._int_friction_angle_d
    #------------
    @ property
    def u_shear_strenght_d(self): # pragma: no cover
        """float: undrained shear strenght for design []
        """
        return self._u_shear_strenght_d
    #------------
    @ property
    def overturning_resitance_y(self): # pragma: no cover
        """float: overturning moment on the length side []
        """
        return self._overturning_resitance_y
    #------------
    @ property
    def overturning_resitance_x(self): # pragma: no cover
        """float: overturning moment on the length side []
        """
        return self._overturning_resitance_x
    #------------
    @ property
    def geometry_pref(self): # pragma: no cover
        """str:  user input - geometry preference, cylinder, rectangular or contact_points
        """
        return self._geometry_pref
    #------------
    @ property
    def ecc_x(self): # pragma: no cover
        """float:  eccentricity on the x axis []
        """
        return self._ecc_x
    #------------
    @ property
    def ecc_y(self): # pragma: no cover
        """float:  eccentricity on the y axis []
        """
        return self._ecc_y
    #------------
    @ property
    def ecc_r(self): # pragma: no cover
        """float:  radial eccentricity for ciruclar foundations []
        """
        return self._ecc_r
    #------------
    @ property
    def eff_area(self): # pragma: no cover
        """float:  radial eccentricity for ciruclar foundations []
        """
        return self._eff_area
    #------------
    @ property
    def material(self): # pragma: no cover
        """str:  material for shallow foundation concrete or steel
        """
        return self._material
    #------------
    @ property
    def theta(self): # pragma: no cover
        """float:  angle between the line of action of H and the long axis of the foundation on the horizontal plane []
        """
        return self._theta
    #------------
    @ property
    def eff_lx(self): # pragma: no cover
        """float:  effective length of the effective area []
        """
        return self._eff_lx
    #------------
    @ property
    def eff_ly(self): # pragma: no cover
        """float:  effective length of the effective area []
        """
        return self._eff_ly
    #------------
    @ property
    def thickness(self): # pragma: no cover
        """float:  foundation thickness initial value equal to 30 cm []
        """
        return self._thickness
    #------------
    @ property
    def material_density(self): # pragma: no cover
        """float:  material density  []
        """
        return self._material_density
    #------------
    @ property
    def qc(self): # pragma: no cover
        """float: bearing capacity stress for cohesion  []
        """
        return self._qc
    #------------
    @ property
    def qq(self): # pragma: no cover
        """float: bearing capacity stress for overburden []
        """
        return self._qq
    #------------
    @ property
    def qg(self): # pragma: no cover
        """float: bearing capacity stress for friction  []
        """
        return self._qg
    #------------
    @ property
    def g_b(self): # pragma: no cover
        """float: bouyant unit weight  []
        """
        return self._g_b
    #------------
    @ property
    def sliding_resistance(self): # pragma: no cover
        """float: depth of embedment of foundation []
        """
        return self._sliding_resistance
    #------------
    @ property
    def slope(self): # pragma: no cover
        """float: soil slope []
        """
        return self._slope
    #------------
    @ property
    def found_weight(self): # pragma: no cover
        """float: vertical load for sliding resistance []
        """
        return self._found_weight
    #------------
    @ property
    def f_soil_surf(self): # pragma: no cover
        """float: footprint []
        """
        return self._f_soil_surf
    #------------
    @ property
    def V_min_load(self): # pragma: no cover
        """float: Minimal vertical load []
        """
        return self._V_min_load
    #------------
    @ property
    def device_weight(self): # pragma: no cover
        """float: device weight []
        """
        return self._device_weight
    #------------
    @ property
    def thickness_max(self): # pragma: no cover
        """float: maximal thickness []
        """
        return self._thickness_max
    #------------
    @ property
    def Fn_bc(self): # pragma: no cover
        """float: Load desing to verify bearing capacity []
        """
        return self._Fn_bc
    #------------
    @ property
    def Fn_sr(self): # pragma: no cover
        """float: Load desing to verify sliding resistance []
        """
        return self._Fn_sr
    #------------
    @ property
    def Fh_sr(self): # pragma: no cover
        """float: Load desing to verify sliding resistance []
        """
        return self._Fh_sr
    #------------
    @ property
    def SF(self): # pragma: no cover
        """float: SF for sliding and bearing capacity resistance []
        """
        return self._SF
    #------------
    @ property
    def stabm_r(self): # pragma: no cover
        """float: radial resistant moment  []
        """
        return self._stabm_r
    #------------
    @ property
    def stabm_x(self): # pragma: no cover
        """float: resistant moment []
        """
        return self._stabm_x
    #------------
    @ property
    def stabm_y(self): # pragma: no cover
        """float: resistant moment []
        """
        return self._stabm_y
    #------------
    @ property
    def Om_r(self): # pragma: no cover
        """float: radial overturning  moment  []
        """
        return self._Om_r
    #------------
    @ property
    def Om_x(self): # pragma: no cover
        """float: overturning  moment x []
        """
        return self._Om_x
    #------------
    @ property
    def Om_y(self): # pragma: no cover
        """float: overturning moment y []
        """
        return self._Om_y
    #------------
    @ property
    def z_ap(self): # pragma: no cover
        """float: high of application point of loads []
        """
        return self._z_ap
    #------------
    @ property
    def moor_V_min(self): # pragma: no cover
        """float: maximal moroing vertical upward load transfert to foundation []
        """
        return self._moor_V_min
    #------------
    @ property
    def moor_V_max(self): # pragma: no cover
        """float: maximal mooring vertical downard load transfert to foundation []
        """
        return self._moor_V_max
    #------------
    @ property
    def moor_H_x(self): # pragma: no cover
        """float: maximal mooring horizontal x load transfert to foundation []
        """
        return self._moor_H_x
    #------------
    @ property
    def moor_H_y(self): # pragma: no cover
        """float: maximal mooring horizontal y load transfert to foundation []
        """
        return self._moor_H_y
    #------------
    @ property
    def moor_M_x(self): # pragma: no cover
        """float: maximal mooring horizontal x load transfert to foundation []
        """
        return self._moor_M_x
    #------------
    @ property
    def moor_M_y(self): # pragma: no cover
        """float: maximal mooring horizontal y load transfert to foundation []
        """
        return self._moor_M_y
    #------------
    @ property
    def seabed_connection_type(self): # pragma: no cover
        """str: fixed or moored
        """
        return self._seabed_connection_type
    #------------
    @ property
    def criteria_bearing_capacity(self): # pragma: no cover
        """bool: Bearing capacity criterion. True or False []
        """
        return self._criteria_bearing_capacity
    #------------
    @ property
    def criteria_stability(self): # pragma: no cover
        """bool: Stability criterion. True or False []
        """
        return self._criteria_stability
    #------------
    @ property
    def criteria_sliding_resistance(self): # pragma: no cover
        """bool: Sliding resistance criterion. True or False []
        """
        return self._criteria_sliding_resistance
    #------------
    @ property
    def name(self): # pragma: no cover
        """str: name of the instance object
        """
        return self._name
    #------------
    @ property
    def description(self): # pragma: no cover
        """str: description of the instance object
        """
        return self._description
    #------------
    #------------
    # Set functions
    #------------
    @ base_lx.setter
    def base_lx(self,val): # pragma: no cover
        self._base_lx=float(val)
    #------------
    @ base_ly.setter
    def base_ly(self,val): # pragma: no cover
        self._base_ly=float(val)
    #------------
    @ base_radius.setter
    def base_radius(self,val): # pragma: no cover
        self._base_radius=float(val)
    #------------
    @ base_r1.setter
    def base_r1(self,val): # pragma: no cover
        self._base_r1=float(val)
    #------------
    @ base_r2.setter
    def base_r2(self,val): # pragma: no cover
        self._base_r2=float(val)
    #------------
    @ base_r3.setter
    def base_r3(self,val): # pragma: no cover
        self._base_r3=float(val)
    #------------
    @ base_alpha01.setter
    def base_alpha01(self,val): # pragma: no cover
        self._base_alpha01=float(val)
    #------------
    @ base_alpha12.setter
    def base_alpha12(self,val): # pragma: no cover
        self._base_alpha12=float(val)
    #------------
    @ base_alpha23.setter
    def base_alpha23(self,val): # pragma: no cover
        self._base_alpha23=float(val)
    #------------
    @ contact_points_mass.setter
    def contact_points_mass(self,val): # pragma: no cover
        self._contact_points_mass=float(val)
    #------------
    @ device_radius.setter
    def device_radius(self,val): # pragma: no cover
        self._device_radius=float(val)
    #------------
    @ u_shear_strenght.setter
    def u_shear_strenght(self,val): # pragma: no cover
        self._u_shear_strenght=float(val)
    #------------
    @ bearing_capacity.setter
    def bearing_capacity(self,val): # pragma: no cover
        self._bearing_capacity=float(val)
    #------------
    @ friction_resistance.setter
    def friction_resistance(self,val): # pragma: no cover
        self._friction_resistance=float(val)
    #------------
    @ n_iterations_max.setter
    def n_iterations_max(self,val): # pragma: no cover
        self._n_iterations_max=float(val)
    #------------
    @ int_friction_angle_d.setter
    def int_friction_angle_d(self,val): # pragma: no cover
        self._int_friction_angle_d=float(val)
    #------------
    @ u_shear_strenght_d.setter
    def u_shear_strenght_d(self,val): # pragma: no cover
        self._u_shear_strenght_d=float(val)
    #------------
    @ overturning_resitance_y.setter
    def overturning_resitance_y(self,val): # pragma: no cover
        self._overturning_resitance_y=float(val)
    #------------
    @ overturning_resitance_x.setter
    def overturning_resitance_x(self,val): # pragma: no cover
        self._overturning_resitance_x=float(val)
    #------------
    @ geometry_pref.setter
    def geometry_pref(self,val): # pragma: no cover
        self._geometry_pref=str(val)
    #------------
    @ ecc_x.setter
    def ecc_x(self,val): # pragma: no cover
        self._ecc_x=float(val)
    #------------
    @ ecc_y.setter
    def ecc_y(self,val): # pragma: no cover
        self._ecc_y=float(val)
    #------------
    @ ecc_r.setter
    def ecc_r(self,val): # pragma: no cover
        self._ecc_r=float(val)
    #------------
    @ eff_area.setter
    def eff_area(self,val): # pragma: no cover
        self._eff_area=float(val)
    #------------
    @ material.setter
    def material(self,val): # pragma: no cover
        self._material=str(val)
    #------------
    @ theta.setter
    def theta(self,val): # pragma: no cover
        self._theta=float(val)
    #------------
    @ eff_lx.setter
    def eff_lx(self,val): # pragma: no cover
        self._eff_lx=float(val)
    #------------
    @ eff_ly.setter
    def eff_ly(self,val): # pragma: no cover
        self._eff_ly=float(val)
    #------------
    @ thickness.setter
    def thickness(self,val): # pragma: no cover
        self._thickness=float(val)
    #------------
    @ material_density.setter
    def material_density(self,val): # pragma: no cover
        self._material_density=float(val)
    #------------
    @ qc.setter
    def qc(self,val): # pragma: no cover
        self._qc=float(val)
    #------------
    @ qq.setter
    def qq(self,val): # pragma: no cover
        self._qq=float(val)
    #------------
    @ qg.setter
    def qg(self,val): # pragma: no cover
        self._qg=float(val)
    #------------
    @ g_b.setter
    def g_b(self,val): # pragma: no cover
        self._g_b=float(val)
    #------------
    @ sliding_resistance.setter
    def sliding_resistance(self,val): # pragma: no cover
        self._sliding_resistance=float(val)
    #------------
    @ slope.setter
    def slope(self,val): # pragma: no cover
        self._slope=float(val)
    #------------
    @ found_weight.setter
    def found_weight(self,val): # pragma: no cover
        self._found_weight=float(val)
    #------------
    @ f_soil_surf.setter
    def f_soil_surf(self,val): # pragma: no cover
        self._f_soil_surf=float(val)
    #------------
    @ V_min_load.setter
    def V_min_load(self,val): # pragma: no cover
        self._V_min_load=float(val)
    #------------
    @ device_weight.setter
    def device_weight(self,val): # pragma: no cover
        self._device_weight=float(val)
    #------------
    @ thickness_max.setter
    def thickness_max(self,val): # pragma: no cover
        self._thickness_max=float(val)
    #------------
    @ Fn_bc.setter
    def Fn_bc(self,val): # pragma: no cover
        self._Fn_bc=float(val)
    #------------
    @ Fn_sr.setter
    def Fn_sr(self,val): # pragma: no cover
        self._Fn_sr=float(val)
    #------------
    @ Fh_sr.setter
    def Fh_sr(self,val): # pragma: no cover
        self._Fh_sr=float(val)
    #------------
    @ SF.setter
    def SF(self,val): # pragma: no cover
        self._SF=float(val)
    #------------
    @ stabm_r.setter
    def stabm_r(self,val): # pragma: no cover
        self._stabm_r=float(val)
    #------------
    @ stabm_x.setter
    def stabm_x(self,val): # pragma: no cover
        self._stabm_x=float(val)
    #------------
    @ stabm_y.setter
    def stabm_y(self,val): # pragma: no cover
        self._stabm_y=float(val)
    #------------
    @ Om_r.setter
    def Om_r(self,val): # pragma: no cover
        self._Om_r=float(val)
    #------------
    @ Om_x.setter
    def Om_x(self,val): # pragma: no cover
        self._Om_x=float(val)
    #------------
    @ Om_y.setter
    def Om_y(self,val): # pragma: no cover
        self._Om_y=float(val)
    #------------
    @ z_ap.setter
    def z_ap(self,val): # pragma: no cover
        self._z_ap=float(val)
    #------------
    @ moor_V_min.setter
    def moor_V_min(self,val): # pragma: no cover
        self._moor_V_min=float(val)
    #------------
    @ moor_V_max.setter
    def moor_V_max(self,val): # pragma: no cover
        self._moor_V_max=float(val)
    #------------
    @ moor_H_x.setter
    def moor_H_x(self,val): # pragma: no cover
        self._moor_H_x=float(val)
    #------------
    @ moor_H_y.setter
    def moor_H_y(self,val): # pragma: no cover
        self._moor_H_y=float(val)
    #------------
    @ moor_M_x.setter
    def moor_M_x(self,val): # pragma: no cover
        self._moor_M_x=float(val)
    #------------
    @ moor_M_y.setter
    def moor_M_y(self,val): # pragma: no cover
        self._moor_M_y=float(val)
    #------------
    @ seabed_connection_type.setter
    def seabed_connection_type(self,val): # pragma: no cover
        self._seabed_connection_type=str(val)
    #------------
    @ criteria_bearing_capacity.setter
    def criteria_bearing_capacity(self,val): # pragma: no cover
        self._criteria_bearing_capacity=val
    #------------
    @ criteria_stability.setter
    def criteria_stability(self,val): # pragma: no cover
        self._criteria_stability=val
    #------------
    @ criteria_sliding_resistance.setter
    def criteria_sliding_resistance(self,val): # pragma: no cover
        self._criteria_sliding_resistance=val
    #------------
    @ name.setter
    def name(self,val): # pragma: no cover
        self._name=str(val)
    #------------
    @ description.setter
    def description(self,val): # pragma: no cover
        self._description=str(val)
    #------------
    #-------------------------
    # Representation functions
    #-------------------------
    def type_rep(self): # pragma: no cover
        """Generate a representation of the object type

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains the representation of the object type
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtosk:foundations:foundationdesign:GravityFoundation"
        rep["name"] = self.name
        rep["description"] = self.description
        return rep
    def prop_rep(self, short = False, deep = True):

        """Generate a representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains a representation of the object properties
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtosk:foundations:foundationdesign:GravityFoundation"
        rep["name"] = self.name
        rep["description"] = self.description
        if self.is_set("base_lx"):
            rep["base_lx"] = self.base_lx
        if self.is_set("base_ly"):
            rep["base_ly"] = self.base_ly
        if self.is_set("base_radius"):
            rep["base_radius"] = self.base_radius
        if self.is_set("base_r1"):
            rep["base_r1"] = self.base_r1
        if self.is_set("base_r2"):
            rep["base_r2"] = self.base_r2
        if self.is_set("base_r3"):
            rep["base_r3"] = self.base_r3
        if self.is_set("base_alpha01"):
            rep["base_alpha01"] = self.base_alpha01
        if self.is_set("base_alpha12"):
            rep["base_alpha12"] = self.base_alpha12
        if self.is_set("base_alpha23"):
            rep["base_alpha23"] = self.base_alpha23
        if self.is_set("contact_points_mass"):
            rep["contact_points_mass"] = self.contact_points_mass
        if self.is_set("device_radius"):
            rep["device_radius"] = self.device_radius
        if self.is_set("u_shear_strenght"):
            rep["u_shear_strenght"] = self.u_shear_strenght
        if self.is_set("bearing_capacity"):
            rep["bearing_capacity"] = self.bearing_capacity
        if self.is_set("friction_resistance"):
            rep["friction_resistance"] = self.friction_resistance
        if self.is_set("n_iterations_max"):
            rep["n_iterations_max"] = self.n_iterations_max
        if self.is_set("int_friction_angle_d"):
            rep["int_friction_angle_d"] = self.int_friction_angle_d
        if self.is_set("u_shear_strenght_d"):
            rep["u_shear_strenght_d"] = self.u_shear_strenght_d
        if self.is_set("overturning_resitance_y"):
            rep["overturning_resitance_y"] = self.overturning_resitance_y
        if self.is_set("overturning_resitance_x"):
            rep["overturning_resitance_x"] = self.overturning_resitance_x
        if self.is_set("geometry_pref"):
            rep["geometry_pref"] = self.geometry_pref
        if self.is_set("ecc_x"):
            rep["ecc_x"] = self.ecc_x
        if self.is_set("ecc_y"):
            rep["ecc_y"] = self.ecc_y
        if self.is_set("ecc_r"):
            rep["ecc_r"] = self.ecc_r
        if self.is_set("eff_area"):
            rep["eff_area"] = self.eff_area
        if self.is_set("material"):
            rep["material"] = self.material
        if self.is_set("theta"):
            rep["theta"] = self.theta
        if self.is_set("eff_lx"):
            rep["eff_lx"] = self.eff_lx
        if self.is_set("eff_ly"):
            rep["eff_ly"] = self.eff_ly
        if self.is_set("thickness"):
            rep["thickness"] = self.thickness
        if self.is_set("material_density"):
            rep["material_density"] = self.material_density
        if self.is_set("qc"):
            rep["qc"] = self.qc
        if self.is_set("qq"):
            rep["qq"] = self.qq
        if self.is_set("qg"):
            rep["qg"] = self.qg
        if self.is_set("g_b"):
            rep["g_b"] = self.g_b
        if self.is_set("sliding_resistance"):
            rep["sliding_resistance"] = self.sliding_resistance
        if self.is_set("slope"):
            rep["slope"] = self.slope
        if self.is_set("found_weight"):
            rep["found_weight"] = self.found_weight
        if self.is_set("f_soil_surf"):
            rep["f_soil_surf"] = self.f_soil_surf
        if self.is_set("V_min_load"):
            rep["V_min_load"] = self.V_min_load
        if self.is_set("device_weight"):
            rep["device_weight"] = self.device_weight
        if self.is_set("thickness_max"):
            rep["thickness_max"] = self.thickness_max
        if self.is_set("Fn_bc"):
            rep["Fn_bc"] = self.Fn_bc
        if self.is_set("Fn_sr"):
            rep["Fn_sr"] = self.Fn_sr
        if self.is_set("Fh_sr"):
            rep["Fh_sr"] = self.Fh_sr
        if self.is_set("SF"):
            rep["SF"] = self.SF
        if self.is_set("stabm_r"):
            rep["stabm_r"] = self.stabm_r
        if self.is_set("stabm_x"):
            rep["stabm_x"] = self.stabm_x
        if self.is_set("stabm_y"):
            rep["stabm_y"] = self.stabm_y
        if self.is_set("Om_r"):
            rep["Om_r"] = self.Om_r
        if self.is_set("Om_x"):
            rep["Om_x"] = self.Om_x
        if self.is_set("Om_y"):
            rep["Om_y"] = self.Om_y
        if self.is_set("z_ap"):
            rep["z_ap"] = self.z_ap
        if self.is_set("moor_V_min"):
            rep["moor_V_min"] = self.moor_V_min
        if self.is_set("moor_V_max"):
            rep["moor_V_max"] = self.moor_V_max
        if self.is_set("moor_H_x"):
            rep["moor_H_x"] = self.moor_H_x
        if self.is_set("moor_H_y"):
            rep["moor_H_y"] = self.moor_H_y
        if self.is_set("moor_M_x"):
            rep["moor_M_x"] = self.moor_M_x
        if self.is_set("moor_M_y"):
            rep["moor_M_y"] = self.moor_M_y
        if self.is_set("seabed_connection_type"):
            rep["seabed_connection_type"] = self.seabed_connection_type
        if self.is_set("criteria_bearing_capacity"):
            rep["criteria_bearing_capacity"] = self.criteria_bearing_capacity
        if self.is_set("criteria_stability"):
            rep["criteria_stability"] = self.criteria_stability
        if self.is_set("criteria_sliding_resistance"):
            rep["criteria_sliding_resistance"] = self.criteria_sliding_resistance
        if self.is_set("name"):
            rep["name"] = self.name
        if self.is_set("description"):
            rep["description"] = self.description
        return rep
    #-------------------------
    # Save functions
    #-------------------------
    def json_rep(self, short=False, deep=True):

        """Generate a JSON representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`str`: string that contains a JSON representation of the object properties
        """

        return ( json.dumps(self.prop_rep(short=short, deep=deep),indent=4, separators=(",",": ")))
    def saveJSON(self, fileName=None):
        """Save the instance of the object to JSON format file

        Args:
            fileName (:obj:`str`, optional): Name of the JSON file, included extension. Defaults is None. If None, the name of the JSON file will be self.name.json. It can also contain an absolute or relative path.

        """

        if fileName==None:
            fileName=self.name + ".json"
        f=open(fileName, "w")
        f.write(self.json_rep())
        f.write("\n")
        f.close()

    def saveHDF5(self,filePath=None):
        """Save the instance of the object to HDF5 format file
        Args:
            filePath (:obj:`str`, optional): Name of the HDF5 file, included extension. Defaults is None. If None, the name of the HDF5 file will be "self.name".h5. It can also contain an absolute or relative path.
        """
        # Define filepath
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".h5"
            else:
                raise Exception("name is required for saving")
        # Open hdf5 file
        h = h5py.File(filePath,"w")
        group = h.create_group(self.name)
        # Save the object to the group
        self.saveToHDF5Handle(group)
        # Close file
        h.close()
        pass
    def saveToHDF5Handle(self, handle):
        """Save the properties of the object to the hdf5 handle.
        Args:
            handle (:obj:`h5py.Group`): Handle used to store the object properties
        """
        handle["base_lx"] = np.array([self.base_lx],dtype=float)
        handle["base_ly"] = np.array([self.base_ly],dtype=float)
        handle["base_radius"] = np.array([self.base_radius],dtype=float)
        handle["base_r1"] = np.array([self.base_r1],dtype=float)
        handle["base_r2"] = np.array([self.base_r2],dtype=float)
        handle["base_r3"] = np.array([self.base_r3],dtype=float)
        handle["base_alpha01"] = np.array([self.base_alpha01],dtype=float)
        handle["base_alpha12"] = np.array([self.base_alpha12],dtype=float)
        handle["base_alpha23"] = np.array([self.base_alpha23],dtype=float)
        handle["contact_points_mass"] = np.array([self.contact_points_mass],dtype=float)
        handle["device_radius"] = np.array([self.device_radius],dtype=float)
        handle["u_shear_strenght"] = np.array([self.u_shear_strenght],dtype=float)
        handle["bearing_capacity"] = np.array([self.bearing_capacity],dtype=float)
        handle["friction_resistance"] = np.array([self.friction_resistance],dtype=float)
        handle["n_iterations_max"] = np.array([self.n_iterations_max],dtype=float)
        handle["int_friction_angle_d"] = np.array([self.int_friction_angle_d],dtype=float)
        handle["u_shear_strenght_d"] = np.array([self.u_shear_strenght_d],dtype=float)
        handle["overturning_resitance_y"] = np.array([self.overturning_resitance_y],dtype=float)
        handle["overturning_resitance_x"] = np.array([self.overturning_resitance_x],dtype=float)
        ar = []
        ar.append(self.geometry_pref.encode("ascii"))
        handle["geometry_pref"] = np.asarray(ar)
        handle["ecc_x"] = np.array([self.ecc_x],dtype=float)
        handle["ecc_y"] = np.array([self.ecc_y],dtype=float)
        handle["ecc_r"] = np.array([self.ecc_r],dtype=float)
        handle["eff_area"] = np.array([self.eff_area],dtype=float)
        ar = []
        ar.append(self.material.encode("ascii"))
        handle["material"] = np.asarray(ar)
        handle["theta"] = np.array([self.theta],dtype=float)
        handle["eff_lx"] = np.array([self.eff_lx],dtype=float)
        handle["eff_ly"] = np.array([self.eff_ly],dtype=float)
        handle["thickness"] = np.array([self.thickness],dtype=float)
        handle["material_density"] = np.array([self.material_density],dtype=float)
        handle["qc"] = np.array([self.qc],dtype=float)
        handle["qq"] = np.array([self.qq],dtype=float)
        handle["qg"] = np.array([self.qg],dtype=float)
        handle["g_b"] = np.array([self.g_b],dtype=float)
        handle["sliding_resistance"] = np.array([self.sliding_resistance],dtype=float)
        handle["slope"] = np.array([self.slope],dtype=float)
        handle["found_weight"] = np.array([self.found_weight],dtype=float)
        handle["f_soil_surf"] = np.array([self.f_soil_surf],dtype=float)
        handle["V_min_load"] = np.array([self.V_min_load],dtype=float)
        handle["device_weight"] = np.array([self.device_weight],dtype=float)
        handle["thickness_max"] = np.array([self.thickness_max],dtype=float)
        handle["Fn_bc"] = np.array([self.Fn_bc],dtype=float)
        handle["Fn_sr"] = np.array([self.Fn_sr],dtype=float)
        handle["Fh_sr"] = np.array([self.Fh_sr],dtype=float)
        handle["SF"] = np.array([self.SF],dtype=float)
        handle["stabm_r"] = np.array([self.stabm_r],dtype=float)
        handle["stabm_x"] = np.array([self.stabm_x],dtype=float)
        handle["stabm_y"] = np.array([self.stabm_y],dtype=float)
        handle["Om_r"] = np.array([self.Om_r],dtype=float)
        handle["Om_x"] = np.array([self.Om_x],dtype=float)
        handle["Om_y"] = np.array([self.Om_y],dtype=float)
        handle["z_ap"] = np.array([self.z_ap],dtype=float)
        handle["moor_V_min"] = np.array([self.moor_V_min],dtype=float)
        handle["moor_V_max"] = np.array([self.moor_V_max],dtype=float)
        handle["moor_H_x"] = np.array([self.moor_H_x],dtype=float)
        handle["moor_H_y"] = np.array([self.moor_H_y],dtype=float)
        handle["moor_M_x"] = np.array([self.moor_M_x],dtype=float)
        handle["moor_M_y"] = np.array([self.moor_M_y],dtype=float)
        ar = []
        ar.append(self.seabed_connection_type.encode("ascii"))
        handle["seabed_connection_type"] = np.asarray(ar)
        handle["criteria_bearing_capacity"] = np.array([self.criteria_bearing_capacity],dtype=bool)
        handle["criteria_stability"] = np.array([self.criteria_stability],dtype=bool)
        handle["criteria_sliding_resistance"] = np.array([self.criteria_sliding_resistance],dtype=bool)
        ar = []
        ar.append(self.name.encode("ascii"))
        handle["name"] = np.asarray(ar)
        ar = []
        ar.append(self.description.encode("ascii"))
        handle["description"] = np.asarray(ar)
    #-------------------------
    # Load functions
    #-------------------------
    def loadJSON(self,name = None, filePath = None):
        """Load an instance of the object from JSON format file

        Args:
            name (:obj:`str`, optional): Name of the object to load.
            filePath (:obj:`str`, optional): Path of the JSON file to load. If None, the function looks for a file with name "name".json.

        The JSON file must contain a datastructure representing an instance of this object's class, as generated by e.g. the function :func:`~saveJSON`.

        """

        if not(name == None):
            self.name = name
        if (name == None) and not(filePath == None):
            self.name = ".".join(filePath.split(os.path.sep)[-1].split(".")[0:-1])
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".json"
            else:
                raise Exception("object needs name for loading.")
        if not(os.path.isfile(filePath)):
            raise Exception("file %s not found."%filePath)
        self._loadedItems = []
        f = open(filePath,"r")
        data = f.read()
        f.close()
        dd = json.loads(data)
        self.loadFromJSONDict(dd)
    def loadFromJSONDict(self, data):
        """Load an instance of the object from a dictionnary representing a JSON structure)

        Args:
            name (:obj:`collections.OrderedDict`): Dictionnary containing a JSON structure, as produced by e.g. :func:`json.loads`

        """

        varName = "base_lx"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "base_ly"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "base_radius"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "base_r1"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "base_r2"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "base_r3"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "base_alpha01"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "base_alpha12"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "base_alpha23"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "contact_points_mass"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "device_radius"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "u_shear_strenght"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "bearing_capacity"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "friction_resistance"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "n_iterations_max"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "int_friction_angle_d"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "u_shear_strenght_d"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "overturning_resitance_y"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "overturning_resitance_x"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "geometry_pref"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "ecc_x"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "ecc_y"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "ecc_r"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "eff_area"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "material"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "theta"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "eff_lx"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "eff_ly"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "thickness"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "material_density"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "qc"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "qq"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "qg"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "g_b"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "sliding_resistance"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "slope"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "found_weight"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "f_soil_surf"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "V_min_load"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "device_weight"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "thickness_max"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "Fn_bc"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "Fn_sr"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "Fh_sr"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "SF"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "stabm_r"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "stabm_x"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "stabm_y"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "Om_r"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "Om_x"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "Om_y"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "z_ap"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "moor_V_min"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "moor_V_max"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "moor_H_x"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "moor_H_y"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "moor_M_x"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "moor_M_y"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "seabed_connection_type"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "criteria_bearing_capacity"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "criteria_stability"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "criteria_sliding_resistance"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "description"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
    def loadHDF5(self,name=None, filePath=None):
        """Load an instance of the object from HDF5 format file
        Args:
            name (:obj:`str`, optional): Name of the object to load. Default is None.
            filePath (:obj:`str`, optional): Path of the HDF5 file to load. If None, the function looks for a file with name "self.name".h5.
        The HDF5 file must contain a datastructure representing an instance of this objects class, as generated by e.g. the function :func:`~saveHDF5`.
        """
        # Define filepath and name
        if not(name == None):
            self.name = name
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".h5"
            else:
                raise Exception("name is required for loading")
        # Open hdf5 file
        h = h5py.File(filePath,"r")
        group = h[self.name]
        # Save the object to the group
        self.loadFromHDF5Handle(group)
        # Close file
        h.close()
        pass
    def loadFromHDF5Handle(self, gr):
        """Load the properties of the object from a hdf5 handle.
        Args:
            gr (:obj:`h5py.Group`): Handle used to read the object properties from
        """
        if ("base_lx" in list(gr.keys())):
            self.base_lx = gr["base_lx"][0]
        if ("base_ly" in list(gr.keys())):
            self.base_ly = gr["base_ly"][0]
        if ("base_radius" in list(gr.keys())):
            self.base_radius = gr["base_radius"][0]
        if ("base_r1" in list(gr.keys())):
            self.base_r1 = gr["base_r1"][0]
        if ("base_r2" in list(gr.keys())):
            self.base_r2 = gr["base_r2"][0]
        if ("base_r3" in list(gr.keys())):
            self.base_r3 = gr["base_r3"][0]
        if ("base_alpha01" in list(gr.keys())):
            self.base_alpha01 = gr["base_alpha01"][0]
        if ("base_alpha12" in list(gr.keys())):
            self.base_alpha12 = gr["base_alpha12"][0]
        if ("base_alpha23" in list(gr.keys())):
            self.base_alpha23 = gr["base_alpha23"][0]
        if ("contact_points_mass" in list(gr.keys())):
            self.contact_points_mass = gr["contact_points_mass"][0]
        if ("device_radius" in list(gr.keys())):
            self.device_radius = gr["device_radius"][0]
        if ("u_shear_strenght" in list(gr.keys())):
            self.u_shear_strenght = gr["u_shear_strenght"][0]
        if ("bearing_capacity" in list(gr.keys())):
            self.bearing_capacity = gr["bearing_capacity"][0]
        if ("friction_resistance" in list(gr.keys())):
            self.friction_resistance = gr["friction_resistance"][0]
        if ("n_iterations_max" in list(gr.keys())):
            self.n_iterations_max = gr["n_iterations_max"][0]
        if ("int_friction_angle_d" in list(gr.keys())):
            self.int_friction_angle_d = gr["int_friction_angle_d"][0]
        if ("u_shear_strenght_d" in list(gr.keys())):
            self.u_shear_strenght_d = gr["u_shear_strenght_d"][0]
        if ("overturning_resitance_y" in list(gr.keys())):
            self.overturning_resitance_y = gr["overturning_resitance_y"][0]
        if ("overturning_resitance_x" in list(gr.keys())):
            self.overturning_resitance_x = gr["overturning_resitance_x"][0]
        if ("geometry_pref" in list(gr.keys())):
            self.geometry_pref = gr["geometry_pref"][0].decode("ascii")
        if ("ecc_x" in list(gr.keys())):
            self.ecc_x = gr["ecc_x"][0]
        if ("ecc_y" in list(gr.keys())):
            self.ecc_y = gr["ecc_y"][0]
        if ("ecc_r" in list(gr.keys())):
            self.ecc_r = gr["ecc_r"][0]
        if ("eff_area" in list(gr.keys())):
            self.eff_area = gr["eff_area"][0]
        if ("material" in list(gr.keys())):
            self.material = gr["material"][0].decode("ascii")
        if ("theta" in list(gr.keys())):
            self.theta = gr["theta"][0]
        if ("eff_lx" in list(gr.keys())):
            self.eff_lx = gr["eff_lx"][0]
        if ("eff_ly" in list(gr.keys())):
            self.eff_ly = gr["eff_ly"][0]
        if ("thickness" in list(gr.keys())):
            self.thickness = gr["thickness"][0]
        if ("material_density" in list(gr.keys())):
            self.material_density = gr["material_density"][0]
        if ("qc" in list(gr.keys())):
            self.qc = gr["qc"][0]
        if ("qq" in list(gr.keys())):
            self.qq = gr["qq"][0]
        if ("qg" in list(gr.keys())):
            self.qg = gr["qg"][0]
        if ("g_b" in list(gr.keys())):
            self.g_b = gr["g_b"][0]
        if ("sliding_resistance" in list(gr.keys())):
            self.sliding_resistance = gr["sliding_resistance"][0]
        if ("slope" in list(gr.keys())):
            self.slope = gr["slope"][0]
        if ("found_weight" in list(gr.keys())):
            self.found_weight = gr["found_weight"][0]
        if ("f_soil_surf" in list(gr.keys())):
            self.f_soil_surf = gr["f_soil_surf"][0]
        if ("V_min_load" in list(gr.keys())):
            self.V_min_load = gr["V_min_load"][0]
        if ("device_weight" in list(gr.keys())):
            self.device_weight = gr["device_weight"][0]
        if ("thickness_max" in list(gr.keys())):
            self.thickness_max = gr["thickness_max"][0]
        if ("Fn_bc" in list(gr.keys())):
            self.Fn_bc = gr["Fn_bc"][0]
        if ("Fn_sr" in list(gr.keys())):
            self.Fn_sr = gr["Fn_sr"][0]
        if ("Fh_sr" in list(gr.keys())):
            self.Fh_sr = gr["Fh_sr"][0]
        if ("SF" in list(gr.keys())):
            self.SF = gr["SF"][0]
        if ("stabm_r" in list(gr.keys())):
            self.stabm_r = gr["stabm_r"][0]
        if ("stabm_x" in list(gr.keys())):
            self.stabm_x = gr["stabm_x"][0]
        if ("stabm_y" in list(gr.keys())):
            self.stabm_y = gr["stabm_y"][0]
        if ("Om_r" in list(gr.keys())):
            self.Om_r = gr["Om_r"][0]
        if ("Om_x" in list(gr.keys())):
            self.Om_x = gr["Om_x"][0]
        if ("Om_y" in list(gr.keys())):
            self.Om_y = gr["Om_y"][0]
        if ("z_ap" in list(gr.keys())):
            self.z_ap = gr["z_ap"][0]
        if ("moor_V_min" in list(gr.keys())):
            self.moor_V_min = gr["moor_V_min"][0]
        if ("moor_V_max" in list(gr.keys())):
            self.moor_V_max = gr["moor_V_max"][0]
        if ("moor_H_x" in list(gr.keys())):
            self.moor_H_x = gr["moor_H_x"][0]
        if ("moor_H_y" in list(gr.keys())):
            self.moor_H_y = gr["moor_H_y"][0]
        if ("moor_M_x" in list(gr.keys())):
            self.moor_M_x = gr["moor_M_x"][0]
        if ("moor_M_y" in list(gr.keys())):
            self.moor_M_y = gr["moor_M_y"][0]
        if ("seabed_connection_type" in list(gr.keys())):
            self.seabed_connection_type = gr["seabed_connection_type"][0].decode("ascii")
        if ("criteria_bearing_capacity" in list(gr.keys())):
            self.criteria_bearing_capacity = gr["criteria_bearing_capacity"][0]
        if ("criteria_stability" in list(gr.keys())):
            self.criteria_stability = gr["criteria_stability"][0]
        if ("criteria_sliding_resistance" in list(gr.keys())):
            self.criteria_sliding_resistance = gr["criteria_sliding_resistance"][0]
        if ("name" in list(gr.keys())):
            self.name = gr["name"][0].decode("ascii")
        if ("description" in list(gr.keys())):
            self.description = gr["description"][0].decode("ascii")
    #------------------------
    # is_set function
    #------------------------
    def is_set(self, varName): # pragma: no cover

        """Check if a given property of the object is set

        Args:
            varName (:obj:`str`): name of the property to check

        Returns:
            :obj:`bool`: True if the property is set, else False
        """

        if (isinstance(getattr(self,varName),list) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (isinstance(getattr(self,varName),np.ndarray) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (getattr(self,varName) != None):
            return True
        return False
    #------------------------
