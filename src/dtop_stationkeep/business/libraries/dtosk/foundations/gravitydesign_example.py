# This is the Station Keeping module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Neil Luxcey, Nicolas Michelet, Rocio Isorna
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import collections 
import numpy as np 
import os
try: 
    import json 
except: 
    print('WARNING: json is not installed.') 
from foundationdesign.GravityFoundation import GravityFoundation
#------------------------------------------------------------------------------------------------
## Gravity foundation design test
#
#-------------------------------------------------------------------------------------------------


gravity = GravityFoundation()

#inputs from user or other modules
gravity.seabed_connection_type ='fixed'
gravity.device_geometry = {'rectangular', 2, 3}
gravity.slope = 3
gravity.int_friction_angle = 35
gravity.u_shear_strenght = 80000
gravity.g_b = 10

#def desing_input_from_mooring (self,V_min, V_max, H_x, H_y, M_x, M_y, z_ap):
update_inputs_mooring= gravity.desing_input_from_mooring(1, 2, 3, 4, 5, 6, 0.2)
design = gravity.design()
esa = gravity.outputs()


print(design)
print(outputs)

gravity.saveJSON("test_gravity")

