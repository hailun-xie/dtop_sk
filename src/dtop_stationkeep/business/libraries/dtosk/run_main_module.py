# This is the Station Keeping module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Neil Luxcey, Nicolas Michelet, Rocio Isorna
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


import numpy as np 
import os
from .run_analysis_cplx_module import run_analysis_cplx
from .outputs.Project import Project
from .results.BodyResults import BodyResults
import copy
import logging
from logging.handlers import RotatingFileHandler
 
def create_logger(ProjectName, log_to_file = True, log_to_stream = True):

	# création de l'objet logger qui va nous servir à écrire dans les logs
	logger = logging.getLogger()
	# on met le niveau du logger à DEBUG, comme ça il écrit tout
	logger.setLevel(logging.DEBUG)
	
	# création d'un formateur qui va ajouter le temps, le niveau
	# de chaque message quand on écrira un message dans le log
	formatter = logging.Formatter('%(asctime)s :: %(levelname)s :: %(message)s')

	if log_to_file:
		# création d'un handler qui va rediriger une écriture du log vers
		# un fichier en mode 'append', avec 1 backup et une taille max de 1Mo
		logfilefullpath = os.path.join('./storage', ProjectName + '/sk.log')
		file_handler = RotatingFileHandler(logfilefullpath, 'a', 100000, 1)
		# on lui met le niveau sur DEBUG, on lui dit qu'il doit utiliser le formateur
		# créé précédement et on ajoute ce handler au logger
		file_handler.setLevel(logging.DEBUG)
		file_handler.setFormatter(formatter)
		logger.addHandler(file_handler)
	
	if log_to_stream:
		# création d'un second handler qui va rediriger chaque écriture de log
		# sur la console
		stream_handler = logging.StreamHandler()
		stream_handler.setLevel(logging.DEBUG)
		logger.addHandler(stream_handler)

def run_main(inputs, ProjectId, log_to_file = True, log_to_stream = True):
	# inputs is of type Inputs()
	create_logger(ProjectId, log_to_file = log_to_file, log_to_stream = log_to_stream)
	logger = logging.getLogger()
	logger.info('*****************')
	logger.info('Start main module')
	logger.info('*****************')
	try:

		# Create project
		project = Project()
		project.projectID=inputs.name	
		project.name=inputs.name	

		# Run analysis
		[farm, project.hierarchy, project.bom, project.environmental_impact, project.design_assessment.uls_environments, uls_weather_directions, project.design_assessment.fls_environments, project.representation] = run_analysis_cplx(inputs.sk_inputs,inputs.catalogue_lines,inputs.catalogue_anchors)

		# Store weather directions used for the uls analysis in [deg]
		project.design_assessment.uls_weather_directions = copy.deepcopy(uls_weather_directions)*180.0/np.pi

		# Populate results from ULS, FLS and Design analyses and save mooring system as map++ file
		[project.design_assessment.devices,project.design_assessment.master_structure,project.design_assessment.master_structure_is_present,project.design_assessment.substation,project.design_assessment.substation_is_present]=populate_design_assessment_results(farm) 

		logger.info('****************************')
		logger.info('End main module successfully')
		logger.info('****************************')

	except Exception as e:

		# Log the complete error stack trace message in the log
		logger.exception(e)
		logger.info('**************************')
		logger.info('End main module with error')
		logger.info('**************************')
		logger.handlers.clear()
		del logger
		raise Exception('Main module failed. Check the log file.')

	logger.handlers.clear()
	del logger
  

	return project


def populate_design_assessment_results(farm):

	# Populate the results for the devices
	devices = []
	master_structure = None
	substation = []
	master_structure_is_present = False
	substation_is_present = False
	c=-1
	for idx in range(0,len(farm.body_array)):
		is_substation = False
		if len(farm.body_array[idx].name)>=10:
			if farm.body_array[idx].name[0:10]=='substation':
				is_substation = True
		if is_substation:
			substation.append(BodyResults())
			substation[-1]=copy.deepcopy(farm.body_array[idx].results)
			substation_is_present = True
		else:
			c=c+1
			devices.append(BodyResults())
			devices[c]=copy.deepcopy(farm.body_array[idx].results)
		# Save mooring system as map++ file and custom mooring input
		folder_path = farm.body_array[idx].mooring_design_criteria.mooring_output_folder_path
		if not folder_path=="":
			if farm.body_array[idx].positioning_type == 'moored':
				filepath_in = os.path.join(folder_path,"input_" + farm.body_array[idx].name)
				# Write custom input file
				reference_system = farm.body_array[idx].mooring_design_criteria.mooring_output_reference_system
				farm.body_array[idx].mooring_system_force.save_as_custom_mooring_input(reference_system,farm.body_array[idx].initial_position[0:3],str(filepath_in + ".json"))
				# Write map++ file
				farm.body_array[idx].mooring_system_force.write_to_map_input_file(filepath_in + ".map")

		
	# Populate the results for the master structure, if present
	if farm.master_structure_is_present:
		master_structure=BodyResults()
		master_structure=copy.deepcopy(farm.master_structure.results)
		folder_path = farm.master_structure.mooring_design_criteria.mooring_output_folder_path
		master_structure_is_present = True
		if not folder_path=="":
			filepath_in = os.path.join(folder_path,"input_master_structure")
			# Write custom mooring input file
			reference_system = farm.master_structure.mooring_design_criteria.mooring_output_reference_system
			farm.master_structure.mooring_system_force.save_as_custom_mooring_input(reference_system,farm.master_structure.initial_position[0:3],str(filepath_in + ".json"))
			# Write map++ file
			farm.master_structure.mooring_system_force.write_to_map_input_file(filepath_in + ".map")

	return devices,master_structure,master_structure_is_present,substation,substation_is_present
	
