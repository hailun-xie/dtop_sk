# This is the Station Keeping module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Neil Luxcey, Nicolas Michelet, Rocio Isorna
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import collections
import numpy as np
import os
try:
    import json
except:
    print('WARNING: json is not installed.')
from ..environment import Environment
#------------------------------------
# @ USER DEFINED IMPORTS START
# @ USER DEFINED IMPORTS END
#------------------------------------

class LineTension():

    #------------
    # Constructor
    #------------
    def __init__(self,name=None):
        self._environments=[]
        self._line_segment_design_id=[]
        self._line_segment_mbl=np.zeros(shape=(1), dtype=float)
        self._line_segment_max_tension=np.zeros(shape=(1,1), dtype=float)
        self._line_segment_mean_tension=np.zeros(shape=(1,1), dtype=float)
        self._name='none'
        self._description=''
        if not(name == None):
            self._name = name

#------------------------------------
# @ USER DEFINED PROPERTIES START
# @ USER DEFINED PROPERTIES END
#------------------------------------

#------------------------------------
# @ USER DEFINED METHODS START
# @ USER DEFINED METHODS END
#------------------------------------

    #------------
    # Get functions
    #------------
    @ property
    def environments(self): # pragma: no cover
        return self._environments
    #------------
    @ property
    def line_segment_design_id(self): # pragma: no cover
        return self._line_segment_design_id
    #------------
    @ property
    def line_segment_mbl(self): # pragma: no cover
        return self._line_segment_mbl
    #------------
    @ property
    def line_segment_max_tension(self): # pragma: no cover
        return self._line_segment_max_tension
    #------------
    @ property
    def line_segment_mean_tension(self): # pragma: no cover
        return self._line_segment_mean_tension
    #------------
    @ property
    def name(self): # pragma: no cover
        return self._name
    #------------
    @ property
    def description(self): # pragma: no cover
        return self._description
    #------------
    #------------
    # Set functions
    #------------
    @ environments.setter
    def environments(self,val): # pragma: no cover
        self._environments=val
    #------------
    @ line_segment_design_id.setter
    def line_segment_design_id(self,val): # pragma: no cover
        self._line_segment_design_id=val
    #------------
    @ line_segment_mbl.setter
    def line_segment_mbl(self,val): # pragma: no cover
        self._line_segment_mbl=val
    #------------
    @ line_segment_max_tension.setter
    def line_segment_max_tension(self,val): # pragma: no cover
        self._line_segment_max_tension=val
    #------------
    @ line_segment_mean_tension.setter
    def line_segment_mean_tension(self,val): # pragma: no cover
        self._line_segment_mean_tension=val
    #------------
    @ name.setter
    def name(self,val): # pragma: no cover
        self._name=str(val)
    #------------
    @ description.setter
    def description(self,val): # pragma: no cover
        self._description=str(val)
    #------------
    #-------------------------
    # Representation functions
    #-------------------------
    def type_rep(self): # pragma: no cover
        rep = collections.OrderedDict()
        rep["__type__"] = "dtosk:outputs:LineTension"
        rep["name"] = self.name
        rep["description"] = self.description
        return rep
    def prop_rep(self, short = False, deep = True):
        rep = collections.OrderedDict()
        rep["__type__"] = "dtosk:outputs:LineTension"
        rep["name"] = self.name
        rep["description"] = self.description
        if self.is_set("environments"):
            rep["environments"] = []
            for i in range(0,len(self.environments)):
                if (short and not(deep)):
                    itemType = self.environments[i].type_rep()
                    rep["environments"].append( itemType )
                else:
                    rep["environments"].append( self.environments[i].prop_rep(short, deep) )
        if self.is_set("line_segment_design_id"):
            if short:
                rep["line_segment_design_id"] = str(len(self.line_segment_design_id))
            else:
                rep["line_segment_design_id"] = self.line_segment_design_id
        if self.is_set("line_segment_mbl"):
            if (short):
                rep["line_segment_mbl"] = str(self.line_segment_mbl.shape)
            else:
                try:
                    rep["line_segment_mbl"] = self.line_segment_mbl.tolist()
                except:
                    rep["line_segment_mbl"] = self.line_segment_mbl
        if self.is_set("line_segment_max_tension"):
            if (short):
                rep["line_segment_max_tension"] = str(self.line_segment_max_tension.shape)
            else:
                try:
                    rep["line_segment_max_tension"] = self.line_segment_max_tension.tolist()
                except:
                    rep["line_segment_max_tension"] = self.line_segment_max_tension
        if self.is_set("line_segment_mean_tension"):
            if (short):
                rep["line_segment_mean_tension"] = str(self.line_segment_mean_tension.shape)
            else:
                try:
                    rep["line_segment_mean_tension"] = self.line_segment_mean_tension.tolist()
                except:
                    rep["line_segment_mean_tension"] = self.line_segment_mean_tension
        if self.is_set("name"):
            rep["name"] = self.name
        if self.is_set("description"):
            rep["description"] = self.description
        return rep
    #-------------------------
    # Save functions
    #-------------------------
    def json_rep(self, short=False, deep=True):
        return ( json.dumps(self.prop_rep(short=short, deep=deep),indent=4, separators=(",",": ")))
    def saveJSON(self, fileName=None):
        if fileName==None:
            fileName=self.name + ".json"
        f=open(fileName, "w")
        f.write(self.json_rep())
        f.close()
    #-------------------------
    # Load functions
    #-------------------------
    def loadJSON(self,name = None, filePath = None):
        if not(name == None):
            self.name = name
        if (name == None) and not(filePath == None):
            self.name = ".".join(filePath.split(os.path.sep)[-1].split(".")[0:-1])
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".json"
            else:
                raise Exception("object needs name for loading.")
        if not(os.path.isfile(filePath)):
            raise Exception("file %s not found."%filePath)
        self._loadedItems = []
        f = open(filePath,"r")
        data = f.read()
        f.close()
        dd = json.loads(data)
        self.loadFromJSONDict(dd)
    def loadFromJSONDict(self, data):
        varName = "environments"
        try :
            if data[varName] != None:
                self.environments=[]
                for i in range(0,len(data[varName])):
                    self.environments.append(Environment.Environment())
                    self.environments[i].loadFromJSONDict(data[varName][i])
        except :
            pass
        varName = "line_segment_design_id"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "line_segment_mbl"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "line_segment_max_tension"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "line_segment_mean_tension"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "description"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
    #------------------------
    # is_set function
    #------------------------
    def is_set(self, varName): # pragma: no cover
        if (isinstance(getattr(self,varName),list) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (isinstance(getattr(self,varName),np.ndarray) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (getattr(self,varName) != None):
            return True
        return False
    #------------------------
