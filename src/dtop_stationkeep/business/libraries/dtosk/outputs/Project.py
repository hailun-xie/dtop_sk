# This is the Station Keeping module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Neil Luxcey, Nicolas Michelet, Rocio Isorna
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import collections
import numpy as np
import os
import json
import h5py
from . import Hierarchy
from . import BoM
from . import EnvironmentalImpactResults
from . import DesignAssessment
from ..dr import DigitalRepresentation
#------------------------------------
# @ USER DEFINED IMPORTS START
# @ USER DEFINED IMPORTS END
#------------------------------------

class Project():

    """Stationkeeping project results
    """

    #------------
    # Constructor
    #------------
    def __init__(self,name=None):
#------------------------------------
# @ USER DEFINED DESCRIPTION START
# @ USER DEFINED DESCRIPTION END
#------------------------------------

        self._projectID=''
        self._hierarchy=Hierarchy.Hierarchy()
        self._hierarchy.description = 'hierarchy description of the station keeping system'
        self._bom=BoM.BoM()
        self._bom.description = 'bill of materials from the station keeping system'
        self._environmental_impact=EnvironmentalImpactResults.EnvironmentalImpactResults()
        self._environmental_impact.description = 'environmental impact of the station keeping system'
        self._design_assessment=DesignAssessment.DesignAssessment()
        self._design_assessment.description = 'design assessment of the station keeping system'
        self._representation=DigitalRepresentation.DigitalRepresentation()
        self._representation.description = 'digital representation of the station keeping system'
        self._name='none'
        self._description=''
        if not(name == None): # pragma: no cover
            self._name = name

#------------------------------------
# @ USER DEFINED PROPERTIES START
# @ USER DEFINED PROPERTIES END
#------------------------------------

#------------------------------------
# @ USER DEFINED METHODS START
# @ USER DEFINED METHODS END
#------------------------------------

    #------------
    # Get functions
    #------------
    @ property
    def projectID(self): # pragma: no cover
        """str: project id
        """
        return self._projectID
    #------------
    @ property
    def hierarchy(self): # pragma: no cover
        """:obj:`~.Hierarchy.Hierarchy`: hierarchy description of the station keeping system
        """
        return self._hierarchy
    #------------
    @ property
    def bom(self): # pragma: no cover
        """:obj:`~.BoM.BoM`: bill of materials from the station keeping system
        """
        return self._bom
    #------------
    @ property
    def environmental_impact(self): # pragma: no cover
        """:obj:`~.EnvironmentalImpactResults.EnvironmentalImpactResults`: environmental impact of the station keeping system
        """
        return self._environmental_impact
    #------------
    @ property
    def design_assessment(self): # pragma: no cover
        """:obj:`~.DesignAssessment.DesignAssessment`: design assessment of the station keeping system
        """
        return self._design_assessment
    #------------
    @ property
    def representation(self): # pragma: no cover
        """:obj:`~.DigitalRepresentation.DigitalRepresentation`: digital representation of the station keeping system
        """
        return self._representation
    #------------
    @ property
    def name(self): # pragma: no cover
        """str: name of the instance object
        """
        return self._name
    #------------
    @ property
    def description(self): # pragma: no cover
        """str: description of the instance object
        """
        return self._description
    #------------
    #------------
    # Set functions
    #------------
    @ projectID.setter
    def projectID(self,val): # pragma: no cover
        self._projectID=str(val)
    #------------
    @ hierarchy.setter
    def hierarchy(self,val): # pragma: no cover
        self._hierarchy=val
    #------------
    @ bom.setter
    def bom(self,val): # pragma: no cover
        self._bom=val
    #------------
    @ environmental_impact.setter
    def environmental_impact(self,val): # pragma: no cover
        self._environmental_impact=val
    #------------
    @ design_assessment.setter
    def design_assessment(self,val): # pragma: no cover
        self._design_assessment=val
    #------------
    @ representation.setter
    def representation(self,val): # pragma: no cover
        self._representation=val
    #------------
    @ name.setter
    def name(self,val): # pragma: no cover
        self._name=str(val)
    #------------
    @ description.setter
    def description(self,val): # pragma: no cover
        self._description=str(val)
    #------------
    #-------------------------
    # Representation functions
    #-------------------------
    def type_rep(self): # pragma: no cover
        """Generate a representation of the object type

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains the representation of the object type
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtosk:outputs:Project"
        rep["name"] = self.name
        rep["description"] = self.description
        return rep
    def prop_rep(self, short = False, deep = True):

        """Generate a representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains a representation of the object properties
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtosk:outputs:Project"
        rep["name"] = self.name
        rep["description"] = self.description
        if self.is_set("projectID"):
            rep["projectID"] = self.projectID
        if self.is_set("hierarchy"):
            if (short and not(deep)):
                rep["hierarchy"] = self.hierarchy.type_rep()
            else:
                rep["hierarchy"] = self.hierarchy.prop_rep(short, deep)
        if self.is_set("bom"):
            if (short and not(deep)):
                rep["bom"] = self.bom.type_rep()
            else:
                rep["bom"] = self.bom.prop_rep(short, deep)
        if self.is_set("environmental_impact"):
            if (short and not(deep)):
                rep["environmental_impact"] = self.environmental_impact.type_rep()
            else:
                rep["environmental_impact"] = self.environmental_impact.prop_rep(short, deep)
        if self.is_set("design_assessment"):
            if (short and not(deep)):
                rep["design_assessment"] = self.design_assessment.type_rep()
            else:
                rep["design_assessment"] = self.design_assessment.prop_rep(short, deep)
        if self.is_set("representation"):
            if (short and not(deep)):
                rep["representation"] = self.representation.type_rep()
            else:
                rep["representation"] = self.representation.prop_rep(short, deep)
        if self.is_set("name"):
            rep["name"] = self.name
        if self.is_set("description"):
            rep["description"] = self.description
        return rep
    #-------------------------
    # Save functions
    #-------------------------
    def json_rep(self, short=False, deep=True):

        """Generate a JSON representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`str`: string that contains a JSON representation of the object properties
        """

        return ( json.dumps(self.prop_rep(short=short, deep=deep),indent=4, separators=(",",": ")))
    def saveJSON(self, fileName=None):
        """Save the instance of the object to JSON format file

        Args:
            fileName (:obj:`str`, optional): Name of the JSON file, included extension. Defaults is None. If None, the name of the JSON file will be self.name.json. It can also contain an absolute or relative path.

        """

        if fileName==None:
            fileName=self.name + ".json"
        f=open(fileName, "w")
        f.write(self.json_rep())
        f.write("\n")
        f.close()

    def saveHDF5(self,filePath=None):
        """Save the instance of the object to HDF5 format file
        Args:
            filePath (:obj:`str`, optional): Name of the HDF5 file, included extension. Defaults is None. If None, the name of the HDF5 file will be "self.name".h5. It can also contain an absolute or relative path.
        """
        # Define filepath
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".h5"
            else:
                raise Exception("name is required for saving")
        # Open hdf5 file
        h = h5py.File(filePath,"w")
        group = h.create_group(self.name)
        # Save the object to the group
        self.saveToHDF5Handle(group)
        # Close file
        h.close()
        pass
    def saveToHDF5Handle(self, handle):
        """Save the properties of the object to the hdf5 handle.
        Args:
            handle (:obj:`h5py.Group`): Handle used to store the object properties
        """
        ar = []
        ar.append(self.projectID.encode("ascii"))
        handle["projectID"] = np.asarray(ar)
        subgroup = handle.create_group("hierarchy")
        self.hierarchy.saveToHDF5Handle(subgroup)
        subgroup = handle.create_group("bom")
        self.bom.saveToHDF5Handle(subgroup)
        subgroup = handle.create_group("environmental_impact")
        self.environmental_impact.saveToHDF5Handle(subgroup)
        subgroup = handle.create_group("design_assessment")
        self.design_assessment.saveToHDF5Handle(subgroup)
        subgroup = handle.create_group("representation")
        self.representation.saveToHDF5Handle(subgroup)
        ar = []
        ar.append(self.name.encode("ascii"))
        handle["name"] = np.asarray(ar)
        ar = []
        ar.append(self.description.encode("ascii"))
        handle["description"] = np.asarray(ar)
    #-------------------------
    # Load functions
    #-------------------------
    def loadJSON(self,name = None, filePath = None):
        """Load an instance of the object from JSON format file

        Args:
            name (:obj:`str`, optional): Name of the object to load.
            filePath (:obj:`str`, optional): Path of the JSON file to load. If None, the function looks for a file with name "name".json.

        The JSON file must contain a datastructure representing an instance of this object's class, as generated by e.g. the function :func:`~saveJSON`.

        """

        if not(name == None):
            self.name = name
        if (name == None) and not(filePath == None):
            self.name = ".".join(filePath.split(os.path.sep)[-1].split(".")[0:-1])
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".json"
            else:
                raise Exception("object needs name for loading.")
        if not(os.path.isfile(filePath)):
            raise Exception("file %s not found."%filePath)
        self._loadedItems = []
        f = open(filePath,"r")
        data = f.read()
        f.close()
        dd = json.loads(data)
        self.loadFromJSONDict(dd)
    def loadFromJSONDict(self, data):
        """Load an instance of the object from a dictionnary representing a JSON structure)

        Args:
            name (:obj:`collections.OrderedDict`): Dictionnary containing a JSON structure, as produced by e.g. :func:`json.loads`

        """

        varName = "projectID"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "hierarchy"
        try :
            if data[varName] != None:
                self.hierarchy=Hierarchy.Hierarchy()
                self.hierarchy.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "bom"
        try :
            if data[varName] != None:
                self.bom=BoM.BoM()
                self.bom.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "environmental_impact"
        try :
            if data[varName] != None:
                self.environmental_impact=EnvironmentalImpactResults.EnvironmentalImpactResults()
                self.environmental_impact.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "design_assessment"
        try :
            if data[varName] != None:
                self.design_assessment=DesignAssessment.DesignAssessment()
                self.design_assessment.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "representation"
        try :
            if data[varName] != None:
                self.representation=DigitalRepresentation.DigitalRepresentation()
                self.representation.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "description"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
    def loadHDF5(self,name=None, filePath=None):
        """Load an instance of the object from HDF5 format file
        Args:
            name (:obj:`str`, optional): Name of the object to load. Default is None.
            filePath (:obj:`str`, optional): Path of the HDF5 file to load. If None, the function looks for a file with name "self.name".h5.
        The HDF5 file must contain a datastructure representing an instance of this objects class, as generated by e.g. the function :func:`~saveHDF5`.
        """
        # Define filepath and name
        if not(name == None):
            self.name = name
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".h5"
            else:
                raise Exception("name is required for loading")
        # Open hdf5 file
        h = h5py.File(filePath,"r")
        group = h[self.name]
        # Save the object to the group
        self.loadFromHDF5Handle(group)
        # Close file
        h.close()
        pass
    def loadFromHDF5Handle(self, gr):
        """Load the properties of the object from a hdf5 handle.
        Args:
            gr (:obj:`h5py.Group`): Handle used to read the object properties from
        """
        if ("projectID" in list(gr.keys())):
            self.projectID = gr["projectID"][0].decode("ascii")
        if ("hierarchy" in list(gr.keys())):
            subgroup = gr["hierarchy"]
            self.hierarchy.loadFromHDF5Handle(subgroup)
        if ("bom" in list(gr.keys())):
            subgroup = gr["bom"]
            self.bom.loadFromHDF5Handle(subgroup)
        if ("environmental_impact" in list(gr.keys())):
            subgroup = gr["environmental_impact"]
            self.environmental_impact.loadFromHDF5Handle(subgroup)
        if ("design_assessment" in list(gr.keys())):
            subgroup = gr["design_assessment"]
            self.design_assessment.loadFromHDF5Handle(subgroup)
        if ("representation" in list(gr.keys())):
            subgroup = gr["representation"]
            self.representation.loadFromHDF5Handle(subgroup)
        if ("name" in list(gr.keys())):
            self.name = gr["name"][0].decode("ascii")
        if ("description" in list(gr.keys())):
            self.description = gr["description"][0].decode("ascii")
    #------------------------
    # is_set function
    #------------------------
    def is_set(self, varName): # pragma: no cover

        """Check if a given property of the object is set

        Args:
            varName (:obj:`str`): name of the property to check

        Returns:
            :obj:`bool`: True if the property is set, else False
        """

        if (isinstance(getattr(self,varName),list) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (isinstance(getattr(self,varName),np.ndarray) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (getattr(self,varName) != None):
            return True
        return False
    #------------------------
