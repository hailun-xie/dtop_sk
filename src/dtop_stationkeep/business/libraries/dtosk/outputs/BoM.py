# This is the Station Keeping module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Neil Luxcey, Nicolas Michelet, Rocio Isorna
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import collections
import numpy as np
import os
import json
import h5py
#------------------------------------
# @ USER DEFINED IMPORTS START
from ..object_list_module import get_object_from_list
import pandas as pd
# @ USER DEFINED IMPORTS END
#------------------------------------

class BoM():

    """BoM from the SK-module
    """

    #------------
    # Constructor
    #------------
    def __init__(self,name=None):
#------------------------------------
# @ USER DEFINED DESCRIPTION START
# @ USER DEFINED DESCRIPTION END
#------------------------------------

        self._module_name=[]
        self._catalogue_ID=[]
        self._product_name=[]
        self._quantity=np.zeros(shape=(1), dtype=float)
        self._unit=[]
        self._unit_cost=np.zeros(shape=(1), dtype=float)
        self._total_cost=np.zeros(shape=(1), dtype=float)
        self._name='none'
        self._description=''
        if not(name == None): # pragma: no cover
            self._name = name

#------------------------------------
# @ USER DEFINED PROPERTIES START
# @ USER DEFINED PROPERTIES END
#------------------------------------

#------------------------------------
# @ USER DEFINED METHODS START
    def populate(self,farm):

        # Set bom name
        self.name = 'SK-BoM'

        # If master structure is present, register its stationkeeping system
        if farm.master_structure_is_present:

            # Register line segments
            self.register_line_segment(farm.master_structure.mooring_system_force)

            # Register anchors or foundations
            self.register_foundations(farm.master_structure.results)

            # Register ancillaries
            self.register_ancillaries(farm.master_structure.mooring_design_criteria.ancillaries)

        # Loop over each device
        for ibdy in range(0,len(farm.body_array)):

            # Register line segments
            self.register_line_segment(farm.body_array[ibdy].mooring_system_force)

            # Register anchors or foundations
            self.register_foundations(farm.body_array[ibdy].results)

            # Register ancillaries
            self.register_ancillaries(farm.body_array[ibdy].mooring_design_criteria.ancillaries)

    def register_ancillaries(self,ancillaries):

        for idx in range(0,len(ancillaries.anc_list_name)):

            # Check if the foundation object is already registered in the BoM. If not, register it.
            if not self.is_object_registered(ancillaries.anc_list_name[idx]):

                # Register bom line
                self.add_bom_line('none',ancillaries.anc_list_name[idx],'unit',ancillaries.anc_list_cost[idx])

            # Increment the bom line corresponding to the type of anchor
            self.increment_bom_line(ancillaries.anc_list_name[idx],1)

    def register_foundations(self,body_results):
        n_anchors = body_results.design_results.anchor_and_foundation.n_anchors
        if n_anchors>0:
            if body_results.design_results.anchor_and_foundation.foundation_type == 'pile':
                nm = body_results.design_results.anchor_and_foundation.pile_foundation_design.name
            elif body_results.design_results.anchor_and_foundation.foundation_type == 'gravity_anchor' or body_results.design_results.anchor_and_foundation.foundation_type == 'shallow':
                nm = body_results.design_results.anchor_and_foundation.gravity_foundation_design.name
            elif body_results.design_results.anchor_and_foundation.foundation_type == 'drag_anchor':
                nm = body_results.design_results.anchor_and_foundation.drag_anchor_design.anchor_id
            elif body_results.design_results.anchor_and_foundation.foundation_type == 'suction_caisson':
                nm = body_results.design_results.anchor_and_foundation.suction_caisson_design.name
            else:
                raise Exception("Error: cannot populate BoM of foundation type " + str(body_results.design_results.anchor_and_foundation.foundation_type))

            for idx in range(0,n_anchors):

                # Check if the foundation object is already registered in the BoM. If not, register it.
                if not self.is_object_registered(nm):

                    self.add_bom_line('none',nm,'unit',body_results.design_results.anchor_and_foundation.unit_cost)

                # Increment the bom line corresponding to the type of anchor/foundation
                self.increment_bom_line(nm,1)

    def register_line_segment(self,mooring):

        for iseg in range(0,len(mooring.line_properties)):

            # Get line type
            line_type = mooring.line_dictionnary[mooring.line_properties[iseg].line_type_index]

            # Check if the line_type is already registered in the BoM. If not, register it
            if not self.is_object_registered(line_type.line_type_name):

                self.add_bom_line(line_type.catalogue_id,line_type.line_type_name,'m',line_type.cost_per_meter)

            # Increment the bom line corresponding to the type of line
            self.increment_bom_line(line_type.line_type_name,mooring.line_properties[iseg].unstretched_length)

    def increment_bom_line(self,product_name,quantity):

        bom_line_found = False
        idx0=0
        for idx in range(0,len(self.product_name)):
            if self.product_name[idx] == product_name:
                bom_line_found = True
                idx0=idx

        if bom_line_found:
            self.quantity[idx0] = self.quantity[idx0] + quantity
            self.total_cost[idx0] = self.unit_cost[idx0] * self.quantity[idx0]
        else:
            raise Exception("Error in BoM: product name is not found in the existing BoM")

    def add_bom_line(self,catalogue_ID,product_name,unit,unit_cost):

        self._module_name.append('SK')
        self._catalogue_ID.append(catalogue_ID)
        self._product_name.append(product_name)
        self._unit.append(unit)

        if len(self.product_name)>1:
            self._quantity = np.append(self._quantity,[0])
            self._total_cost = np.append(self._total_cost,[0])
            self._unit_cost = np.append(self._unit_cost,[0])

        self.unit_cost[-1]=unit_cost

    def is_object_registered(self,name):
        flag = False
        if name in self.product_name:
            flag = True
        return flag

    def fill_pandas_table(self):

        d = {'Module': self.module_name, 'Catalogue ID': self.catalogue_ID,'Name': self.product_name,'Qnt': self.quantity,'UOM': self.unit,'Unit_cost': self.unit_cost,'Total_cost': self.total_cost}
        bom_as_pandas_table = pd.DataFrame(data=d)
        bom_as_pandas_table = bom_as_pandas_table.to_json()

        return bom_as_pandas_table

# @ USER DEFINED METHODS END
#------------------------------------

    #------------
    # Get functions
    #------------
    @ property
    def module_name(self): # pragma: no cover
        """:obj:`list` of :obj:`str`:name of the module the bom comes from
        """
        return self._module_name
    #------------
    @ property
    def catalogue_ID(self): # pragma: no cover
        """:obj:`list` of :obj:`str`:catalogue ID
        """
        return self._catalogue_ID
    #------------
    @ property
    def product_name(self): # pragma: no cover
        """:obj:`list` of :obj:`str`:name of the product
        """
        return self._product_name
    #------------
    @ property
    def quantity(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:product quantity dim(*) []
        """
        return self._quantity
    #------------
    @ property
    def unit(self): # pragma: no cover
        """:obj:`list` of :obj:`str`:unit of product quantity
        """
        return self._unit
    #------------
    @ property
    def unit_cost(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:cost for one unit of product dim(*) []
        """
        return self._unit_cost
    #------------
    @ property
    def total_cost(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:cost for the total quantity of product dim(*) []
        """
        return self._total_cost
    #------------
    @ property
    def name(self): # pragma: no cover
        """str: name of the instance object
        """
        return self._name
    #------------
    @ property
    def description(self): # pragma: no cover
        """str: description of the instance object
        """
        return self._description
    #------------
    #------------
    # Set functions
    #------------
    @ module_name.setter
    def module_name(self,val): # pragma: no cover
        self._module_name=val
    #------------
    @ catalogue_ID.setter
    def catalogue_ID(self,val): # pragma: no cover
        self._catalogue_ID=val
    #------------
    @ product_name.setter
    def product_name(self,val): # pragma: no cover
        self._product_name=val
    #------------
    @ quantity.setter
    def quantity(self,val): # pragma: no cover
        self._quantity=val
    #------------
    @ unit.setter
    def unit(self,val): # pragma: no cover
        self._unit=val
    #------------
    @ unit_cost.setter
    def unit_cost(self,val): # pragma: no cover
        self._unit_cost=val
    #------------
    @ total_cost.setter
    def total_cost(self,val): # pragma: no cover
        self._total_cost=val
    #------------
    @ name.setter
    def name(self,val): # pragma: no cover
        self._name=str(val)
    #------------
    @ description.setter
    def description(self,val): # pragma: no cover
        self._description=str(val)
    #------------
    #-------------------------
    # Representation functions
    #-------------------------
    def type_rep(self): # pragma: no cover
        """Generate a representation of the object type

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains the representation of the object type
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtosk:outputs:BoM"
        rep["name"] = self.name
        rep["description"] = self.description
        return rep
    def prop_rep(self, short = False, deep = True):

        """Generate a representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains a representation of the object properties
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtosk:outputs:BoM"
        rep["name"] = self.name
        rep["description"] = self.description
        if self.is_set("module_name"):
            if short:
                rep["module_name"] = str(len(self.module_name))
            else:
                rep["module_name"] = self.module_name
        else:
            rep["module_name"] = []
        if self.is_set("catalogue_ID"):
            if short:
                rep["catalogue_ID"] = str(len(self.catalogue_ID))
            else:
                rep["catalogue_ID"] = self.catalogue_ID
        else:
            rep["catalogue_ID"] = []
        if self.is_set("product_name"):
            if short:
                rep["product_name"] = str(len(self.product_name))
            else:
                rep["product_name"] = self.product_name
        else:
            rep["product_name"] = []
        if self.is_set("quantity"):
            if (short):
                rep["quantity"] = str(self.quantity.shape)
            else:
                try:
                    rep["quantity"] = self.quantity.tolist()
                except:
                    rep["quantity"] = self.quantity
        if self.is_set("unit"):
            if short:
                rep["unit"] = str(len(self.unit))
            else:
                rep["unit"] = self.unit
        else:
            rep["unit"] = []
        if self.is_set("unit_cost"):
            if (short):
                rep["unit_cost"] = str(self.unit_cost.shape)
            else:
                try:
                    rep["unit_cost"] = self.unit_cost.tolist()
                except:
                    rep["unit_cost"] = self.unit_cost
        if self.is_set("total_cost"):
            if (short):
                rep["total_cost"] = str(self.total_cost.shape)
            else:
                try:
                    rep["total_cost"] = self.total_cost.tolist()
                except:
                    rep["total_cost"] = self.total_cost
        if self.is_set("name"):
            rep["name"] = self.name
        if self.is_set("description"):
            rep["description"] = self.description
        return rep
    #-------------------------
    # Save functions
    #-------------------------
    def json_rep(self, short=False, deep=True):

        """Generate a JSON representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`str`: string that contains a JSON representation of the object properties
        """

        return ( json.dumps(self.prop_rep(short=short, deep=deep),indent=4, separators=(",",": ")))
    def saveJSON(self, fileName=None):
        """Save the instance of the object to JSON format file

        Args:
            fileName (:obj:`str`, optional): Name of the JSON file, included extension. Defaults is None. If None, the name of the JSON file will be self.name.json. It can also contain an absolute or relative path.

        """

        if fileName==None:
            fileName=self.name + ".json"
        f=open(fileName, "w")
        f.write(self.json_rep())
        f.write("\n")
        f.close()

    def saveHDF5(self,filePath=None):
        """Save the instance of the object to HDF5 format file
        Args:
            filePath (:obj:`str`, optional): Name of the HDF5 file, included extension. Defaults is None. If None, the name of the HDF5 file will be "self.name".h5. It can also contain an absolute or relative path.
        """
        # Define filepath
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".h5"
            else:
                raise Exception("name is required for saving")
        # Open hdf5 file
        h = h5py.File(filePath,"w")
        group = h.create_group(self.name)
        # Save the object to the group
        self.saveToHDF5Handle(group)
        # Close file
        h.close()
        pass
    def saveToHDF5Handle(self, handle):
        """Save the properties of the object to the hdf5 handle.
        Args:
            handle (:obj:`h5py.Group`): Handle used to store the object properties
        """
        asciiList = [n.encode("ascii", "ignore") for n in self.module_name]
        handle["module_name"] = asciiList
        asciiList = [n.encode("ascii", "ignore") for n in self.catalogue_ID]
        handle["catalogue_ID"] = asciiList
        asciiList = [n.encode("ascii", "ignore") for n in self.product_name]
        handle["product_name"] = asciiList
        handle["quantity"] = np.array(self.quantity,dtype=float)
        asciiList = [n.encode("ascii", "ignore") for n in self.unit]
        handle["unit"] = asciiList
        handle["unit_cost"] = np.array(self.unit_cost,dtype=float)
        handle["total_cost"] = np.array(self.total_cost,dtype=float)
        ar = []
        ar.append(self.name.encode("ascii"))
        handle["name"] = np.asarray(ar)
        ar = []
        ar.append(self.description.encode("ascii"))
        handle["description"] = np.asarray(ar)
    #-------------------------
    # Load functions
    #-------------------------
    def loadJSON(self,name = None, filePath = None):
        """Load an instance of the object from JSON format file

        Args:
            name (:obj:`str`, optional): Name of the object to load.
            filePath (:obj:`str`, optional): Path of the JSON file to load. If None, the function looks for a file with name "name".json.

        The JSON file must contain a datastructure representing an instance of this object's class, as generated by e.g. the function :func:`~saveJSON`.

        """

        if not(name == None):
            self.name = name
        if (name == None) and not(filePath == None):
            self.name = ".".join(filePath.split(os.path.sep)[-1].split(".")[0:-1])
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".json"
            else:
                raise Exception("object needs name for loading.")
        if not(os.path.isfile(filePath)):
            raise Exception("file %s not found."%filePath)
        self._loadedItems = []
        f = open(filePath,"r")
        data = f.read()
        f.close()
        dd = json.loads(data)
        self.loadFromJSONDict(dd)
    def loadFromJSONDict(self, data):
        """Load an instance of the object from a dictionnary representing a JSON structure)

        Args:
            name (:obj:`collections.OrderedDict`): Dictionnary containing a JSON structure, as produced by e.g. :func:`json.loads`

        """

        varName = "module_name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "catalogue_ID"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "product_name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "quantity"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "unit"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "unit_cost"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "total_cost"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "description"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
    def loadHDF5(self,name=None, filePath=None):
        """Load an instance of the object from HDF5 format file
        Args:
            name (:obj:`str`, optional): Name of the object to load. Default is None.
            filePath (:obj:`str`, optional): Path of the HDF5 file to load. If None, the function looks for a file with name "self.name".h5.
        The HDF5 file must contain a datastructure representing an instance of this objects class, as generated by e.g. the function :func:`~saveHDF5`.
        """
        # Define filepath and name
        if not(name == None):
            self.name = name
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".h5"
            else:
                raise Exception("name is required for loading")
        # Open hdf5 file
        h = h5py.File(filePath,"r")
        group = h[self.name]
        # Save the object to the group
        self.loadFromHDF5Handle(group)
        # Close file
        h.close()
        pass
    def loadFromHDF5Handle(self, gr):
        """Load the properties of the object from a hdf5 handle.
        Args:
            gr (:obj:`h5py.Group`): Handle used to read the object properties from
        """
        if ("module_name" in list(gr.keys())):
            self.module_name = [n.decode("ascii", "ignore") for n in gr["module_name"][:]]
        if ("catalogue_ID" in list(gr.keys())):
            self.catalogue_ID = [n.decode("ascii", "ignore") for n in gr["catalogue_ID"][:]]
        if ("product_name" in list(gr.keys())):
            self.product_name = [n.decode("ascii", "ignore") for n in gr["product_name"][:]]
        if ("quantity" in list(gr.keys())):
            self.quantity = gr["quantity"][:]
        if ("unit" in list(gr.keys())):
            self.unit = [n.decode("ascii", "ignore") for n in gr["unit"][:]]
        if ("unit_cost" in list(gr.keys())):
            self.unit_cost = gr["unit_cost"][:]
        if ("total_cost" in list(gr.keys())):
            self.total_cost = gr["total_cost"][:]
        if ("name" in list(gr.keys())):
            self.name = gr["name"][0].decode("ascii")
        if ("description" in list(gr.keys())):
            self.description = gr["description"][0].decode("ascii")
    #------------------------
    # is_set function
    #------------------------
    def is_set(self, varName): # pragma: no cover

        """Check if a given property of the object is set

        Args:
            varName (:obj:`str`): name of the property to check

        Returns:
            :obj:`bool`: True if the property is set, else False
        """

        if (isinstance(getattr(self,varName),list) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (isinstance(getattr(self,varName),np.ndarray) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (getattr(self,varName) != None):
            return True
        return False
    #------------------------
