# This is the Station Keeping module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Neil Luxcey, Nicolas Michelet, Rocio Isorna
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import collections
import numpy as np
import os
import json
import h5py
from . import EnvironmentalImpact
#------------------------------------
# @ USER DEFINED IMPORTS START
from ..environmental_impact_module import compute_mooring_footprint
from ..environmental_impact_module import compute_mooring_submerged_surface
from ..environmental_impact_module import compute_material_list
from ..environmental_impact_module import merge_material_list
from ..environmental_impact_module import add_to_material_list
# @ USER DEFINED IMPORTS END
#------------------------------------

class EnvironmentalImpactResults():

    """EnvironmentalImpact from the SK-module
    """

    #------------
    # Constructor
    #------------
    def __init__(self,name=None):
#------------------------------------
# @ USER DEFINED DESCRIPTION START
# @ USER DEFINED DESCRIPTION END
#------------------------------------

        self._farm=EnvironmentalImpact.EnvironmentalImpact()
        self._farm.description = 'environmental impact of the farm from mooring and foundations [m2]'
        self._devices=[]
        self._name='none'
        self._description=''
        if not(name == None): # pragma: no cover
            self._name = name

#------------------------------------
# @ USER DEFINED PROPERTIES START
# @ USER DEFINED PROPERTIES END
#------------------------------------

#------------------------------------
# @ USER DEFINED METHODS START
    def populate(self,farm):

        # Set bom name
        self.name = 'SK-Environmental_Impact'

        # Loop over each device
        for ibdy in range(0,len(farm.body_array)):

            # Inititate environmental impact for a device
            self.devices.append(EnvironmentalImpact.EnvironmentalImpact())

            # Register line segments
            if farm.body_array[ibdy].positioning_type == 'moored':
                self.register_line_segment(ibdy,farm.body_array[ibdy].mooring_system_force)

            # Register foundations
            self.register_foundation(ibdy,farm.body_array[ibdy].results)

        # Add masterstructure, if any
        if farm.master_structure_is_present:
            # Inititate environmental impact for a device
            self.devices.append(EnvironmentalImpact.EnvironmentalImpact())

            # Register line segments
            if farm.master_structure.positioning_type == 'moored':
                self.register_line_segment(len(farm.body_array),farm.master_structure.mooring_system_force)

            # Register foundations
            self.register_foundation(len(farm.body_array),farm.master_structure.results)

        # Sum everything to get the impact at the farm level
        for ibdy in range(0,len(self.devices)):
            self.farm.footprint = self.farm.footprint + self.devices[ibdy].footprint
            self.farm.submerged_surface = self.farm.submerged_surface + self.devices[ibdy].submerged_surface
            merge_material_list(self.farm.material_quantity_list,self.devices[ibdy].material_quantity_list)


    def register_line_segment(self,ibdy,mooring):

        # Compute footprint (simple model)
        footprint = compute_mooring_footprint(mooring)
        self.devices[ibdy].footprint = self.devices[ibdy].footprint + footprint

        # Compute submerged surface
        submerged_surface = compute_mooring_submerged_surface(mooring)
        self.devices[ibdy].submerged_surface = self.devices[ibdy].submerged_surface + submerged_surface

        # Register material quantity
        compute_material_list(mooring,self.devices[ibdy].material_quantity_list)

    def register_foundation(self,ibdy,body_results):
        if body_results.design_results.anchor_and_foundation.n_anchors>0:
            if body_results.design_results.anchor_and_foundation.foundation_type == 'pile':
                self.register_pile_foundation(ibdy,body_results.design_results.anchor_and_foundation.pile_foundation_design,body_results.design_results.anchor_and_foundation.n_anchors)
            elif body_results.design_results.anchor_and_foundation.foundation_type == 'gravity_anchor' or body_results.design_results.anchor_and_foundation.foundation_type == 'shallow':
                self.register_gravity_foundation(ibdy,body_results.design_results.anchor_and_foundation.gravity_foundation_design,body_results.design_results.anchor_and_foundation.n_anchors)
            elif body_results.design_results.anchor_and_foundation.foundation_type == 'drag_anchor':
                self.register_drag_anchor(ibdy,body_results.design_results.anchor_and_foundation.drag_anchor_design,body_results.design_results.anchor_and_foundation.n_anchors)
            elif body_results.design_results.anchor_and_foundation.foundation_type == 'suction_caisson':
                self.register_suction_caisson(ibdy,body_results.design_results.anchor_and_foundation.suction_caisson_design,body_results.design_results.anchor_and_foundation.n_anchors)
            else:
                raise Exception("Error: cannot populate BoM of foundation type " + str(body_results.design_results.anchor_and_foundation.foundation_type))

    def register_drag_anchor(self,ibdy,found_design,n_anchors):

        # Compute footprint
        footprint = found_design.footprint
        self.devices[ibdy].footprint = self.devices[ibdy].footprint + footprint

        # Compute submerged surface
        submerged_surface = 0.0
        self.devices[ibdy].submerged_surface = self.devices[ibdy].submerged_surface + submerged_surface

        # Register material quantity
        add_to_material_list('steel',found_design.weight,self.devices[ibdy].material_quantity_list)

    def register_suction_caisson(self,ibdy,found_design,n_anchors):

        # Compute footprint
        footprint = found_design.cs_area
        self.devices[ibdy].footprint = self.devices[ibdy].footprint + footprint

        # Compute submerged surface (same as footprint for suction caisson)
        submerged_surface = found_design.cs_area
        self.devices[ibdy].submerged_surface = self.devices[ibdy].submerged_surface + submerged_surface

        # Register material quantity
        add_to_material_list('steel',found_design.anchor_mass,self.devices[ibdy].material_quantity_list)

    def register_gravity_foundation(self,ibdy,found_design,n_anchors):

        # Compute footprint
        footprint = found_design.f_soil_surf
        self.devices[ibdy].footprint = self.devices[ibdy].footprint + footprint

        # Compute submerged surface
        submerged_surface = found_design.f_soil_surf
        self.devices[ibdy].submerged_surface = self.devices[ibdy].submerged_surface + submerged_surface

        # Register material quantity
        add_to_material_list(found_design.material,found_design.found_weight/9.81,self.devices[ibdy].material_quantity_list)

    def register_pile_foundation(self,ibdy,found_design,n_anchors):

        # Compute footprint
        footprint = found_design.footprint
        self.devices[ibdy].footprint = self.devices[ibdy].footprint + footprint

        # Compute submerged surface
        submerged_surface = found_design.pile_surface
        self.devices[ibdy].submerged_surface = self.devices[ibdy].submerged_surface + submerged_surface

        # Register material quantity
        add_to_material_list('steel',found_design.pile_weight/9.81,self.devices[ibdy].material_quantity_list)


# @ USER DEFINED METHODS END
#------------------------------------

    #------------
    # Get functions
    #------------
    @ property
    def farm(self): # pragma: no cover
        """:obj:`~.EnvironmentalImpact.EnvironmentalImpact`: environmental impact of the farm from mooring and foundations [m2]
        """
        return self._farm
    #------------
    @ property
    def devices(self): # pragma: no cover
        """:obj:`list` of :obj:`~.EnvironmentalImpact.EnvironmentalImpact`: environmental impact of each device from mooring and foundations [m2]
        """
        return self._devices
    #------------
    @ property
    def name(self): # pragma: no cover
        """str: name of the instance object
        """
        return self._name
    #------------
    @ property
    def description(self): # pragma: no cover
        """str: description of the instance object
        """
        return self._description
    #------------
    #------------
    # Set functions
    #------------
    @ farm.setter
    def farm(self,val): # pragma: no cover
        self._farm=val
    #------------
    @ devices.setter
    def devices(self,val): # pragma: no cover
        self._devices=val
    #------------
    @ name.setter
    def name(self,val): # pragma: no cover
        self._name=str(val)
    #------------
    @ description.setter
    def description(self,val): # pragma: no cover
        self._description=str(val)
    #------------
    #-------------------------
    # Representation functions
    #-------------------------
    def type_rep(self): # pragma: no cover
        """Generate a representation of the object type

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains the representation of the object type
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtosk:outputs:EnvironmentalImpactResults"
        rep["name"] = self.name
        rep["description"] = self.description
        return rep
    def prop_rep(self, short = False, deep = True):

        """Generate a representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains a representation of the object properties
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtosk:outputs:EnvironmentalImpactResults"
        rep["name"] = self.name
        rep["description"] = self.description
        if self.is_set("farm"):
            if (short and not(deep)):
                rep["farm"] = self.farm.type_rep()
            else:
                rep["farm"] = self.farm.prop_rep(short, deep)
        if self.is_set("devices"):
            rep["devices"] = []
            for i in range(0,len(self.devices)):
                if (short and not(deep)):
                    itemType = self.devices[i].type_rep()
                    rep["devices"].append( itemType )
                else:
                    rep["devices"].append( self.devices[i].prop_rep(short, deep) )
        else:
            rep["devices"] = []
        if self.is_set("name"):
            rep["name"] = self.name
        if self.is_set("description"):
            rep["description"] = self.description
        return rep
    #-------------------------
    # Save functions
    #-------------------------
    def json_rep(self, short=False, deep=True):

        """Generate a JSON representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`str`: string that contains a JSON representation of the object properties
        """

        return ( json.dumps(self.prop_rep(short=short, deep=deep),indent=4, separators=(",",": ")))
    def saveJSON(self, fileName=None):
        """Save the instance of the object to JSON format file

        Args:
            fileName (:obj:`str`, optional): Name of the JSON file, included extension. Defaults is None. If None, the name of the JSON file will be self.name.json. It can also contain an absolute or relative path.

        """

        if fileName==None:
            fileName=self.name + ".json"
        f=open(fileName, "w")
        f.write(self.json_rep())
        f.write("\n")
        f.close()

    def saveHDF5(self,filePath=None):
        """Save the instance of the object to HDF5 format file
        Args:
            filePath (:obj:`str`, optional): Name of the HDF5 file, included extension. Defaults is None. If None, the name of the HDF5 file will be "self.name".h5. It can also contain an absolute or relative path.
        """
        # Define filepath
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".h5"
            else:
                raise Exception("name is required for saving")
        # Open hdf5 file
        h = h5py.File(filePath,"w")
        group = h.create_group(self.name)
        # Save the object to the group
        self.saveToHDF5Handle(group)
        # Close file
        h.close()
        pass
    def saveToHDF5Handle(self, handle):
        """Save the properties of the object to the hdf5 handle.
        Args:
            handle (:obj:`h5py.Group`): Handle used to store the object properties
        """
        subgroup = handle.create_group("farm")
        self.farm.saveToHDF5Handle(subgroup)
        subgroup = handle.create_group("devices")
        for idx in range(0,len(self.devices)):
            subgroup1 = subgroup.create_group("devices" + "_" + str(idx))
            self.devices[idx].saveToHDF5Handle(subgroup1)
        ar = []
        ar.append(self.name.encode("ascii"))
        handle["name"] = np.asarray(ar)
        ar = []
        ar.append(self.description.encode("ascii"))
        handle["description"] = np.asarray(ar)
    #-------------------------
    # Load functions
    #-------------------------
    def loadJSON(self,name = None, filePath = None):
        """Load an instance of the object from JSON format file

        Args:
            name (:obj:`str`, optional): Name of the object to load.
            filePath (:obj:`str`, optional): Path of the JSON file to load. If None, the function looks for a file with name "name".json.

        The JSON file must contain a datastructure representing an instance of this object's class, as generated by e.g. the function :func:`~saveJSON`.

        """

        if not(name == None):
            self.name = name
        if (name == None) and not(filePath == None):
            self.name = ".".join(filePath.split(os.path.sep)[-1].split(".")[0:-1])
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".json"
            else:
                raise Exception("object needs name for loading.")
        if not(os.path.isfile(filePath)):
            raise Exception("file %s not found."%filePath)
        self._loadedItems = []
        f = open(filePath,"r")
        data = f.read()
        f.close()
        dd = json.loads(data)
        self.loadFromJSONDict(dd)
    def loadFromJSONDict(self, data):
        """Load an instance of the object from a dictionnary representing a JSON structure)

        Args:
            name (:obj:`collections.OrderedDict`): Dictionnary containing a JSON structure, as produced by e.g. :func:`json.loads`

        """

        varName = "farm"
        try :
            if data[varName] != None:
                self.farm=EnvironmentalImpact.EnvironmentalImpact()
                self.farm.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "devices"
        try :
            if data[varName] != None:
                self.devices=[]
                for i in range(0,len(data[varName])):
                    self.devices.append(EnvironmentalImpact.EnvironmentalImpact())
                    self.devices[i].loadFromJSONDict(data[varName][i])
            else:
                self.devices = []
        except :
            pass
        varName = "name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "description"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
    def loadHDF5(self,name=None, filePath=None):
        """Load an instance of the object from HDF5 format file
        Args:
            name (:obj:`str`, optional): Name of the object to load. Default is None.
            filePath (:obj:`str`, optional): Path of the HDF5 file to load. If None, the function looks for a file with name "self.name".h5.
        The HDF5 file must contain a datastructure representing an instance of this objects class, as generated by e.g. the function :func:`~saveHDF5`.
        """
        # Define filepath and name
        if not(name == None):
            self.name = name
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".h5"
            else:
                raise Exception("name is required for loading")
        # Open hdf5 file
        h = h5py.File(filePath,"r")
        group = h[self.name]
        # Save the object to the group
        self.loadFromHDF5Handle(group)
        # Close file
        h.close()
        pass
    def loadFromHDF5Handle(self, gr):
        """Load the properties of the object from a hdf5 handle.
        Args:
            gr (:obj:`h5py.Group`): Handle used to read the object properties from
        """
        if ("farm" in list(gr.keys())):
            subgroup = gr["farm"]
            self.farm.loadFromHDF5Handle(subgroup)
        if ("devices" in list(gr.keys())):
            subgroup = gr["devices"]
            self.devices=[]
            for idx in range(0,len(subgroup.keys())):
                ssubgroup = subgroup["devices"+"_"+str(idx)]
                self.devices.append(EnvironmentalImpact.EnvironmentalImpact())
                self.devices[idx].loadFromHDF5Handle(ssubgroup)
        if ("name" in list(gr.keys())):
            self.name = gr["name"][0].decode("ascii")
        if ("description" in list(gr.keys())):
            self.description = gr["description"][0].decode("ascii")
    #------------------------
    # is_set function
    #------------------------
    def is_set(self, varName): # pragma: no cover

        """Check if a given property of the object is set

        Args:
            varName (:obj:`str`): name of the property to check

        Returns:
            :obj:`bool`: True if the property is set, else False
        """

        if (isinstance(getattr(self,varName),list) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (isinstance(getattr(self,varName),np.ndarray) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (getattr(self,varName) != None):
            return True
        return False
    #------------------------
