# This is the Station Keeping module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Neil Luxcey, Nicolas Michelet, Rocio Isorna
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import collections
import numpy as np
import os
import json
import h5py
from . import HierarchyDataSeabedConnection
from . import HierarchyDataMooringLine
#------------------------------------
# @ USER DEFINED IMPORTS START
# @ USER DEFINED IMPORTS END
#------------------------------------

class HierarchyData():

    """Physical properties of the nodes present in the hierarchy
    """

    #------------
    # Constructor
    #------------
    def __init__(self,name=None):
#------------------------------------
# @ USER DEFINED DESCRIPTION START
# @ USER DEFINED DESCRIPTION END
#------------------------------------

        self._anchor_list=[]
        self._foundation_list=[]
        self._line_segment_list=[]
        self._name='none'
        self._description=''
        if not(name == None): # pragma: no cover
            self._name = name

#------------------------------------
# @ USER DEFINED PROPERTIES START
# @ USER DEFINED PROPERTIES END
#------------------------------------

#------------------------------------
# @ USER DEFINED METHODS START
    def add_line_segment(self,design_id, catalogue_id, material,length,total_mass,diameter,cost,upstream_id,downstream_id):
        line_segment = HierarchyDataMooringLine.HierarchyDataMooringLine()
        line_segment.design_id=design_id
        line_segment.catalogue_id=catalogue_id
        line_segment.material=material
        line_segment.length=length
        line_segment.total_mass=total_mass
        line_segment.diameter=diameter
        line_segment.cost=cost
        line_segment.upstream_id=upstream_id
        line_segment.downstream_id=downstream_id
        self.line_segment_list.append(line_segment)
    def add_anchor(self,design_id,catalogue_id,typein,coordinates,height,width,length,buried_height,mass,cost,upstream_id,downstream_id):
        anchor = HierarchyDataSeabedConnection.HierarchyDataSeabedConnection()
        anchor.design_id=design_id
        anchor.catalogue_id=catalogue_id
        anchor.type=typein
        anchor.coordinates=coordinates
        anchor.height=height
        anchor.width=width
        anchor.length=length
        anchor.buried_height=buried_height
        anchor.mass=mass
        anchor.cost=cost
        anchor.upstream_id=upstream_id
        anchor.downstream_id=downstream_id
        self.anchor_list.append(anchor)
    def add_foundation(self,design_id,catalogue_id,typein,coordinates,height,width,length,buried_height,mass,cost,upstream_id,downstream_id):
        foundation = HierarchyDataSeabedConnection.HierarchyDataSeabedConnection()
        foundation.design_id=design_id
        foundation.catalogue_id=catalogue_id
        foundation.type=typein
        foundation.coordinates=coordinates
        foundation.height=height
        foundation.width=width
        foundation.length=length
        foundation.buried_height=buried_height
        foundation.mass=mass
        foundation.cost=cost
        foundation.upstream_id=upstream_id
        foundation.downstream_id=downstream_id
        self.foundation_list.append(foundation)
# @ USER DEFINED METHODS END
#------------------------------------

    #------------
    # Get functions
    #------------
    @ property
    def anchor_list(self): # pragma: no cover
        """:obj:`list` of :obj:`~.HierarchyDataSeabedConnection.HierarchyDataSeabedConnection`: object that contains physical properties of the anchors present in the hierarchy
        """
        return self._anchor_list
    #------------
    @ property
    def foundation_list(self): # pragma: no cover
        """:obj:`list` of :obj:`~.HierarchyDataSeabedConnection.HierarchyDataSeabedConnection`: object that contains physical properties of the foundations present in the hierarchy
        """
        return self._foundation_list
    #------------
    @ property
    def line_segment_list(self): # pragma: no cover
        """:obj:`list` of :obj:`~.HierarchyDataMooringLine.HierarchyDataMooringLine`: object that contains physical properties of the mooring line segment present in the hierarchy
        """
        return self._line_segment_list
    #------------
    @ property
    def name(self): # pragma: no cover
        """str: name of the instance object
        """
        return self._name
    #------------
    @ property
    def description(self): # pragma: no cover
        """str: description of the instance object
        """
        return self._description
    #------------
    #------------
    # Set functions
    #------------
    @ anchor_list.setter
    def anchor_list(self,val): # pragma: no cover
        self._anchor_list=val
    #------------
    @ foundation_list.setter
    def foundation_list(self,val): # pragma: no cover
        self._foundation_list=val
    #------------
    @ line_segment_list.setter
    def line_segment_list(self,val): # pragma: no cover
        self._line_segment_list=val
    #------------
    @ name.setter
    def name(self,val): # pragma: no cover
        self._name=str(val)
    #------------
    @ description.setter
    def description(self,val): # pragma: no cover
        self._description=str(val)
    #------------
    #-------------------------
    # Representation functions
    #-------------------------
    def type_rep(self): # pragma: no cover
        """Generate a representation of the object type

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains the representation of the object type
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtosk:outputs:HierarchyData"
        rep["name"] = self.name
        rep["description"] = self.description
        return rep
    def prop_rep(self, short = False, deep = True):

        """Generate a representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains a representation of the object properties
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtosk:outputs:HierarchyData"
        rep["name"] = self.name
        rep["description"] = self.description
        if self.is_set("anchor_list"):
            rep["anchor_list"] = []
            for i in range(0,len(self.anchor_list)):
                if (short and not(deep)):
                    itemType = self.anchor_list[i].type_rep()
                    rep["anchor_list"].append( itemType )
                else:
                    rep["anchor_list"].append( self.anchor_list[i].prop_rep(short, deep) )
        else:
            rep["anchor_list"] = []
        if self.is_set("foundation_list"):
            rep["foundation_list"] = []
            for i in range(0,len(self.foundation_list)):
                if (short and not(deep)):
                    itemType = self.foundation_list[i].type_rep()
                    rep["foundation_list"].append( itemType )
                else:
                    rep["foundation_list"].append( self.foundation_list[i].prop_rep(short, deep) )
        else:
            rep["foundation_list"] = []
        if self.is_set("line_segment_list"):
            rep["line_segment_list"] = []
            for i in range(0,len(self.line_segment_list)):
                if (short and not(deep)):
                    itemType = self.line_segment_list[i].type_rep()
                    rep["line_segment_list"].append( itemType )
                else:
                    rep["line_segment_list"].append( self.line_segment_list[i].prop_rep(short, deep) )
        else:
            rep["line_segment_list"] = []
        if self.is_set("name"):
            rep["name"] = self.name
        if self.is_set("description"):
            rep["description"] = self.description
        return rep
    #-------------------------
    # Save functions
    #-------------------------
    def json_rep(self, short=False, deep=True):

        """Generate a JSON representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`str`: string that contains a JSON representation of the object properties
        """

        return ( json.dumps(self.prop_rep(short=short, deep=deep),indent=4, separators=(",",": ")))
    def saveJSON(self, fileName=None):
        """Save the instance of the object to JSON format file

        Args:
            fileName (:obj:`str`, optional): Name of the JSON file, included extension. Defaults is None. If None, the name of the JSON file will be self.name.json. It can also contain an absolute or relative path.

        """

        if fileName==None:
            fileName=self.name + ".json"
        f=open(fileName, "w")
        f.write(self.json_rep())
        f.write("\n")
        f.close()

    def saveHDF5(self,filePath=None):
        """Save the instance of the object to HDF5 format file
        Args:
            filePath (:obj:`str`, optional): Name of the HDF5 file, included extension. Defaults is None. If None, the name of the HDF5 file will be "self.name".h5. It can also contain an absolute or relative path.
        """
        # Define filepath
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".h5"
            else:
                raise Exception("name is required for saving")
        # Open hdf5 file
        h = h5py.File(filePath,"w")
        group = h.create_group(self.name)
        # Save the object to the group
        self.saveToHDF5Handle(group)
        # Close file
        h.close()
        pass
    def saveToHDF5Handle(self, handle):
        """Save the properties of the object to the hdf5 handle.
        Args:
            handle (:obj:`h5py.Group`): Handle used to store the object properties
        """
        subgroup = handle.create_group("anchor_list")
        for idx in range(0,len(self.anchor_list)):
            subgroup1 = subgroup.create_group("anchor_list" + "_" + str(idx))
            self.anchor_list[idx].saveToHDF5Handle(subgroup1)
        subgroup = handle.create_group("foundation_list")
        for idx in range(0,len(self.foundation_list)):
            subgroup1 = subgroup.create_group("foundation_list" + "_" + str(idx))
            self.foundation_list[idx].saveToHDF5Handle(subgroup1)
        subgroup = handle.create_group("line_segment_list")
        for idx in range(0,len(self.line_segment_list)):
            subgroup1 = subgroup.create_group("line_segment_list" + "_" + str(idx))
            self.line_segment_list[idx].saveToHDF5Handle(subgroup1)
        ar = []
        ar.append(self.name.encode("ascii"))
        handle["name"] = np.asarray(ar)
        ar = []
        ar.append(self.description.encode("ascii"))
        handle["description"] = np.asarray(ar)
    #-------------------------
    # Load functions
    #-------------------------
    def loadJSON(self,name = None, filePath = None):
        """Load an instance of the object from JSON format file

        Args:
            name (:obj:`str`, optional): Name of the object to load.
            filePath (:obj:`str`, optional): Path of the JSON file to load. If None, the function looks for a file with name "name".json.

        The JSON file must contain a datastructure representing an instance of this object's class, as generated by e.g. the function :func:`~saveJSON`.

        """

        if not(name == None):
            self.name = name
        if (name == None) and not(filePath == None):
            self.name = ".".join(filePath.split(os.path.sep)[-1].split(".")[0:-1])
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".json"
            else:
                raise Exception("object needs name for loading.")
        if not(os.path.isfile(filePath)):
            raise Exception("file %s not found."%filePath)
        self._loadedItems = []
        f = open(filePath,"r")
        data = f.read()
        f.close()
        dd = json.loads(data)
        self.loadFromJSONDict(dd)
    def loadFromJSONDict(self, data):
        """Load an instance of the object from a dictionnary representing a JSON structure)

        Args:
            name (:obj:`collections.OrderedDict`): Dictionnary containing a JSON structure, as produced by e.g. :func:`json.loads`

        """

        varName = "anchor_list"
        try :
            if data[varName] != None:
                self.anchor_list=[]
                for i in range(0,len(data[varName])):
                    self.anchor_list.append(HierarchyDataSeabedConnection.HierarchyDataSeabedConnection())
                    self.anchor_list[i].loadFromJSONDict(data[varName][i])
            else:
                self.anchor_list = []
        except :
            pass
        varName = "foundation_list"
        try :
            if data[varName] != None:
                self.foundation_list=[]
                for i in range(0,len(data[varName])):
                    self.foundation_list.append(HierarchyDataSeabedConnection.HierarchyDataSeabedConnection())
                    self.foundation_list[i].loadFromJSONDict(data[varName][i])
            else:
                self.foundation_list = []
        except :
            pass
        varName = "line_segment_list"
        try :
            if data[varName] != None:
                self.line_segment_list=[]
                for i in range(0,len(data[varName])):
                    self.line_segment_list.append(HierarchyDataMooringLine.HierarchyDataMooringLine())
                    self.line_segment_list[i].loadFromJSONDict(data[varName][i])
            else:
                self.line_segment_list = []
        except :
            pass
        varName = "name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "description"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
    def loadHDF5(self,name=None, filePath=None):
        """Load an instance of the object from HDF5 format file
        Args:
            name (:obj:`str`, optional): Name of the object to load. Default is None.
            filePath (:obj:`str`, optional): Path of the HDF5 file to load. If None, the function looks for a file with name "self.name".h5.
        The HDF5 file must contain a datastructure representing an instance of this objects class, as generated by e.g. the function :func:`~saveHDF5`.
        """
        # Define filepath and name
        if not(name == None):
            self.name = name
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".h5"
            else:
                raise Exception("name is required for loading")
        # Open hdf5 file
        h = h5py.File(filePath,"r")
        group = h[self.name]
        # Save the object to the group
        self.loadFromHDF5Handle(group)
        # Close file
        h.close()
        pass
    def loadFromHDF5Handle(self, gr):
        """Load the properties of the object from a hdf5 handle.
        Args:
            gr (:obj:`h5py.Group`): Handle used to read the object properties from
        """
        if ("anchor_list" in list(gr.keys())):
            subgroup = gr["anchor_list"]
            self.anchor_list=[]
            for idx in range(0,len(subgroup.keys())):
                ssubgroup = subgroup["anchor_list"+"_"+str(idx)]
                self.anchor_list.append(HierarchyDataSeabedConnection.HierarchyDataSeabedConnection())
                self.anchor_list[idx].loadFromHDF5Handle(ssubgroup)
        if ("foundation_list" in list(gr.keys())):
            subgroup = gr["foundation_list"]
            self.foundation_list=[]
            for idx in range(0,len(subgroup.keys())):
                ssubgroup = subgroup["foundation_list"+"_"+str(idx)]
                self.foundation_list.append(HierarchyDataSeabedConnection.HierarchyDataSeabedConnection())
                self.foundation_list[idx].loadFromHDF5Handle(ssubgroup)
        if ("line_segment_list" in list(gr.keys())):
            subgroup = gr["line_segment_list"]
            self.line_segment_list=[]
            for idx in range(0,len(subgroup.keys())):
                ssubgroup = subgroup["line_segment_list"+"_"+str(idx)]
                self.line_segment_list.append(HierarchyDataMooringLine.HierarchyDataMooringLine())
                self.line_segment_list[idx].loadFromHDF5Handle(ssubgroup)
        if ("name" in list(gr.keys())):
            self.name = gr["name"][0].decode("ascii")
        if ("description" in list(gr.keys())):
            self.description = gr["description"][0].decode("ascii")
    #------------------------
    # is_set function
    #------------------------
    def is_set(self, varName): # pragma: no cover

        """Check if a given property of the object is set

        Args:
            varName (:obj:`str`): name of the property to check

        Returns:
            :obj:`bool`: True if the property is set, else False
        """

        if (isinstance(getattr(self,varName),list) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (isinstance(getattr(self,varName),np.ndarray) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (getattr(self,varName) != None):
            return True
        return False
    #------------------------
