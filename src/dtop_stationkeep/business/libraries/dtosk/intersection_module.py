# This is the Station Keeping module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Neil Luxcey, Nicolas Michelet, Rocio Isorna
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


import numpy as np

def intersection_segment_plane_z0(p1,p2):
	point=np.zeros(shape=(3), dtype=float)
	intersection_exists=False

	if p1[2]>0.0 and p2[2]>0.0:
		intersection_exists=False
	elif p1[2]<0.0 and p2[2]<0.0:
		intersection_exists=False
	else:
		if p2[2]==p1[2]:
			if p2[2]==0:
				point=p2
				intersection_exists=True
			else:
				intersection_exists=False
		else:
			alpha=-p1[2]/(p2[2]-p1[2])
			point=p1+alpha*(p2-p1)
			intersection_exists=True

	return point,intersection_exists


