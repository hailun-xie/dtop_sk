# This is the Station Keeping module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Neil Luxcey, Nicolas Michelet, Rocio Isorna
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import numpy as np
from .global_parameters import acceleration_of_gravity

# def compute_weight_in_water(weight_in_air,rho_steel,sea_water_density):
	
# 	# Compute equivalent diameter (in order to get correct weight in water)
# 	weight_in_water = weight_in_air*(rho_steel - sea_water_density)/rho_steel

# 	return weight_in_water


def compute_anchor_point_position(Th,weight_in_water,line_length,water_depth):
	# Init
	g = acceleration_of_gravity()
	a = Th/(weight_in_water*g)
	# Faltinsen, 1990, Eq.8.21
	X = line_length - water_depth*(1+2*a/water_depth)**0.5 + a*np.arccosh(1+water_depth/a)
	return X

