# This is the Station Keeping module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Neil Luxcey, Nicolas Michelet, Rocio Isorna
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import collections
import numpy as np
import os
import json
import h5py
from . import SteadyForceModel
from . import FoundationDesignParameters
#------------------------------------
# @ USER DEFINED IMPORTS START
# @ USER DEFINED IMPORTS END
#------------------------------------

class SubstationProperties():

    """data model representing substation properties
    """

    #------------
    # Constructor
    #------------
    def __init__(self,name=None):
#------------------------------------
# @ USER DEFINED DESCRIPTION START
# @ USER DEFINED DESCRIPTION END
#------------------------------------

        self._steady_force_model=SteadyForceModel.SteadyForceModel()
        self._steady_force_model.description = 'geometry description for steady force models: wind, current, mean wave drift'
        self._mass=0.0
        self._water_depth=0.0
        self._cog=np.zeros(shape=(3), dtype=float)
        self._positioning_type='fixed'
        self._positioning_reference='seabed'
        self._device_foundation_profile='cylinder'
        self._foundation_design_parameters=FoundationDesignParameters.FoundationDesignParameters()
        self._foundation_design_parameters.description = 'Parameter inputs for foundation design of substation'
        self._east=0.0
        self._north=0.0
        self._cost_of_support_structure=0.0
        self._name='none'
        self._description=''
        if not(name == None): # pragma: no cover
            self._name = name

#------------------------------------
# @ USER DEFINED PROPERTIES START
# @ USER DEFINED PROPERTIES END
#------------------------------------

#------------------------------------
# @ USER DEFINED METHODS START
# @ USER DEFINED METHODS END
#------------------------------------

    #------------
    # Get functions
    #------------
    @ property
    def steady_force_model(self): # pragma: no cover
        """:obj:`~.SteadyForceModel.SteadyForceModel`: geometry description for steady force models: wind, current, mean wave drift
        """
        return self._steady_force_model
    #------------
    @ property
    def mass(self): # pragma: no cover
        """float: device mass [kg]
        """
        return self._mass
    #------------
    @ property
    def water_depth(self): # pragma: no cover
        """float: water depth at substation position [m]
        """
        return self._water_depth
    #------------
    @ property
    def cog(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:position of cog in body-fixed coordinate system dim(3) [kg]
        """
        return self._cog
    #------------
    @ property
    def positioning_type(self): # pragma: no cover
        """str: fixed
        """
        return self._positioning_type
    #------------
    @ property
    def positioning_reference(self): # pragma: no cover
        """str: positioning reference for a substation is seabed
        """
        return self._positioning_reference
    #------------
    @ property
    def device_foundation_profile(self): # pragma: no cover
        """str: device foundation profile: cylinder or rectangular. Required if positionning_type is fixed.
        """
        return self._device_foundation_profile
    #------------
    @ property
    def foundation_design_parameters(self): # pragma: no cover
        """:obj:`~.FoundationDesignParameters.FoundationDesignParameters`: Parameter inputs for foundation design of substation
        """
        return self._foundation_design_parameters
    #------------
    @ property
    def east(self): # pragma: no cover
        """float: Substation east position []
        """
        return self._east
    #------------
    @ property
    def north(self): # pragma: no cover
        """float: Substation north position []
        """
        return self._north
    #------------
    @ property
    def cost_of_support_structure(self): # pragma: no cover
        """float: Cost of jacket or monopile in euros []
        """
        return self._cost_of_support_structure
    #------------
    @ property
    def name(self): # pragma: no cover
        """str: name of the instance object
        """
        return self._name
    #------------
    @ property
    def description(self): # pragma: no cover
        """str: description of the instance object
        """
        return self._description
    #------------
    #------------
    # Set functions
    #------------
    @ steady_force_model.setter
    def steady_force_model(self,val): # pragma: no cover
        self._steady_force_model=val
    #------------
    @ mass.setter
    def mass(self,val): # pragma: no cover
        self._mass=float(val)
    #------------
    @ water_depth.setter
    def water_depth(self,val): # pragma: no cover
        self._water_depth=float(val)
    #------------
    @ cog.setter
    def cog(self,val): # pragma: no cover
        self._cog=val
    #------------
    @ positioning_type.setter
    def positioning_type(self,val): # pragma: no cover
        self._positioning_type=str(val)
    #------------
    @ positioning_reference.setter
    def positioning_reference(self,val): # pragma: no cover
        self._positioning_reference=str(val)
    #------------
    @ device_foundation_profile.setter
    def device_foundation_profile(self,val): # pragma: no cover
        self._device_foundation_profile=str(val)
    #------------
    @ foundation_design_parameters.setter
    def foundation_design_parameters(self,val): # pragma: no cover
        self._foundation_design_parameters=val
    #------------
    @ east.setter
    def east(self,val): # pragma: no cover
        self._east=float(val)
    #------------
    @ north.setter
    def north(self,val): # pragma: no cover
        self._north=float(val)
    #------------
    @ cost_of_support_structure.setter
    def cost_of_support_structure(self,val): # pragma: no cover
        self._cost_of_support_structure=float(val)
    #------------
    @ name.setter
    def name(self,val): # pragma: no cover
        self._name=str(val)
    #------------
    @ description.setter
    def description(self,val): # pragma: no cover
        self._description=str(val)
    #------------
    #-------------------------
    # Representation functions
    #-------------------------
    def type_rep(self): # pragma: no cover
        """Generate a representation of the object type

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains the representation of the object type
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtosk:inputs:SubstationProperties"
        rep["name"] = self.name
        rep["description"] = self.description
        return rep
    def prop_rep(self, short = False, deep = True):

        """Generate a representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains a representation of the object properties
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtosk:inputs:SubstationProperties"
        rep["name"] = self.name
        rep["description"] = self.description
        if self.is_set("steady_force_model"):
            if (short and not(deep)):
                rep["steady_force_model"] = self.steady_force_model.type_rep()
            else:
                rep["steady_force_model"] = self.steady_force_model.prop_rep(short, deep)
        if self.is_set("mass"):
            rep["mass"] = self.mass
        if self.is_set("water_depth"):
            rep["water_depth"] = self.water_depth
        if self.is_set("cog"):
            if (short):
                rep["cog"] = str(self.cog.shape)
            else:
                try:
                    rep["cog"] = self.cog.tolist()
                except:
                    rep["cog"] = self.cog
        if self.is_set("positioning_type"):
            rep["positioning_type"] = self.positioning_type
        if self.is_set("positioning_reference"):
            rep["positioning_reference"] = self.positioning_reference
        if self.is_set("device_foundation_profile"):
            rep["device_foundation_profile"] = self.device_foundation_profile
        if self.is_set("foundation_design_parameters"):
            if (short and not(deep)):
                rep["foundation_design_parameters"] = self.foundation_design_parameters.type_rep()
            else:
                rep["foundation_design_parameters"] = self.foundation_design_parameters.prop_rep(short, deep)
        if self.is_set("east"):
            rep["east"] = self.east
        if self.is_set("north"):
            rep["north"] = self.north
        if self.is_set("cost_of_support_structure"):
            rep["cost_of_support_structure"] = self.cost_of_support_structure
        if self.is_set("name"):
            rep["name"] = self.name
        if self.is_set("description"):
            rep["description"] = self.description
        return rep
    #-------------------------
    # Save functions
    #-------------------------
    def json_rep(self, short=False, deep=True):

        """Generate a JSON representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`str`: string that contains a JSON representation of the object properties
        """

        return ( json.dumps(self.prop_rep(short=short, deep=deep),indent=4, separators=(",",": ")))
    def saveJSON(self, fileName=None):
        """Save the instance of the object to JSON format file

        Args:
            fileName (:obj:`str`, optional): Name of the JSON file, included extension. Defaults is None. If None, the name of the JSON file will be self.name.json. It can also contain an absolute or relative path.

        """

        if fileName==None:
            fileName=self.name + ".json"
        f=open(fileName, "w")
        f.write(self.json_rep())
        f.write("\n")
        f.close()

    def saveHDF5(self,filePath=None):
        """Save the instance of the object to HDF5 format file
        Args:
            filePath (:obj:`str`, optional): Name of the HDF5 file, included extension. Defaults is None. If None, the name of the HDF5 file will be "self.name".h5. It can also contain an absolute or relative path.
        """
        # Define filepath
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".h5"
            else:
                raise Exception("name is required for saving")
        # Open hdf5 file
        h = h5py.File(filePath,"w")
        group = h.create_group(self.name)
        # Save the object to the group
        self.saveToHDF5Handle(group)
        # Close file
        h.close()
        pass
    def saveToHDF5Handle(self, handle):
        """Save the properties of the object to the hdf5 handle.
        Args:
            handle (:obj:`h5py.Group`): Handle used to store the object properties
        """
        subgroup = handle.create_group("steady_force_model")
        self.steady_force_model.saveToHDF5Handle(subgroup)
        handle["mass"] = np.array([self.mass],dtype=float)
        handle["water_depth"] = np.array([self.water_depth],dtype=float)
        handle["cog"] = np.array(self.cog,dtype=float)
        ar = []
        ar.append(self.positioning_type.encode("ascii"))
        handle["positioning_type"] = np.asarray(ar)
        ar = []
        ar.append(self.positioning_reference.encode("ascii"))
        handle["positioning_reference"] = np.asarray(ar)
        ar = []
        ar.append(self.device_foundation_profile.encode("ascii"))
        handle["device_foundation_profile"] = np.asarray(ar)
        subgroup = handle.create_group("foundation_design_parameters")
        self.foundation_design_parameters.saveToHDF5Handle(subgroup)
        handle["east"] = np.array([self.east],dtype=float)
        handle["north"] = np.array([self.north],dtype=float)
        handle["cost_of_support_structure"] = np.array([self.cost_of_support_structure],dtype=float)
        ar = []
        ar.append(self.name.encode("ascii"))
        handle["name"] = np.asarray(ar)
        ar = []
        ar.append(self.description.encode("ascii"))
        handle["description"] = np.asarray(ar)
    #-------------------------
    # Load functions
    #-------------------------
    def loadJSON(self,name = None, filePath = None):
        """Load an instance of the object from JSON format file

        Args:
            name (:obj:`str`, optional): Name of the object to load.
            filePath (:obj:`str`, optional): Path of the JSON file to load. If None, the function looks for a file with name "name".json.

        The JSON file must contain a datastructure representing an instance of this object's class, as generated by e.g. the function :func:`~saveJSON`.

        """

        if not(name == None):
            self.name = name
        if (name == None) and not(filePath == None):
            self.name = ".".join(filePath.split(os.path.sep)[-1].split(".")[0:-1])
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".json"
            else:
                raise Exception("object needs name for loading.")
        if not(os.path.isfile(filePath)):
            raise Exception("file %s not found."%filePath)
        self._loadedItems = []
        f = open(filePath,"r")
        data = f.read()
        f.close()
        dd = json.loads(data)
        self.loadFromJSONDict(dd)
    def loadFromJSONDict(self, data):
        """Load an instance of the object from a dictionnary representing a JSON structure)

        Args:
            name (:obj:`collections.OrderedDict`): Dictionnary containing a JSON structure, as produced by e.g. :func:`json.loads`

        """

        varName = "steady_force_model"
        try :
            if data[varName] != None:
                self.steady_force_model=SteadyForceModel.SteadyForceModel()
                self.steady_force_model.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "mass"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "water_depth"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "cog"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "positioning_type"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "positioning_reference"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "device_foundation_profile"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "foundation_design_parameters"
        try :
            if data[varName] != None:
                self.foundation_design_parameters=FoundationDesignParameters.FoundationDesignParameters()
                self.foundation_design_parameters.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "east"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "north"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "cost_of_support_structure"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "description"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
    def loadHDF5(self,name=None, filePath=None):
        """Load an instance of the object from HDF5 format file
        Args:
            name (:obj:`str`, optional): Name of the object to load. Default is None.
            filePath (:obj:`str`, optional): Path of the HDF5 file to load. If None, the function looks for a file with name "self.name".h5.
        The HDF5 file must contain a datastructure representing an instance of this objects class, as generated by e.g. the function :func:`~saveHDF5`.
        """
        # Define filepath and name
        if not(name == None):
            self.name = name
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".h5"
            else:
                raise Exception("name is required for loading")
        # Open hdf5 file
        h = h5py.File(filePath,"r")
        group = h[self.name]
        # Save the object to the group
        self.loadFromHDF5Handle(group)
        # Close file
        h.close()
        pass
    def loadFromHDF5Handle(self, gr):
        """Load the properties of the object from a hdf5 handle.
        Args:
            gr (:obj:`h5py.Group`): Handle used to read the object properties from
        """
        if ("steady_force_model" in list(gr.keys())):
            subgroup = gr["steady_force_model"]
            self.steady_force_model.loadFromHDF5Handle(subgroup)
        if ("mass" in list(gr.keys())):
            self.mass = gr["mass"][0]
        if ("water_depth" in list(gr.keys())):
            self.water_depth = gr["water_depth"][0]
        if ("cog" in list(gr.keys())):
            self.cog = gr["cog"][:]
        if ("positioning_type" in list(gr.keys())):
            self.positioning_type = gr["positioning_type"][0].decode("ascii")
        if ("positioning_reference" in list(gr.keys())):
            self.positioning_reference = gr["positioning_reference"][0].decode("ascii")
        if ("device_foundation_profile" in list(gr.keys())):
            self.device_foundation_profile = gr["device_foundation_profile"][0].decode("ascii")
        if ("foundation_design_parameters" in list(gr.keys())):
            subgroup = gr["foundation_design_parameters"]
            self.foundation_design_parameters.loadFromHDF5Handle(subgroup)
        if ("east" in list(gr.keys())):
            self.east = gr["east"][0]
        if ("north" in list(gr.keys())):
            self.north = gr["north"][0]
        if ("cost_of_support_structure" in list(gr.keys())):
            self.cost_of_support_structure = gr["cost_of_support_structure"][0]
        if ("name" in list(gr.keys())):
            self.name = gr["name"][0].decode("ascii")
        if ("description" in list(gr.keys())):
            self.description = gr["description"][0].decode("ascii")
    #------------------------
    # is_set function
    #------------------------
    def is_set(self, varName): # pragma: no cover

        """Check if a given property of the object is set

        Args:
            varName (:obj:`str`): name of the property to check

        Returns:
            :obj:`bool`: True if the property is set, else False
        """

        if (isinstance(getattr(self,varName),list) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (isinstance(getattr(self,varName),np.ndarray) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (getattr(self,varName) != None):
            return True
        return False
    #------------------------
