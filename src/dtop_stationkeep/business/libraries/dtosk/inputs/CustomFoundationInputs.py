import collections
import numpy as np
import os
import json
import h5py
#------------------------------------
# @ USER DEFINED IMPORTS START
# @ USER DEFINED IMPORTS END
#------------------------------------

class CustomFoundationInputs():

    """data model representing parameters for custom foundation design
    """

    #------------
    # Constructor
    #------------
    def __init__(self,name=None):
#------------------------------------
# @ USER DEFINED DESCRIPTION START
# @ USER DEFINED DESCRIPTION END
#------------------------------------

        self._d_ext=0.0
        self._w_t=0.0
        self._length=0.0
        self._geometry_pref=''
        self._base_lx=0.0
        self._base_ly=0.0
        self._base_radius=0.0
        self._thickness=0.0
        self._mass=0.0
        self._drag_anchor_coefficients=np.zeros(shape=(8), dtype=float)
        self._base_r1=0.0
        self._base_r2=0.0
        self._base_r3=0.0
        self._base_alpha01=0.0
        self._base_alpha12=0.0
        self._base_alpha23=0.0
        self._contact_points_mass=0.0
        self._name='none'
        self._description=''
        if not(name == None): # pragma: no cover
            self._name = name

#------------------------------------
# @ USER DEFINED PROPERTIES START
# @ USER DEFINED PROPERTIES END
#------------------------------------

#------------------------------------
# @ USER DEFINED METHODS START
# @ USER DEFINED METHODS END
#------------------------------------

    #------------
    # Get functions
    #------------
    @ property
    def d_ext(self): # pragma: no cover
        """float: External diameter [m] (pile, suction caisson) []
        """
        return self._d_ext
    #------------
    @ property
    def w_t(self): # pragma: no cover
        """float: Thickness [m] (pile, suction caisson) []
        """
        return self._w_t
    #------------
    @ property
    def length(self): # pragma: no cover
        """float: Pile length [m] (pile, suction caisson) []
        """
        return self._length
    #------------
    @ property
    def geometry_pref(self): # pragma: no cover
        """str: Geometry preference. Rectangular or cylinder. (gravity)
        """
        return self._geometry_pref
    #------------
    @ property
    def base_lx(self): # pragma: no cover
        """float: Length [m]. Required if geometry_pref is rectangular. (gravity) []
        """
        return self._base_lx
    #------------
    @ property
    def base_ly(self): # pragma: no cover
        """float: Width [m]. Required if geometry_pref is rectangular. (gravity) []
        """
        return self._base_ly
    #------------
    @ property
    def base_radius(self): # pragma: no cover
        """float: Radius [m]. Required if geometry_pref is cylinder. (gravity) []
        """
        return self._base_radius
    #------------
    @ property
    def thickness(self): # pragma: no cover
        """float: Thickness [m]. (gravity) []
        """
        return self._thickness
    #------------
    @ property
    def mass(self): # pragma: no cover
        """float: Drag anchor mass [kg]. (drag) []
        """
        return self._mass
    #------------
    @ property
    def drag_anchor_coefficients(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:Drag anchor coefficients [m_anchor, b_anchor, m_a, b_a, m_b, b_b, m_ef, b_ef] (drag) dim(8) []
        """
        return self._drag_anchor_coefficients
    #------------
    @ property
    def base_r1(self): # pragma: no cover
        """float: radius of the first contact point foundation for contact points geometry (gravity) [m]
        """
        return self._base_r1
    #------------
    @ property
    def base_r2(self): # pragma: no cover
        """float: radius of the second contact point foundation for contact points geometry (gravity) [m]
        """
        return self._base_r2
    #------------
    @ property
    def base_r3(self): # pragma: no cover
        """float: radius of the third contact point foundation for contact points geometry (gravity) [m]
        """
        return self._base_r3
    #------------
    @ property
    def base_alpha01(self): # pragma: no cover
        """float: Absolute angle between X-axis and first contact point foundation for contact points geometry (gravity) [deg]
        """
        return self._base_alpha01
    #------------
    @ property
    def base_alpha12(self): # pragma: no cover
        """float: angle between the first and second contact points foundation for contact points geometry (gravity) [deg]
        """
        return self._base_alpha12
    #------------
    @ property
    def base_alpha23(self): # pragma: no cover
        """float: angle between the second and third contact points foundation for contact points geometry (gravity) [deg]
        """
        return self._base_alpha23
    #------------
    @ property
    def contact_points_mass(self): # pragma: no cover
        """float: total mass of the contact points foundation (gravity) [kg]
        """
        return self._contact_points_mass
    #------------
    @ property
    def name(self): # pragma: no cover
        """str: name of the instance object
        """
        return self._name
    #------------
    @ property
    def description(self): # pragma: no cover
        """str: description of the instance object
        """
        return self._description
    #------------
    #------------
    # Set functions
    #------------
    @ d_ext.setter
    def d_ext(self,val): # pragma: no cover
        self._d_ext=float(val)
    #------------
    @ w_t.setter
    def w_t(self,val): # pragma: no cover
        self._w_t=float(val)
    #------------
    @ length.setter
    def length(self,val): # pragma: no cover
        self._length=float(val)
    #------------
    @ geometry_pref.setter
    def geometry_pref(self,val): # pragma: no cover
        self._geometry_pref=str(val)
    #------------
    @ base_lx.setter
    def base_lx(self,val): # pragma: no cover
        self._base_lx=float(val)
    #------------
    @ base_ly.setter
    def base_ly(self,val): # pragma: no cover
        self._base_ly=float(val)
    #------------
    @ base_radius.setter
    def base_radius(self,val): # pragma: no cover
        self._base_radius=float(val)
    #------------
    @ thickness.setter
    def thickness(self,val): # pragma: no cover
        self._thickness=float(val)
    #------------
    @ mass.setter
    def mass(self,val): # pragma: no cover
        self._mass=float(val)
    #------------
    @ drag_anchor_coefficients.setter
    def drag_anchor_coefficients(self,val): # pragma: no cover
        self._drag_anchor_coefficients=val
    #------------
    @ base_r1.setter
    def base_r1(self,val): # pragma: no cover
        self._base_r1=float(val)
    #------------
    @ base_r2.setter
    def base_r2(self,val): # pragma: no cover
        self._base_r2=float(val)
    #------------
    @ base_r3.setter
    def base_r3(self,val): # pragma: no cover
        self._base_r3=float(val)
    #------------
    @ base_alpha01.setter
    def base_alpha01(self,val): # pragma: no cover
        self._base_alpha01=float(val)
    #------------
    @ base_alpha12.setter
    def base_alpha12(self,val): # pragma: no cover
        self._base_alpha12=float(val)
    #------------
    @ base_alpha23.setter
    def base_alpha23(self,val): # pragma: no cover
        self._base_alpha23=float(val)
    #------------
    @ contact_points_mass.setter
    def contact_points_mass(self,val): # pragma: no cover
        self._contact_points_mass=float(val)
    #------------
    @ name.setter
    def name(self,val): # pragma: no cover
        self._name=str(val)
    #------------
    @ description.setter
    def description(self,val): # pragma: no cover
        self._description=str(val)
    #------------
    #-------------------------
    # Representation functions
    #-------------------------
    def type_rep(self): # pragma: no cover
        """Generate a representation of the object type

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains the representation of the object type
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtosk:inputs:CustomFoundationInputs"
        rep["name"] = self.name
        rep["description"] = self.description
        return rep
    def prop_rep(self, short = False, deep = True):

        """Generate a representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains a representation of the object properties
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtosk:inputs:CustomFoundationInputs"
        rep["name"] = self.name
        rep["description"] = self.description
        if self.is_set("d_ext"):
            rep["d_ext"] = self.d_ext
        if self.is_set("w_t"):
            rep["w_t"] = self.w_t
        if self.is_set("length"):
            rep["length"] = self.length
        if self.is_set("geometry_pref"):
            rep["geometry_pref"] = self.geometry_pref
        if self.is_set("base_lx"):
            rep["base_lx"] = self.base_lx
        if self.is_set("base_ly"):
            rep["base_ly"] = self.base_ly
        if self.is_set("base_radius"):
            rep["base_radius"] = self.base_radius
        if self.is_set("thickness"):
            rep["thickness"] = self.thickness
        if self.is_set("mass"):
            rep["mass"] = self.mass
        if self.is_set("drag_anchor_coefficients"):
            if (short):
                rep["drag_anchor_coefficients"] = str(self.drag_anchor_coefficients.shape)
            else:
                try:
                    rep["drag_anchor_coefficients"] = self.drag_anchor_coefficients.tolist()
                except:
                    rep["drag_anchor_coefficients"] = self.drag_anchor_coefficients
        if self.is_set("base_r1"):
            rep["base_r1"] = self.base_r1
        if self.is_set("base_r2"):
            rep["base_r2"] = self.base_r2
        if self.is_set("base_r3"):
            rep["base_r3"] = self.base_r3
        if self.is_set("base_alpha01"):
            rep["base_alpha01"] = self.base_alpha01
        if self.is_set("base_alpha12"):
            rep["base_alpha12"] = self.base_alpha12
        if self.is_set("base_alpha23"):
            rep["base_alpha23"] = self.base_alpha23
        if self.is_set("contact_points_mass"):
            rep["contact_points_mass"] = self.contact_points_mass
        if self.is_set("name"):
            rep["name"] = self.name
        if self.is_set("description"):
            rep["description"] = self.description
        return rep
    #-------------------------
    # Save functions
    #-------------------------
    def json_rep(self, short=False, deep=True):

        """Generate a JSON representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`str`: string that contains a JSON representation of the object properties
        """

        return ( json.dumps(self.prop_rep(short=short, deep=deep),indent=4, separators=(",",": ")))
    def saveJSON(self, fileName=None):
        """Save the instance of the object to JSON format file

        Args:
            fileName (:obj:`str`, optional): Name of the JSON file, included extension. Defaults is None. If None, the name of the JSON file will be self.name.json. It can also contain an absolute or relative path.

        """

        if fileName==None:
            fileName=self.name + ".json"
        f=open(fileName, "w")
        f.write(self.json_rep())
        f.write("\n")
        f.close()

    def saveHDF5(self,filePath=None):
        """Save the instance of the object to HDF5 format file
        Args:
            filePath (:obj:`str`, optional): Name of the HDF5 file, included extension. Defaults is None. If None, the name of the HDF5 file will be "self.name".h5. It can also contain an absolute or relative path.
        """
        # Define filepath
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".h5"
            else:
                raise Exception("name is required for saving")
        # Open hdf5 file
        h = h5py.File(filePath,"w")
        group = h.create_group(self.name)
        # Save the object to the group
        self.saveToHDF5Handle(group)
        # Close file
        h.close()
        pass
    def saveToHDF5Handle(self, handle):
        """Save the properties of the object to the hdf5 handle.
        Args:
            handle (:obj:`h5py.Group`): Handle used to store the object properties
        """
        handle["d_ext"] = np.array([self.d_ext],dtype=float)
        handle["w_t"] = np.array([self.w_t],dtype=float)
        handle["length"] = np.array([self.length],dtype=float)
        ar = []
        ar.append(self.geometry_pref.encode("ascii"))
        handle["geometry_pref"] = np.asarray(ar)
        handle["base_lx"] = np.array([self.base_lx],dtype=float)
        handle["base_ly"] = np.array([self.base_ly],dtype=float)
        handle["base_radius"] = np.array([self.base_radius],dtype=float)
        handle["thickness"] = np.array([self.thickness],dtype=float)
        handle["mass"] = np.array([self.mass],dtype=float)
        handle["drag_anchor_coefficients"] = np.array(self.drag_anchor_coefficients,dtype=float)
        handle["base_r1"] = np.array([self.base_r1],dtype=float)
        handle["base_r2"] = np.array([self.base_r2],dtype=float)
        handle["base_r3"] = np.array([self.base_r3],dtype=float)
        handle["base_alpha01"] = np.array([self.base_alpha01],dtype=float)
        handle["base_alpha12"] = np.array([self.base_alpha12],dtype=float)
        handle["base_alpha23"] = np.array([self.base_alpha23],dtype=float)
        handle["contact_points_mass"] = np.array([self.contact_points_mass],dtype=float)
        ar = []
        ar.append(self.name.encode("ascii"))
        handle["name"] = np.asarray(ar)
        ar = []
        ar.append(self.description.encode("ascii"))
        handle["description"] = np.asarray(ar)
    #-------------------------
    # Load functions
    #-------------------------
    def loadJSON(self,name = None, filePath = None):
        """Load an instance of the object from JSON format file

        Args:
            name (:obj:`str`, optional): Name of the object to load.
            filePath (:obj:`str`, optional): Path of the JSON file to load. If None, the function looks for a file with name "name".json.

        The JSON file must contain a datastructure representing an instance of this object's class, as generated by e.g. the function :func:`~saveJSON`.

        """

        if not(name == None):
            self.name = name
        if (name == None) and not(filePath == None):
            self.name = ".".join(filePath.split(os.path.sep)[-1].split(".")[0:-1])
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".json"
            else:
                raise Exception("object needs name for loading.")
        if not(os.path.isfile(filePath)):
            raise Exception("file %s not found."%filePath)
        self._loadedItems = []
        f = open(filePath,"r")
        data = f.read()
        f.close()
        dd = json.loads(data)
        self.loadFromJSONDict(dd)
    def loadFromJSONDict(self, data):
        """Load an instance of the object from a dictionnary representing a JSON structure)

        Args:
            name (:obj:`collections.OrderedDict`): Dictionnary containing a JSON structure, as produced by e.g. :func:`json.loads`

        """

        varName = "d_ext"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "w_t"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "length"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "geometry_pref"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "base_lx"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "base_ly"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "base_radius"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "thickness"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "mass"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "drag_anchor_coefficients"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "base_r1"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "base_r2"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "base_r3"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "base_alpha01"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "base_alpha12"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "base_alpha23"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "contact_points_mass"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "description"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
    def loadHDF5(self,name=None, filePath=None):
        """Load an instance of the object from HDF5 format file
        Args:
            name (:obj:`str`, optional): Name of the object to load. Default is None.
            filePath (:obj:`str`, optional): Path of the HDF5 file to load. If None, the function looks for a file with name "self.name".h5.
        The HDF5 file must contain a datastructure representing an instance of this objects class, as generated by e.g. the function :func:`~saveHDF5`.
        """
        # Define filepath and name
        if not(name == None):
            self.name = name
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".h5"
            else:
                raise Exception("name is required for loading")
        # Open hdf5 file
        h = h5py.File(filePath,"r")
        group = h[self.name]
        # Save the object to the group
        self.loadFromHDF5Handle(group)
        # Close file
        h.close()
        pass
    def loadFromHDF5Handle(self, gr):
        """Load the properties of the object from a hdf5 handle.
        Args:
            gr (:obj:`h5py.Group`): Handle used to read the object properties from
        """
        if ("d_ext" in list(gr.keys())):
            self.d_ext = gr["d_ext"][0]
        if ("w_t" in list(gr.keys())):
            self.w_t = gr["w_t"][0]
        if ("length" in list(gr.keys())):
            self.length = gr["length"][0]
        if ("geometry_pref" in list(gr.keys())):
            self.geometry_pref = gr["geometry_pref"][0].decode("ascii")
        if ("base_lx" in list(gr.keys())):
            self.base_lx = gr["base_lx"][0]
        if ("base_ly" in list(gr.keys())):
            self.base_ly = gr["base_ly"][0]
        if ("base_radius" in list(gr.keys())):
            self.base_radius = gr["base_radius"][0]
        if ("thickness" in list(gr.keys())):
            self.thickness = gr["thickness"][0]
        if ("mass" in list(gr.keys())):
            self.mass = gr["mass"][0]
        if ("drag_anchor_coefficients" in list(gr.keys())):
            self.drag_anchor_coefficients = gr["drag_anchor_coefficients"][:]
        if ("base_r1" in list(gr.keys())):
            self.base_r1 = gr["base_r1"][0]
        if ("base_r2" in list(gr.keys())):
            self.base_r2 = gr["base_r2"][0]
        if ("base_r3" in list(gr.keys())):
            self.base_r3 = gr["base_r3"][0]
        if ("base_alpha01" in list(gr.keys())):
            self.base_alpha01 = gr["base_alpha01"][0]
        if ("base_alpha12" in list(gr.keys())):
            self.base_alpha12 = gr["base_alpha12"][0]
        if ("base_alpha23" in list(gr.keys())):
            self.base_alpha23 = gr["base_alpha23"][0]
        if ("contact_points_mass" in list(gr.keys())):
            self.contact_points_mass = gr["contact_points_mass"][0]
        if ("name" in list(gr.keys())):
            self.name = gr["name"][0].decode("ascii")
        if ("description" in list(gr.keys())):
            self.description = gr["description"][0].decode("ascii")
    #------------------------
    # is_set function
    #------------------------
    def is_set(self, varName): # pragma: no cover

        """Check if a given property of the object is set

        Args:
            varName (:obj:`str`): name of the property to check

        Returns:
            :obj:`bool`: True if the property is set, else False
        """

        if (isinstance(getattr(self,varName),list) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (isinstance(getattr(self,varName),np.ndarray) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (getattr(self,varName) != None):
            return True
        return False
    #------------------------
