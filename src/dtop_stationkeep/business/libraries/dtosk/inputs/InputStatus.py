# This is the Station Keeping module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Neil Luxcey, Nicolas Michelet, Rocio Isorna
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import collections
import numpy as np
import os
import json
import h5py
#------------------------------------
# @ USER DEFINED IMPORTS START
# @ USER DEFINED IMPORTS END
#------------------------------------

class InputStatus():

    """Status structure to indicate to the user what he filled in the GUI
    """

    #------------
    # Constructor
    #------------
    def __init__(self,name=None):
#------------------------------------
# @ USER DEFINED DESCRIPTION START
# @ USER DEFINED DESCRIPTION END
#------------------------------------

        self._device_properties_page='False'
        self._master_structure_properties_page='False'
        self._substation_properties_page='False'
        self._uls_fls_analysis_parameters_page='False'
        self._devices_positionning_page='False'
        self._complexity_level=1
        self._name='none'
        self._flag_url_sc_get_device_statistics='False'
        self._flag_url_sc_get_farm='False'
        self._flag_url_mc_get_single_machine_hydrodynamic='False'
        self._flag_url_mc_get_wec_cplx1='False'
        self._flag_url_mc_get_wec_cplx2='False'
        self._flag_url_mc_get_wec_cplx3='False'
        self._flag_url_mc_get_tec_cplx1='False'
        self._flag_url_mc_get_tec_cplx2='False'
        self._flag_url_mc_get_tec_cplx3='False'
        self._flag_url_mc_get_general='False'
        self._flag_url_mc_get_dimensions='False'
        self._flag_url_mc_get_machine='False'
        self._flag_url_ec_get_farm='False'
        self._flag_url_ec_get_farm_inputs='False'
        self._flag_url_ed_get_study_results='False'
        self._entity_id=''
        self._entity_status=0
        self._description=''
        if not(name == None): # pragma: no cover
            self._name = name

#------------------------------------
# @ USER DEFINED PROPERTIES START
# @ USER DEFINED PROPERTIES END
#------------------------------------

#------------------------------------
# @ USER DEFINED METHODS START
# @ USER DEFINED METHODS END
#------------------------------------

    #------------
    # Get functions
    #------------
    @ property
    def device_properties_page(self): # pragma: no cover
        """str: Check if the Device Properties page was filled by the user.
        """
        return self._device_properties_page
    #------------
    @ property
    def master_structure_properties_page(self): # pragma: no cover
        """str: Check if the Master Structure Properties page was filled by the user. Used only if the complexity level is 3 and if the user select the positioning_reference = masterstructure
        """
        return self._master_structure_properties_page
    #------------
    @ property
    def substation_properties_page(self): # pragma: no cover
        """str: Check if the Substation Properties page was filled by the user. Used only if substation_is_present = True
        """
        return self._substation_properties_page
    #------------
    @ property
    def uls_fls_analysis_parameters_page(self): # pragma: no cover
        """str: Check if the ULS FLS Analysis Parameter page was filled by the user.
        """
        return self._uls_fls_analysis_parameters_page
    #------------
    @ property
    def devices_positionning_page(self): # pragma: no cover
        """str: Check if the Devices Positionning page was filled by the user.
        """
        return self._devices_positionning_page
    #------------
    @ property
    def complexity_level(self): # pragma: no cover
        """int: Complexity level. []
        """
        return self._complexity_level
    #------------
    @ property
    def name(self): # pragma: no cover
        """str: name of the instance object
        """
        return self._name
    #------------
    @ property
    def flag_url_sc_get_device_statistics(self): # pragma: no cover
        """str: Used only if the module is in integrated mode. True if data from flag_url_sc_get_device_statistics were loaded
        """
        return self._flag_url_sc_get_device_statistics
    #------------
    @ property
    def flag_url_sc_get_farm(self): # pragma: no cover
        """str: Used only if the module is in integrated mode. True if data from flag_url_sc_get_farm were loaded
        """
        return self._flag_url_sc_get_farm
    #------------
    @ property
    def flag_url_mc_get_single_machine_hydrodynamic(self): # pragma: no cover
        """str: Used only if the module is in integrated mode. True if data from flag_url_mc_get_single_machine_hydrodynamic were loaded
        """
        return self._flag_url_mc_get_single_machine_hydrodynamic
    #------------
    @ property
    def flag_url_mc_get_wec_cplx1(self): # pragma: no cover
        """str: Used only if the module is in integrated mode. True if data from flag_url_mc_get_wec_cplx1 were loaded
        """
        return self._flag_url_mc_get_wec_cplx1
    #------------
    @ property
    def flag_url_mc_get_wec_cplx2(self): # pragma: no cover
        """str: Used only if the module is in integrated mode. True if data from flag_url_mc_get_wec_cplx2 were loaded
        """
        return self._flag_url_mc_get_wec_cplx2
    #------------
    @ property
    def flag_url_mc_get_wec_cplx3(self): # pragma: no cover
        """str: Used only if the module is in integrated mode. True if data from flag_url_mc_get_wec_cplx3 were loaded
        """
        return self._flag_url_mc_get_wec_cplx3
    #------------
    @ property
    def flag_url_mc_get_tec_cplx1(self): # pragma: no cover
        """str: Used only if the module is in integrated mode. True if data from flag_url_mc_get_tec_cplx1 were loaded
        """
        return self._flag_url_mc_get_tec_cplx1
    #------------
    @ property
    def flag_url_mc_get_tec_cplx2(self): # pragma: no cover
        """str: Used only if the module is in integrated mode. True if data from flag_url_mc_get_tec_cplx2 were loaded
        """
        return self._flag_url_mc_get_tec_cplx2
    #------------
    @ property
    def flag_url_mc_get_tec_cplx3(self): # pragma: no cover
        """str: Used only if the module is in integrated mode. True if data from flag_url_mc_get_tec_cplx3 were loaded
        """
        return self._flag_url_mc_get_tec_cplx3
    #------------
    @ property
    def flag_url_mc_get_general(self): # pragma: no cover
        """str: Used only if the module is in integrated mode. True if data from flag_url_mc_get_general were loaded
        """
        return self._flag_url_mc_get_general
    #------------
    @ property
    def flag_url_mc_get_dimensions(self): # pragma: no cover
        """str: Used only if the module is in integrated mode. True if data from flag_url_mc_get_dimensions were loaded
        """
        return self._flag_url_mc_get_dimensions
    #------------
    @ property
    def flag_url_mc_get_machine(self): # pragma: no cover
        """str: Used only if the module is in integrated mode. True if data from flag_url_mc_get_machine were loaded
        """
        return self._flag_url_mc_get_machine
    #------------
    @ property
    def flag_url_ec_get_farm(self): # pragma: no cover
        """str: Used only if the module is in integrated mode. True if data from flag_url_ec_get_farm were loaded
        """
        return self._flag_url_ec_get_farm
    #------------
    @ property
    def flag_url_ec_get_farm_inputs(self): # pragma: no cover
        """str: Used only if the module is in integrated mode. True if data from flag_url_ec_get_farm_inputs were loaded
        """
        return self._flag_url_ec_get_farm_inputs
    #------------
    @ property
    def flag_url_ed_get_study_results(self): # pragma: no cover
        """str: Used only if the module is in integrated mode. True if data from flag_url_ed_get_study_results were loaded
        """
        return self._flag_url_ed_get_study_results
    #------------
    @ property
    def entity_id(self): # pragma: no cover
        """int: ID of the entity.
        """
        return self._entity_id
    #------------
    @ property
    def entity_status(self): # pragma: no cover
        """int: status of the entity.
        """
        return self._entity_status
    #------------
    @ property
    def description(self): # pragma: no cover
        """str: description of the instance object
        """
        return self._description
    #------------
    #------------
    # Set functions
    #------------
    @ device_properties_page.setter
    def device_properties_page(self,val): # pragma: no cover
        self._device_properties_page=str(val)
    #------------
    @ master_structure_properties_page.setter
    def master_structure_properties_page(self,val): # pragma: no cover
        self._master_structure_properties_page=str(val)
    #------------
    @ substation_properties_page.setter
    def substation_properties_page(self,val): # pragma: no cover
        self._substation_properties_page=str(val)
    #------------
    @ uls_fls_analysis_parameters_page.setter
    def uls_fls_analysis_parameters_page(self,val): # pragma: no cover
        self._uls_fls_analysis_parameters_page=str(val)
    #------------
    @ devices_positionning_page.setter
    def devices_positionning_page(self,val): # pragma: no cover
        self._devices_positionning_page=str(val)
    #------------
    @ complexity_level.setter
    def complexity_level(self,val): # pragma: no cover
        self._complexity_level=int(val)
    #------------
    @ name.setter
    def name(self,val): # pragma: no cover
        self._name=str(val)
    #------------
    @ flag_url_sc_get_device_statistics.setter
    def flag_url_sc_get_device_statistics(self,val): # pragma: no cover
        self._flag_url_sc_get_device_statistics=str(val)
    #------------
    @ flag_url_sc_get_farm.setter
    def flag_url_sc_get_farm(self,val): # pragma: no cover
        self._flag_url_sc_get_farm=str(val)
    #------------
    @ flag_url_mc_get_single_machine_hydrodynamic.setter
    def flag_url_mc_get_single_machine_hydrodynamic(self,val): # pragma: no cover
        self._flag_url_mc_get_single_machine_hydrodynamic=str(val)
    #------------
    @ flag_url_mc_get_wec_cplx1.setter
    def flag_url_mc_get_wec_cplx1(self,val): # pragma: no cover
        self._flag_url_mc_get_wec_cplx1=str(val)
    #------------
    @ flag_url_mc_get_wec_cplx2.setter
    def flag_url_mc_get_wec_cplx2(self,val): # pragma: no cover
        self._flag_url_mc_get_wec_cplx2=str(val)
    #------------
    @ flag_url_mc_get_wec_cplx3.setter
    def flag_mflag_url_mc_get_wec_cplx3c(self,val): # pragma: no cover
        self._flag_url_mc_get_wec_cplx3=str(val)
    #------------
    @ flag_url_mc_get_tec_cplx1.setter
    def flag_url_mc_get_tec_cplx1(self,val): # pragma: no cover
        self._flag_url_mc_get_tec_cplx1=str(val)
    #------------
    @ flag_url_mc_get_tec_cplx2.setter
    def flag_url_mc_get_tec_cplx2(self,val): # pragma: no cover
        self._flag_url_mc_get_tec_cplx2=str(val)
    #------------
    @ flag_url_mc_get_tec_cplx3.setter
    def flag_url_mc_get_tec_cplx3(self,val): # pragma: no cover
        self._flag_url_mc_get_tec_cplx3=str(val)
    #------------
    @ flag_url_mc_get_general.setter
    def flag_url_mc_get_general(self,val): # pragma: no cover
        self._flag_url_mc_get_general=str(val)
    #------------
    @ flag_url_mc_get_dimensions.setter
    def flag_url_mc_get_dimensions(self,val): # pragma: no cover
        self._flag_url_mc_get_dimensions=str(val)
    #------------
    @ flag_url_mc_get_machine.setter
    def flag_url_mc_get_machine(self,val): # pragma: no cover
        self._flag_url_mc_get_machine=str(val)
    #------------
    @ flag_url_ec_get_farm.setter
    def flag_url_ec_get_farm(self,val): # pragma: no cover
        self._flag_url_ec_get_farm=str(val)
    #------------
    @ flag_url_ec_get_farm_inputs.setter
    def flag_url_ec_get_farm_inputs(self,val): # pragma: no cover
        self._flag_url_ec_get_farm_inputs=str(val)
    #------------
    @ flag_url_ed_get_study_results.setter
    def flag_url_ed_get_study_results(self,val): # pragma: no cover
        self._flag_url_ed_get_study_results=str(val)
    #------------
    @ entity_id.setter
    def entity_id(self,val): # pragma: no cover
        self._entity_id=int(val)
    #------------
    @ entity_status.setter
    def entity_status(self,val): # pragma: no cover
        self._entity_status=int(val)
    #------------
    @ description.setter
    def description(self,val): # pragma: no cover
        self._description=str(val)
    #------------
    #-------------------------
    # Representation functions
    #-------------------------
    def type_rep(self): # pragma: no cover
        """Generate a representation of the object type

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains the representation of the object type
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtosk:inputs:InputStatus"
        rep["name"] = self.name
        rep["description"] = self.description
        return rep
    def prop_rep(self, short = False, deep = True):

        """Generate a representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains a representation of the object properties
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtosk:inputs:InputStatus"
        rep["name"] = self.name
        rep["description"] = self.description
        if self.is_set("device_properties_page"):
            rep["device_properties_page"] = self.device_properties_page
        if self.is_set("master_structure_properties_page"):
            rep["master_structure_properties_page"] = self.master_structure_properties_page
        if self.is_set("substation_properties_page"):
            rep["substation_properties_page"] = self.substation_properties_page
        if self.is_set("uls_fls_analysis_parameters_page"):
            rep["uls_fls_analysis_parameters_page"] = self.uls_fls_analysis_parameters_page
        if self.is_set("devices_positionning_page"):
            rep["devices_positionning_page"] = self.devices_positionning_page
        if self.is_set("complexity_level"):
            rep["complexity_level"] = self.complexity_level
        if self.is_set("name"):
            rep["name"] = self.name
        if self.is_set("flag_url_sc_get_device_statistics"):
            rep["flag_url_sc_get_device_statistics"] = self.flag_url_sc_get_device_statistics
        if self.is_set("flag_url_sc_get_farm"):
            rep["flag_url_sc_get_farm"] = self.flag_url_sc_get_farm
        if self.is_set("flag_url_mc_get_single_machine_hydrodynamic"):
            rep["flag_url_mc_get_single_machine_hydrodynamic"] = self.flag_url_mc_get_single_machine_hydrodynamic
        if self.is_set("flag_url_mc_get_wec_cplx1"):
            rep["flag_url_mc_get_wec_cplx1"] = self.flag_url_mc_get_wec_cplx1
        if self.is_set("flag_url_mc_get_wec_cplx2"):
            rep["flag_url_mc_get_wec_cplx2"] = self.flag_url_mc_get_wec_cplx2
        if self.is_set("flag_url_mc_get_wec_cplx3"):
            rep["flag_url_mc_get_wec_cplx3"] = self.flag_url_mc_get_wec_cplx3
        if self.is_set("flag_url_mc_get_tec_cplx1"):
            rep["flag_url_mc_get_tec_cplx1"] = self.flag_url_mc_get_tec_cplx1
        if self.is_set("flag_url_mc_get_tec_cplx2"):
            rep["flag_url_mc_get_tec_cplx2"] = self.flag_url_mc_get_tec_cplx2
        if self.is_set("flag_url_mc_get_tec_cplx3"):
            rep["flag_url_mc_get_tec_cplx3"] = self.flag_url_mc_get_tec_cplx3
        if self.is_set("flag_url_mc_get_general"):
            rep["flag_url_mc_get_general"] = self.flag_url_mc_get_general
        if self.is_set("flag_url_mc_get_dimensions"):
            rep["flag_url_mc_get_dimensions"] = self.flag_url_mc_get_dimensions
        if self.is_set("flag_url_mc_get_machine"):
            rep["flag_url_mc_get_machine"] = self.flag_url_mc_get_machine
        if self.is_set("flag_url_ec_get_farm"):
            rep["flag_url_ec_get_farm"] = self.flag_url_ec_get_farm
        if self.is_set("flag_url_ec_get_farm_inputs"):
            rep["flag_url_ec_get_farm_inputs"] = self.flag_url_ec_get_farm_inputs
        if self.is_set("flag_url_ed_get_study_results"):
            rep["flag_url_ed_get_study_results"] = self.flag_url_ed_get_study_results
        if self.is_set("entity_id"):
            rep["entity_id"] = self.entity_id
        if self.is_set("entity_status"):
            rep["entity_status"] = self.entity_status
        if self.is_set("description"):
            rep["description"] = self.description
        return rep
    #-------------------------
    # Save functions
    #-------------------------
    def json_rep(self, short=False, deep=True):

        """Generate a JSON representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`str`: string that contains a JSON representation of the object properties
        """

        return ( json.dumps(self.prop_rep(short=short, deep=deep),indent=4, separators=(",",": ")))
    def saveJSON(self, fileName=None):
        """Save the instance of the object to JSON format file

        Args:
            fileName (:obj:`str`, optional): Name of the JSON file, included extension. Defaults is None. If None, the name of the JSON file will be self.name.json. It can also contain an absolute or relative path.

        """

        if fileName==None:
            fileName=self.name + ".json"
        f=open(fileName, "w")
        f.write(self.json_rep())
        f.write("\n")
        f.close()

    def saveHDF5(self,filePath=None):
        """Save the instance of the object to HDF5 format file
        Args:
            filePath (:obj:`str`, optional): Name of the HDF5 file, included extension. Defaults is None. If None, the name of the HDF5 file will be "self.name".h5. It can also contain an absolute or relative path.
        """
        # Define filepath
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".h5"
            else:
                raise Exception("name is required for saving")
        # Open hdf5 file
        h = h5py.File(filePath,"w")
        group = h.create_group(self.name)
        # Save the object to the group
        self.saveToHDF5Handle(group)
        # Close file
        h.close()
        pass
    def saveToHDF5Handle(self, handle):
        """Save the properties of the object to the hdf5 handle.
        Args:
            handle (:obj:`h5py.Group`): Handle used to store the object properties
        """
        ar = []
        ar.append(self.device_properties_page.encode("ascii"))
        handle["device_properties_page"] = np.asarray(ar)
        ar = []
        ar.append(self.master_structure_properties_page.encode("ascii"))
        handle["master_structure_properties_page"] = np.asarray(ar)
        ar = []
        ar.append(self.substation_properties_page.encode("ascii"))
        handle["substation_properties_page"] = np.asarray(ar)
        ar = []
        ar.append(self.uls_fls_analysis_parameters_page.encode("ascii"))
        handle["uls_fls_analysis_parameters_page"] = np.asarray(ar)
        ar = []
        ar.append(self.devices_positionning_page.encode("ascii"))
        handle["devices_positionning_page"] = np.asarray(ar)
        handle["complexity_level"] = np.array([self.complexity_level],dtype=int)
        ar = []
        ar.append(self.name.encode("ascii"))
        handle["name"] = np.asarray(ar)
        ar = []
        ar.append(self.description.encode("ascii"))
        handle["description"] = np.asarray(ar)
    #-------------------------
    # Load functions
    #-------------------------
    def loadJSON(self,name = None, filePath = None):
        """Load an instance of the object from JSON format file

        Args:
            name (:obj:`str`, optional): Name of the object to load.
            filePath (:obj:`str`, optional): Path of the JSON file to load. If None, the function looks for a file with name "name".json.

        The JSON file must contain a datastructure representing an instance of this object's class, as generated by e.g. the function :func:`~saveJSON`.

        """

        if not(name == None):
            self.name = name
        if (name == None) and not(filePath == None):
            self.name = ".".join(filePath.split(os.path.sep)[-1].split(".")[0:-1])
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".json"
            else:
                raise Exception("object needs name for loading.")
        if not(os.path.isfile(filePath)):
            raise Exception("file %s not found."%filePath)
        self._loadedItems = []
        f = open(filePath,"r")
        data = f.read()
        f.close()
        dd = json.loads(data)
        self.loadFromJSONDict(dd)
    def loadFromJSONDict(self, data):
        """Load an instance of the object from a dictionnary representing a JSON structure)

        Args:
            name (:obj:`collections.OrderedDict`): Dictionnary containing a JSON structure, as produced by e.g. :func:`json.loads`

        """

        varName = "device_properties_page"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "master_structure_properties_page"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "substation_properties_page"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "uls_fls_analysis_parameters_page"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "devices_positionning_page"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "complexity_level"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "flag_url_sc_get_device_statistics"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "flag_url_sc_get_farm"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "flag_url_mc_get_single_machine_hydrodynamic"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "flag_url_mc_get_wec_cplx1"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "flag_url_mc_get_wec_cplx2"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "flag_url_mc_get_wec_cplx3"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "flag_url_mc_get_tec_cplx1"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "flag_url_mc_get_tec_cplx2"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "flag_url_mc_get_tec_cplx3"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "flag_url_mc_get_general"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "flag_url_mc_get_dimensions"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "flag_url_mc_get_machine"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "flag_url_ec_get_farm"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "flag_url_ec_get_farm_inputs"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "flag_url_ed_get_study_results"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "entity_id"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "entity_status"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "description"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
    def loadHDF5(self,name=None, filePath=None):
        """Load an instance of the object from HDF5 format file
        Args:
            name (:obj:`str`, optional): Name of the object to load. Default is None.
            filePath (:obj:`str`, optional): Path of the HDF5 file to load. If None, the function looks for a file with name "self.name".h5.
        The HDF5 file must contain a datastructure representing an instance of this objects class, as generated by e.g. the function :func:`~saveHDF5`.
        """
        # Define filepath and name
        if not(name == None):
            self.name = name
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".h5"
            else:
                raise Exception("name is required for loading")
        # Open hdf5 file
        h = h5py.File(filePath,"r")
        group = h[self.name]
        # Save the object to the group
        self.loadFromHDF5Handle(group)
        # Close file
        h.close()
        pass
    def loadFromHDF5Handle(self, gr):
        """Load the properties of the object from a hdf5 handle.
        Args:
            gr (:obj:`h5py.Group`): Handle used to read the object properties from
        """
        if ("device_properties_page" in list(gr.keys())):
            self.device_properties_page = gr["device_properties_page"][0].decode("ascii")
        if ("master_structure_properties_page" in list(gr.keys())):
            self.master_structure_properties_page = gr["master_structure_properties_page"][0].decode("ascii")
        if ("substation_properties_page" in list(gr.keys())):
            self.substation_properties_page = gr["substation_properties_page"][0].decode("ascii")
        if ("uls_fls_analysis_parameters_page" in list(gr.keys())):
            self.uls_fls_analysis_parameters_page = gr["uls_fls_analysis_parameters_page"][0].decode("ascii")
        if ("devices_positionning_page" in list(gr.keys())):
            self.devices_positionning_page = gr["devices_positionning_page"][0].decode("ascii")
        if ("complexity_level" in list(gr.keys())):
            self.complexity_level = gr["complexity_level"][0]
        if ("name" in list(gr.keys())):
            self.name = gr["name"][0].decode("ascii")
        if ("description" in list(gr.keys())):
            self.description = gr["description"][0].decode("ascii")
    #------------------------
    # is_set function
    #------------------------
    def is_set(self, varName): # pragma: no cover

        """Check if a given property of the object is set

        Args:
            varName (:obj:`str`): name of the property to check

        Returns:
            :obj:`bool`: True if the property is set, else False
        """

        if (isinstance(getattr(self,varName),list) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (isinstance(getattr(self,varName),np.ndarray) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (getattr(self,varName) != None):
            return True
        return False
    #------------------------
