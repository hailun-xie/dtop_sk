# This is the Station Keeping module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Neil Luxcey, Nicolas Michelet, Rocio Isorna
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import collections
import numpy as np
import os
import json
import h5py
from . import FoundationDesignParameters
from ..mooring import MooringDesignCriteria
from . import CustomMooringInput
#------------------------------------
# @ USER DEFINED IMPORTS START
# @ USER DEFINED IMPORTS END
#------------------------------------

class MasterStructureProperties():

    """data model representing a masterstructure to which some other devices are moored/fixed to
    """

    #------------
    # Constructor
    #------------
    def __init__(self,name=None):
#------------------------------------
# @ USER DEFINED DESCRIPTION START
# @ USER DEFINED DESCRIPTION END
#------------------------------------

        self._positioning_type='moored'
        self._mass=0.0
        self._mooring_type='catenary'
        self._foundation_design_parameters=FoundationDesignParameters.FoundationDesignParameters()
        self._foundation_design_parameters.description = 'Parameter inputs for foundation design'
        self._mooring_design_criteria=MooringDesignCriteria.MooringDesignCriteria()
        self._mooring_design_criteria.description = 'Criteria for mooring design, safety_factor is always necessary, the rest is required only if design is chosen'
        self._mooring_input_flag='custom'
        self._custom_mooring_input=CustomMooringInput.CustomMooringInput()
        self._custom_mooring_input.description = 'Mooring system input of the device, required if mooring_input_flag is custom'
        self._file_mooring_input=''
        self._east=0.0
        self._north=0.0
        self._water_depth=0.0
        self._name='none'
        self._description=''
        if not(name == None): # pragma: no cover
            self._name = name

#------------------------------------
# @ USER DEFINED PROPERTIES START
# @ USER DEFINED PROPERTIES END
#------------------------------------

#------------------------------------
# @ USER DEFINED METHODS START
# @ USER DEFINED METHODS END
#------------------------------------

    #------------
    # Get functions
    #------------
    @ property
    def positioning_type(self): # pragma: no cover
        """str: moored or fixed
        """
        return self._positioning_type
    #------------
    @ property
    def mass(self): # pragma: no cover
        """float: Mass of the masterstrucutre, if fixed []
        """
        return self._mass
    #------------
    @ property
    def mooring_type(self): # pragma: no cover
        """str: mooring type of the master structure : catenary, taut, semitaut
        """
        return self._mooring_type
    #------------
    @ property
    def foundation_design_parameters(self): # pragma: no cover
        """:obj:`~.FoundationDesignParameters.FoundationDesignParameters`: Parameter inputs for foundation design
        """
        return self._foundation_design_parameters
    #------------
    @ property
    def mooring_design_criteria(self): # pragma: no cover
        """:obj:`~.MooringDesignCriteria.MooringDesignCriteria`: Criteria for mooring design, safety_factor is always necessary, the rest is required only if design is chosen
        """
        return self._mooring_design_criteria
    #------------
    @ property
    def mooring_input_flag(self): # pragma: no cover
        """str: Flag describing how the mooring system is defined: custom, input_file (design is not available for masterstructure)
        """
        return self._mooring_input_flag
    #------------
    @ property
    def custom_mooring_input(self): # pragma: no cover
        """:obj:`~.CustomMooringInput.CustomMooringInput`: Mooring system input of the device, required if mooring_input_flag is custom
        """
        return self._custom_mooring_input
    #------------
    @ property
    def file_mooring_input(self): # pragma: no cover
        """str: Path to JSON file that describes a custom_mooring_input_flag, required if mooring_input_flag is input_file
        """
        return self._file_mooring_input
    #------------
    @ property
    def east(self): # pragma: no cover
        """float: Master structure east position []
        """
        return self._east
    #------------
    @ property
    def north(self): # pragma: no cover
        """float: Master structure north position []
        """
        return self._north
    #------------
    @ property
    def water_depth(self): # pragma: no cover
        """float: Water depth at master structure location [m]
        """
        return self._water_depth
    #------------
    @ property
    def name(self): # pragma: no cover
        """str: name of the instance object
        """
        return self._name
    #------------
    @ property
    def description(self): # pragma: no cover
        """str: description of the instance object
        """
        return self._description
    #------------
    #------------
    # Set functions
    #------------
    @ positioning_type.setter
    def positioning_type(self,val): # pragma: no cover
        self._positioning_type=str(val)
    #------------
    @ mass.setter
    def mass(self,val): # pragma: no cover
        self._mass=float(val)
    #------------
    @ mooring_type.setter
    def mooring_type(self,val): # pragma: no cover
        self._mooring_type=str(val)
    #------------
    @ foundation_design_parameters.setter
    def foundation_design_parameters(self,val): # pragma: no cover
        self._foundation_design_parameters=val
    #------------
    @ mooring_design_criteria.setter
    def mooring_design_criteria(self,val): # pragma: no cover
        self._mooring_design_criteria=val
    #------------
    @ mooring_input_flag.setter
    def mooring_input_flag(self,val): # pragma: no cover
        self._mooring_input_flag=str(val)
    #------------
    @ custom_mooring_input.setter
    def custom_mooring_input(self,val): # pragma: no cover
        self._custom_mooring_input=val
    #------------
    @ file_mooring_input.setter
    def file_mooring_input(self,val): # pragma: no cover
        self._file_mooring_input=str(val)
    #------------
    @ east.setter
    def east(self,val): # pragma: no cover
        self._east=float(val)
    #------------
    @ north.setter
    def north(self,val): # pragma: no cover
        self._north=float(val)
    #------------
    @ water_depth.setter
    def water_depth(self,val): # pragma: no cover
        self._water_depth=float(val)
    #------------
    @ name.setter
    def name(self,val): # pragma: no cover
        self._name=str(val)
    #------------
    @ description.setter
    def description(self,val): # pragma: no cover
        self._description=str(val)
    #------------
    #-------------------------
    # Representation functions
    #-------------------------
    def type_rep(self): # pragma: no cover
        """Generate a representation of the object type

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains the representation of the object type
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtosk:inputs:MasterStructureProperties"
        rep["name"] = self.name
        rep["description"] = self.description
        return rep
    def prop_rep(self, short = False, deep = True):

        """Generate a representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains a representation of the object properties
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtosk:inputs:MasterStructureProperties"
        rep["name"] = self.name
        rep["description"] = self.description
        if self.is_set("positioning_type"):
            rep["positioning_type"] = self.positioning_type
        if self.is_set("mass"):
            rep["mass"] = self.mass
        if self.is_set("mooring_type"):
            rep["mooring_type"] = self.mooring_type
        if self.is_set("foundation_design_parameters"):
            if (short and not(deep)):
                rep["foundation_design_parameters"] = self.foundation_design_parameters.type_rep()
            else:
                rep["foundation_design_parameters"] = self.foundation_design_parameters.prop_rep(short, deep)
        if self.is_set("mooring_design_criteria"):
            if (short and not(deep)):
                rep["mooring_design_criteria"] = self.mooring_design_criteria.type_rep()
            else:
                rep["mooring_design_criteria"] = self.mooring_design_criteria.prop_rep(short, deep)
        if self.is_set("mooring_input_flag"):
            rep["mooring_input_flag"] = self.mooring_input_flag
        if self.is_set("custom_mooring_input"):
            if (short and not(deep)):
                rep["custom_mooring_input"] = self.custom_mooring_input.type_rep()
            else:
                rep["custom_mooring_input"] = self.custom_mooring_input.prop_rep(short, deep)
        if self.is_set("file_mooring_input"):
            rep["file_mooring_input"] = self.file_mooring_input
        if self.is_set("east"):
            rep["east"] = self.east
        if self.is_set("north"):
            rep["north"] = self.north
        if self.is_set("water_depth"):
            rep["water_depth"] = self.water_depth
        if self.is_set("name"):
            rep["name"] = self.name
        if self.is_set("description"):
            rep["description"] = self.description
        return rep
    #-------------------------
    # Save functions
    #-------------------------
    def json_rep(self, short=False, deep=True):

        """Generate a JSON representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`str`: string that contains a JSON representation of the object properties
        """

        return ( json.dumps(self.prop_rep(short=short, deep=deep),indent=4, separators=(",",": ")))
    def saveJSON(self, fileName=None):
        """Save the instance of the object to JSON format file

        Args:
            fileName (:obj:`str`, optional): Name of the JSON file, included extension. Defaults is None. If None, the name of the JSON file will be self.name.json. It can also contain an absolute or relative path.

        """

        if fileName==None:
            fileName=self.name + ".json"
        f=open(fileName, "w")
        f.write(self.json_rep())
        f.write("\n")
        f.close()

    def saveHDF5(self,filePath=None):
        """Save the instance of the object to HDF5 format file
        Args:
            filePath (:obj:`str`, optional): Name of the HDF5 file, included extension. Defaults is None. If None, the name of the HDF5 file will be "self.name".h5. It can also contain an absolute or relative path.
        """
        # Define filepath
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".h5"
            else:
                raise Exception("name is required for saving")
        # Open hdf5 file
        h = h5py.File(filePath,"w")
        group = h.create_group(self.name)
        # Save the object to the group
        self.saveToHDF5Handle(group)
        # Close file
        h.close()
        pass
    def saveToHDF5Handle(self, handle):
        """Save the properties of the object to the hdf5 handle.
        Args:
            handle (:obj:`h5py.Group`): Handle used to store the object properties
        """
        ar = []
        ar.append(self.positioning_type.encode("ascii"))
        handle["positioning_type"] = np.asarray(ar)
        handle["mass"] = np.array([self.mass],dtype=float)
        ar = []
        ar.append(self.mooring_type.encode("ascii"))
        handle["mooring_type"] = np.asarray(ar)
        subgroup = handle.create_group("foundation_design_parameters")
        self.foundation_design_parameters.saveToHDF5Handle(subgroup)
        subgroup = handle.create_group("mooring_design_criteria")
        self.mooring_design_criteria.saveToHDF5Handle(subgroup)
        ar = []
        ar.append(self.mooring_input_flag.encode("ascii"))
        handle["mooring_input_flag"] = np.asarray(ar)
        subgroup = handle.create_group("custom_mooring_input")
        self.custom_mooring_input.saveToHDF5Handle(subgroup)
        ar = []
        ar.append(self.file_mooring_input.encode("ascii"))
        handle["file_mooring_input"] = np.asarray(ar)
        handle["east"] = np.array([self.east],dtype=float)
        handle["north"] = np.array([self.north],dtype=float)
        handle["water_depth"] = np.array([self.water_depth],dtype=float)
        ar = []
        ar.append(self.name.encode("ascii"))
        handle["name"] = np.asarray(ar)
        ar = []
        ar.append(self.description.encode("ascii"))
        handle["description"] = np.asarray(ar)
    #-------------------------
    # Load functions
    #-------------------------
    def loadJSON(self,name = None, filePath = None):
        """Load an instance of the object from JSON format file

        Args:
            name (:obj:`str`, optional): Name of the object to load.
            filePath (:obj:`str`, optional): Path of the JSON file to load. If None, the function looks for a file with name "name".json.

        The JSON file must contain a datastructure representing an instance of this object's class, as generated by e.g. the function :func:`~saveJSON`.

        """

        if not(name == None):
            self.name = name
        if (name == None) and not(filePath == None):
            self.name = ".".join(filePath.split(os.path.sep)[-1].split(".")[0:-1])
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".json"
            else:
                raise Exception("object needs name for loading.")
        if not(os.path.isfile(filePath)):
            raise Exception("file %s not found."%filePath)
        self._loadedItems = []
        f = open(filePath,"r")
        data = f.read()
        f.close()
        dd = json.loads(data)
        self.loadFromJSONDict(dd)
    def loadFromJSONDict(self, data):
        """Load an instance of the object from a dictionnary representing a JSON structure)

        Args:
            name (:obj:`collections.OrderedDict`): Dictionnary containing a JSON structure, as produced by e.g. :func:`json.loads`

        """

        varName = "positioning_type"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "mass"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "mooring_type"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "foundation_design_parameters"
        try :
            if data[varName] != None:
                self.foundation_design_parameters=FoundationDesignParameters.FoundationDesignParameters()
                self.foundation_design_parameters.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "mooring_design_criteria"
        try :
            if data[varName] != None:
                self.mooring_design_criteria=MooringDesignCriteria.MooringDesignCriteria()
                self.mooring_design_criteria.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "mooring_input_flag"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "custom_mooring_input"
        try :
            if data[varName] != None:
                self.custom_mooring_input=CustomMooringInput.CustomMooringInput()
                self.custom_mooring_input.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "file_mooring_input"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "east"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "north"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "water_depth"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "description"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
    def loadHDF5(self,name=None, filePath=None):
        """Load an instance of the object from HDF5 format file
        Args:
            name (:obj:`str`, optional): Name of the object to load. Default is None.
            filePath (:obj:`str`, optional): Path of the HDF5 file to load. If None, the function looks for a file with name "self.name".h5.
        The HDF5 file must contain a datastructure representing an instance of this objects class, as generated by e.g. the function :func:`~saveHDF5`.
        """
        # Define filepath and name
        if not(name == None):
            self.name = name
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".h5"
            else:
                raise Exception("name is required for loading")
        # Open hdf5 file
        h = h5py.File(filePath,"r")
        group = h[self.name]
        # Save the object to the group
        self.loadFromHDF5Handle(group)
        # Close file
        h.close()
        pass
    def loadFromHDF5Handle(self, gr):
        """Load the properties of the object from a hdf5 handle.
        Args:
            gr (:obj:`h5py.Group`): Handle used to read the object properties from
        """
        if ("positioning_type" in list(gr.keys())):
            self.positioning_type = gr["positioning_type"][0].decode("ascii")
        if ("mass" in list(gr.keys())):
            self.mass = gr["mass"][0]
        if ("mooring_type" in list(gr.keys())):
            self.mooring_type = gr["mooring_type"][0].decode("ascii")
        if ("foundation_design_parameters" in list(gr.keys())):
            subgroup = gr["foundation_design_parameters"]
            self.foundation_design_parameters.loadFromHDF5Handle(subgroup)
        if ("mooring_design_criteria" in list(gr.keys())):
            subgroup = gr["mooring_design_criteria"]
            self.mooring_design_criteria.loadFromHDF5Handle(subgroup)
        if ("mooring_input_flag" in list(gr.keys())):
            self.mooring_input_flag = gr["mooring_input_flag"][0].decode("ascii")
        if ("custom_mooring_input" in list(gr.keys())):
            subgroup = gr["custom_mooring_input"]
            self.custom_mooring_input.loadFromHDF5Handle(subgroup)
        if ("file_mooring_input" in list(gr.keys())):
            self.file_mooring_input = gr["file_mooring_input"][0].decode("ascii")
        if ("east" in list(gr.keys())):
            self.east = gr["east"][0]
        if ("north" in list(gr.keys())):
            self.north = gr["north"][0]
        if ("water_depth" in list(gr.keys())):
            self.water_depth = gr["water_depth"][0]
        if ("name" in list(gr.keys())):
            self.name = gr["name"][0].decode("ascii")
        if ("description" in list(gr.keys())):
            self.description = gr["description"][0].decode("ascii")
    #------------------------
    # is_set function
    #------------------------
    def is_set(self, varName): # pragma: no cover

        """Check if a given property of the object is set

        Args:
            varName (:obj:`str`): name of the property to check

        Returns:
            :obj:`bool`: True if the property is set, else False
        """

        if (isinstance(getattr(self,varName),list) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (isinstance(getattr(self,varName),np.ndarray) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (getattr(self,varName) != None):
            return True
        return False
    #------------------------
