# This is the Station Keeping module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Neil Luxcey, Nicolas Michelet, Rocio Isorna
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import collections
import numpy as np
import os
import json
import h5py
from . import CustomMooringInput
#------------------------------------
# @ USER DEFINED IMPORTS START
# @ USER DEFINED IMPORTS END
#------------------------------------

class MooringCheckInputs():

    """data model containing enough data to run a mooring check
    """

    #------------
    # Constructor
    #------------
    def __init__(self,name=None):
#------------------------------------
# @ USER DEFINED DESCRIPTION START
# @ USER DEFINED DESCRIPTION END
#------------------------------------

        self._water_depth=100
        self._water_density=1025.0
        self._device_position=np.zeros(shape=(6), dtype=float)
        self._custom_mooring_input=CustomMooringInput.CustomMooringInput()
        self._custom_mooring_input.description = 'Mooring system input of the device, required if mooring_input_flag is custom'
        self._html_plot_file_name=''
        self._logo_file_name=''
        self._results_file_name=''
        self._name='none'
        self._description=''
        if not(name == None): # pragma: no cover
            self._name = name

#------------------------------------
# @ USER DEFINED PROPERTIES START
# @ USER DEFINED PROPERTIES END
#------------------------------------

#------------------------------------
# @ USER DEFINED METHODS START
# @ USER DEFINED METHODS END
#------------------------------------

    #------------
    # Get functions
    #------------
    @ property
    def water_depth(self): # pragma: no cover
        """float: Water depth [m] []
        """
        return self._water_depth
    #------------
    @ property
    def water_density(self): # pragma: no cover
        """float: Water density [kg/m**3] []
        """
        return self._water_density
    #------------
    @ property
    def device_position(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:Position of device [m] and [deg] dim(6) []
        """
        return self._device_position
    #------------
    @ property
    def custom_mooring_input(self): # pragma: no cover
        """:obj:`~.CustomMooringInput.CustomMooringInput`: Mooring system input of the device, required if mooring_input_flag is custom
        """
        return self._custom_mooring_input
    #------------
    @ property
    def html_plot_file_name(self): # pragma: no cover
        """str: Path used to save html plot file
        """
        return self._html_plot_file_name
    #------------
    @ property
    def logo_file_name(self): # pragma: no cover
        """str: Path to logo picture of DTO+ to display on plot
        """
        return self._logo_file_name
    #------------
    @ property
    def results_file_name(self): # pragma: no cover
        """str: Path used to save result file
        """
        return self._results_file_name
    #------------
    @ property
    def name(self): # pragma: no cover
        """str: name of the instance object
        """
        return self._name
    #------------
    @ property
    def description(self): # pragma: no cover
        """str: description of the instance object
        """
        return self._description
    #------------
    #------------
    # Set functions
    #------------
    @ water_depth.setter
    def water_depth(self,val): # pragma: no cover
        self._water_depth=float(val)
    #------------
    @ water_density.setter
    def water_density(self,val): # pragma: no cover
        self._water_density=float(val)
    #------------
    @ device_position.setter
    def device_position(self,val): # pragma: no cover
        self._device_position=val
    #------------
    @ custom_mooring_input.setter
    def custom_mooring_input(self,val): # pragma: no cover
        self._custom_mooring_input=val
    #------------
    @ html_plot_file_name.setter
    def html_plot_file_name(self,val): # pragma: no cover
        self._html_plot_file_name=str(val)
    #------------
    @ logo_file_name.setter
    def logo_file_name(self,val): # pragma: no cover
        self._logo_file_name=str(val)
    #------------
    @ results_file_name.setter
    def results_file_name(self,val): # pragma: no cover
        self._results_file_name=str(val)
    #------------
    @ name.setter
    def name(self,val): # pragma: no cover
        self._name=str(val)
    #------------
    @ description.setter
    def description(self,val): # pragma: no cover
        self._description=str(val)
    #------------
    #-------------------------
    # Representation functions
    #-------------------------
    def type_rep(self): # pragma: no cover
        """Generate a representation of the object type

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains the representation of the object type
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtosk:inputs:MooringCheckInputs"
        rep["name"] = self.name
        rep["description"] = self.description
        return rep
    def prop_rep(self, short = False, deep = True):

        """Generate a representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains a representation of the object properties
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtosk:inputs:MooringCheckInputs"
        rep["name"] = self.name
        rep["description"] = self.description
        if self.is_set("water_depth"):
            rep["water_depth"] = self.water_depth
        if self.is_set("water_density"):
            rep["water_density"] = self.water_density
        if self.is_set("device_position"):
            if (short):
                rep["device_position"] = str(self.device_position.shape)
            else:
                try:
                    rep["device_position"] = self.device_position.tolist()
                except:
                    rep["device_position"] = self.device_position
        if self.is_set("custom_mooring_input"):
            if (short and not(deep)):
                rep["custom_mooring_input"] = self.custom_mooring_input.type_rep()
            else:
                rep["custom_mooring_input"] = self.custom_mooring_input.prop_rep(short, deep)
        if self.is_set("html_plot_file_name"):
            rep["html_plot_file_name"] = self.html_plot_file_name
        if self.is_set("logo_file_name"):
            rep["logo_file_name"] = self.logo_file_name
        if self.is_set("results_file_name"):
            rep["results_file_name"] = self.results_file_name
        if self.is_set("name"):
            rep["name"] = self.name
        if self.is_set("description"):
            rep["description"] = self.description
        return rep
    #-------------------------
    # Save functions
    #-------------------------
    def json_rep(self, short=False, deep=True):

        """Generate a JSON representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`str`: string that contains a JSON representation of the object properties
        """

        return ( json.dumps(self.prop_rep(short=short, deep=deep),indent=4, separators=(",",": ")))
    def saveJSON(self, fileName=None):
        """Save the instance of the object to JSON format file

        Args:
            fileName (:obj:`str`, optional): Name of the JSON file, included extension. Defaults is None. If None, the name of the JSON file will be self.name.json. It can also contain an absolute or relative path.

        """

        if fileName==None:
            fileName=self.name + ".json"
        f=open(fileName, "w")
        f.write(self.json_rep())
        f.write("\n")
        f.close()

    def saveHDF5(self,filePath=None):
        """Save the instance of the object to HDF5 format file
        Args:
            filePath (:obj:`str`, optional): Name of the HDF5 file, included extension. Defaults is None. If None, the name of the HDF5 file will be "self.name".h5. It can also contain an absolute or relative path.
        """
        # Define filepath
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".h5"
            else:
                raise Exception("name is required for saving")
        # Open hdf5 file
        h = h5py.File(filePath,"w")
        group = h.create_group(self.name)
        # Save the object to the group
        self.saveToHDF5Handle(group)
        # Close file
        h.close()
        pass
    def saveToHDF5Handle(self, handle):
        """Save the properties of the object to the hdf5 handle.
        Args:
            handle (:obj:`h5py.Group`): Handle used to store the object properties
        """
        handle["water_depth"] = np.array([self.water_depth],dtype=float)
        handle["water_density"] = np.array([self.water_density],dtype=float)
        handle["device_position"] = np.array(self.device_position,dtype=float)
        subgroup = handle.create_group("custom_mooring_input")
        self.custom_mooring_input.saveToHDF5Handle(subgroup)
        ar = []
        ar.append(self.html_plot_file_name.encode("ascii"))
        handle["html_plot_file_name"] = np.asarray(ar)
        ar = []
        ar.append(self.logo_file_name.encode("ascii"))
        handle["logo_file_name"] = np.asarray(ar)
        ar = []
        ar.append(self.results_file_name.encode("ascii"))
        handle["results_file_name"] = np.asarray(ar)
        ar = []
        ar.append(self.name.encode("ascii"))
        handle["name"] = np.asarray(ar)
        ar = []
        ar.append(self.description.encode("ascii"))
        handle["description"] = np.asarray(ar)
    #-------------------------
    # Load functions
    #-------------------------
    def loadJSON(self,name = None, filePath = None):
        """Load an instance of the object from JSON format file

        Args:
            name (:obj:`str`, optional): Name of the object to load.
            filePath (:obj:`str`, optional): Path of the JSON file to load. If None, the function looks for a file with name "name".json.

        The JSON file must contain a datastructure representing an instance of this object's class, as generated by e.g. the function :func:`~saveJSON`.

        """

        if not(name == None):
            self.name = name
        if (name == None) and not(filePath == None):
            self.name = ".".join(filePath.split(os.path.sep)[-1].split(".")[0:-1])
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".json"
            else:
                raise Exception("object needs name for loading.")
        if not(os.path.isfile(filePath)):
            raise Exception("file %s not found."%filePath)
        self._loadedItems = []
        f = open(filePath,"r")
        data = f.read()
        f.close()
        dd = json.loads(data)
        self.loadFromJSONDict(dd)
    def loadFromJSONDict(self, data):
        """Load an instance of the object from a dictionnary representing a JSON structure)

        Args:
            name (:obj:`collections.OrderedDict`): Dictionnary containing a JSON structure, as produced by e.g. :func:`json.loads`

        """

        varName = "water_depth"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "water_density"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "device_position"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "custom_mooring_input"
        try :
            if data[varName] != None:
                self.custom_mooring_input=CustomMooringInput.CustomMooringInput()
                self.custom_mooring_input.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "html_plot_file_name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "logo_file_name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "results_file_name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "description"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
    def loadHDF5(self,name=None, filePath=None):
        """Load an instance of the object from HDF5 format file
        Args:
            name (:obj:`str`, optional): Name of the object to load. Default is None.
            filePath (:obj:`str`, optional): Path of the HDF5 file to load. If None, the function looks for a file with name "self.name".h5.
        The HDF5 file must contain a datastructure representing an instance of this objects class, as generated by e.g. the function :func:`~saveHDF5`.
        """
        # Define filepath and name
        if not(name == None):
            self.name = name
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".h5"
            else:
                raise Exception("name is required for loading")
        # Open hdf5 file
        h = h5py.File(filePath,"r")
        group = h[self.name]
        # Save the object to the group
        self.loadFromHDF5Handle(group)
        # Close file
        h.close()
        pass
    def loadFromHDF5Handle(self, gr):
        """Load the properties of the object from a hdf5 handle.
        Args:
            gr (:obj:`h5py.Group`): Handle used to read the object properties from
        """
        if ("water_depth" in list(gr.keys())):
            self.water_depth = gr["water_depth"][0]
        if ("water_density" in list(gr.keys())):
            self.water_density = gr["water_density"][0]
        if ("device_position" in list(gr.keys())):
            self.device_position = gr["device_position"][:]
        if ("custom_mooring_input" in list(gr.keys())):
            subgroup = gr["custom_mooring_input"]
            self.custom_mooring_input.loadFromHDF5Handle(subgroup)
        if ("html_plot_file_name" in list(gr.keys())):
            self.html_plot_file_name = gr["html_plot_file_name"][0].decode("ascii")
        if ("logo_file_name" in list(gr.keys())):
            self.logo_file_name = gr["logo_file_name"][0].decode("ascii")
        if ("results_file_name" in list(gr.keys())):
            self.results_file_name = gr["results_file_name"][0].decode("ascii")
        if ("name" in list(gr.keys())):
            self.name = gr["name"][0].decode("ascii")
        if ("description" in list(gr.keys())):
            self.description = gr["description"][0].decode("ascii")
    #------------------------
    # is_set function
    #------------------------
    def is_set(self, varName): # pragma: no cover

        """Check if a given property of the object is set

        Args:
            varName (:obj:`str`): name of the property to check

        Returns:
            :obj:`bool`: True if the property is set, else False
        """

        if (isinstance(getattr(self,varName),list) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (isinstance(getattr(self,varName),np.ndarray) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (getattr(self,varName) != None):
            return True
        return False
    #------------------------
