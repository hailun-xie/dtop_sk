# This is the Station Keeping module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Neil Luxcey, Nicolas Michelet, Rocio Isorna
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import collections
import numpy as np
import os
import json
import h5py
from . import DeviceProperties
from . import MasterStructureProperties
from . import SubstationProperties
from . import ULSAnalysisParameters
from . import FLSAnalysisParameters
#------------------------------------
# @ USER DEFINED IMPORTS START
# @ USER DEFINED IMPORTS END
#------------------------------------

class Cplx():

    """data model representing input data
    """

    #------------
    # Constructor
    #------------
    def __init__(self,name=None):
#------------------------------------
# @ USER DEFINED DESCRIPTION START
# @ USER DEFINED DESCRIPTION END
#------------------------------------

        self._device_properties=DeviceProperties.DeviceProperties()
        self._device_properties.description = 'Device properties'
        self._master_structure_properties=MasterStructureProperties.MasterStructureProperties()
        self._master_structure_properties.description = 'Master structure properties'
        self._substation_properties=[]
        self._substation_is_present=False
        self._uls_analysis_parameters=ULSAnalysisParameters.ULSAnalysisParameters()
        self._uls_analysis_parameters.description = 'Parameters for ULS motion analysis'
        self._fls_analysis_parameters=FLSAnalysisParameters.FLSAnalysisParameters()
        self._fls_analysis_parameters.description = 'Parameters for FLS motion analysis'
        self._deviceId=np.zeros(shape=(1), dtype=int)
        self._water_density=1025.0
        self._east=np.zeros(shape=(1), dtype=float)
        self._north=np.zeros(shape=(1), dtype=float)
        self._farm_east=0.0
        self._farm_north=0.0
        self._yaw=np.zeros(shape=(1), dtype=float)
        self._water_depth=np.zeros(shape=(1), dtype=float)
        self._path_to_figure_folder=''
        self._name='none'
        self._description=''
        if not(name == None): # pragma: no cover
            self._name = name

#------------------------------------
# @ USER DEFINED PROPERTIES START
# @ USER DEFINED PROPERTIES END
#------------------------------------

#------------------------------------
# @ USER DEFINED METHODS START
# @ USER DEFINED METHODS END
#------------------------------------

    #------------
    # Get functions
    #------------
    @ property
    def device_properties(self): # pragma: no cover
        """:obj:`~.DeviceProperties.DeviceProperties`: Device properties
        """
        return self._device_properties
    #------------
    @ property
    def master_structure_properties(self): # pragma: no cover
        """:obj:`~.MasterStructureProperties.MasterStructureProperties`: Master structure properties
        """
        return self._master_structure_properties
    #------------
    @ property
    def substation_properties(self): # pragma: no cover
        """:obj:`list` of :obj:`~.SubstationProperties.SubstationProperties`: Substation properties. Note that in the current version, only fixed structure is allowed for substations
        """
        return self._substation_properties
    #------------
    @ property
    def substation_is_present(self): # pragma: no cover
        """bool: Flag specifying if a substation is present. Default is false. []
        """
        return self._substation_is_present
    #------------
    @ property
    def uls_analysis_parameters(self): # pragma: no cover
        """:obj:`~.ULSAnalysisParameters.ULSAnalysisParameters`: Parameters for ULS motion analysis
        """
        return self._uls_analysis_parameters
    #------------
    @ property
    def fls_analysis_parameters(self): # pragma: no cover
        """:obj:`~.FLSAnalysisParameters.FLSAnalysisParameters`: Parameters for FLS motion analysis
        """
        return self._fls_analysis_parameters
    #------------
    @ property
    def deviceId(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`int`:Devices identity number []
        """
        return self._deviceId
    #------------
    @ property
    def water_density(self): # pragma: no cover
        """float: Sea Water density at the farm location [kg/m3]
        """
        return self._water_density
    #------------
    @ property
    def east(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:Devices east position utm dim(*) []
        """
        return self._east
    #------------
    @ property
    def north(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:Devices north position utm dim(*) []
        """
        return self._north
    #------------
    @ property
    def farm_east(self): # pragma: no cover
        """float: Farm center east position utm []
        """
        return self._farm_east
    #------------
    @ property
    def farm_north(self): # pragma: no cover
        """float: Farm center north position utm []
        """
        return self._farm_north
    #------------
    @ property
    def yaw(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:Devices north position dim(*) [deg]
        """
        return self._yaw
    #------------
    @ property
    def water_depth(self): # pragma: no cover
        """:obj:`.numpy.ndarray` of :obj:`float`:Water depth at each device location dim(*) [m]
        """
        return self._water_depth
    #------------
    @ property
    def path_to_figure_folder(self): # pragma: no cover
        """str: Path to folder used to store the figures
        """
        return self._path_to_figure_folder
    #------------
    @ property
    def name(self): # pragma: no cover
        """str: name of the instance object
        """
        return self._name
    #------------
    @ property
    def description(self): # pragma: no cover
        """str: description of the instance object
        """
        return self._description
    #------------
    #------------
    # Set functions
    #------------
    @ device_properties.setter
    def device_properties(self,val): # pragma: no cover
        self._device_properties=val
    #------------
    @ master_structure_properties.setter
    def master_structure_properties(self,val): # pragma: no cover
        self._master_structure_properties=val
    #------------
    @ substation_properties.setter
    def substation_properties(self,val): # pragma: no cover
        self._substation_properties=val
    #------------
    @ substation_is_present.setter
    def substation_is_present(self,val): # pragma: no cover
        self._substation_is_present=val
    #------------
    @ uls_analysis_parameters.setter
    def uls_analysis_parameters(self,val): # pragma: no cover
        self._uls_analysis_parameters=val
    #------------
    @ fls_analysis_parameters.setter
    def fls_analysis_parameters(self,val): # pragma: no cover
        self._fls_analysis_parameters=val
    #------------
    @ deviceId.setter
    def deviceId(self,val): # pragma: no cover
        self._deviceId=val
    #------------
    @ water_density.setter
    def water_density(self,val): # pragma: no cover
        self._water_density=float(val)
    #------------
    @ east.setter
    def east(self,val): # pragma: no cover
        self._east=val
    #------------
    @ north.setter
    def north(self,val): # pragma: no cover
        self._north=val
    #------------
    @ farm_east.setter
    def farm_east(self,val): # pragma: no cover
        self._farm_east=float(val)
    #------------
    @ farm_north.setter
    def farm_north(self,val): # pragma: no cover
        self._farm_north=float(val)
    #------------
    @ yaw.setter
    def yaw(self,val): # pragma: no cover
        self._yaw=val
    #------------
    @ water_depth.setter
    def water_depth(self,val): # pragma: no cover
        self._water_depth=val
    #------------
    @ path_to_figure_folder.setter
    def path_to_figure_folder(self,val): # pragma: no cover
        self._path_to_figure_folder=str(val)
    #------------
    @ name.setter
    def name(self,val): # pragma: no cover
        self._name=str(val)
    #------------
    @ description.setter
    def description(self,val): # pragma: no cover
        self._description=str(val)
    #------------
    #-------------------------
    # Representation functions
    #-------------------------
    def type_rep(self): # pragma: no cover
        """Generate a representation of the object type

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains the representation of the object type
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtosk:inputs:Cplx"
        rep["name"] = self.name
        rep["description"] = self.description
        return rep
    def prop_rep(self, short = False, deep = True):

        """Generate a representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`collections.OrderedDict`: dictionnary that contains a representation of the object properties
        """

        rep = collections.OrderedDict()
        rep["__type__"] = "dtosk:inputs:Cplx"
        rep["name"] = self.name
        rep["description"] = self.description
        if self.is_set("device_properties"):
            if (short and not(deep)):
                rep["device_properties"] = self.device_properties.type_rep()
            else:
                rep["device_properties"] = self.device_properties.prop_rep(short, deep)
        if self.is_set("master_structure_properties"):
            if (short and not(deep)):
                rep["master_structure_properties"] = self.master_structure_properties.type_rep()
            else:
                rep["master_structure_properties"] = self.master_structure_properties.prop_rep(short, deep)
        if self.is_set("substation_properties"):
            rep["substation_properties"] = []
            for i in range(0,len(self.substation_properties)):
                if (short and not(deep)):
                    itemType = self.substation_properties[i].type_rep()
                    rep["substation_properties"].append( itemType )
                else:
                    rep["substation_properties"].append( self.substation_properties[i].prop_rep(short, deep) )
        else:
            rep["substation_properties"] = []
        if self.is_set("substation_is_present"):
            rep["substation_is_present"] = self.substation_is_present
        if self.is_set("uls_analysis_parameters"):
            if (short and not(deep)):
                rep["uls_analysis_parameters"] = self.uls_analysis_parameters.type_rep()
            else:
                rep["uls_analysis_parameters"] = self.uls_analysis_parameters.prop_rep(short, deep)
        if self.is_set("fls_analysis_parameters"):
            if (short and not(deep)):
                rep["fls_analysis_parameters"] = self.fls_analysis_parameters.type_rep()
            else:
                rep["fls_analysis_parameters"] = self.fls_analysis_parameters.prop_rep(short, deep)
        if self.is_set("deviceId"):
            if (short):
                rep["deviceId"] = str(self.deviceId.shape)
            else:
                try:
                    rep["deviceId"] = self.deviceId.tolist()
                except:
                    rep["deviceId"] = self.deviceId
        if self.is_set("water_density"):
            rep["water_density"] = self.water_density
        if self.is_set("east"):
            if (short):
                rep["east"] = str(self.east.shape)
            else:
                try:
                    rep["east"] = self.east.tolist()
                except:
                    rep["east"] = self.east
        if self.is_set("north"):
            if (short):
                rep["north"] = str(self.north.shape)
            else:
                try:
                    rep["north"] = self.north.tolist()
                except:
                    rep["north"] = self.north
        if self.is_set("farm_east"):
            rep["farm_east"] = self.farm_east
        if self.is_set("farm_north"):
            rep["farm_north"] = self.farm_north
        if self.is_set("yaw"):
            if (short):
                rep["yaw"] = str(self.yaw.shape)
            else:
                try:
                    rep["yaw"] = self.yaw.tolist()
                except:
                    rep["yaw"] = self.yaw
        if self.is_set("water_depth"):
            if (short):
                rep["water_depth"] = str(self.water_depth.shape)
            else:
                try:
                    rep["water_depth"] = self.water_depth.tolist()
                except:
                    rep["water_depth"] = self.water_depth
        if self.is_set("path_to_figure_folder"):
            rep["path_to_figure_folder"] = self.path_to_figure_folder
        if self.is_set("name"):
            rep["name"] = self.name
        if self.is_set("description"):
            rep["description"] = self.description
        return rep
    #-------------------------
    # Save functions
    #-------------------------
    def json_rep(self, short=False, deep=True):

        """Generate a JSON representation of the object properties

        Args:
            short (:obj:`bool`,optional): if True, properties are represented by their type only. If False, the values of the properties are included.
            deep (:obj:`bool`,optional): if True, the properties of each property will be included.

        Returns:
            :obj:`str`: string that contains a JSON representation of the object properties
        """

        return ( json.dumps(self.prop_rep(short=short, deep=deep),indent=4, separators=(",",": ")))
    def saveJSON(self, fileName=None):
        """Save the instance of the object to JSON format file

        Args:
            fileName (:obj:`str`, optional): Name of the JSON file, included extension. Defaults is None. If None, the name of the JSON file will be self.name.json. It can also contain an absolute or relative path.

        """

        if fileName==None:
            fileName=self.name + ".json"
        f=open(fileName, "w")
        f.write(self.json_rep())
        f.write("\n")
        f.close()

    def saveHDF5(self,filePath=None):
        """Save the instance of the object to HDF5 format file
        Args:
            filePath (:obj:`str`, optional): Name of the HDF5 file, included extension. Defaults is None. If None, the name of the HDF5 file will be "self.name".h5. It can also contain an absolute or relative path.
        """
        # Define filepath
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".h5"
            else:
                raise Exception("name is required for saving")
        # Open hdf5 file
        h = h5py.File(filePath,"w")
        group = h.create_group(self.name)
        # Save the object to the group
        self.saveToHDF5Handle(group)
        # Close file
        h.close()
        pass
    def saveToHDF5Handle(self, handle):
        """Save the properties of the object to the hdf5 handle.
        Args:
            handle (:obj:`h5py.Group`): Handle used to store the object properties
        """
        subgroup = handle.create_group("device_properties")
        self.device_properties.saveToHDF5Handle(subgroup)
        subgroup = handle.create_group("master_structure_properties")
        self.master_structure_properties.saveToHDF5Handle(subgroup)
        subgroup = handle.create_group("substation_properties")
        for idx in range(0,len(self.substation_properties)):
            subgroup1 = subgroup.create_group("substation_properties" + "_" + str(idx))
            self.substation_properties[idx].saveToHDF5Handle(subgroup1)
        handle["substation_is_present"] = np.array([self.substation_is_present],dtype=bool)
        subgroup = handle.create_group("uls_analysis_parameters")
        self.uls_analysis_parameters.saveToHDF5Handle(subgroup)
        subgroup = handle.create_group("fls_analysis_parameters")
        self.fls_analysis_parameters.saveToHDF5Handle(subgroup)
        handle["deviceId"] = np.array(self.deviceId,dtype=int)
        handle["water_density"] = np.array([self.water_density],dtype=float)
        handle["east"] = np.array(self.east,dtype=float)
        handle["north"] = np.array(self.north,dtype=float)
        handle["farm_east"] = np.array([self.farm_east],dtype=float)
        handle["farm_north"] = np.array([self.farm_north],dtype=float)
        handle["yaw"] = np.array(self.yaw,dtype=float)
        handle["water_depth"] = np.array(self.water_depth,dtype=float)
        ar = []
        ar.append(self.path_to_figure_folder.encode("ascii"))
        handle["path_to_figure_folder"] = np.asarray(ar)
        ar = []
        ar.append(self.name.encode("ascii"))
        handle["name"] = np.asarray(ar)
        ar = []
        ar.append(self.description.encode("ascii"))
        handle["description"] = np.asarray(ar)
    #-------------------------
    # Load functions
    #-------------------------
    def loadJSON(self,name = None, filePath = None):
        """Load an instance of the object from JSON format file

        Args:
            name (:obj:`str`, optional): Name of the object to load.
            filePath (:obj:`str`, optional): Path of the JSON file to load. If None, the function looks for a file with name "name".json.

        The JSON file must contain a datastructure representing an instance of this object's class, as generated by e.g. the function :func:`~saveJSON`.

        """

        if not(name == None):
            self.name = name
        if (name == None) and not(filePath == None):
            self.name = ".".join(filePath.split(os.path.sep)[-1].split(".")[0:-1])
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".json"
            else:
                raise Exception("object needs name for loading.")
        if not(os.path.isfile(filePath)):
            raise Exception("file %s not found."%filePath)
        self._loadedItems = []
        f = open(filePath,"r")
        data = f.read()
        f.close()
        dd = json.loads(data)
        self.loadFromJSONDict(dd)
    def loadFromJSONDict(self, data):
        """Load an instance of the object from a dictionnary representing a JSON structure)

        Args:
            name (:obj:`collections.OrderedDict`): Dictionnary containing a JSON structure, as produced by e.g. :func:`json.loads`

        """

        varName = "device_properties"
        try :
            if data[varName] != None:
                self.device_properties=DeviceProperties.DeviceProperties()
                self.device_properties.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "master_structure_properties"
        try :
            if data[varName] != None:
                self.master_structure_properties=MasterStructureProperties.MasterStructureProperties()
                self.master_structure_properties.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "substation_properties"
        try :
            if data[varName] != None:
                self.substation_properties=[]
                for i in range(0,len(data[varName])):
                    self.substation_properties.append(SubstationProperties.SubstationProperties())
                    self.substation_properties[i].loadFromJSONDict(data[varName][i])
            else:
                self.substation_properties = []
        except :
            pass
        varName = "substation_is_present"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "uls_analysis_parameters"
        try :
            if data[varName] != None:
                self.uls_analysis_parameters=ULSAnalysisParameters.ULSAnalysisParameters()
                self.uls_analysis_parameters.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "fls_analysis_parameters"
        try :
            if data[varName] != None:
                self.fls_analysis_parameters=FLSAnalysisParameters.FLSAnalysisParameters()
                self.fls_analysis_parameters.loadFromJSONDict(data[varName])
        except :
            pass
        varName = "deviceId"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "water_density"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "east"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "north"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "farm_east"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "farm_north"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "yaw"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "water_depth"
        try :
            setattr(self,varName, np.array(data[varName]))
        except :
            pass
        varName = "path_to_figure_folder"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "name"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
        varName = "description"
        try :
            setattr(self,varName, data[varName])
        except :
            pass
    def loadHDF5(self,name=None, filePath=None):
        """Load an instance of the object from HDF5 format file
        Args:
            name (:obj:`str`, optional): Name of the object to load. Default is None.
            filePath (:obj:`str`, optional): Path of the HDF5 file to load. If None, the function looks for a file with name "self.name".h5.
        The HDF5 file must contain a datastructure representing an instance of this objects class, as generated by e.g. the function :func:`~saveHDF5`.
        """
        # Define filepath and name
        if not(name == None):
            self.name = name
        if (filePath == None):
            if hasattr(self, "name"):
                filePath = self.name + ".h5"
            else:
                raise Exception("name is required for loading")
        # Open hdf5 file
        h = h5py.File(filePath,"r")
        group = h[self.name]
        # Save the object to the group
        self.loadFromHDF5Handle(group)
        # Close file
        h.close()
        pass
    def loadFromHDF5Handle(self, gr):
        """Load the properties of the object from a hdf5 handle.
        Args:
            gr (:obj:`h5py.Group`): Handle used to read the object properties from
        """
        if ("device_properties" in list(gr.keys())):
            subgroup = gr["device_properties"]
            self.device_properties.loadFromHDF5Handle(subgroup)
        if ("master_structure_properties" in list(gr.keys())):
            subgroup = gr["master_structure_properties"]
            self.master_structure_properties.loadFromHDF5Handle(subgroup)
        if ("substation_properties" in list(gr.keys())):
            subgroup = gr["substation_properties"]
            self.substation_properties=[]
            for idx in range(0,len(subgroup.keys())):
                ssubgroup = subgroup["substation_properties"+"_"+str(idx)]
                self.substation_properties.append(SubstationProperties.SubstationProperties())
                self.substation_properties[idx].loadFromHDF5Handle(ssubgroup)
        if ("substation_is_present" in list(gr.keys())):
            self.substation_is_present = gr["substation_is_present"][0]
        if ("uls_analysis_parameters" in list(gr.keys())):
            subgroup = gr["uls_analysis_parameters"]
            self.uls_analysis_parameters.loadFromHDF5Handle(subgroup)
        if ("fls_analysis_parameters" in list(gr.keys())):
            subgroup = gr["fls_analysis_parameters"]
            self.fls_analysis_parameters.loadFromHDF5Handle(subgroup)
        if ("deviceId" in list(gr.keys())):
            self.deviceId = gr["deviceId"][:]
        if ("water_density" in list(gr.keys())):
            self.water_density = gr["water_density"][0]
        if ("east" in list(gr.keys())):
            self.east = gr["east"][:]
        if ("north" in list(gr.keys())):
            self.north = gr["north"][:]
        if ("farm_east" in list(gr.keys())):
            self.farm_east = gr["farm_east"][0]
        if ("farm_north" in list(gr.keys())):
            self.farm_north = gr["farm_north"][0]
        if ("yaw" in list(gr.keys())):
            self.yaw = gr["yaw"][:]
        if ("water_depth" in list(gr.keys())):
            self.water_depth = gr["water_depth"][:]
        if ("path_to_figure_folder" in list(gr.keys())):
            self.path_to_figure_folder = gr["path_to_figure_folder"][0].decode("ascii")
        if ("name" in list(gr.keys())):
            self.name = gr["name"][0].decode("ascii")
        if ("description" in list(gr.keys())):
            self.description = gr["description"][0].decode("ascii")
    #------------------------
    # is_set function
    #------------------------
    def is_set(self, varName): # pragma: no cover

        """Check if a given property of the object is set

        Args:
            varName (:obj:`str`): name of the property to check

        Returns:
            :obj:`bool`: True if the property is set, else False
        """

        if (isinstance(getattr(self,varName),list) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (isinstance(getattr(self,varName),np.ndarray) ):
            if (len(getattr(self,varName)) > 0 and not any([np.any(a==None) for a in getattr(self,varName)])  ):
                return True
            else :
                return False
        if (getattr(self,varName) != None):
            return True
        return False
    #------------------------
