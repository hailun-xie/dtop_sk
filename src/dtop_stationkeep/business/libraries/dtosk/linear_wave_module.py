# This is the Station Keeping module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Neil Luxcey, Nicolas Michelet, Rocio Isorna
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


import numpy as np 
from scipy import optimize
from .global_parameters import acceleration_of_gravity
import logging
logger = logging.getLogger() # grabs root logger

def compute_wave_number(omega,water_depth):

	if water_depth<= 0.0:
		raise Exception('Cannot compute wave number for a water depth equal to zero!')
	
	def wnumfunc(wavnum):
			
		g = acceleration_of_gravity()
			
		return ((omega ** 2.0)/ (g * np.tanh(wavnum * water_depth)))
	
	try:
		wavenumber = optimize.fixed_point(wnumfunc,1.0e-10)
	except:
		logger.info('FAILED to compute wave number')
		logger.info('water depth : ' + str(water_depth) + ' m')
		logger.info('omega : ' + str(omega) + ' rad/s')
		raise Exception('Failed to compute wave number: check that omega and water depth have realistic values (see log)')
		
	return wavenumber

def compute_cplx_elevation(omega,amp,water_depth,x0,x):

	# Get wave number
	k = compute_wave_number(omega,water_depth)

	# Complex formulation
	zeta = amp*np.exp(-(1j)*k*(x-x0))

	return zeta

def compute_cplx_pressure(rho,omega,amp,water_depth,x0,x,z):

	# Init
	g = acceleration_of_gravity()
	h = water_depth

	# Get wave number
	k = compute_wave_number(omega,water_depth)

	# Complex formulation
	pressure = rho * g * amp * np.cosh(k*(z+h))/np.cosh(k*h) * np.exp(-(1j)*k*(x-x0))

	return pressure

def compute_cplx_velocity(omega,amp,water_depth,x0,x,z):

	# Init 
	h = water_depth

	# Get wave number
	k = compute_wave_number(omega,water_depth)

	# Compute water particle acceleration
	ux = omega * amp * np.cosh(k*(z+h))/np.sinh(k*h)  *np.exp(-(1j)*k*(x-x0))

	# Compute water particle acceleration
	uz = omega * amp * np.sinh(k*(z+h))/np.sinh(k*h)  * np.exp(-(1j)*k*(x-x0)) * np.exp((1j)*np.pi/2)

	return ux,uz


def compute_cplx_acceleration(omega,amp,water_depth,x0,x,z):

	# Init 
	h = water_depth

	# Get wave number
	k = compute_wave_number(omega,water_depth)

	# Compute water particle acceleration
	ax = omega**2 * amp * np.cosh(k*(z+h))/np.sinh(k*h)  *np.exp(-(1j)*k*(x-x0)) * np.exp((1j)*np.pi/2)

	# Compute water particle acceleration
	az = -omega**2 * amp * np.sinh(k*(z+h))/np.sinh(k*h)  * np.exp(-(1j)*k*(x-x0))

	return ax,az

def compute_jonswap_spectrum(hs,tp,omega_array):

	# Init
	S=np.zeros(shape=(len(omega_array)), dtype=float)
	t1 = 0.834*tp

	# JONSWAP formulation from 17th ITTC
	if tp>0 and hs>0:
		for idx in range(0,len(omega_array)):
			if omega_array[idx]<=(5.24/t1):
				sig = 0.07
			else:
				sig = 0.09
			G = np.exp(-((0.191*omega_array[idx]*t1-1)/(2**0.5*sig))**2)
			S[idx] = 155 * hs**2 / (t1**4 * omega_array[idx]**5) * np.exp(-944/(t1**4 * omega_array[idx]**4)) * 3.3 ** G

	return S