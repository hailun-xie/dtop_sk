# This is the Station Keeping module of the DTOceanPlus suite
# Copyright (C) 2021 France Energies Marines - Neil Luxcey, Nicolas Michelet, Rocio Isorna
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


import numpy as np 

def Rab(euler_angles):
	# Rotation conventions from https://map-plus-plus.readthedocs.io/en/latest/theory.html#vessel
	# Rotation matrix of {b} in {n}
	# v_in_n=Rab*v_in_b
	cr, cp, cy = np.cos( euler_angles ) # cos( roll, pitch, yaw )
	sr, sp, sy = np.sin( euler_angles ) # sin( roll, pitch, yaw )
	R = np.array([[cy*cp, cy*sp*sr-sy*cr, cy*sp*cr+sy*sr],
				  [sy*cp, sy*sp*sr+cy*cr, sy*sp*cr-cy*sr],
				  [  -sp,    cp*sr      ,    cp*cr      ]]) # Apply yaw, then pitch, then roll
	return R

        
def rotation_about_z( vector_in_b, angle ):
	# Rotate a 3D vector of an angle about the vertical z-axis (third coordinate)
	euler_angles=np.array([0.0, 0.0, angle])
	R=Rab(euler_angles)
	vector_in_n=R.dot(vector_in_b.T).T
	return vector_in_n