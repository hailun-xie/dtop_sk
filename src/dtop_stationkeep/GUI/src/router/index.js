// This is the Station Keeping module of the DTOceanPlus suite
// Copyright (C) 2021 France Energies Marines - Neil Luxcey, Nicolas Michelet, Rocio Isorna
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'

export const constantRoutes = [

  {
    path: '/404',
    component: () => import('@/views/404'),
    hidden: true
  },

//   {
//     path: '/',
//     component: Layout,
//     redirect: '/dashboard',
//     children: [{
//       path: 'dashboard',
//       name: 'Dashboard',
//       component: () => import('@/views/dashboard/index'),
//       meta: { title: 'Dashboard', icon: 'dashboard' }
//     }]
//   },
// 
//   {
//     path: '/skhome',
//     component: Layout,
//     redirect: '/skhome',
//     children: [{
//       path: 'skhome',
//       name: 'SKHome',
//       component: () => import('@/views/skhome/index'),
//       meta: { title: 'SK Home', icon: 'home' }
//     }]
//   },

  {
    path: '/skinputs',
    component: Layout,
    redirect: '/skinputs',
    name: 'SKInputs',
    meta: { title: 'Inputs', icon: 'nested' },
    children: [
      {
        path: 'devices_positioning',
        name: 'Devices_Positioning',
        component: () => import('@/views/inputs_page/Devices_positioning/index'),
        meta: { title: 'Farm layout' }
      },
      {
        path: 'device_properties',
        name: 'Device_Properties',
        component: () => import('@/views/inputs_page/Device_properties/index'),
        meta: { title: 'Device Properties' }
      },
      {
        path: 'master_structure_properties',
        name: 'Master_Structure_Properties',
        component: () => import('@/views/inputs_page/Master_structure_properties/index'),
        meta: { title: 'Master Structure Properties' }
      },
      {
        path: 'substation_properties',
        name: 'Substation_Properties',
        component: () => import('@/views/inputs_page/Substation_properties/index'),
        meta: { title: 'Substation Properties' }
      },
      {
        path: 'uls_fls_analysis_parameters',
        name: 'ULS_FLS_Analysis_Parameters',
        component: () => import('@/views/inputs_page/ULS_FLS_analysis_parameters/index'),
        meta: { title: 'Analysis Parameters' }
      }
    ]
  },

  {
    path: '/run_module',
    component: Layout,
    redirect: '/run_module',
    children: [{
      path: 'run_module',
      name: 'Run_Module',
      component: () => import('@/views/run_module/index'),
      meta: { title: 'Run Module' }
    }]
  },

  {
    path: '/skoutputs',
    component: Layout,
    redirect: '/skoutputs',
    name: 'SKOutputs',
    meta: { title: 'Outputs', icon: 'nested' },
    children: [
      {
        path: 'bom',
        name: 'BOM',
        component: () => import('@/views/outputs_page/Bom/index'),
        meta: { title: 'Bill of Materials' }
      },
      {
        path: 'environmental_impact',
        name: 'Environmental_Impact',
        component: () => import('@/views/outputs_page/Environmental_Impact/index'),
        meta: { title: 'Environmental Impact' }
      },
      {
        path: 'design_assessment',
        name: 'Design_Assessment',
        component: () => import('@/views/outputs_page/Design_Assessment/index'),
        meta: { title: 'Design Assessment' }
      },
      {
        path: 'uls_fls',
        name: 'ULS_FLS',
        component: () => import('@/views/outputs_page/ULS_FLS/index'),
        meta: { title: 'ULS / FLS' }
      }
    ]
  },

  {
    path: '/pdf',
    name: 'PDF',
    component: () => import('@/views/pdf/download'),
    hidden: true
  },

  {
    path: 'external-link',
    component: Layout,
    meta: { title: 'External links', icon: 'link' },
    children: [
      {
        path: 'https://dtoceanplus.eu',
        meta: { title: 'DTOcean+ website'}
      },
      {
        path: 'https://dtoceanplus.gitlab.io/documentation/index.html',
        meta: { title: 'DTOcean+ documentation'}
      },
      {
        path: 'https://dtoceanplus.gitlab.io/documentation/deployment/sk/docs/index.html',
        meta: { title: 'Station Keeping documentation'}
      }
    ]
  },

  // 404 page must be placed at the end !!!
  { path: '*', redirect: '/404', hidden: true }
]

const createRouter = () => new Router({
  // mode: 'history', // require service support
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRoutes
})

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}

export default router
