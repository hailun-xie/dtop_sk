import json
import sys
import os
import dredd_hooks as hooks
import requests
from functools import partial
import copy
import traceback



filename = "dredd-hooks-output.txt"
if os.path.isfile(filename):
    os.remove(filename)

# file = sys.stdout
file = open(filename, "w")
print = partial(print, file=file)

def print_log(func):
    def wrapper(transaction):
        try:
            func(transaction)
        except:
            traceback.print_exc(file=file)
        finally:
            file.flush()

    return wrapper


project_id = '1'

def if_not_skipped(func):
    def wrapper(transaction):
        if not transaction['skip']:
            func(transaction)
    return wrapper

@hooks.before('/sk/{ProjectId} > Stationkeeping project results > 500')
@hooks.before('/sk/{ProjectId}/inputs > post input data and save it to database > 400')
@hooks.before('/sk/{ProjectId}/run > post input data and save it to database, run analysis and save results to database > 400')
@hooks.before('/sk/{ProjectId}/hierarchy > hierarchy description of the station keeping system > 500')
@hooks.before('/sk/{ProjectId}/bom > bill of materials from the station keeping system > 500')
@hooks.before('/sk/{ProjectId}/environmental_impact > environmental impact of the station keeping system > 500')
@hooks.before('/sk/{ProjectId}/design_assessment > design assessment of the station keeping system > 500')
@hooks.before('/representation/{ProjectId} > digital representation of the station keeping system > 500')
# @hooks.before('/sk/{ProjectId} > Stationkeeping project results > 200 > application/json')
# @hooks.before('/sk/{ProjectId}/hierarchy > hierarchy description of the station keeping system > 200 > application/json')
# @hooks.before('/sk/{ProjectId}/bom > bill of materials from the station keeping system > 200 > application/json')
# @hooks.before('/sk/{ProjectId}/environmental_impact > environmental impact of the station keeping system > 200 > application/json')
# @hooks.before('/sk/{ProjectId}/design_assessment > design assessment of the station keeping system > 200 > application/json')
# @hooks.before('/representation/{ProjectId} > digital representation of the station keeping system > 200 > application/json')
# @hooks.before('/sk/{ProjectId} > Stationkeeping project results > 404')
# @hooks.before('/sk/{ProjectId}/hierarchy > hierarchy description of the station keeping system > 404')
# @hooks.before('/sk/{ProjectId}/bom > bill of materials from the station keeping system > 404')
# @hooks.before('/sk/{ProjectId}/environmental_impact > environmental impact of the station keeping system > 404')
# @hooks.before('/sk/{ProjectId}/design_assessment > design assessment of the station keeping system > 404')
# @hooks.before('/representation/{ProjectId} > digital representation of the station keeping system > 404')
def skip(transaction):
    transaction['skip'] = True

@hooks.before('/sk/{ProjectId}/inputs > post input data and save it to database > 201')
@if_not_skipped
@print_log
def post_project_inputs(transaction):
    # Load a valid input structure
    this_dir = os.path.dirname(os.path.realpath(__file__))
    data_dir = os.path.join(this_dir,"src","dtop_stationkeep","sample_data")
    json_file = os.path.join(data_dir,"RM1_SK3_inputs.json")
    project_dir = os.path.join(this_dir,"storage","dredd_input")
    if not os.path.exists(project_dir):
        os.makedirs(project_dir)
    with open(json_file) as jsonf:
        json_data = json.load(jsonf)
    full_path = transaction['fullPath']
    if '1' in full_path:
        transaction['fullPath'] = full_path.replace('1', 'dredd_input')
    transaction['request']['body'] = json.dumps(json_data)

@hooks.before('/sk/{ProjectId}/run > post input data and save it to database, run analysis and save results to database > 201')
@if_not_skipped
@print_log
def post_project_run(transaction):
    # Load a valid input structure
    this_dir = os.path.dirname(os.path.realpath(__file__))
    data_dir = os.path.join(this_dir,"src","dtop_stationkeep","sample_data")
    json_file = os.path.join(data_dir,"RM1_SK3_inputs.json")
    project_dir = os.path.join(this_dir,"storage","dredd_run")
    if not os.path.exists(project_dir):
        os.makedirs(project_dir)
    with open(json_file) as jsonf:
        json_data = json.load(jsonf)
    full_path = transaction['fullPath']
    if '1' in full_path:
        transaction['fullPath'] = full_path.replace('1', 'dredd_run')
    transaction['request']['body'] = json.dumps(json_data)

@hooks.before('/sk/{ProjectId} > Stationkeeping project results > 404')
@hooks.before('/sk/{ProjectId}/hierarchy > hierarchy description of the station keeping system > 404')
@hooks.before('/sk/{ProjectId}/bom > bill of materials from the station keeping system > 404')
@hooks.before('/sk/{ProjectId}/environmental_impact > environmental impact of the station keeping system > 404')
@hooks.before('/sk/{ProjectId}/design_assessment > design assessment of the station keeping system > 404')
@hooks.before('/representation/{ProjectId} > digital representation of the station keeping system > 404')
@if_not_skipped
@print_log
def replace_project_id_by_non_existing_project(transaction):
    full_path = transaction['fullPath']
    if '1' in full_path:
        transaction['fullPath'] = full_path.replace('1', '0')
