FROM conda/miniconda3

RUN conda config --set always_yes True
RUN conda update -n base -c defaults conda
RUN conda create --name map++ python=3
RUN activate map++
RUN conda install --channel anaconda git make

RUN apt-get --yes update \
&& apt-get --yes install build-essential \
&& apt-get --yes install liblapacke-dev

RUN python -V

RUN conda install -c conda-forge numpy
RUN conda install -c conda-forge matplotlib=3.1.2

RUN conda install -c conda-forge pandas
RUN conda install -c conda-forge scipy

RUN conda install pip
RUN pip install -U plotly Pillow pytest pytest-cov flask python-dotenv Flask-Babel matplotlib pytest-mock pytest-html utm h5py

COPY . /app

WORKDIR /app/dtop-shared-library
RUN python setup.py install

WORKDIR /app/pymap_submodule
RUN python setup.py install

WORKDIR /app
RUN python setup.py sdist
RUN pip install dtop-stationkeep --find-links dist

CMD [ "/bin/bash", "-c"]
