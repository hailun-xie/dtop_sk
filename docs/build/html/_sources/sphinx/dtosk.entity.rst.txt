dtosk.entity package
====================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   dtosk.entity.geometry

Submodules
----------

dtosk.entity.Body module
------------------------

.. automodule:: dtosk.entity.Body
   :members:
   :undoc-members:
   :show-inheritance:

dtosk.entity.BodyPart module
----------------------------

.. automodule:: dtosk.entity.BodyPart
   :members:
   :undoc-members:
   :show-inheritance:

dtosk.entity.Farm module
------------------------

.. automodule:: dtosk.entity.Farm
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: dtosk.entity
   :members:
   :undoc-members:
   :show-inheritance:
