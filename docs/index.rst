.. _sk-home:

Station Keeping
===============

Introduction
------------

The Station Keeping module (SK) aims to design the mooring and foundation subsystems for various technology choices of wave or tidal devices on different kind of seabed.

Structure
---------

This module's documentation is divided into four main sections :

- :ref:`sk-tutorials` to give step-by-step instructions on using SK for new users.

- :ref:`sk-how-to` that show how to achieve specific outcomes using SK.

- A section on :ref:`background, theory and calculation methods <sk-explanation>` that describes how SK works and aims to give confidence in the tools.

- The :ref:`API reference <sk-reference>` section documents the code of modules, classes, API, and GUI.

.. toctree::
   :hidden:
   :maxdepth: 1

   tutorials/index
   how_to/index
   explanation/index
   reference/index

Functionalities
---------------
The main purpose of the Station Keeping module is to design and assess the mooring system, anchors and foundations of the devices and substation including (see figure below) :

- Mooring lines for floating structure (design, ULS analysis and FLS analysis)

- Anchors (design and ULS analysis)

- Foundation for fixed structure (design and ULS analysis)

The design is based on user choices and inputs, design parameters from other modules, and a catalogue of typical line types and anchors.

.. image:: images/SK_scope.png
   :width: 600px
   :align: center

.. centered:: Scope of the Station Keeping module

The main outputs are the assessment of the mooring system, foundation and anchor design, the total cost and bill of materials for the components used, a hierarchy of how they are connected.

The module can either be run in simplified, medium or advanced mode (complexity level 1, 2, or 3). The level of details of inputs increases with the level of complexity. For example, at low levels of complexity (1 and 2), it is proposed to the user to let the SK module automatically define suitable dimensions of the mooring system, anchors and foundations.

Workflow for using the SK module
--------------------------------

The workflow for using the Station Keeping module can be summarised as 1) provide inputs, 2) run the design analysis, and 3) view the results, as shown in the figure below.

.. image:: images/Workflow.png
   :width: 600px
   :align: center

Overview of SK data requirements
--------------------------------

This section summarises the types of input data required to run the Station Keeping module.

The required inputs to run the module are summarised in the next table. Note that in integrated mode, these all come from other modules except for the mooring, anchor and foundation properties.


.. csv-table:: summary of the required inputs.
   :header: "", "Full complexity"
   :widths: 25, 50

   "Site characteristics", "
   | Sea state statistics
   | Wind statistics
   | Current statistics
   | Bathymetry"
   "Device characteristics", "
   | Main dimensions
   | Hydrostatic data
   | Hydrodynamic data
   | If tidal, rotor characteristics"
   "Array characteristics", "Layout of devices"
   "Mooring system properties", "
   | Mooring lines properties
   | Mooring lines layout"
   "Foundation / anchors", "
   | Foundation / anchor type
   | Foundation / anchor main dimensions"
..