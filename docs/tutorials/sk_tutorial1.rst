.. _sk-study-tutorial1:

Designing monopile foundation of a fixed tidal machine in standalone mode
=========================================================================

Once the entity is created in the Main Module with a complexity level set to 3, the Station Keeping module will open on the Device Positioning page. With this page the inputs are devided in five pages and this tutorial is then divided into seven parts, one per input page, one for the "Run Module" page where the user can launch the calculation and one to show a part of the outputs.

Device Positioning
~~~~~~~~~~~~~~~~~~

- The water density is set to the default value of 1025 :math:`kg/m^{3}`.

- This example consider only one device which is positioned at the location [north, east] = [0, 0] with a yaw = 0° and with a water depth of 50 m.

- Click on "Next Page"

Device Properties
~~~~~~~~~~~~~~~~~

- As described in the tiel this tutorial consider a fixed Tidal Energy Converter (TEC) and the corresponding options has to be set in the Type of Machine section.

- The machine is submerged so no Wind Force Model is included.

- The Current and Mean Wave Drift Force Model consider a Cylinder device profile with both horizontal and vertical main dimensions set to 0. No current forces, and no mean wave drift forces are applied here. Forces on rotor are defined separately, in the ‘Machine Characteristics’ section.

- The device mass is 119700 kg. This field does not include the foundation (since finding the mass of the foundation is the goal of the present analysis).

- To fill the Rotor thrust coefficient curve table, the "Set to Default" button can be used to automatically fill it with the RM1 example. The table should then begin with these the three row described in the following table.

.. csv-table:: Three first row of the Rotor thrust coefficient curve example.
   :header: "Velocity", "Thrust Coefficient"
   :widths: 200, 200
   :align: center

   "0", "0"
   "0.503313181", "0"
   "0.569906809", "0.338483064"
..

- This device is defined with two 20 m rotor set at [x, y, z] = [0, -14, 30] and [x, y, z] = [0, 14, 30]. The position of the two rotors is relative to the seabed level. If the device was floating, this would be relative to the free surface level.

- It is installed on a "dense_sand" seabed type.

- If we assume that the seabed is flat, the soil slope is 0. We can use the default soil safety factor, and we set the load safety factor equal to 1.3 (which is the default value).

- In this example the Foundation type selection would be set to "manual" in order to impose to the module a "Pile" foundation type.

- Let ‘Dimensioning method’ be ‘Automatic’ so that SK module will compute the suitable dimensions.

- The maximum deflection criteria is commonly 5% for fixed structure. Define pile end tip to "Open" and length of the pile above seabed = 30 m.

- Click on "Next Page"

Masterstructure Properties
~~~~~~~~~~~~~~~~~~~~~~~~~~

- In this example, no masterstructure were defined in the previous page as explained by the displayed message "Masterstructure not present. A master structure model is used when several floating devices are moored together. This is not the case here.

- Click on "Next Page"

Substation Properties
~~~~~~~~~~~~~~~~~~~~~

- No substation is required in this example.

- Click on "Next Page"

Analysis Parameters
~~~~~~~~~~~~~~~~~~~

- Set the weather direction to zero. This does not matter much in this example, because we always assume that the rotor faces the current.

- Define Hs = [8] and Tp = [10]: their value will be used to compute a water particle velocity which will be added to the current velocity in order to compute drag forces on the pile foundation. For the forces on the rotors, however, this does not change anything, since only the velocity from the current is used.

- Set the current velocity value to 2.85.

- Set the wind velocity to zero, since we are not interested in wind in this example.

- Click on "Next Page"

Run Module
~~~~~~~~~~

- On this page an input summary describe if all pages where filled. To launch the calculation click on "Run Module".

- The log of the calculation will be displayed and the module allow the user to click on "See Results"

Outputs overview
~~~~~~~~~~~~~~~~

- The total cost of this system if 139387€. This corresponds to the cost of the whole pile.

- In the "Design Assessment" page, the dimensions of the Pile foundation are displayed and summarised in the following table.

.. csv-table:: Anchor and foundation design assessment.
   :header: "Type", "Diameter", "Thickness", "Total length", "Buried length", "Mass"
   :align: center
   
   "Pile foundation", "2.1 m", "0.043 m", "42.6 m", "12.6 m", "92924.85"
..