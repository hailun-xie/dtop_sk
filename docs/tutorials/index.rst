.. _sk-tutorials:

*********
Tutorials
*********

Below is a set of tutorials that guide the user through each of the main functionalities of the Station Keeping tool. All these tutorials describe the functionning of the Station Keeping module in standalone mode.

In integrated mode this tool is called after others modules and some inputs are therefore automatically filled by the others modules' outputs. The concerning fields would then be disabled to edition.

- :ref:`sk-study-tutorial1`

- :ref:`sk-study-tutorial2`

.. toctree::
   :hidden:
   :maxdepth: 1

   sk_tutorial1
   sk_tutorial2