.. _sk-dynamic_analysis:

Dynamic Analysis
****************

The dynamic analysis is based on frequency domain approach. This functionality is associated to Limit State assessments (ULS and FLS) and is thus not directly exposed to the end user; it is called by  the ULS Analysis and FLS Analysis. 

Objectives
==========

•	Compute the Response Amplitude Operator (RAO) of the device at a given position. In practice, this position is the static equilibrium position :math:`x_{eq}` resulting from the :ref:`Static Analysis <sk-static_analysis>`. 

•	For a given wave spectrum, compute the motion response power spectra  :math:`S_{xx}` of the device.


Inputs
======

.. csv-table:: Inputs used to perform a dynamic analysis.
    :header: "Inputs description", "Origin of the Data", "Data Model in SK", "Units"
    :widths: 30, 30, 30, 30

    "Hs – significant wave height",  	"SC", 	"float", 	"[m]" 
    "Tp – wave peak period", 	"SC", 	"float", 	"[s]" 
    "Dir – wave direction", 	"SC", 	"float", 	"[deg]" 
    "M – mass matrix of the device", 	"MC", 	"float array", 	"[kg]"
    "A – added mass matrix of the device", 	"MC", 	"float array", 	"[kg]" 
    "B – damping matrix of the device", 	"MC", 	"float array", 	"[N.s/m]" 
    "K – hydrostatic restoring matrix of the device",	"MC",	"float array",	"[N/m]"
    "Fe – 1st order wave force transfer function", 	"MC", 	"complex array", 	"[N/m]" 
    "ω – frequencies for which A, B and Fe are given", 	"MC", 	"float array",  	"[rad/s]" 
    "Mooring system model", 	"User/Computed", 	"MAP++ model", ""
..


Methods and Outputs
===================

The first step of the dynamic analysis is to compute the RAO of the device, from wave elevation :math:`η` to response motion :math:`x`. For a given environmental condition, the static equilibrium position :math:`x_{eq}`  of the device is computed in the :ref:`Static Analysis <sk-static_analysis>`. The RAO is computed as follows: 

.. math:: H(ω)= [-ω^2 (M+A)+jωB+(K+K_{moor} )]^{(-1)} F_e


where :math:`K_{moor}` is the linearized stiffness matrix representing the restoring force from the mooring system at the static equilibrium position :math:`x_{eq}`: 

.. math:: K_{moor}=\frac{∂F_{moor}}{∂x}(x_{eq})


The second step of the Dynamic Analysis is to compute the response spectra of the device motion :math:`S_{xx}`. In order to do so, the wave elevation spectrum :math:`S_{ηη}` is computed from Hs and Tp, using the JONSWAP formulation from the DNV-RP-C205 [SKRef1]_ with a default value for the non-dimensional peak shape parameter equal to 3.3. The response spectra of the device motion are then computed as follows: 

.. math:: S_{xx}=H(ω) S_{ηη} H(ω)^*


where the symbol ∗ indicates the complex conjugate transpose of a complex matrix. 


