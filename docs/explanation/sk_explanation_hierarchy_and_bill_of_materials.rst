.. _sk-hierarchy_and_bill_of_materials:

Hierarchy and Bill of Materials
*******************************

Objectives
==========

Based on the mooring system design and foundation base/anchor design, a hierarchy of the components is constructed, as well as a bill of materials. The hierarchy is a digital representation of the component-to-component connection relationship within the station keeping system. The bill of materials is the list of components in the station keeping system, associated with their cost. Both the hierarchy and the bill of materials are used as inputs by other modules (LMAO, RAMS and SLC).

Inputs
======

.. csv-table:: Inputs for hierarchy and bill of materials
    :header: "Inputs description", "Origin of the Data"

    "Mooring system design", "User or results from Catenary Mooring System Design"
    "Foundation Base/Anchor design", "User or results from Foundation Base/Anchor Design"
..

Methods and Outputs : hierarchy
===============================

The hierarchy is constructed based on 3 main levels:

•	*Station keeping*: represents the station keeping concept of one moored entity

•	*Mooring line*: represents a collection of components linked together

•	*Components*: represents a physical component, *e.g.* a line segment or an anchor

The hierarchy can be visualized as a table, where each line represents one of the three levels above, and the columns correspond to the output variables described in the next Table.

.. csv-table:: Hierarchy outputs
    :header: "Outputs description", "Data Model in SK"

    "system", "string array"
    "name_of_node", "string array"
    "design_id", "string array"
    "node_type", "string array"
    "node_subtype", "string array"
    "category", "string array"
    "parent", "string array"
    "child", "array of string array"
    "gate_type", "string array"
    "failure_rate_repair", "float array"
    "failure_rate_replacement", "float array"
..

Methods and Outputs : bill of materials
=======================================

The bill of materials is constructed as a list of components with their associated quantity and cost. It contains the variables described in the next Table.

.. csv-table:: Bill of materials outputs
    :header: "Outputs description", "Data Model in SK", "Units"

    "module_name", "string array", "[-]"
    "catalogue_id", "string array", "[-]"
    "product_name", "string array", "[-]"
    "quantity", "float array", "[unit]"
    "unit", "string array", "[-]"
    "unit_cost", "float array", "[€/unit]"
    "total_cost", "float array", "[€]"
..