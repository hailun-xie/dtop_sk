.. _sk-foundation_suitability:

Foundation Suitability
**********************

Objective
=========

The choice of foundation type depends on soil conditions. If the soil is sandy, then a shallow foundation is usually recommended. Alternatively, conventional driven piles may be used as anchors in sandy soil conditions [SKRef11]_.

For clay soil stratigraphy, suction anchors or suction buckets can be used. These are quicker to install and easy to remove. Conventional driven piles can also be used in clay. However, due to noise pollution while driving, driven piles may not be desirable [SKRef11]_.

The SK module will determine the more suitable type of foundation at the farm level. This task is performed taking into account the following aspects / criteria :

•	Seabed connection type: Fixed or moored

•	Soil type: 

    o	Cohesive soils: very soft clay, soft clay, firm clay, stiff clay, very stiff clay and hard clay

    o	Cohesionless soils:  very loose sand, loose sand, medium dense sand, dense sand, very dense sand and gravels/pebbles

•	Seabed slope: moderate (<10°) or steep (>10°)

•	Main load orientation of the load applied to the foundation: Downward, upward, or horizontal

•	Environmental impact: recovery, footprint, noise

A suitability score is given to every foundation for each of the five criteria defined above. The approach to determining foundation suitability is based on a simple sum of all criteria from the five matrices for each foundation type. The lowest score is deemed to be the most suitable for the study case that is being analysed. Matrices were built taking into account data/feedback from literature. [SKRef11]_ – [SKRef15]_.

Inputs
======

The inputs needed for carrying out the assessment of the foundation suitability are in Table 1.

.. csv-table:: Table 1 : Inputs for foundation type suitability evaluation
    :header: "Inputs description", "Origin of the Data", "Data Model in SK", "Units"
    :widths: 30, 30, 30, 30

    "Soil Type", "User/SC", "string", "
    |'very_soft_clay', 'soft_clay',
    | 'firm_clay', 'stiff_clay',
    | very_stiff_clay', 'hard_clay',
    | 'very_loose_sand', loose_sand',
    | 'medium_dense_sand', 'dense_sand',
    | 'very_dense_sand', 'gravels_pebbles'"
    "Soil slope", "User/SC", "float", "[°]"
    "Seabed connection type", "EC", "string", "'moored' or 'fixed'"
..

Methods and Outputs : foundation suitability
============================================

The foundation assessment class is performed through suitability matrices for each type of foundation and conditions/criteria (Table 2, Table 3, Table 4, Table 5 and Table 6). A value is given to every foundation for each condition/criteria, to attribute this value a review of different literature was carried out :

•	**1** means it is suitable, 

•	**2** means that it is possible, even if not optimal, and,

•	**100** that it is unsuitable. 

The approach to determining foundation suitability is based on a simple sum of each parameter from the matrices.
Table 2 presents the suitability matrix for soil type, it summarizes all the data/feedback from documents [SKRef11]_ – [SKRef15]_

.. csv-table:: Table 2 : Suitability matrix for soil type
    :header: "Type of soil", "Shallow foundations", "Deadweight anchors", "Piles", "Drag anchors", "Suction caisson"

    "**Very soft clay**", "1", "1", "2", "2", "1"
    "**Soft clay**", "1", "1", "2", "1", "1"
    "**Firm clay**", "1", "1", "1", "1", "1"
    "**Stiff clay**", "1", "1", "1", "1", "1"
    "**Very stiff clay**", "1", "1", "1", "1", "1"
    "**Hard clay**", "1", "1", "1", "1", "1"
    "**Very loose sand**", "1", "1", "2", "1", "1"
    "**Loose sand**", "1", "1", "2", "1", "1"
    "**Medium dense sand**", "1", "1", "1", "1", "1"
    "**Dense sand**", "1", "1", "1", "1", "1"
    "**Very dense sand**", "1", "1", "2", "1", "2"
    "**Gravels, pebbles**", "2", "2", "100", "100", "100"
..

.. csv-table:: Table 3 : Suitability matrix for seabed slope
    :header: "Seafloor topography", "Shallow foundations", "Gravity anchors", "Piles", "Drag anchors", "Suction caisson"

    "Moderate < 10°", "1", "1", "1", "1", "1"
    "steep ≥ 10° ", "100", "100", "1", "100", "2"
..

.. csv-table:: Table 4 : Suitability matrix for main load direction
    :header: "Laoding main direction", "Shallow foundations", "Gravity anchors", "Piles", "Drag anchors", "Suction caisson"

    "Downward load", "1", "100", "2", "100", "1"
    "Vertical Uplift", "2", "1", "1", "100", "1"
    "Horizontal load", "2", "2", "2", "1", "2"
..

.. csv-table:: Table 5 : Suitability matrix for seabed connexion type
    :header: "Device type", "Shallow foundations", "Gravity anchors", "Piles", "Drag anchors", "Suction caisson"

    "Moored", "100", "1", "1", "1", "1"
    "Fixed", "1", "100", "1", "100", "2"
..

.. csv-table:: Table 6 : Suitability matrix for environmental impacts
    :header: "Environmental impacts", "Shallow foundations", "Gravity anchors", "Piles", "Drag anchors", "Suction caisson"

    "Recovery", "2", "2", "100", "1", "1"
    "Footprint", "2", "2", "1", "100", "2"
    "Noise", "2", "2", "100", "2", "2"
..

The output of this methodology is a list of foundations types and their score. The foundation with the smaller score will be the more suitable solution and it will be design.