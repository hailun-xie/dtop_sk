.. _sk-explanation:

***********
Explanation
***********

Introduction
============

The Station Keeping (SK) Module produces outputs and assessments based on the following main functionalities: 

1.	:ref:`sk-static_analysis`: Static analysis of the device is performed. The main outputs are the equilibrium position of the device and the amplitude of the environmental forces that apply on the device and the tension in the lines of the mooring system. 

2.	:ref:`sk-dynamic_analysis`: Dynamic analysis of the floating device is performed in frequency domain. The main outputs are the main six degrees of freedom motions in terms of power spectra. 

3.	:ref:`sk-uls_analysis`: Ultimate Limit State analysis of the mooring lines segments is performed. Maximum expected tensions and ULS criteria check in each line segments are the main outputs. For a fixed structure, the main output is the maximum expected force on the foundation base. 

4.	:ref:`sk-fls_analysis`: Fatigue Limit State analysis of the mooring lines segments is performed. Cumulated fatigue damage and stress range long term distribution of each line segment are the main outputs. 

5.	:ref:`sk-mooring_system_design`: A mooring system based on catenary lines is designed. Number of lines, chain diameter, length of lines and anchor points positions are the main outputs. For catenary system, an automated design algorithm is available. 

6.	:ref:`sk-soil_properties`: The mechanical and physical properties of the soil are determined based on the soil type.

7.	:ref:`sk-foundation_suitability`: The type of foundation base (for fixed structure) and anchor (for floating structures) are selected based on the soil type and the type of external loads.

8.	:ref:`sk-foundation_design`: Foundation base and anchors are designed. Dimensions and type of foundation base and anchors are the main outputs.

9.	:ref:`sk-hierarchy_and_bill_of_materials`: a hierarchy and a bill of materials are produced based on the mooring system design and foundation design.

10. :ref:`sk-environmental_impact`: a set of metrics required by the Environmental and Social Acceptance (ESA) module is computed based on the mooring system design and foundation base/anchor design.

.. toctree::
   :hidden:
   :maxdepth: 1
  
   sk_explanation_static_analysis
   sk_explanation_dynamic_analysis
   sk_explanation_uls_analysis
   sk_explanation_fls_analysis
   sk_explanation_mooring_system_design
   sk_explanation_soil_properties
   sk_explanation_foundation_suitability
   sk_explanation_foundation_design
   sk_explanation_hierarchy_and_bill_of_materials
   sk_explanation_environmental_impact

In order for the modules of DTOceanPlus to be used at different stages of a development project, the code has been implemented so that it can cope with 3 levels of complexity (CPX).
The CPX are universal for the DTOceanPlus tools. The main idea is that the precision and fidelity of the results increases with increasing CPX, while the amount and complexity of the required inputs increases as well. In the SK module, the logic and methods used for the calculation are the same for each CPX. The main difference between the different CPX is the precision and availability of input data:

•	CPX1 and CPX2

    o	hydrodynamic data of the floater are not available from the MC module. Instead, the user can choose a set of hydrodynamic data from a pre-generated database with different simple floater geometries
    
    o	the user might want to define a mooring system but lacks some input. An automated algorithm for design of a catenary mooring system is available

•	CPX3
    o	hydrodynamic data of the floater are available from the MC module.
    
    o	mooring system input is required. Results from the Catenary Mooring System Design performed at CPX1 and CPX2 can be used.

For each level of complexity, the following tables summarize the available functionalities the required inputs.

.. csv-table:: Functionalities and complexity levels in the SK module.
   :header: "Functionality", "Availability at CPX1", "Availability at CPX2", "Availability at CPX3"
   :widths: 40, 30, 30, 30

    "Static Analysis",	"Yes",	"Yes",	"Yes"
    "Dynamic Analysis",	"Yes",	"Yes",	"Yes"
    "ULS Analysis", 	"Yes",	"Yes",	"Yes"
    "FLS Analysis", 	"Yes",	"Yes",	"Yes"
    "Mooring System Design", 	"Custom/Automated",	"Custom/Automated",	"Custom"
    "Foundation Base/Anchor Design", 	"Yes",	"Yes",	"Yes"
    "Hierarchy and Bill of Materials", 	"Yes",	"Yes",	"Yes"
    "Environmental Impact", 	"Yes",	"Yes",	"Yes"
..

.. csv-table:: Inputs and complexity levels in the SK module.
   :header: "Inputs", "CPX1", "CPX2", "CPX3"
   :widths: 40, 30, 30, 30

    "Environmental conditions",	"Required",	"Required",	"Required"
    "Device properties",	"Required",	"Required",	"Required"
    "Description of mooring system", 	"Required",	"Required",	"Required"
    "Parameters for ULS analysis", 	"Required",	"Required",	"Required"
    "Parameters for FLS analysis", 	"Not required",	"Not required",	"Required"
    "Parameters for foundation  / anchor design", 	"Default values",	"Default values",	"Required"
..


References
==========

.. [SKRef1] DNV, ‘DNV-RP-C205: environmental conditions and environmental loads. Recommended practice’, Det Norske Veritas, 2010.
.. [SKRef2] P. Bearman and D. Trueman, ‘An investigation of the flow around rectangular cylinders’, The Aeronautical Quarterly, vol. 23, no. 3, pp. 229–237, 1972.
.. [SKRef3] R. E. Taylor, C. Hu, and F. Nielsen, ‘Mean drift forces on a slowly advancing vertical cylinder in long waves’, Applied ocean research, vol. 12, no. 3, pp. 141–152, 1990.
.. [SKRef4] P. McIver, ‘Mean drift forces on arrays of bodies due to incident long waves’, Journal of Fluid Mechanics, vol. 185, pp. 469–482, 1987.
.. [SKRef5] A. Hermans and G. Remery, ‘Resonance of moored objects in wave trains’, in Coastal Engineering 1970, 1970, pp. 1685–1700.
.. [SKRef6] NREL, ‘MAP++ - release notes’, Release Note, 2015. https://map-plus-plus.readthedocs.io/en/latest/theory.html#line-theory.
.. [SKRef7] DNV-GL, ‘DNVGL-OS-E301 Position Mooring’, DNV GL, Oslo, 2015.
.. [SKRef8] O. Faltinsen, ‘Sea Loads on ships and offshore structures’ Cambridge University Press’, Chapters, vol. 2, pp. 13–101, 1990.
.. [SKRef9] Anchors Vryhof, ‘Anchor manual 2015’, Anchors Vryhof, 2015.
.. [SKRef10] P. Le Tirant and J. Meunier, Anchoring of floating structures, vol. 2. Editions Technip, 1990.
.. [SKRef11] R. F. Stevens, A. Rahim, and others, ‘Mooring Anchors for Marine Renewable Energy Foundations’, 2014.
.. [SKRef12] The Carbon Trust, ‘Foundations and Moorings for tidal stream system’, The Carbon Trust, 2009.
.. [SKRef13] D. Thompson et al., ‘Handbook for marine geotechnical engineering’, Naval Facilities Engineering Command Port Hueneme CA Engineering Service Center, 2012.
.. [SKRef14] C. Aubeny, Geomechanics of Marine Anchors. CRC Press, 2018.
.. [SKRef15] D. Greaves and G. Iglesias, Wave and tidal energy. John Wiley & Sons, 2018.
.. [SKRef16] DNV, ‘DNV-OS-J101-Design of offshore wind turbine structures’, Det Norske Veritas, 2014.
.. [SKRef17] DNV, ‘DNV: Classification Notes No. 30.4 Foundations’, Det Norske Veritas, Hovik, 1992.
.. [SKRef18] M. Randolph and S. Gourvenec, Offshore geotechnical engineering. CRC press, 2011.
.. [SKRef19] G. Chatzivasileiou, ‘Installation of suction caissons in layered sand: assessment of geotechnical aspects’, The Netherlands: Delft University of Technology, 2014.
.. [SKRef20] R. H. Romp, ‘Installation effects of suction caissons in non-standard soil conditions’, PhD Thesis, Delft University of Technology, 2013.
.. [SKRef21] K. Andersen et al., ‘Suction anchors for deepwater applications’, in Proceedings of the 1st International Symposium on Frontiers in Offshore Geotechnics, ISFOG, Perth, 2005, pp. 3–30.
.. [SKRef22] J. B. Hansen, ‘A revised and extended formula for bearing capacity’, 1970.
.. [SKRef23] DNV GL, ‘DNVGL-RP-E303: Geotechnical design and installation of suction anchors in clay’, DNV GL, 2017.
.. [SKRef24] J. D. Murff and J. M. Hamilton, ‘P-ultimate for undrained analysis of laterally loaded piles’, Journal of Geotechnical Engineering, vol. 119, no. 1, pp. 91–107, 1993.
.. [SKRef25] H. Taiebat and J. Carter, ‘Interaction of forces on caissons in undrained soils’, 2005.
.. [SKRef26] W. Deng, J. Carter, and H. Taiebat, ‘Prediction of the lateral capacity of suction caissons’, in Proceedings, 10th International Conference of the International Association for Computer Methods and Advances in Geomechanics, 2000, pp. 33–38.
.. [SKRef27] S. Bhattacharya, Design of foundations for offshore wind turbines. Wiley Online Library, 2019.
.. [SKRef28] G. T. Houlsby and B. W. Byrne, ‘Design procedures for installation of suction caissons in sand’, Proceedings of the Institution of Civil Engineers-Geotechnical Engineering, vol. 158, no. 3, pp. 135–144, 2005.
.. [SKRef29] M. Iskander, S. El-Gharbawy, and R. Olson, ‘Performance of suction caissons in sand and clay’, Canadian Geotechnical Journal, vol. 39, no. 3, pp. 576–584, 2002.
.. [SKRef30] L. Arany and S. Bhattacharya, ‘Simplified load estimation and sizing of suction anchors for spar buoy type floating offshore wind turbines’, Ocean Engineering, vol. 159, pp. 348–357, 2018.
.. [SKRef31] S. R. Neubecker, M. Randolph, and others, ‘Performance of embedded anchor chains and consequences for anchor design’, 1995.
.. [SKRef32] DNV, ‘DNV-RP-E301: Design and installation of fluke anchors in clay’, DNV, 2012.
.. [SKRef33] H. Liu, Y. Li, H. Yang, W. Zhang, and C. Liu, ‘Analytical study on the ultimate embedment depth of drag anchors’, Ocean engineering, vol. 37, no. 14–15, pp. 1292–1306, 2010.
.. [SKRef34] OpenCASCADE, ‘Deliverable 7.4: Software handbook, DTOceanPlus’, 2019.
.. [SKRef35] A. Babarit and G. Delhommeau, ‘Theoretical and numerical aspects of the open source BEM solver NEMOH’, 2015