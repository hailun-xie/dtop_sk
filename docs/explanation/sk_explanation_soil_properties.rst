.. _sk-soil_properties:

Soil Properties
***************

Objectives
==========

In order to be able to design a foundation or anchor, soil mechanical properties are needed. For CPL1 and CPL2 the user chooses one type of soil, from the catalogue for standalone mode, or from the SC module. The type of soil will be used to attribute mechanical and physical properties to the soil that are essential to be able to dimension the foundation.

For CPL3 soil mechanical properties will be defined by the user as input information. 

Inputs
======

The inputs needed for carrying out the assessment of the soil properties are presented in the next Table.

.. csv-table:: Inputs to determine soil properties
    :header: "Inputs description", "Origin of the Data", "Data Model in SK", "Units"

    "Soil Type", "User/SC", "string", "
    | 'very_soft_clay', 'soft_clay',
    | 'firm_clay', 'stiff_clay',
    | very_stiff_clay', 'hard_clay',
    | 'very_loose_sand', loose_sand',
    | 'medium_dense_sand', 'dense_sand',
    | 'very_dense_sand', 'gravels_pebbles',
    | 'custom'"
..

Methods and Outputs : Soil properties definition
================================================

Depending on the soil type, mechanical properties such as: friction angle, cohesion, buoyant weight and relative density index will be attributed to be able to design the foundations/anchors.

The soil mechanical properties values that are used are regrouped on the following Table, they were determined through different literature [SKRef9]_, [SKRef10]_.

.. csv-table:: InSoil properties catalogue for CPL 1 and 2
    :header: "Soil Type", "Buoyant unit weight [:math:`N/m^3`]", "Friction angle [°]", "Relative density index", "Undrained shear strength [Pa]"

    "Very soft clay", "7200", "-", "", "10e+03"
    "Soft clay", "7200", "-", "", "20e+03"
    "Firm clay", "7200", "-", "", "33e+03"
    "Stiff clay", "9800", "-", "", "75e+03"
    "Very stiff clay", "9800", "-", "", "150e+03"
    "Hard clay", "9800", "-", "", "200e+03"
    "Very loose sand", "8700", "25", "0.1", "-"
    "Loose sand", "8700", "30", "0.25", "-"
    "Medium dense sand", "8700", "32", "0.45", "-"
    "Dense sand", "10600", "35", "0.75", "-"
    "Very dense sand", "10600", "38", "0.85", "-"
    "Gravels, pebbles", "10600", "45", "", "-"
..

For CPL 3 the user can choose the soil type “custom” and define its mechanical properties.