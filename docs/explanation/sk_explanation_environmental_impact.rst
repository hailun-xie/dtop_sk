.. _sk-environmental_impact:

Environmental Impact
********************

Objectives
==========

Based on the mooring system design and foundation base/anchor design, a set of metrics required by the ESA-module are computed.

Inputs
======

.. csv-table:: Inputs for environmental impact.
    :header: "Inputs description", "Origin of the Data"
    :widths: 60, 60

    "Mooring system design",  	"User or results from Catenary Mooring System Design"
    "Foundation Base/Anchor design", 	"User or results from Foundation Base/Anchor Design" 
..

Methods and Outputs
===================

The metrics computed for the ESA-module are the following:

•	The quantity of material sorted by material type

•	The total submerged surface of the station keeping system

•	The footprint of the station keeping system, i.e. the area of the seabed covered by the components of the station keeping system

The quantity of material and the total submerged surface are computed from the dimensions and material quantity of the mooring system components: line segments, anchors and foundation bases.

The footprint of the line segments is computed as follows:

•	A Static Analysis is performed at the initial position of the device

•	The touchdown length of each segment is computed at this position

•	The maximum touchdown length 𝐿𝑡𝑑 among all the line segments is selected

•	The footprint is computed as 𝜋𝐿𝑡𝑑²

The outputs of the Environmental Impact functionality are listed in the following Table.

.. csv-table:: Environmental impact outputs.
    :header: "Outputs description", "Data Model in SK", "Units"
    :widths: 60, 30, 30

    "footprint", "float array", "[m²]"
    "submerged surface", "float array", "[m²]"
    "material quantity", "float array", "[kg]"
    "material", "string array", "[-]" 
..