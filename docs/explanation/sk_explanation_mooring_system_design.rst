.. _sk-mooring_system_design:

Mooring System Design
*********************

The user can choose to define a :ref:`custom mooring system <sk-custom_mooring_system_design>`, or use the :ref:`automated catenary mooring system design <sk-automated_mooring_system_design>`.

.. _sk-custom_mooring_system_design:

Custom Mooring System Design
****************************

Objectives
==========

Allow the user to define a custom mooring system

Inputs
======

The input data used to model the mooring system are given in the following table. They are divided in three categories: 

•	**Node**: a node can be an anchor point fixed to the seabed, a fairlead point fixed to the device or a point that connects two or more lines segments together. A constant force, a buoyancy volume or a mass can be defined at a node. Several nodes can be defined.

•	**Line type**: a line type contains the material properties of a line segments, e.g. elasticity, diameter, mass per unit length, friction coefficient with the seabed. Several line types can be defined.

•	**Line segment**: a line segment is defined by two nodes, one line type and the unstretched length of the segment. Several line segments can be defined.

.. csv-table:: Inputs for custom mooring system design
    :header: "Inputs description", "Category", "Data Model in SK", "Units"
    :widths: 30, 30, 30, 30

    "Node number- node identity number",	"Node",	"integer"	
    "Node type – Vessel, Connect or Fix",	"Node",	"string"
    "Position – node position (x,y,z)",	"Node",	"float array",	"[m]"
    "Point_mass – mass point attached to the node",	"Node",	"float",	"[kg]"
    "Point_volume – buoyancy volume attached to the node", 	"Node",	"float",	"[m3]" 
    "Line type number – line type identity number",	"Line type",	"integer"	
    "Mass_in_air – mass density of the line in air",	"Line type",	"float",	"[kg/m]"
    "Diameter – line diameter used to compute weight in water",	"Line type",	"float",	"[m]"
    "ea – line elasticity",	"Line type",	"float",	"[N]"
    "Cb – friction coefficient on the seabed",	"Line type",	"float",	"[-]"
    "Mbl – minimum breaking load",	"Line type",	"float",	"[N]"
    "Mblc – corroded minimum breaking load",	"Line type",	"float",	"[N]"
    "aD – intercept parameter of the S-N curve", 	"Line type",	"float",	"[-]" 
    "m – slope of the S-N curve", 	"Line type",	"float",	"[-]" 
    "Line number – line segment identity number",	"Line segment",	"integer"	
    "Line segment type number – identity number of the line type", "Line segment",	"integer"	
    "Node1 number – identity number of node at end1 of segment",	"Line segment",	"integer"	
    "Node2 number – identity number of node at end2 of segment",	"Line segment",	"integer"	
    "Line length – unstretched line segment length",	"Line segment",	 "float",	"[m]"
    "Flag – optional flag. Use “LINEAR_SPRING” for taut lines",	"Line segment",	"string"
..

Those input data are used to write a MAP++ input file which is read by the MAP++ library.

More details about the modelling possibilities in MAP++ library can be found on the dedicated website https://map-plus-plus.readthedocs.io/en/latest/input_file.html. The following figure shows an example of a mooring system modelled in MAP++.

.. image:: ../images/fig-2-6.png
    :align: center
    :width: 500px

.. centered:: Example of a mooring system modelled using MAP++ library

.. _sk-automated_mooring_system_design:

Automated catenary Mooring System Design
****************************************

For CPL 1 and CPL2, it is expected that the end-user has not yet defined the design of the device mooring system. A robust algorithm has been implemented to perform the automated design of a catenary mooring system. This algorithm is based on common engineering practice and return of experience when designing a catenary mooring system.

Objectives
==========

Perform the design of a catenary mooring system.

Inputs
======

.. csv-table:: Inputs to perform catenary mooring system design
    :header: "Inputs description", "Origin of the Data", "Data Model in SK", "Units", "Default value"
    :widths: 30, 30, 30, 30, 30

    "Inputs necessary for ULS Analysis (c.f. :ref:`link <sk-uls_analysis>`)", "Various", "Various", "", ""
    "Chain catalogue – list of chain type and properties", "Catalogue module", "Various", "", ""
    "Nlines_min – minimum number of mooring lines", "User", "float", "[-]", "3"
    "Nlines_max – maximum number of mooring lines", "User", "float", "[-]", "10"
    "Pten_min – minimum pretension in the mooring lines", "User", "float", "[% of MBL]", "0.03"
    "Pten_max – maximum pretension in the mooring lines", "User", "float", "[% of MBL]", "0.06"
    "O_max – maximum allowable total offset (static+dynamic)", "User", "float", "[m]", "30"
    "Teig_min – minimum horizontal eigenperiod", "User", "float", "[s]", "30"
    "C_td – coefficient of touchdown length reduction", "User", "float", "[-]", "0.95"
..

The inputs in the previous Table specific to the design of the catenary system are proposed with default values, but the user has the possibility to modify them.

Mooring line catalogue
======================

The chain catalogue is extracted from the line catalogue that contains a list of line component properties of different diameter. For each line type, the following properties are given :

.. csv-table:: Line catalogue
    :header: "Property name", "Property description", "Data Model", "Units", "Example"
    :widths: 30, 30, 30, 30, 30

    "catalogue_id", "Component ID in the catalogue", "string", "", "chain_1"
    "type", "Line type", "string", "", "chain"
    "material", "Line material", "string", "", "steel"
    "chain_link_diameter", "Link diameter if line is of type chain", "float", "[m]", "0.001"
    "diameter", "Line diameter used to compute the weight in water", "float", "[m]", "0.3"
    "ea", "Elasticity of the line", "float", "[N]", "1.8E+08"
    "weigth_in_air", "Weight in air of the line", "float", "[kg/m]", "130"
    "mbl", "Minimum breaking load", "float", "[N]", "137000"
    "cost_per_meter", "Cost per meter of the line", "float", "[€/m]", "134"
    "ad", "SN-curve parameter ad", "float", "[-]", "600000"
    "m", "SN-curve parameter m", "float", "[-]", "3"
..

Methods and Outputs
===================

The goal of the algorithm is to design a catenary system, *i.e.* to identify the following quantities:
	
•	The number of mooring lines :math:`N_{lines}`

•	The length of the mooring lines :math:`L`

•	The diameter of the mooring lines :math:`D`

•	The mooring radius :math:`R_{anch}`

Those quantities define a mooring system that needs to satisfy the following criteria :
	
•	Criterion 1: the pretension at the fairlead in each line :math:`T_{pre}` is between the specified values :

.. math:: P_{ten_{min}} < \frac{T_{pre}}{MBL} < P_{ten_{max}}
	
•	Criterion 2: the ULS analysis criterion is satisfied :

.. math:: C_{uls} ≤ 1
	
•	Criterion 3: the device offset :math:`O_{uls}` obtained from ULS analysis is smaller than the specified offset :

.. math:: O_{uls} ≤ O_{max}
	
•	Criterion 4: the horizontal eigen period :math:`T_{eig}` of the system is larger than the specified eigen period :

.. math:: T_{eig} ≥ T_{eig_{min}}

In addition, the total cost of the mooring system needs to be minimized, thus the following quantities are to be minimized :

•	The number of mooring lines :math:`N_{lines}`

•	The unstretched length of the mooring lines :math:`L`

•	The diameter of the mooring lines :math:`D`

In order to simplify the problem, we assume the following:

•	The mooring lines are uniformly distributed around the device

•	Each mooring line is made of a unique line segment of type chain

The algorithm used to determine the quantities :math:`N_{lines}`, :math:`L`, :math:`D` and :math:`R_{anch}` consists in the following steps:

•	Step 1: set :math:`N_{lines}` equal to :math:`N_{lines_{min}}`

•	Step 2: initiate the mooring system with:
	
    o	:math:`N_{lines}` mooring lines
	
    o	:math:`D` equals to the minimum chain diameter available in the catalogue
	
    o	:math:`L` computed as the theoretical minimum line length to avoid vertical force on anchor point when the tension reaches the value of the minimum breaking load of the line ([SKRef8]_, Eq. 8.19)

.. math:: L = h \sqrt{2 \frac{MBL}{m_w gh}-1}

where h is the water depth, MBL the minimum breaking load of the line segment and :math:`m_w` the weight in water per unit length of the line segment.

    o	:math:`R_{anch}` so that the theoretical pretension is 10% of MBL ([SKRef8]_, Eq.8.21) :

    .. math:: R_{anch}= L - h \sqrt{1+\frac{2a}{h}} + a*arccosh⁡(1+\frac{h}{a})

where :math:`a = \frac{0.1*MBL}{m_w g}`.

•	Step 3: compute static equilibrium of the moored device as done in the Static Analysis, without environment. This obtained tension in the lines is the actual pretension. Adjust the value of :math:`L` so that Criterion 1 (pretension) is satisfied. A dichotomy algorithm is used.	

•	Step 4: compute the maximum dynamic offset :math:`O_{dyn_{max}}` by performing a Dynamic Analysis (*i.e.* as if the floater was not moored). 

•	Step 5: compute the maximum acceptable mean offset :math:`O_{st_{max}} = O_{max} - O_{dyn_{max}}`. If :math:`O_{st_{max}}<0`, the design process stops and an error is sent to the user: “the maximum allowable total offset :math:`O_{max}` needs to be increased”. In practice, the maximum allowable offset is defined in ED module. It might be necessary for the user to run the ED module again.

•	Step 6: for all the ULS environmental conditions, compute the mean offset :math:`O_{st}` by performing a Static Analysis. If :math:`O_{st} > O_{st_{max}}`, increase the chain diameter by selecting the first chain type in the catalogue with a larger diameter. Equilibrium of the system is performed (perform Step 3). 

•	Step 7: perform Step 6 until :math:`O_{st} < O_{st_{max}}`.

•	Step 8: compute the eigen period :math:`T_{eig}` of the moored device in the horizontal plane. Check Criterion 4 (system eigen period > minimum user-defined period). If it is not satisfied, the design process is stops and an error is sent to the user: “the mooring system is too stiff, the maximum allowable total offset needs to be increased”.

•	Step 9: perform ULS (dynamic) analysis for all the environmental conditions, compute :math:`C_{uls}` and the minimum touchdown length of the mooring line :math:`L_{td}`.

•	Step 10: check ULS Criterion 2. If it is not satisfied, proceed to next design Step 11, else proceed to final Step 12.

•	Step 11: Increase the chain line diameter by selecting the first chain type in the catalogue with a larger diameter. Perform Step 3 (system equilibrium). Go back to Step 9 (ULS analysis). If it is not possible to increase the diameter because the current chain type has the largest diameter of the catalogue, add one additional mooring line :math:`N_{lines}=N_{lines}+1`. Go back to Step 2.

•	Step 12: reduce :math:`R_{anch}` and :math:`L` by the value :math:`c_{td} * L_{td}`.

The design procedure is summarized in the following figure.

.. image:: ../images/fig-2-7.png
    :align: center

.. centered:: Algorithm for catenary mooring system design

The main outputs of the Catenary Mooring System Design are given in the next table.

.. csv-table:: Outputs from catenary mooring system design
    :header: "Outputs description", "Data Model in SK", "Units"
    :widths: 60, 30, 30

    ":math:`N_{lines}` – number of mooring lines", "float", "[-]"
    ":math:`L` – line length", "float", "[m]"
    ":math:`D` – mooring line diameter", "float", "[m]"
    ":math:`R_{anch}` – mooring radius", "float", "[m]"
..

In addition, a MAP++ input file is generated, containing the mooring system data necessary to run a mooring analysis.