.. _sk-fls_analysis:

FLS Analysis
************

Fatigue Limit State analysis (FLS) is performed for the mooring system of a floating device, using a method based on [SKRef7]_, Section 6.3. 

Objectives
==========

Compute the line segment cumulated damage during the lifetime of the mooring system.

Inputs
======

.. csv-table:: Inputs used to perform FLS Analysis
    :header: "Inputs description", "Origin of the Data", "Data Model in SK", "Units"
    :widths: 30, 30, 30, 30

    "Inputs necessary for :ref:`Static Analysis <sk-static_analysis>` and :ref:`Dynamic Analysis <sk-dynamic_analysis>`", "", "", ""	
    "aD – intercept parameter of the S-N curve", 	"User",	"float array",	"[-]"
    "m – slope of the S-N curve", 	"User",	"float array",	"[-]"
    "{Hs_lb,Hs_up} – bins of significant wave height Hs",	"SC",	"float array",	"[m]"
    "{Dp_lb,Dp_up} – bins of wave direction Dp",	"SC",	"float array",	"[deg]"
    "Pb –probability of occurrence of each {Hs,Dp} bin",	"SC",	"float array",	"[-]"
    "Tp – wave peak period associated with each {Hs,Dp} bin",	"SC",	"float array",	"[s]"
    "Vw – wind velocity associated with each {Hs,Dp} bin",	"SC",	"float array",	"[m/s]"
    "Dirw – wind direction associated with each {Hs,Dp} bin",	"SC",	"float array",	["deg]"
    "Vc – current velocity associated with each {Hs,Dp} bin",	"SC",	"float array",	"[m/s]"
    "Dirc – current direction associated with each {Hs,Dp} bin",	"SC",	"float array",	"[deg]"
    "T_lifetime – Lifetime duration of the mooring system",	"User",	"float array",	"[s]"
    "γ_fls- safety factor for fatigue limit state",	"User",	"float",	"[-]"
..

Methods and Outputs
===================

Based on the recommendations in [SKRef7]_, Section 6.3.1, the environmental conditions used for the FLS Analysis are discretized in two-dimensional bins: significant wave height Hs and wave direction Dp. For each bin, the probability of occurrence is required, as well as the associated peak period and current and wind velocities and directions. 
The FLS analysis consists in the following steps:

•	**Step 1**: select a {Hs,Dp} bin. The corresponding environmental condition is defined as follows:

	•	Hs value is taken as the upper bound of the considered bin

	•	Dp value is taken as the centre of the considered bin

	•	Wind velocity and direction associated with the considered bin

	•	Current velocity and direction associated with the considered bin

•	**Step 2**: perform a :ref:`Static Analysis <sk-static_analysis>`. The main output is the device static equilibrium position :math:`x_{eq}`. In addition, for each line segment, we compute the tension :math:`T_{eq}` in the line when the device position is equal to :math:`x_{eq}`.

•	**Step 3**: perform a :ref:`Dynamic Analysis <sk-dynamic_analysis>`. The main output is the device motion response spectra :math:`S_{xx}`.

•	**Step 4**: compute the standard deviations of motion responses as follows:

.. math:: σ_{xx}=\sqrt{ \int_0^{\infty} S_{xx} (ω)dω}

•	**Step 5**: for each line segment, compute the tension :math:`T_{eq_σ}` in the line when the device position is equal to :math:`X_σ=x_{eq}+σ_{xx}`. 

•	**Step 6**: for each line segment, compute the standard deviation of the tension :math:`σ_T` as follows:

.. math:: σ_T=|T_{eq_σ}-T_{eq} |

Since the tension may vary along the line segment, σ_T is taken as the maximum of the values at both ends of the line segment.

•	**Step 7**: for each line segment, compute the standard deviation :math:`σ_S` of the stress process as:

.. math:: σ_S=\frac{σ_T}{A_{cs}}

where :math:`A_{cs}` is the cross-Sectional area of the line; i.e. for a chain of nominal diameter D, :math:`A_{cs}=2πD^2/4`, and for a rope of diameter D, :math:`A_{cs}=πD^2/4`, as recommended in [SKRef7]_, Section 6.1.2.
	
•	**Step 8**:: compute the damage :math:`d_i` of the line segment due to the environment no. i selected in Step 1. As in the approach detailed in [SKRef7]_, Section 6.3.5, it is assumed that the low-frequency content of the stress process is negligible. This assumption is applied here because we do not have access to the low-frequency motion of the device in the current version of the SK module. A narrow-banded assumption is applied to give:
 
.. math:: d_i = \frac{ν_{0i} T_i}{a_D}  (2 \sqrt{2} σ_S )^m \Gamma (\frac{m}{2}+1)



where :math:`\Gamma` is the gamma function, {:math:`{a_D,m}`} the S-N curve parameters of the line segment, :math:`T_i` the total duration of the environment no. i, and :math:`ν_{0i}` is the mean up-crossing period of the stress process. :math:`T_i` is given by:

 
.. math:: T_i=T_{lifetime} P_{bi}

The value of :math:`ν_{0i}` is computed based on the wave spectrum :math:`S_{ηη}` as follows:

.. math:: ν_{0i} = \frac{1}{2π \sqrt { \frac {\int_0^{\infty} S_{xx} (ω) dω}   {\int_0^{\infty} S_{xx} (ω) ω dω} }}


Using the wave spectrum to compute :math:`ν_{0i}` is an approximation. The ideal method would be to use the stress range spectrum, which we do not have access to in the current version of the code, because of the quasi-static formulation used in the MAP++ library.

•	**Step 9**: perform step 1 to step 8 for all the environmental conditions

•	**Step 10**: compute the total cumulated damage :math:`D_{lifetime}` of each line tension during the lifetime of the mooring system by summing the damages :math:`d_i` due to each environmental condition:


.. math:: D_{lifetime} = \sum_{i=1}^n d_i


where n is the number of environmental conditions.

•	**Step 11**: compute FLS criteria:


.. math:: C_{fls}= 1 - \gamma_{fls} D_{lifetime} ≥ 0


where :math:`\gamma_{fls}` is the safety factor for fatigue limit state. Default value is 8. For more details about the choice of safety factor value, we refer to [SKRef7]_, Section 6.4.
The main outputs of the FLS Analysis are given in the following table:

.. csv-table:: Outputs from FLS Analysis
    :header: "Outputs description", "Data Model in SK", "Units"
    :widths: 30, 30, 30

    "σ_S – standard deviation of the stress process, for all line segments and all environmental conditions",	"float array",	"[MPa]"
    "Cfls – FLS design criteria for all line segments, environmental conditions, and weather directions",	"float array",	"[-]"
..

The FLS analysis is not performed for a fixed device or a master structure.
