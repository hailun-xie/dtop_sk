foundationtype
==============

FoundationConditions
--------------------

.. automodule:: dtosk.foundations.foundationtype.FoundationConditions
   :members:
   :undoc-members:
   :show-inheritance:

FoundationMatrixSuitability
---------------------------

.. automodule:: dtosk.foundations.foundationtype.FoundationMatrixSuitability
   :members:
   :undoc-members:
   :show-inheritance:

catalogue\_example\_dic
-----------------------

.. automodule:: dtosk.foundations.foundationtype.catalogue_example_dic
   :members:
   :undoc-members:
   :show-inheritance: