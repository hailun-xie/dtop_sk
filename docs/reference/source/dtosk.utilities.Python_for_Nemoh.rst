Python\_for\_Nemoh
==================

AxiMesh
-------

.. automodule:: dtosk.utilities.Python_for_Nemoh.AxiMesh
   :members:
   :undoc-members:
   :show-inheritance:

Mesh
----

.. automodule:: dtosk.utilities.Python_for_Nemoh.Mesh
   :members:
   :undoc-members:
   :show-inheritance:

MeshReader
----------

.. automodule:: dtosk.utilities.Python_for_Nemoh.MeshReader
   :members:
   :undoc-members:
   :show-inheritance:

Nemoh
-----

.. automodule:: dtosk.utilities.Python_for_Nemoh.Nemoh
   :members:
   :undoc-members:
   :show-inheritance:

NemohReader
-----------

.. automodule:: dtosk.utilities.Python_for_Nemoh.NemohReader
   :members:
   :undoc-members:
   :show-inheritance:

main\_test\_read\_results
-------------------------

.. automodule:: dtosk.utilities.Python_for_Nemoh.main_test_read_results
   :members:
   :undoc-members:
   :show-inheritance:
