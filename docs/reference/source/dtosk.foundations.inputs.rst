inputs
======

FoundationInputs
----------------

.. automodule:: dtosk.foundations.inputs.FoundationInputs
   :members:
   :undoc-members:
   :show-inheritance:

SoilProperties
--------------

.. automodule:: dtosk.foundations.inputs.SoilProperties
   :members:
   :undoc-members:
   :show-inheritance: