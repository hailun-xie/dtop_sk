geometry
========

Cylinder
--------

.. automodule:: dtosk.entity.geometry.Cylinder
   :members:
   :undoc-members:
   :show-inheritance:

RectangularCuboid
-----------------

.. automodule:: dtosk.entity.geometry.RectangularCuboid
   :members:
   :undoc-members:
   :show-inheritance: