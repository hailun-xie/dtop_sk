getResults
==========

.. automodule:: dtop_stationkeep.service.api.getResults
   :members:
   :undoc-members:
   :show-inheritance:
