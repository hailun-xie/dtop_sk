environment
===========

Current
-------

.. automodule:: dtosk.environment.Current
   :members:
   :undoc-members:
   :show-inheritance:

Environment
-----------

.. automodule:: dtosk.environment.Environment
   :members:
   :undoc-members:
   :show-inheritance:

Wave
----

.. automodule:: dtosk.environment.Wave
   :members:
   :undoc-members:
   :show-inheritance:

Wind
----

.. automodule:: dtosk.environment.Wind
   :members:
   :undoc-members:
   :show-inheritance: