.. _sk-how-to:

*************
How-to Guides
*************

Each *how-to guide* listed below contains step-by-step instructions on how to achieve specific outcomes using the Station Keeping module.

These guides are intended for users who have previously completed all the :ref:`Station Keeping tutorials <sk-tutorials>` and have a good knowledge of the features and workings of the Station Keeping module. 
While the tutorials give an introduction to the basic usage of the module, these *how to guides* tackle slightly more advanced topics, such as how export the mooring system define in an entity to Use it later in another one.
 
- :ref:`sk-Excel-files`
- :ref:`sk-Nemoh-files`
- :ref:`sk-Mooring-system-export`
- :ref:`sk-export_import`

.. toctree::
   :maxdepth: 1
   :hidden:

   Excel-files
   Nemoh-files
   Mooring_system_export
   Export_import