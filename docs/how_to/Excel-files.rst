.. _sk-Excel-files:

How to generate Excel files for SK module
=========================================

The attempt of this how-to guide is to explain how to generate the different Excel files that could be loaded into the Station Keeping module. There are three files into the module that could be generated which describe :

    - The position of each machines
    - The rotor thrust coefficient curve for a Tidal Energy Converter
    - The probability of environments occurence regarding intervals described in the analysis parameters

Machine positions file
----------------------

The table below show how the Excel file must be filled in order to be compatible with the module (the values described on this example are not representative of any situations and are just examples to test the import). The decimal could be indicated either by points "." or by coma ",".

The names of the column should be consistent with the one described on the following table and the file should be save with the extension xlsx.

.. csv-table:: Machine positioning table example
    :header: "North", "East", "Yaw", "Water Depth"
    :align: center

    "10", "5", "1", "50"
    "15", "10", "2", "50"
..

Note that there is no column for the Device ID that is automatically defined by the module and is not editable by the user.

Rotor thrust coefficient curve file
-----------------------------------

The table below show how the Excel file must be filled in order to be compatible with the module (the values described on this example are not representative of any situations and are just examples to test the import). The decimal could be indicated either by points "." or by coma ",".

The names of the column should be consistent with the one described on the following table and the file should be save with the extension xlsx.

Note that the velocity must be in creasing order.

.. csv-table:: Thrust coefficient curve table example
    :header: "Velocity", "Thrust Coefficient"
    :align: center

    "0", "0"
    "0.25", "0"
    "0.5", "0"
    "0.75", "0.68"
    "1", "0.6"
..


FLS Analysis Parameters file
----------------------------

The table below show how the Excel file must be filled in order to be compatible with the module (the values described on this example are not representative of any situations and are just examples to test the import). The decimal could be indicated either by points "." or by coma ",".

The names of the column should be consistent with the one described on the following table and the file should be save with the extension xlsx.

.. csv-table:: FLS analysis parameters table example
    :header: "HS Min", "HS Max", "DP Min", "DP Max", "Associated Tp", "Current Magnitude", "Current Direction", "Wind Magnitude", "Wind Direction", "Probability"
    :align: center

    "1.5", "2.6", "516", "1231", "156", "0.56", "23", "489", "156", "1.2"
    "16", "164", "135", "156", "16", "203", "4", "132", "465", "1.56"
..