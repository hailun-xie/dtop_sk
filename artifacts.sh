#!/bin/bash

conda install nodejs
npm install -g dredd
pip install dredd-hooks
export NODE_OPTIONS="--max-old-space-size=4076"
dredd --hooks-worker-connect-timeout 40000
pytest src/dtop_stationkeep --cov=dtop_stationkeep --junitxml=report.xml