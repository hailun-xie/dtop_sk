FROM python:3.8-slim-buster

WORKDIR /app

COPY requirements.txt .
COPY setup.py .
COPY storage/ ./storage/
COPY src/dtop_stationkeep/ ./src/dtop_stationkeep/

COPY ./dtop-shared-library/setup.py ./dtop-shared-library/setup.py
COPY ./dtop-shared-library/dtop_shared_library/ ./dtop-shared-library/dtop_shared_library/
COPY ./dtop-shared-library/bin/ ./dtop-shared-library/bin/

COPY ./pymap_submodule/setup.py ./pymap_submodule/setup.py
COPY ./pymap_submodule/src/ ./pymap_submodule/src/

RUN apt-get update && \
    apt-get install --yes --no-install-recommends build-essential liblapacke-dev && \
    pip install --requirement requirements.txt && \
    (cd ./dtop-shared-library && pip install --editable .) && \
    (cd ./pymap_submodule && pip install --editable .) && \
    pip install --editable . && \
    rm -rf /var/lib/apt/lists/* && \
    apt-get purge --auto-remove --yes build-essential liblapacke-dev

ENV FLASK_APP src.dtop_stationkeep.service

EXPOSE 5000
